//
//  ResponseSerializer.swift
//  AirdronCore
//
//  Created by Roman Makeev on 10.10.2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public protocol ResponseSerializer {
    
    func deserialize(data: Data?) throws -> Any
}

public struct JSONSerializer: ResponseSerializer {
    
    public init() {}
    
    public func deserialize(data: Data?) throws -> Any {
        guard let data = data else { throw AirdronError.dataIsEmptyError() }
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch {
            throw AirdronError.jsonSerializeError()
        }
    }
}

