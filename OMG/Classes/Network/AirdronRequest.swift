//
//  AirdronRequest.swift
//  AirdronCore
//
//  Created by Roman Makeev on 25.07.17.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation
import Alamofire

public class AirdronRequest {
    
    let method: Alamofire.HTTPMethod
    let endpoint: String
    let encoding: Alamofire.ParameterEncoding
    let parameters: Alamofire.Parameters?
    let headers: Alamofire.HTTPHeaders?
    
    public init(method: Alamofire.HTTPMethod,
                endpoint: String,
                parameters: Alamofire.Parameters? = nil,
                headers: Alamofire.HTTPHeaders? = nil,
                encoding: Alamofire.ParameterEncoding = Alamofire.JSONEncoding.default) {
        self.method = method
        self.endpoint = endpoint
        self.encoding = encoding
        self.parameters = parameters
        self.headers = headers
    }
}
