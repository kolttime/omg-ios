//
//  DataRequestBuilder.swift
//  AirdronCore
//
//  Created by Roman Makeev on 26/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation
import Alamofire

public class DataRequestBuilder {
    
    private let sessionManager: Alamofire.SessionManager
    
    public init(sessionManager: Alamofire.SessionManager) {
        self.sessionManager = sessionManager
    }
    
    public func make(request: AirdronRequest) -> Alamofire.DataRequest {
        return self.sessionManager.request(request.endpoint,
                                           method: request.method,
                                           parameters: request.parameters,
                                           encoding: request.encoding,
                                           headers: request.headers)
    }
    
    public func makeUpload(data: Data, request: AirdronRequest) -> Alamofire.UploadRequest {
        return self.sessionManager.upload(data,
                                          to: request.endpoint,
                                          method: request.method,
                                          headers: request.headers)
    }
}
