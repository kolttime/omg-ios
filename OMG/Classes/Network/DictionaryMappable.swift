//
//  DictionaryMappable.swift
//  AirdronCoreTests
//
//  Created by Roman Makeev on 27/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public protocol DictionaryMappable {
    
    func toDictionary() -> JsonDictionary
}
