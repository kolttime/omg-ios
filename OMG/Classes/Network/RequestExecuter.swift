//
//  RequestExecuter.swift
//  AirdronCoreTests
//
//  Created by Roman Makeev on 23/11/2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public extension Notification.Name {
    
    public static let notAuthorized = Notification.Name("AirdronNotAuthorizedIdentifier")
}

public protocol RequestExecutable {
    
    func execute<T>(dataResponsible: DataResponsible,
                    processData: @escaping ((_ responseData: Any) throws -> T),
                    serializer: ResponseSerializer,
                    completionHandler: ((Result<T>) -> Void)?,
                    responseHandler: ((HTTPURLResponse?) -> Void)?)
    
    func execute<T>(dataResponsible: DataResponsible,
                    processData: @escaping ((_ responseData: Data) throws -> T),
                    completionHandler: ((Result<T>) -> Void)?,
                    responseHandler: ((HTTPURLResponse?) -> Void)?)
}

public class RequestExecuter: RequestExecutable {
    
    public func execute<T>(dataResponsible: DataResponsible,
                           processData: @escaping ((_ responseData: Any) throws -> T),
                           serializer: ResponseSerializer,
                           completionHandler: ((Result<T>) -> Void)?,
                           responseHandler: ((HTTPURLResponse?) -> Void)?) {
        
        dataResponsible.response(queue: nil) { response in
            
            if let innerError = response.error as NSError? {
                completionHandler?(.failure(AirdronError(error: innerError)))
                return
            }
            
            if let innerError = response.error {
                completionHandler?(.failure(AirdronError(error: innerError)))
                return
            }
            
            guard let statusCode = response.response?.statusCode else {
                completionHandler?(.failure(AirdronError.unknownError()))
                return
            }
            responseHandler?(response.response)
            if statusCode >= 200 && statusCode < 300 {
                do {
                    let responseData = try serializer.deserialize(data: response.data)
                    let entity: T = try processData(responseData)
                    completionHandler?(.success(entity))
                } catch let error as AirdronError {
                    completionHandler?(.failure(error))
                } catch let error as NSError {
                    completionHandler?(.failure(AirdronError(error: error)))
                } catch {
                    completionHandler?(.failure(AirdronError(error: error)))
                }
            } else {
                completionHandler?(.failure(AirdronError.backendError(statusCode, responseData: response.data)))
            }
        }
    }
    
    public func execute<T>(dataResponsible: DataResponsible,
                           processData: @escaping ((_ responseData: Data) throws -> T),
                           completionHandler: ((Result<T>) -> Void)?,
                           responseHandler: ((HTTPURLResponse?) -> Void)?) {
        dataResponsible.response(queue: nil) { response in
            
            if let innerError = response.error as NSError? {
                completionHandler?(.failure(AirdronError(error: innerError)))
                return
            }
            
            if let innerError = response.error {
                completionHandler?(.failure(AirdronError(error: innerError)))
                return
            }
            
            guard let statusCode = response.response?.statusCode else {
                completionHandler?(.failure(AirdronError.unknownError()))
                return
            }
            responseHandler?(response.response)
//            if let data = response.data {
//                print(String.init(data: data, encoding: .utf8))
//            }
            if statusCode >= 200 && statusCode < 300 {
                do {
                    guard let data = response.data else { throw AirdronError.dataIsEmptyError() }
                    let entity: T = try processData(data)
                    completionHandler?(.success(entity))
                } catch let error as AirdronError {
                    completionHandler?(.failure(error))
                } catch let error as DecodingError {
                    let debugError: Error? = error
                    print(debugError.debugDescription)
                    print(error.localizedDescription)
                    completionHandler?(.failure(AirdronError(error: error)))
                } catch let error as NSError {
                    completionHandler?(.failure(AirdronError(error: error)))
                } catch {
                    completionHandler?(.failure(AirdronError(error: error)))
                }
            } else {
                if statusCode == 401 {
                    NotificationCenter.default.post(name: Notification.Name.notAuthorized, object: nil)
                }
                completionHandler?(.failure(AirdronError.backendError(statusCode, responseData: response.data)))
            }
        }
    }
}
