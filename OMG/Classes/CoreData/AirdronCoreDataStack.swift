//
//  AirdronCoreDataStack.swift
//  AirdronCore
//
//  Created by Roman Makeev on 31.10.2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation
import CoreData

public class AirdronCoreDataStack {
    
    private let settings: AirdronCoreDataSettings
    private let persistentContainer: NSPersistentContainer
    
    public init(settings: AirdronCoreDataSettings) {
        self.settings = settings
        let container = NSPersistentContainer(name: self.settings.modelName)
        self.persistentContainer = container
    }
    
    public func loadPersistentContainer(completion: Action?) {
        self.persistentContainer.loadPersistentStores(completionHandler: { [weak self] (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            } else {
                print("core data success loaded")
                self?.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
                self?.persistentContainer.viewContext.mergePolicy = NSMergePolicy(merge: .overwriteMergePolicyType)
                completion?()
            }
        })
    }
    
    public func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    public var mainContext: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    public lazy var backgroundContext: NSManagedObjectContext = {
        let context = self.persistentContainer.newBackgroundContext()
        context.automaticallyMergesChangesFromParent = true
        context.mergePolicy = NSMergePolicy(merge: .overwriteMergePolicyType)
        return context
    }()
    
    public func performForegroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        self.mainContext.performChanges { context in
            block(context)
        }
    }
    
    public func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        let performBlock: (NSManagedObjectContext) -> Void = { context in
            block(context)
            context.performSaveOrRollback()
        }
        self.persistentContainer.performBackgroundTask(performBlock)
    }
    
    public func performSyncBackgroundTask(block: @escaping (NSManagedObjectContext) throws -> Void) throws {
        try self.backgroundContext.performChangesAndWait { context in
            try block(context)
        }
    }
    
    public func resetDatabase() {
        do {
            try self.performSyncBackgroundTask { context in
                try context.deleteAllObjects()
            }
        } catch {
            print("database clean error : \(error)")
        }
    }
}
