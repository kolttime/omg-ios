//
//  AirdronTabbarController.swift
//  MDW
//
//  Created by Roman Makeev on 20/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public typealias AirdronTabResponder = AirdronTabDataSource & UIViewController

public protocol AirdronTabDataSource: class {
    
    var airdronTabbarController: AirdronTabbarController? { get set }
    var airdronTabItem: AirdronTabItem { get set }
    func needShowTabbar() -> Bool
}

open class AirdronTabbarController: AirdronViewController {
    
    private let tabbarHeight: CGFloat = 54
    private let iphoneXtabbarHeight: CGFloat = 89
    private lazy var contentView = UIView()
    private lazy var tabbarContentView = UIView()
    private lazy var separatorView = UIView()
    private var currentSelectedTabIndex: Int = 0
    
    private var tabbarHeightConstraint: Constraint?
    
    public var viewControllers: [AirdronTabResponder] = [] {
        didSet {
            let tabItems = self.viewControllers.map { $0.airdronTabItem }
            self.tabItems = tabItems
            self.viewControllers.enumerated().forEach { offset, vc in
                vc.airdronTabItem.onSelectTab = { [weak self] in
                    self?.didSelectTab(atIndex: offset)
                }
            }
            self.viewControllers.forEach { $0.airdronTabbarController = self }
        }
    }
    
    private var tabItems: [AirdronTabItem] = [] {
        didSet {
            self.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
            self.tabItems.forEach { self.stackView.addArrangedSubview($0) }
        }
    }
    
    public var separatorColor: UIColor = UIColor.gray {
        didSet {
            self.separatorView.backgroundColor = self.separatorColor
        }
    }
    
    public var tabbarColor: UIColor = UIColor.white {
        didSet {
            self.tabbarContentView.backgroundColor = self.tabbarColor
        }
    }
    
    public var currentTabbarHeight: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return self.iphoneXtabbarHeight
        } else {
            return self.tabbarHeight
        }
    }
    
    public var onSelectedTab: ((Int) -> Void)?
    
    public func selectTab(atIndex index: Int) {
        self.didSelectTab(atIndex: index)
        self.checkShowingTabbar()
    }
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private func didSelectTab(atIndex index: Int) {
        self.currentSelectedTabIndex = index
        self.tabItems.forEach { $0.isSelected = false }
        self.tabItems[index].isSelected = true
        self.viewControllers.forEach { self.removeTabController($0) }
        self.addTabController(self.viewControllers[index])
        self.onSelectedTab?(index)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.contentView)
        self.view.addSubview(self.tabbarContentView)
        self.view.addSubview(self.separatorView)
        self.separatorView.backgroundColor = self.separatorColor
        self.tabbarContentView.backgroundColor = self.tabbarColor
        self.tabbarContentView.addSubview(self.stackView)
        self.setupConstraints()
        self.view.setNeedsUpdateConstraints()
    }
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.updateTabbarHeightConstraint()
    }
    
    func setupConstraints() {
        self.contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        self.tabbarContentView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            self.tabbarHeightConstraint = $0.height.equalTo(0).constraint
            self.updateTabbarHeightConstraint()
        }
        self.stackView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
            $0.height.equalTo(self.tabbarHeight)
        }
        self.separatorView.snp.makeConstraints {
            $0.bottom.equalTo(self.tabbarContentView.snp.top)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    private func updateTabbarHeightConstraint() {
        if Device.isIphoneX || Device.isIphoneXSmax {
            self.tabbarHeightConstraint?.update(offset: self.iphoneXtabbarHeight)
        } else {
            self.tabbarHeightConstraint?.update(offset: self.tabbarHeight)
        }
    }
    
    public func checkShowingTabbar() {
        var vc: AirdronTabResponder? = self.viewControllers[self.currentSelectedTabIndex]
        if let topVc = vc as? AirdronNavigationController {
            vc = topVc.topViewController as? AirdronTabResponder
        }
        self.tabbarContentView.isHidden = !(vc?.needShowTabbar() ?? true)
        self.separatorView.isHidden = self.tabbarContentView.isHidden
    }
}

extension AirdronTabbarController {
    
    func addTabController(_ child: UIViewController) {
        self.addChild(child)
        self.contentView.addSubview(child.view)
        child.view.frame = self.contentView.bounds
        child.didMove(toParent: self)
    }
    func removeTabController(_ child: UIViewController) {
        guard child.parent != nil else {
            return
        }
        child.willMove(toParent: nil)
        child.removeFromParent()
        child.view.removeFromSuperview()
    }
}
