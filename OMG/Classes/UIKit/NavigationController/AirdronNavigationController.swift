//
//  AirdronNavigationController.swift
//  MDW
//
//  Created by Roman Makeev on 26/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

private class InteractivePopRecognizer: NSObject, UIGestureRecognizerDelegate {
    
    weak var navigationController: UINavigationController?
    
    public var canSwipe: Bool = true
    
    init(controller: UINavigationController) {
        self.navigationController = controller
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return self.canSwipe && (navigationController?.viewControllers.count ?? 0) > 1
    }
    
    // This is necessary because without it, subviews of your top controller can
    // cancel out your gesture recognizer on the edge.
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // TODO: attention! i toggle it to false
        return false
    }
}

open class AirdronNavigationController: UINavigationController, AirdronTabDataSource {
    
    public weak var airdronTabbarController: AirdronTabbarController?
    public var airdronTabItem = AirdronTabItem()
    
    private var popRecognizer: InteractivePopRecognizer?
    
    public var canSwipe: Bool = true {
        didSet {
            self.popRecognizer?.canSwipe = self.canSwipe
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.setInteractiveRecognizer()
    }
    
    private func setInteractiveRecognizer() {
        self.popRecognizer = InteractivePopRecognizer(controller: self)
        self.interactivePopGestureRecognizer?.delegate = self.popRecognizer
    }
    
    open override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        (viewController as? AirdronTabDataSource)?.airdronTabbarController = self.airdronTabbarController
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func popViewController(animated: Bool) -> UIViewController? {
        let vc = super.popViewController(animated: animated)
        self.airdronTabbarController?.checkShowingTabbar()
        return vc
    }
    
    open override func setViewControllers(_ viewControllers: [UIViewController], animated: Bool) {
        viewControllers.forEach { ($0 as? AirdronTabDataSource)?.airdronTabbarController = self.airdronTabbarController }
        super.setViewControllers(viewControllers, animated: animated)
    }
    
    public func needShowTabbar() -> Bool {
        return true
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
}
