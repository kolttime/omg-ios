//
//  WaterfallCollectionViewCell.swift
//  MDW
//
//  Created by Roman Makeev on 27/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

public protocol WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat
}
