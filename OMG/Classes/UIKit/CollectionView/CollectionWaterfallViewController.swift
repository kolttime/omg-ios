//
//  CollectionWaterfallViewController.swift
//  MDW
//
//  Created by Roman Makeev on 27/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

public class CollectionWaterfallViewController: UICollectionViewController, ScrollInteraction {
    
    private lazy var collectionWaterfallLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.delegate = self
        return layout
    }()
    
    public init(layout: AirdronCollectionViewWaterfallLayout) {
        super.init(collectionViewLayout: layout)
        layout.delegate = self
        self.collectionView!.backgroundColor = UIColor.clear
        self.collectionView!.registerHeader(headerClass: EmptyCollectionHeaderView.self)
        self.collectionView!.registerFooter(footerClass: EmptyCollectionFooterView.self)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var viewModels: [CollectionSectionViewModel] = []
    
    public var scrollView: UIScrollView! { return self.collectionView! }
    public var onScroll: ((UIScrollView) -> Void)?
    
    open override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.onScroll?(scrollView)
    }
    
    public override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.viewModels.count
    }
    
    public override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return self.viewModels[section].cellModels.count
    }
    
    public override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let viewModel = self.viewModels[indexPath.section].cellModels[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: viewModel.cellType), for: indexPath) as! CollectionViewCell
        cell.configure(viewModel: viewModel)
        return cell
    }
    
    public override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CollectionViewCell
        cell.didSelect()
    }
    
    public func update(viewModels: [CollectionSectionViewModel]) {
        self.viewModels = viewModels
        self.collectionView!.reloadData()
    }
    
    public override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let viewModel = self.viewModels[indexPath.section]
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let viewModel = viewModel.headerViewModel else {
                return collectionView.obtainEmptyHeader(atIndexPath: indexPath)
            }
            return collectionView.obtainHeader(for: viewModel,
                                               atIndexPath: indexPath)
        case UICollectionView.elementKindSectionFooter:
            guard let viewModel = viewModel.footerViewModel else {
                return collectionView.obtainEmptyFooter(atIndexPath: indexPath)
            }
            return collectionView.obtainFooter(for: viewModel,
                                               atIndexPath: indexPath)
        default:
            fatalError("unexpected supplementary kind")
        }
    }
    
}

extension CollectionWaterfallViewController: AirdronCollectionViewWaterfallLayoutDelegate {
    
    public func collectionView(_ collectionView: UICollectionView,
                        layout: UICollectionViewLayout,
                        columnWidth: CGFloat,
                        heightForItemAtIndexPath indexPath: IndexPath) -> CGFloat {
        let viewModel = self.viewModels[indexPath.section].cellModels[indexPath.row]
        let cell: WaterfallCollectionViewCell.Type = viewModel.cellType as! WaterfallCollectionViewCell.Type
        return cell.height(forViewModel: viewModel, width: columnWidth)
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout: UICollectionViewLayout,
                               heightForFooterInSection section: Int) -> CGFloat {
        guard let viewModel = self.viewModels[section].footerViewModel else { return self.collectionWaterfallLayout.headerHeight }
        return viewModel.type.height(for: viewModel, collectionView: collectionView)
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout: UICollectionViewLayout,
                               heightForHeaderInSection section: Int) -> CGFloat {
        guard let viewModel = self.viewModels[section].headerViewModel else { return CGFloat.leastNonzeroMagnitude }
        return viewModel.type.height(for: viewModel, collectionView: collectionView)
    }
}

extension CollectionWaterfallViewController {
    
    func insert(_ cellViewModel: CollectionCellViewModel,
                for item: Int,
                in section: Int) {
        self.viewModels[section].cellModels.insert(cellViewModel, at: item)
        let indexPath = IndexPath(item: item, section: section)
        self.collectionView.insertItems(at: [indexPath])
    }
    
    func update(_ cellViewModel: CollectionCellViewModel,
                at indexPath: IndexPath,
                reload: Bool = true) {
        self.viewModels[indexPath.section].cellModels[indexPath.item] = cellViewModel
        if reload {
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
    
    func update(_ cellViewModel: CollectionCellViewModel,
                for item: Int,
                in section: Int,
                reload: Bool = true) {
        let indexPath = IndexPath(item: item, section: section)
        self.update(cellViewModel, at: indexPath, reload: reload)
    }
    
    func delete(_ item: Int,
                in section: Int) {
        self.viewModels[section].cellModels.remove(at: item)
        let indexPath = IndexPath(item: item, section: section)
        self.collectionView.deleteItems(at: [indexPath])
    }
}
