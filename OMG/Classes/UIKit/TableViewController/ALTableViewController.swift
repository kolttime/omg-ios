//
//  ALTableViewController.swift
//  AirdronKit
//
//  Created by Roman Makeev on 19/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

open class ALTableViewController: UITableViewController, TableInteraction, ScrollInteraction {
    
    public var scrollView: UIScrollView! { return self.tableView }
    
    public var viewModels: [TableSectionViewModel] = []
    public var onScroll: ((UIScrollView) -> Void)?

    public init() {
        super.init(style: .grouped)
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModels.count
    }
    
    open override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModels[section].cellModels.count
    }
    
    open override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = self.viewModels[indexPath.section].cellModels[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: viewModel.cellType),
                                                 for: indexPath) as! TableViewCell
        cell.configure(viewModel: viewModel)
        return cell
    }
    
    open override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let viewModel = self.viewModels[section].headerViewModel else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: viewModel.viewType)) as! TableHeaderFooterView
        view.configure(viewModel: viewModel)
        return view
    }
    
    open override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let viewModel = self.viewModels[section].footerViewModel else { return nil }
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: viewModel.viewType)) as! TableHeaderFooterView
        view.configure(viewModel: viewModel)
        return view
    }
    
    open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? TableViewCell
        cell?.didSelect()
    }
    
    open override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as? TableViewCell
        cell?.willSelect()
        return indexPath
    }
    
    open override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.onScroll?(scrollView)
    }
    
    // TODO: Section bug for group table view
    open override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if let _ = self.viewModels[section].footerViewModel {
            return UITableView.automaticDimension
        } else {
            return CGFloat.leastNormalMagnitude
        }
    }
    
    open override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let _ = self.viewModels[section].headerViewModel {
            return UITableView.automaticDimension
        } else {
            return CGFloat.leastNormalMagnitude
        }
    }
}

public extension ALTableViewController {
    
    public static func makeTestingALTableViewController() -> ALTableViewController {
        let controller = ALTableViewController()
        controller.tableView.register(cellClass: DefaultTableViewCell.self)
        let viewModels = [DefaultTableSectionViewModel(cellModels: (0..<100).map { _ in return DefaultTableCellViewModel() })]
        controller.update(viewModels: viewModels)
        return controller
    }
}
