//
//  ScrollInteraction.swift
//  MDW
//
//  Created by Roman Makeev on 30/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

public protocol ScrollInteraction: class {
    
    var scrollView: UIScrollView! { get }
    var onScroll: ((UIScrollView) -> Void)? { get set }
}


