//
//  AirdronViewController.swift
//  MDW
//
//  Created by Roman Makeev on 20/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class AirdronViewController: UIViewController, AirdronTabDataSource {
    
    public weak var airdronTabbarController: AirdronTabbarController?
    public var airdronTabItem = AirdronTabItem()
    
    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func needShowTabbar() -> Bool {
        return true
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.airdronTabbarController?.checkShowingTabbar()
    }
}
