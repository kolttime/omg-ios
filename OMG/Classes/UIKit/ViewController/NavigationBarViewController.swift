//
//  NavigationBarViewController.swift
//  MDW
//
//  Created by Roman Makeev on 29/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class NavigationBarViewController: AirdronViewController {
    
    lazy var navigationBarView = NavigationBarView()
    private lazy var containerBarView = UIView()

    public var navigationBarAreaHeight: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 100
        } else if Device.isIphone5 {
            return 70
        } else {
            return 75
        }
    }
    
    private var navigationBarBottomOffset: CGFloat {
        return 5
    }
    
    public var backgroundBarColor: UIColor? {
        didSet {
            self.containerBarView.backgroundColor = self.backgroundBarColor
        }
    }
    
    public var navigationTitle: NSAttributedString? {
        didSet {
            self.navigationBarView.title = self.navigationTitle
        }
    }
    
    public var backButtonTitle: NSAttributedString? {
        didSet {
            self.navigationBarView.backButton.text = self.backButtonTitle
        }
    }
    
    public var backButtonTintColor: UIColor? {
        didSet {
            self.navigationBarView.backButton.imageTintColor = self.backButtonTintColor
        }
    }
    
    public var backButtonImage: UIImage? {
        didSet {
            self.navigationBarView.backButton.image = self.backButtonImage
        }
    }
    
    public var leftAction: UIView? {
        didSet {
            self.navigationBarView.leftAction = self.leftAction
        }
    }
    
    public var rightActions: [UIView] = [] {
        didSet {
            self.navigationBarView.rightActions = self.rightActions
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.initialSetup()
        self.view.addSubview(self.containerBarView)
        self.containerBarView.addSubview(self.navigationBarView)
        self.navigationBarView.backButton.addTarget(self, action: #selector(backTap), for: .touchUpInside)
        self.setupConstraints()
        self.view.setNeedsUpdateConstraints()
    }
    
    @objc
    private func backTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    open func initialSetup() {
        
    }
    
    open func setupConstraints() {
        self.containerBarView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.navigationBarAreaHeight)
        }
        self.navigationBarView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-self.navigationBarBottomOffset)
        }
    }
}
