//
//  NavigationBarView.swift
//  MDW
//
//  Created by Roman Makeev on 26/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class NavigationBarView: AirdronView {
    
    public lazy var backButton = ImageButton()
    public var leftAction: UIView? {
        willSet {
            self.resetConstraints()
            self.leftAction?.removeFromSuperview()
        }
        didSet {
            if let view = self.leftAction {
                view.setResistance(priority: .defaultHigh)
                self.addSubview(view)
            }
            self.makeConstraints()
        }
    }
    public var rightActions: [UIView] = [] {
        willSet {
            self.resetConstraints()
            self.rightActions.forEach { $0.removeFromSuperview() }
        }
        didSet {
            self.rightActions.forEach {
                $0.setHugging(priority: .defaultHigh)
                $0.setResistance(priority: .defaultHigh)
                self.addSubview($0)
            }
            self.makeConstraints()
        }
    }
    
    private lazy var titleLabel = UILabel()
    public var space: CGFloat = 16
    private var sideOffset: CGFloat { return Device.isIphone5 ? 15 : 20 }
    private var contentHeight: CGFloat { return 40 }

    open override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.backButton)
        self.addSubview(self.titleLabel)
        self.titleLabel.setResistance(priority: .defaultLow)
        self.setNeedsUpdateConstraints()
        self.leftAction = self.backButton
        self.titleLabel.lineBreakMode = .byTruncatingTail
    }
    
    open override func setupConstraints() {
        super.setupConstraints()
        self.resetConstraints()
        self.makeConstraints()
    }
    
    func resetConstraints() {
        self.leftAction?.snp.removeConstraints()
        self.titleLabel.snp.removeConstraints()
        self.rightActions.forEach { $0.snp.removeConstraints() }
    }
    
    // TODO: navigation bar layout is needed rework
    func makeConstraints() {
        self.leftAction?.snp.makeConstraints {
            $0.left.equalToSuperview().offset(self.sideOffset - 3)
            $0.centerY.equalToSuperview()
        }
        
        let reversedActions = Array(self.rightActions.reversed())
        
        reversedActions.enumerated().forEach { offset, view in
            if offset == 0 {
                view.snp.makeConstraints {
                    $0.right.equalToSuperview().offset(-self.sideOffset).priority(ConstraintPriority.high)
                    $0.centerY.equalToSuperview()
                }
            } else {
                view.snp.makeConstraints {
                    $0.right.equalTo(reversedActions[offset - 1].snp.left).offset(-self.space).priority(ConstraintPriority.high)
                    $0.centerY.equalToSuperview()
                }
            }
        }
        self.titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview()
            if let leftAction = self.leftAction {
                $0.left.greaterThanOrEqualTo(leftAction.snp.right).offset(self.space)
            } else {
                $0.left.greaterThanOrEqualToSuperview().offset(self.space)
            }
            if let rightAction = self.rightActions.first {
                $0.right.lessThanOrEqualTo(rightAction.snp.left).offset(-self.space)
            } else {
                $0.right.lessThanOrEqualToSuperview().offset(-self.space)
            }
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentHeight)
    }
    
    public var title: NSAttributedString? {
        didSet {
            self.titleLabel.attributedText = title
            self.titleLabel.lineBreakMode = .byTruncatingTail
            self.setNeedsLayout()
        }
    }
    
    public func setTitleHidden(value: Bool, withAnimation animation: Bool) {
        if !animation {
            self.titleLabel.alpha = value ? 0 : 1
        } else {
            UIView.animate(withDuration: 0.3, animations: { self.titleLabel.alpha = value ? 0 : 1 })
        }
    }
}

