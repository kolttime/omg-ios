//
//  KeyboardObserver.swift
//  MDW
//
//  Created by Roman Makeev on 05/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

public final class KeyboardObserver {
    
    public init() {
        self.addObservers()
    }
    
    public var onKeyboardWillShow: ((CGRect, Double) -> Void)?
    public var onKeyboardDidShow: ((CGRect, Double) -> Void)?

    public var onKeyboardWillHide: ((CGRect, Double) -> Void)?
    public var onKeyboardFrameWillChange: ((CGRect, Double) -> Void)?
    
    private(set) public var isKeyboardShown: Bool = false
    public var isSuspended: Bool = false
    
    deinit {
        self.removeObservers()
    }
    
    func addObservers() {
        self.removeObservers()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow),
                                               name: UIResponder.keyboardDidShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardFrameWillChange),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    private func keyboardWillShow(_ notification: Notification) {
        guard !self.isSuspended else {
            return
        }
        guard !self.isKeyboardShown else { return }
        self.isKeyboardShown = true
        guard let frameValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
        let keyboardFrame = frameValue.cgRectValue
        self.onKeyboardWillShow?(keyboardFrame, animationDuration)
    }
    
    @objc
    private func keyboardDidShow(_ notification: Notification) {
        guard !self.isSuspended else {
            return
        }
        guard let frameValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
        let keyboardFrame = frameValue.cgRectValue
        self.onKeyboardDidShow?(keyboardFrame, animationDuration)
    }
    
    @objc
    private func keyboardWillHide(_ notification: Notification) {
        guard !self.isSuspended else {
            return
        }
        guard self.isKeyboardShown else { return }
        self.isKeyboardShown = false
        guard let frameValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
        let keyboardFrame = frameValue.cgRectValue
        self.onKeyboardWillHide?(keyboardFrame, animationDuration)
    }
    
    @objc
    private func keyboardFrameWillChange(_ notification: Notification) {
        guard !self.isSuspended else {
            return
        }
        guard self.isKeyboardShown else { return }
        guard let frameValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double else { return }
        let keyboardFrame = frameValue.cgRectValue
        self.onKeyboardFrameWillChange?(keyboardFrame, animationDuration)
    }
}
