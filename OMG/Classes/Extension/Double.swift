//
//  Double.swift
//  OMG
//
//  Created by Roman Makeev on 27/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

extension Double {
    
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 16
        return String(formatter.string(from: number) ?? "")
    }
}
