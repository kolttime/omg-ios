//
//  Date.swift
//  MDW
//
//  Created by Roman Makeev on 13/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public extension DateFormatter {
    
    public func date(from string: String?) -> Date? {
        if let string = string {
            return self.date(from: string)
        } else {
            return nil
        }
    }
}
