//
//  Functions.swift
//  AirdronCore
//
//  Created by Roman Makeev on 10.10.2017.
//  Copyright © 2017 airdron. All rights reserved.
//

import Foundation

public func printAndFail<T>(_ message: String, file: String = #file, line: Int = #line) -> T? {
    print(message + " file: \(file) line: \(line)")
    return nil
}
