//
//  Types.swift
//  AirdronKit
//
//  Created by Roman Makeev on 10/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public typealias Identifier = Int64
