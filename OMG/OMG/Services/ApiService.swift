//
//  ApiService.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Alamofire
import Photos
import Gallery

protocol ApiService {
    
    func auth(byPhone phone: String, completion: ((Result<AuthResponse>) -> Void)?)
    
    func verification(byKey key: String, code: String, completion: ((Result<VerificationResponse>) -> Void)?)
    
    func fetchCountry(completion: ((Result<[Country]>) -> Void)?)
    
    func fetchClothingSizes(completion: ((Result<[SelectableValue]>) -> Void)?)
    
    func fetchFootwears(completion: ((Result<[Footwear]>) -> Void)?)
    
    func fetchTattoos(completion: ((Result<[SelectableValue]>) -> Void)?)
    
    func fetchHair(completion: ((Result<[SelectableValue]>) -> Void)?)
    
    func fetchEyes(completion: ((Result<[SelectableValue]>) -> Void)?)
    
    func fetchLanguage(completion: ((Result<[SelectableValue]>) -> Void)?)
    
    func fetchLanguageLevel(completion: ((Result<[SelectableValue]>) -> Void)?)
    
    func activation(userActivation: UserCreation,
                    completion: ((Result<User>) -> Void)?)
    
    func updateBodyInfo(info: BodyInfo,
                        completion: ((Result<Model>) -> Void)?)
    
    func fetchPrivacy(privacy: Privacy, completion: ((Result<StaticContent>) -> Void)?)
    
    func fetchCastings(completion: ((Result<[CastingShort]>) -> Void)?)
    
    func fetchCasting(id: String, completion: ((Result<Casting>) -> Void)?)
    
    func uploadImage(byAsset asset: PHAsset,
                     completion: ((Result<Image>) -> Void)?)
    
    func uploadImage(byFileUrl url: URL,
                     requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void)?,
                     progressHandler: ((Double) -> Void)?,
                     completion: ((Result<Image>) -> Void)?)
    
    func confirmParticipation(id: String,
                              fullId: String,
                              faceId: String,
                              sideId: String,
                              completion: ((Result<CastingParticipationResponse>) -> Void)?)
    
    func fetchProfile(completion: ((Result<Model>) -> Void)?)
    
    func updateAvatar(id: String,
                      completion: ((Result<Model>) -> Void)?)
    
    func updateMainInfo(user: UserCreation,
                        completion: ((Result<Model>) -> Void)?)
    
    func updateAdditionalInfo(info: AdditionalInfo,
                              completion: ((Result<Model>) -> Void)?)
    
    func fetchPortfolio(completion: ((Result<[AlbumShort]>) -> Void)?)
    
    func fetchAlbum(id: String, completion: ((Result<Album>) -> Void)?)
    
    func deletePhotosFromAlbum(albumId: String, photoIds: [String], completion: ((Result<Album>) -> Void)?)
    
    func uploadImagesInAlbum(albumId: String,
                             albumImages: [AlbumImage],
                             payment: Payment?,
                             completion: ((Result<UploadImagesResponse>) -> Void)?)
    
    func updateSocialNetworks(networks: SocialNetworks, completion: ((Result<Model>) -> Void)?)
    
    func fetchInstagramToken(completion: ((Result<Token>) -> Void)?)
    
    func instagramCode(url: URL, completion: ((Result<Success>) -> Void)?)
    
    func instagramAuth(url: URL, completion: ((Result<VerificationResponse>) -> Void)?)
    
    func fetchChat(completion: ((Result<Chat>) -> Void)?)
    
    func fetchMessages(chatId: String, lastMessageId: String?, completion: ((Result<[Message]>) -> Void)?)
    
    func sendMessage(chatId: String, message: String, completion: ((Result<Message>) -> Void)?)
    
    func logout()
}

class ApiServiceImp: ApiService {
    
    private let networkManager: NetworkManager
    private let uploadManager: UploadManager
    private let backgroundUploadManager: UploadManager
    private let sessionManager: OMGUserSessionManager
    private var credential: CredentialStorage
    
    init(networkManager: NetworkManager,
         uploadManager: UploadManager,
         backgroundUploadManager: UploadManager,
         sessionManager: OMGUserSessionManager,
         credential: CredentialStorage) {
        self.networkManager = networkManager
        self.credential = credential
        self.backgroundUploadManager = backgroundUploadManager
        self.sessionManager = sessionManager
        self.uploadManager = uploadManager
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.notAuthorizedHandler),
                                               name: Notification.Name.notAuthorized,
                                               object: nil)
    }
    
    func makeTokenHeader() -> [String: String] {
        if let token = self.credential.getToken() {
            return ["Token": token]
        }
        return [:]
    }
    
    private let langParameters: String = "?lang=\(LanguageManager.defaultLanguage().rawValue)"
    private let langDictParameters: [String : Any] = ["lang" : LanguageManager.defaultLanguage().rawValue]
    
    @objc
    func notAuthorizedHandler() {
        self.logout()
    }
    
    func logout() {
        self.sessionManager.logout()
        NotificationCenter.default.post(name: Notification.Name.OMGNotificationLogout, object: nil)
    }
}

// MARK: Login
extension ApiServiceImp {
    
    func auth(byPhone phone: String, completion: ((Result<AuthResponse>) -> Void)?) {
        let parameters: [String: Any] = ["phone": phone]
        let request = AirdronRequest(method: .post, endpoint: Endpoints.auth.url, parameters: parameters)
        self.networkManager.perform(request: request,
                                    processData: { data in try AuthResponse(data: data) },
                                    completionHandler: completion)
    }
    
    func verification(byKey key: String, code: String, completion: ((Result<VerificationResponse>) -> Void)?) {
        let parameters: [String: Any] = ["key": key, "code": code, "device": "IOS"]
        let request = AirdronRequest(method: .post, endpoint: Endpoints.verification.url, parameters: parameters)
        func processData(data: Data) throws -> VerificationResponse {
            let response = try VerificationResponse(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func instagramAuth(url: URL, completion: ((Result<VerificationResponse>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: url.absoluteString)
        self.networkManager.perform(request: request,
                                    processData: { data in try VerificationResponse(data: data) },
                                    completionHandler: completion)
    }
    
    func fetchCountry(completion: ((Result<[Country]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "COUNTRY"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [Country] {
            let response = try [Country](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchFootwears(completion: ((Result<[Footwear]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "FOOTWEAR"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [Footwear] {
            let response = try [Footwear](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchClothingSizes(completion: ((Result<[SelectableValue]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "CLOTHING_SIZE"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [SelectableValue] {
            let response = try [SelectableValue](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchTattoos(completion: ((Result<[SelectableValue]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "TATTOOS"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [SelectableValue] {
            let response = try [SelectableValue](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchHair(completion: ((Result<[SelectableValue]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "HAIR"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [SelectableValue] {
            let response = try [SelectableValue](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchEyes(completion: ((Result<[SelectableValue]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "EYES"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [SelectableValue] {
            let response = try [SelectableValue](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func activation(userActivation: UserCreation,
                    completion: ((Result<User>) -> Void)?) {
        let parameters: [String: Any] = userActivation.toDictionary()
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.activation.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> User {
            let response = try User(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    
    func fetchInstagramToken(completion: ((Result<Token>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.instagramToken.url,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try Token(data: data) },
                                    completionHandler: completion)
    }
    
    func instagramCode(url: URL, completion: ((Result<Success>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: url.absoluteString,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try Success(data: data) },
                                    completionHandler: completion)
    }
}

// MARK: Privacy
extension ApiServiceImp {
    
    func fetchPrivacy(privacy: Privacy, completion: ((Result<StaticContent>) -> Void)?) {
        let parameters = ["name": privacy.rawValue]
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.staticContent.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        self.networkManager.perform(request: request,
                                    processData: { data in try StaticContent(data: data) },
                                    completionHandler: completion)
    }
}

// MARK: Castings
extension ApiServiceImp {
    
    func fetchCastings(completion: ((Result<[CastingShort]>) -> Void)?) {
        let parameters: [String: Any] = self.langDictParameters
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.castings.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        self.networkManager.perform(request: request,
                                    processData: { data in try [CastingShort](data: data) },
                                    completionHandler: completion)
    }
    
    func fetchCasting(id: String, completion: ((Result<Casting>) -> Void)?) {
        let parameters: [String: Any] = self.langDictParameters
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.casting(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        self.networkManager.perform(request: request,
                                    processData: { data in try Casting(data: data) },
                                    completionHandler: completion)
    }
    
    func confirmParticipation(id: String, fullId: String, faceId: String, sideId: String, completion: ((Result<CastingParticipationResponse>) -> Void)?) {
        var dictInner: [String: Any] = ["full": fullId]
        dictInner["face"] = faceId
        dictInner["side"] = sideId
        let photosParamters: [String: Any] = ["photos": dictInner]
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.castingParticipation(id: id).url,
                                     parameters: photosParamters,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try CastingParticipationResponse(data: data) },
                                    completionHandler: completion)
    }
}

// MARK: Profile
extension ApiServiceImp {
    
    func fetchProfile(completion: ((Result<Model>) -> Void)?) {
        let parameters: [String: Any] = self.langDictParameters
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.profile.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        self.networkManager.perform(request: request,
                                    processData: { data in try Model(data: data) },
                                    completionHandler: completion)
    }
    
    func uploadImage(byAsset asset: PHAsset, completion: ((Result<Image>) -> Void)?) {
        DispatchQueue.global().async {
            asset.resolveEditingImageAndCopy { [weak self] _, error, tempUrl, _ in
                guard let self = self else { return }
                if let error = error {
                    DispatchQueue.main.async {
                        completion?(.failure(AirdronError(error: error)))
                    }
                    return
                }
                guard let temp = tempUrl else {
                    DispatchQueue.main.async {
                        completion?(.failure(AirdronError.dataIsEmptyError()))
                    }
                    return
                }
                let request = AirdronRequest(method: .put, endpoint: Endpoints.upload.url, headers: self.makeTokenHeader())
                
                self.backgroundUploadManager.upload(request: request,
                                                    data: AirdronUrlResource(meta: .image(fileURL: temp)),
                                                    processData: { data in
                                                        let image = try Image.init(data: data)
                                                        return image
                                                    },
                                                    completionHandler: completion)
            }
        }
    }
    
    func uploadImage(byFileUrl url: URL,
                     requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void)?,
                     progressHandler: ((Double) -> Void)?,
                     completion: ((Result<Image>) -> Void)?) {
        let request = AirdronRequest(method: .put, endpoint: Endpoints.upload.url, headers: self.makeTokenHeader())
        
        self.backgroundUploadManager.upload(request: request,
                                            data: AirdronUrlResource(meta: .image(fileURL: url)),
                                            processData: { data in
                                                let image = try Image.init(data: data)
                                                return image
        },
                                            
                                            completionHandler: completion,
                                            requestCompletionHandler: requestCompletionHandler,
                                            progressHandler: progressHandler)
    }
    
    func updateAvatar(id: String,
                      completion: ((Result<Model>) -> Void)?) {
        let parameters: [String: Any] = ["avatar": id]
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.profile.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Model {
            let response = try Model(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    
    func updateBodyInfo(info: BodyInfo,
                        completion: ((Result<Model>) -> Void)?) {
        let parameters: [String: Any] = ["body_info": info.toDictionary()]
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.profile.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Model {
            let response = try Model(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func updateMainInfo(user: UserCreation,
                        completion: ((Result<Model>) -> Void)?) {
        let parameters: [String: Any] = user.toDictionary()
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.profile.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Model {
            let response = try Model(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func updateAdditionalInfo(info: AdditionalInfo,
                              completion: ((Result<Model>) -> Void)?) {
        let parameters: [String: Any] = ["additional_info": info.toDictionary()]
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.profile.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        func processData(data: Data) throws -> Model {
            let response = try Model(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    func fetchLanguage(completion: ((Result<[SelectableValue]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "LANGUAGE"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [SelectableValue] {
            let response = try [SelectableValue](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchLanguageLevel(completion: ((Result<[SelectableValue]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        parameters["type"] = "LANGUAGE_LEVEL"
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.selectableValues.url,
                                     parameters: parameters,
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [SelectableValue] {
            let response = try [SelectableValue](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchChat(completion: ((Result<Chat>) -> Void)?) {
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.chat.url,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try Chat(data: data) },
                                    completionHandler: completion)
    }
    
    func fetchMessages(chatId: String,
                       lastMessageId: String?,
                       completion: ((Result<[Message]>) -> Void)?) {
        var parameters: [String: Any] = [:]
        if let lastId = lastMessageId {
            parameters["last_id"] = lastId
        }
        parameters["count"] = 50
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.messages(chatId: chatId).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        self.networkManager.perform(request: request,
                                    processData: { data in try [Message](data: data) },
                                    completionHandler: completion)
    }
    
    func sendMessage(chatId: String, message: String, completion: ((Result<Message>) -> Void)?) {
        let parameters: [String: Any] = ["message": message]
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.messages(chatId: chatId).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try Message(data: data) },
                                    completionHandler: completion)
    }

}

// MARK: Portfolio
extension ApiServiceImp {
    
    func fetchPortfolio(completion: ((Result<[AlbumShort]>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.portfolio.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> [AlbumShort] {
            let response = try [AlbumShort](data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func fetchAlbum(id: String, completion: ((Result<Album>) -> Void)?) {
        var parameters: [String: Any] = self.langDictParameters
        let request = AirdronRequest(method: .get,
                                     endpoint: Endpoints.album(id: id).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: URLEncoding.default)
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func deletePhotosFromAlbum(albumId: String, photoIds: [String], completion: ((Result<Album>) -> Void)?) {
        var parameters: [String: Any] = photoIds.asAlamofireParameters()
        let request = AirdronRequest(method: .delete,
                                     endpoint: Endpoints.albumImages(id: albumId).url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader(),
                                     encoding: ArrayEncoding())
        func processData(data: Data) throws -> Album {
            let response = try Album(data: data)
            return response
        }
        self.networkManager.perform(request: request,
                                    processData: processData,
                                    completionHandler: completion)
    }
    
    func uploadImagesInAlbum(albumId: String,
                             albumImages: [AlbumImage],
                             payment: Payment?,
                             completion: ((Result<UploadImagesResponse>) -> Void)?) {
        var dict: [String: Any] = [:]
        dict["images"] = albumImages.map { $0.toDictionary() }
        if let payment = payment {
            dict["payment"] = payment.toDictionary()
        }
        let request = AirdronRequest(method: .post,
                                     endpoint: Endpoints.album(id: albumId).url,
                                     parameters: dict,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try UploadImagesResponse.init(data: data) },
                                    completionHandler: completion)
    }
    
    func updateSocialNetworks(networks: SocialNetworks, completion: ((Result<Model>) -> Void)?) {
        let parameters: [String: Any] = ["social_networks": networks.toDictionary()]
        let request = AirdronRequest(method: .patch,
                                     endpoint: Endpoints.profile.url,
                                     parameters: parameters,
                                     headers: self.makeTokenHeader())
        self.networkManager.perform(request: request,
                                    processData: { data in try Model.init(data: data) },
                                    completionHandler: completion)
    }
    
}
