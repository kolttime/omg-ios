//
//  SessionManager.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

class OMGUserSessionManager {
    
    private let credential: CredentialStorage
    public let loginObserver: Observable<Bool>
    
    init(credential: CredentialStorage) {
        self.credential = credential
        self.loginObserver = Observable<Bool>(value: false)
        self.loginObserver.value = self.isRegistred
    }
    
    private let firstLaunchKey = "OMG.firstLaunch.key"
    private let userKey = "OMG.user.key"
    
    func save(user: User) {
        do {
            let data = try JSONEncoder().encode(user)
            UserDefaults.standard.set(data, forKey: self.userKey)
            self.loginObserver.value = self.isRegistred
        } catch {
            print("Exeption while saving user")
        }
    }
    
    var user: User? {
        guard let userData = UserDefaults.standard.data(forKey: self.userKey),
            let user = try? JSONDecoder().decode(User.self, from: userData) else { return nil }
        return user
    }
    
    func deleteUser() {
        UserDefaults.standard.removeObject(forKey: self.userKey)
    }
    
    func save(token: String) {
        self.loginObserver.value = self.isRegistred
        self.credential.save(token: token)
    }
    
    func deleteToken() {
        self.credential.deleteToken()
    }
    
    func setFirstLaunch() {
        UserDefaults.standard.set(true, forKey: self.firstLaunchKey)
    }
    
    var didFirstLaunch: Bool {
        return UserDefaults.standard.bool(forKey: self.firstLaunchKey)
    }
    
    var isRegistred: Bool {
        return self.credential.getToken() != nil && self.user != nil
    }
    
    func logout() {
        self.deleteUser()
        self.deleteToken()
        self.loginObserver.value = self.isRegistred
    }
}
