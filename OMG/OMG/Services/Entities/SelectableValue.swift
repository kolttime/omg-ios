//
//  SelectableValue.swift
//  OMG
//
//  Created by Roman Makeev on 19/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct SelectableValue: Codable {
    
    let name: String
    let text: String
    
    init() {
        self.name = String.empty
        self.text = String.empty
    }
}

extension SelectableValue {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(SelectableValue.self, from: data)
    }
}

extension Array where Element == SelectableValue {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode([SelectableValue].self, from: data)
    }
}
