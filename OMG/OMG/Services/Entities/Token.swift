//
//  Token.swift
//  OMG
//
//  Created by Roman Makeev on 13/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Token: Codable {
    
    let token: String
}

extension Token {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Token.self, from: data)
    }
}
