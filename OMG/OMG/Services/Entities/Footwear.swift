//
//  Footwear.swift
//  OMG
//
//  Created by Roman Makeev on 01/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum FootwearUnit: String, Codable {
    
    case ru = "RU"
    case us = "US"
    case eu = "EU"
}

struct Footwear: Codable {
    
    let name: String
    let text: String
    let unit: FootwearUnit
    let sex: SexType
}

extension Footwear {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Footwear.self, from: data)
    }
}

extension Array where Element == Footwear {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Footwear].self, from: data)
    }
}
