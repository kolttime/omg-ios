//
//  Currency.swift
//  OMG
//
//  Created by Roman Makeev on 08/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum Currency: String, Codable {
    
    case rub = "RUB"
    case usd = "USD"
    case eur = "EUR"
    
    var representableString: String {
        switch self {
        case .rub:
            return "₽"
        case .usd:
            return "$"
        case .eur:
            return "€"
        }
    }
}
