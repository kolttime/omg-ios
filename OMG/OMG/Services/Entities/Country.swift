//
//  Country.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Country: Codable {
    
    let name: String
    let text: String
    let countryCode: String
    let callingCode: String
}

extension Country {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Country.self, from: data)
    }
}

extension Array where Element == Country {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Country].self, from: data)
    }
}
