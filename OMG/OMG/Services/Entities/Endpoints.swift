//
//  Endpoints.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum Endpoints {
    
    case auth
    case verification
    case activation
    case staticContent
    case selectableValues
    case castings
    case casting(id: String)
    case profile
    case upload
    case castingParticipation(id: String)
    case portfolio
    case album(id: String)
    case albumImages(id: String)
    case instagramCode
    case instagramRedirect
    case instagramToken
    case instagramAuth
    case instagramLogin
    case chat
    case messages(chatId: String)
    
    static var host: String  { return "http://85.143.173.172:3000/api/v2" }
    static var urlAddress: String  { return "http://85.143.173.172:3000/" }
    static var sharingAddress: String  { return "https://omg.link/" }
    
    var url: String {
        switch self {
        case .auth:
            return Endpoints.host + "/authorization"
        case .verification:
            return Endpoints.host + "/verification"
        case .staticContent:
            return Endpoints.host + "/static_content"
        case .selectableValues:
            return Endpoints.host + "/selectable_values"
        case .castings:
            return Endpoints.host + "/castings"
        case .casting(let id):
            return Endpoints.host + "/castings/\(id)"
        case .activation:
            return Endpoints.host + "/activation"
        case .profile:
            return Endpoints.host + "/profile"
        case .upload:
            return Endpoints.host + "/upload"
        case .castingParticipation(let id):
            return Endpoints.host + "/castings/\(id)/application"
        case .portfolio:
            return Endpoints.host + "/portfolio"
        case .album(let id):
            return Endpoints.host + "/portfolio/\(id)"
        case .albumImages(let id):
            return Endpoints.host + "/portfolio/\(id)/images"
        case .instagramToken:
            return Endpoints.host + "/profile/get_network_token"
        case .instagramAuth:
            return "https://api.instagram.com/oauth/authorize/"
        case .instagramCode:
            return Endpoints.host + "/profile/social_network/instagram?code"
        case .instagramRedirect:
            return Endpoints.host + "/profile/social_network/instagram"
        case .instagramLogin:
            return Endpoints.host + "/oauth/instagram"
        case .chat:
            return Endpoints.host + "/messages"
        case .messages(let chatId):
            return Endpoints.host + "/messages/\(chatId)"
        }
    }
}
