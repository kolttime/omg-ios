//
//  Secure3D.swift
//  OMG
//
//  Created by Roman Makeev on 18/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Secure3D: Codable {
    
    let transactionId: Int
    let paReq: String
    let acsUrl: String
    let tempUrl: String
    
    enum CodingKeys: String, CodingKey {
        case transactionId = "TransactionId"
        case paReq = "PaReq"
        case acsUrl = "AcsUrl"
        case tempUrl = "TermUrl"
    }
    
    func toSendable() -> Secure3dSend {
        let secure = Secure3dSend(transactionId: self.transactionId, paReq: self.paReq, tempUrl: self.tempUrl)
        return secure
    }
}

extension Secure3D {
    init(data: Data) throws {
        self = try JSONDecoder().decode(Secure3D.self, from: data)
    }
}
