//
//  UserActivation.swift
//  OMG
//
//  Created by Roman Makeev on 21/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

class UserCreation: DictionaryMappable {
    
    var email: String?
    var givenName: String?
    var familyName: String?
    var birthday: String?
    var sex: String?
    var country: String?
    var city: String?
    var countryDisplayName: String?
    
    init() {
        self.email = nil
        self.givenName = nil
        self.familyName = nil
        self.birthday = nil
        self.sex = nil
        self.country = nil
        self.city = nil
        self.countryDisplayName = nil
    }
    
    init(email: String?,
         givenName: String?,
         familyName: String?,
         birthday: String?,
         sex: String?,
         country: String?,
         city: String?,
         countryDisplayName: String?) {
        self.email = email
        self.givenName = givenName
        self.familyName = familyName
        self.birthday = birthday
        self.sex = sex
        self.country = country
        self.city = city
        self.countryDisplayName = countryDisplayName
    }
    
    func toDictionary() -> JsonDictionary {
        guard let email = self.email,
              let givenName = self.givenName,
              let familyName = self.familyName,
              let birthday = self.birthday,
              let sex = self.sex,
              let country = self.country,
            let city = self.city else { return [:] }
        let parameters: [String: Any] = ["email": email,
                                         "given_name": givenName,
                                         "family_name": familyName,
                                         "birthday": birthday,
                                         "sex": sex,
                                         "country": country,
                                         "city": city]
        return parameters
    }
    
}
