//
//  AlbumImage.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum PolaroidImageType: String, Codable {
    
    case frontFullHeight = "front_full_height"
    case behindFullHeight = "behind_full_height"
    case leftSideFullHeight = "left_side_full_height"
    case rightSideFullHeight = "right_side_full_height"
    case facePortrait = "face_portrait"
    case faceProfileLeft = "face_profile_left"
    case faceProfileRight = "face_profile_right"
}

struct AlbumImage: Codable {
    
    let type: PolaroidImageType?
    let image: Image
    
    init(image: Image, type: PolaroidImageType?) {
        self.image = image
        self.type = type
    }
}

extension AlbumImage: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict = JsonDictionary()
        dict["image"] = self.image.id
        if let type = self.type {
            dict["type"] = type.rawValue
        }
        return dict
    }
}
