//
//  SexType.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum SexType: String, CaseIterable, Codable {
    
    case male = "MALE"
    case female = "FEMALE"
    case nonBinary = "NON-BINARY"
    
    var localize: String {
        switch self {
        case .male:
            return L10n.profileSexMale
        case .female:
            return L10n.profileSexFemale
        case .nonBinary:
            return L10n.profileSexNonBinary
        }
    }
}
