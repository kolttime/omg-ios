//
//  UploadImagesResponse.swift
//  OMG
//
//  Created by Roman Makeev on 18/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct UploadImagesResponse: Codable {
    
    let success: Bool
    let count: Int
    let secure3d: Secure3D?
    let errorMessage: String?
    
    enum CodingKeys: String, CodingKey {
        case success = "success"
        case count = "count"
        case secure3d = "3d_secure"
        case errorMessage = "error_message"
    }
}

extension UploadImagesResponse {
    
    init(data: Data) throws {
        self = try JSONDecoder().decode(UploadImagesResponse.self, from: data)
    }
}


