//
//  AlbumShort.swift
//  OMG
//
//  Created by Roman Makeev on 08/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct AlbumShort: Codable {
    
    let id: String
    let title: String
    let lead: String
    let paid: Bool
    let price: Double?
    let currency: Currency
    var count: Int
    
    init(id: String,
         title: String,
         lead: String,
         paid: Bool,
         price: Double?,
         currency: Currency,
         count: Int) {
        self.id = id
        self.title = title
        self.lead = lead
        self.paid = paid
        self.price = price
        self.currency = currency
        self.count = count
    }
}

extension AlbumShort {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(AlbumShort.self, from: data)
    }
}

extension Array where Element == AlbumShort {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([AlbumShort].self, from: data)
    }
}
