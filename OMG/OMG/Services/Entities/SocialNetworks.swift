//
//  SocialNetworks.swift
//  OMG
//
//  Created by Roman Makeev on 22/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct SocialNetworks: Codable {
    
    var instagram: String?
    var facebook: String?
    var vk: String?
    var twitter: String?
    var other: String?
}

extension SocialNetworks {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(SocialNetworks.self, from: data)
    }
}

extension SocialNetworks: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict = JsonDictionary()
        if let instagram = self.instagram {
            dict["instagram"] = instagram
        } else {
            dict["instagram"] = NSNull()
        }
        return dict
    }
}

extension Array where Element == SocialNetworks {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([SocialNetworks].self, from: data)
    }
}
