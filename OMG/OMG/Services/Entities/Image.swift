//
//  Image.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct ImageSrc: Codable {
    
    enum CodingKeys: String, CodingKey {
        case w400 = "400w"
        case w600 = "600w"
        case w800 = "800w"
        case w1100 = "1100w"
        case w1400 = "1400w"
        case w1600 = "1600w"
        case w2000 = "2000w"
        case w2500 = "2500w"
    }
    
    let w400: URL?
    let w600: URL?
    let w800: URL?
    let w1100: URL?
    let w1400: URL?
    let w1600: URL?
    let w2000: URL?
    let w2500: URL?

}

struct Image: Codable, Hashable {
    
    static func == (lhs: Image, rhs: Image) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
    
    var base64Preview: String {
        return self.preview.components(separatedBy: ",").last ?? String.empty
    }
    
    let id: String
    let preview: String
    let original: URL
    let width: Int
    let height: Int
    let size: Int
    let srcset: ImageSrc?
}

extension Image {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Image.self, from: data)
    }
}
