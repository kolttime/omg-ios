//
//  User.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct User: Codable {
    
    let permalink: String
    let type: UserType
    let givenName: String
    let familyName: String
    let sex: SexType
    let birthday: Date
    let avatar: Image?
    let phone: String?
    let email: String
    let country: Country
    let city: String
    
    init(permalink: String,
         type: UserType,
         givenName: String,
         familyName: String,
         sex: SexType,
         birthday: Date,
         avatar: Image?,
         phone: String?,
         email: String,
         country: Country,
         city: String) {
        self.permalink = permalink
        self.type = type
        self.givenName = givenName
        self.familyName = familyName
        self.sex = sex
        self.birthday = birthday
        self.avatar = avatar
        self.phone = phone
        self.email = email
        self.country = country
        self.city = city
    }
    
    func toModel() -> Model {
        return Model(permalink: self.permalink,
                     type: self.type,
                     givenName: self.givenName,
                     familyName: self.familyName,
                     sex: self.sex,
                     birthday: self.birthday,
                     avatar: self.avatar,
                     phone: self.phone,
                     email: self.email,
                     country: self.country,
                     city: self.city)
    }
}

extension User {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(User.self, from: data)
    }
}

extension Array where Element == User {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode([User].self, from: data)
    }
}
