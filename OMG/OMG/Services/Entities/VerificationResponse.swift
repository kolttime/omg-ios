//
//  VerificationResponse.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct VerificationResponse: Codable {
    
    let success: Bool
    let token: String?
    let user: User?
}

extension VerificationResponse {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(VerificationResponse.self, from: data)
    }
}
