//
//  Chat.swift
//  OMG
//
//  Created by Roman Makeev on 18/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Chat: Codable {
    
    let id: String
    let newMessages: Int
}

extension Chat {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(Chat.self, from: data)
    }
}

extension Array where Element == Chat {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode([Chat].self, from: data)
    }
}
