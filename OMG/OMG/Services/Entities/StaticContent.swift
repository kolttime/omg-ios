//
//  StaticContent.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct StaticContent: Codable {
    
    let name: String
    let title: String
    let text: String
}

extension StaticContent {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(StaticContent.self, from: data)
    }
}
