//
//  Payment.swift
//  OMG
//
//  Created by Roman Makeev on 16/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Payment: Codable {
    
    let cardCryptogramPacket: String
    let name: String?
}

extension Payment: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict = JsonDictionary()
        dict["card_cryptogram_packet"] = self.cardCryptogramPacket
        if let name = self.name {
            dict["name"] = name
        }
        return dict
    }
}
