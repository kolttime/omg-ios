//
//  Album.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Album: Codable {
    
    let id: String
    let title: String
    let lead: String
    let paid: Bool
    let price: Double?
    let currency: Currency
    let count: Int
    var images: [AlbumImage]
    
    func toShort() -> AlbumShort {
        return AlbumShort(id: self.id,
                          title: self.title,
                          lead: self.lead,
                          paid: self.paid,
                          price: self.price,
                          currency: self.currency,
                          count: self.count)
    }
}

extension Album {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Album.self, from: data)
    }
}

extension Array where Element == Album {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([Album].self, from: data)
    }
}
