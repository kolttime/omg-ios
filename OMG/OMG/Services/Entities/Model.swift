//
//  Model.swift
//  OMG
//
//  Created by Roman Makeev on 21/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Model: Codable {
    
    let permalink: String
    let type: UserType
    let givenName: String
    let familyName: String
    let sex: SexType
    let birthday: Date
    let avatar: Image?
    let phone: String?
    let email: String
    let country: Country
    let city: String
    var socialNetworks: SocialNetworks
    
    var mainInfo: UserCreation {
        return UserCreation(email: self.email,
                              givenName: self.givenName,
                              familyName: self.familyName,
                              birthday: DateFormatter.dateDateFormatter().string(from: self.birthday),
                              sex: self.sex.rawValue,
                              country: self.country.name,
                              city: self.city,
                              countryDisplayName: self.country.text)
    }
    
    let bodyInfo: BodyInfo
    let additionalInfo: AdditionalInfo
    let fillProgress: Int?
    
    init(permalink: String,
         type: UserType,
         givenName: String,
         familyName: String,
         sex: SexType,
         birthday: Date,
         avatar: Image?,
         phone: String?,
         email: String,
         country: Country,
         city: String) {
        self.permalink = permalink
        self.type = type
        self.givenName = givenName
        self.familyName = familyName
        self.sex = sex
        self.birthday = birthday
        self.avatar = avatar
        self.phone = phone
        self.email = email
        self.country = country
        self.city = city
        self.bodyInfo = BodyInfo.init()
        self.additionalInfo = AdditionalInfo.init()
        self.socialNetworks = SocialNetworks.init()
        self.fillProgress = nil
    }
}

extension Model {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(Model.self, from: data)
    }
}

extension Array where Element == Model {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode([Model].self, from: data)
    }
}
