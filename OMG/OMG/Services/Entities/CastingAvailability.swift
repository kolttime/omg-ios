//
//  CastingAvailability.swift
//  OMG
//
//  Created by Roman Makeev on 08/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum CastingAvailability: String, Codable {
    case no = "NO"
    case yes = "YES"
    case fillingRequired = "FILLING_REQUIRED"
}
