//
//  CastingParticipationResponse.swift
//  OMG
//
//  Created by Roman Makeev on 25/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct CastingParticipationResponse: Codable {
    
    let applicationStatus: CastingApplicationStatus
    
}

extension CastingParticipationResponse {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(CastingParticipationResponse.self, from: data)
    }
}

extension Array where Element == CastingParticipationResponse {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([CastingParticipationResponse].self, from: data)
    }
}
