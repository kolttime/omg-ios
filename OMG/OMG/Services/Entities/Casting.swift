//
//  Casting.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Casting: Decodable {
    
    let id: String
    let type: MaterialType
    let permalink: String
    let title: String
    let lead: String
    let keywords: String
    let thumbnail: Image
    let cover: Image
    let published: Date
    let blocks: [MaterialBlock]
    let partner: Partner?
    let untilDate: Date
    var available: CastingAvailability
    var applicationStatus: CastingApplicationStatus
    let results: [UserShort]
    
    var listUntilDateString: String {
        return L10n.sharedUntil + " " + self.untilDateString
    }
    
    var untilDateString: String {
        return DateFormatter.showDateDateFormatter().string(from: self.untilDate)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case permalink
        case title
        case lead
        case keywords
        case thumbnail
        case cover
        case published
        case blocks
        case partner
        case untilDate
        case available
        case applicationStatus
        case results
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(String.self, forKey: .id)
        self.title = try values.decode(String.self, forKey: .title)
        let typeString = try values.decode(String.self, forKey: .type)
        self.type = MaterialType(rawValue: typeString)!
        self.permalink = try values.decode(String.self, forKey: .permalink)
        self.lead = try values.decode(String.self, forKey: .lead)
        self.keywords = try values.decode(String.self, forKey: .keywords)
        self.thumbnail = try values.decode(Image.self, forKey: .thumbnail)
        self.cover = try values.decode(Image.self, forKey: .thumbnail)
        self.blocks = try values.decode([MaterialBlock].self, forKey: .blocks)
        let publishedString: String? = try values.decode(String.self, forKey: .published)
        self.published = DateFormatter.dateTimeDateFormatter().date(from: publishedString)!
        self.partner = try values.decodeIfPresent(Partner.self, forKey: .partner)
        let untilDateString: String? = try values.decode(String.self, forKey: .untilDate)
        self.untilDate = DateFormatter.dateDateFormatter().date(from: untilDateString) ?? Date()
        self.available = try values.decode(CastingAvailability.self, forKey: .available)
        self.applicationStatus = try values.decode(CastingApplicationStatus.self, forKey: .applicationStatus)
        self.results = try values.decodeIfPresent([UserShort].self, forKey: .results) ?? []
    }
}

extension Casting {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Casting.self, from: data)
    }
}
