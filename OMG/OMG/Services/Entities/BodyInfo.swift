//
//  BodyInfo.swift
//  OMG
//
//  Created by Roman Makeev on 21/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct BodyInfo: Codable {
    
    var height: Double?
    var weight: Double?
    var bust: Double?
    var waist: Double?
    var hip: Double?
    var footwear: Footwear?
    var tattoos: SelectableValue?
    var hair: SelectableValue?
    var eyes: SelectableValue?
}

extension BodyInfo {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(BodyInfo.self, from: data)
    }
}

extension Array where Element == BodyInfo {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([BodyInfo].self, from: data)
    }
}

extension BodyInfo: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict = JsonDictionary()
        dict["height"] = self.height ?? NSNull()
        dict["weight"] = self.weight ?? NSNull()
        dict["bust"] = self.bust ?? NSNull()
        dict["waist"] = self.waist ?? NSNull()
        dict["footwear"] = self.footwear?.name ?? NSNull()
        dict["hip"] = self.hip ?? NSNull()
        dict["tattoos"] = self.tattoos?.name ?? NSNull()
        dict["hair"] = self.hair?.name ?? NSNull()
        dict["eyes"] = self.eyes?.name ?? NSNull()
        return dict
    }
}
