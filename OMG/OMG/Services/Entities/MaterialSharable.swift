//
//  MaterialSharable.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol MaterialSharable {
    
    var title: String { get set }
    var permalink: String { get set }
    var additionalPath: String { get }
}

extension MaterialSharable {
    
    var additionalPath: String { return "castings/" }
}
