//
//  MaterialBlock.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum MaterialBlockType: String, Codable {
    
    case image = "IMAGE"
    case text = "TEXT"
}

struct MaterialBlock: Decodable {
    
    struct TextBlock: Codable {
        
        let head: String?
        let text: String
    }
    
    struct ImageBlock: Codable {
        
        let image: Image?
        let caption: String?
    }
    
    let type: MaterialBlockType
    let image: Image?
    let caption: String?
    let text: String?
    let head: String?
    
    enum CodingKeys: String, CodingKey {
        case type
        case content
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let typeString = try values.decode(String.self, forKey: .type)
        self.type = MaterialBlockType(rawValue: typeString)!
        if case .image = self.type {
            let content = try values.decode(ImageBlock.self, forKey: .content)
            self.image = content.image
            self.caption = content.caption
            self.text = nil
            self.head = nil
        } else {
            let content = try values.decode(TextBlock.self, forKey: .content)
            self.text = content.text
            self.head = content.head
            self.caption = nil
            self.image = nil
        }
    }
}
