//
//  Message.swift
//  OMG
//
//  Created by Roman Makeev on 18/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Message: Codable {
    
    let id: String
    let my: Bool
    let sender: String?
    let message: String
    let viewed: Bool
}

extension Message {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode(Message.self, from: data)
    }
}

extension Array where Element == Message {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.dateDateFormatter())
        self = try decoder.decode([Message].self, from: data)
    }
}
