//
//  UserType.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum UserType: String {
    
    case model = "MODEL"
}

extension UserType: Codable {
    
//    enum Key: CodingKey {
//        case rawValue
//    }
//    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: Key.self)
//        let rawValue = try container.decode(String.self, forKey: .rawValue)
//        self = UserType(rawValue: rawValue)!
//    }
//    
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: Key.self)
//        try container.encode(self.rawValue, forKey: .rawValue)
//    }
    
}
