//
//  CastingShort.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct CastingShort: Decodable, MaterialSharable {
    
    let id: String
    let type: MaterialType
    var permalink: String
    var title: String
    let lead: String
    let thumbnail: Image
    let published: Date
    let partner: Partner?
    let untilDate: Date
    
    var listUntilDateString: String {
        return L10n.sharedUntil + " " + self.untilDateString
    }
    
    var untilDateString: String {
        return DateFormatter.showDateDateFormatter().string(from: self.untilDate)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case permalink
        case title
        case lead
        case thumbnail
        case published
        case partner
        case untilDate
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(String.self, forKey: .id)
        self.title = try values.decode(String.self, forKey: .title)
        let typeString = try values.decode(String.self, forKey: .type)
        self.type = MaterialType(rawValue: typeString)!
        self.permalink = try values.decode(String.self, forKey: .permalink)
        self.lead = try values.decode(String.self, forKey: .lead)
        self.thumbnail = try values.decode(Image.self, forKey: .thumbnail)
        let publishedString: String? = try values.decode(String.self, forKey: .published)
        self.published = DateFormatter.dateTimeDateFormatter().date(from: publishedString)!
        self.partner = try values.decodeIfPresent(Partner.self, forKey: .partner)
        let untilDateString: String? = try values.decode(String.self, forKey: .untilDate)
        self.untilDate = DateFormatter.dateDateFormatter().date(from: untilDateString) ?? Date()
    }
}

extension CastingShort {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(CastingShort.self, from: data)
    }
}

extension Array where Element == CastingShort {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([CastingShort].self, from: data)
    }
}
