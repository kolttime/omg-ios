//
//  Success.swift
//  OMG
//
//  Created by Roman Makeev on 12/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Success: Codable {
    
    let success: Bool
}

extension Success {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(Success.self, from: data)
    }
}
