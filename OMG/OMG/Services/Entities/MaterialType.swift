//
//  MaterialType.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum MaterialType: String, Codable {
    
    case casting = "CASTING"
    case news = "NEWS"
}
