//
//  AuthResponse.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct AuthResponse: Codable {
    
    let key: String
}

extension AuthResponse {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(AuthResponse.self, from: data)
    }
}
