//
//  UserShort.swift
//  OMG
//
//  Created by Roman Makeev on 08/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct UserShort: Codable {
    
    let permalink: String
    let givenName: String
    let familyName: String
}

extension UserShort {
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(UserShort.self, from: data)
    }
}

extension Array where Element == UserShort {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([UserShort].self, from: data)
    }
}
