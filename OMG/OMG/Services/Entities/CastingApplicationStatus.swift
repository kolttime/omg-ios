//
//  СastingApplicationStatus.swift
//  OMG
//
//  Created by Roman Makeev on 08/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum CastingApplicationStatus: String, Codable {
    
    case notExist = "NOT_EXIST"
    case inReview = "IN_REVIEW"
    case reviewed = "REVIEWED"
    case accepted = "ACCEPTED"
    case rejected = "REJECTED"
    
    var localizedString: String {
        switch self {
        case .accepted:
            return L10n.castingStatusAccepted
        case .rejected:
            return L10n.castingStatusRejected
        case .inReview:
            return L10n.castingStatusInReview
        case .reviewed:
            return L10n.castingStatusReviewed
        default:
            return String.empty
        }
    }
}
