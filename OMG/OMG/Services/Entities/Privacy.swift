//
//  Privacy.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

enum Privacy: String {
    
    case termsOfUse = "terms_of_use"
    case privacyPolicy = "privacy_policy"
}
