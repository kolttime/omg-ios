//
//  AdditionalInfo.swift
//  OMG
//
//  Created by Roman Makeev on 22/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct AdditionalInfo: Codable {
    
    var citizenship: Country?
    var internationalPassport: Bool?
    var motherAgency: Bool?
    var languages: [UserLanguage] = []
}

struct UserLanguage: Codable {
    
    var language: SelectableValue
    var level: SelectableValue
    
    init(language: SelectableValue, level: SelectableValue) {
        self.language = language
        self.level = level
    }
}

extension UserLanguage: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict = JsonDictionary()
        dict["language"] = self.language.name
        dict["level"] = self.level.name
        return dict
    }
}

extension AdditionalInfo: DictionaryMappable {
    
    func toDictionary() -> JsonDictionary {
        var dict: JsonDictionary = [:]
        dict["citizenship"] = self.citizenship?.name
        dict["international_passport"] = self.internationalPassport
        dict["mother_agency"] = self.motherAgency
        dict["languages"] = self.languages.map { $0.toDictionary() }
        return dict
    }
}

extension AdditionalInfo {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode(AdditionalInfo.self, from: data)
    }
}

extension Array where Element == AdditionalInfo {
    
    init(data: Data) throws {
        let decoder = JSONDecoder.makeCamelDecoder()
        self = try decoder.decode([AdditionalInfo].self, from: data)
    }
}
