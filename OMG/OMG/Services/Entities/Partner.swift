//
//  Partner.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Partner: Codable {
    
    let id: String
    let name: String
    let displayName: String
}
