//
//  Secure3DSend.swift
//  OMG
//
//  Created by Roman Makeev on 18/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

struct Secure3dSend: Codable {
    
    let transactionId: Int
    let paReq: String
    let tempUrl: String
    
    enum CodingKeys: String, CodingKey {
        case transactionId = "MD"
        case paReq = "PaReq"
        case tempUrl = "TermUrl"
    }
}
