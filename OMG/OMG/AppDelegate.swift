//
//  AppDelegate.swift
//  OMG
//
//  Created by Roman Makeev on 04/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import UIKit
import Appsee

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var dependencyContainer: DependencyContainer = {
        return DependencyContainer()
    }()
    
    private lazy var mainCoordinator: MainCoordinator = {
        return MainCoordinator(presenter: self.window!,
                               sessionManager: self.dependencyContainer.serviceContainer.sessionManager,
                               coordinatorFactory: self.dependencyContainer.coordinatorFactory)
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Appsee.start()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.mainCoordinator.start()
        return true
    }
}

