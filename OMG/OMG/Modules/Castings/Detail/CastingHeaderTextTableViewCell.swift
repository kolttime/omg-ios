//
//  CastingHeaderTextTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class CastingHeaderTextTableViewCell: TableViewCell {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.titleLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CastingHeaderTextTableCellViewModel
        self.titleLabel.attributedText = CustomFont.headline.attributesWithParagraph.make(string: viewModel.text)
    }
}

struct CastingHeaderTextTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return CastingHeaderTextTableViewCell.self }
    let text: String
}
