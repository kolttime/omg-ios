//
//  CastingDetailViewController.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class CastingDetailViewController: OMGNavigationBarViewController, ToastAlertPresentable, SharePresentable {
    
    var onCastingForm: ((String, String) -> Void)?
    var onCastingInfo: ((String) -> Void)?
    
    var shareContent: MaterialSharable { return self.castingShort }
    
    private var castingShort: CastingShort
    private var casting: Casting?
    
    private let apiService: ApiService
    lazy var tableViewController = ALTableViewController()
    var toastPresenter = ToastAlertPresenter()
    let viewModelBuilder = MaterialBlockViewModelBuilder()
    private lazy var separatorView = UIView()
    
    lazy var shareButton: ImageButton = {
        return ImageButton.makeShareButton()
    }()
    
    init(castingShort: CastingShort, apiService: ApiService) {
        self.apiService = apiService
        self.castingShort = castingShort
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph
            .colored(color: Color.grey44.value)
            .make(string: L10n.sharedCastings)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: self.castingShort.title)
        self.navigationBarView.setTitleHidden(value: true, withAnimation: false)
        self.add(self.tableViewController)
        self.view.addSubview(self.separatorView)
        self.separatorView.backgroundColor = Color.grey10.value
        self.separatorView.alpha = 0
        self.render(castingShort: self.castingShort)
        
        self.tableViewController.tableView.register(cellClass: CastingHeaderTextTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: CastingHeaderImageTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: CastingSendButtonTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: CastingInfoTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: ParagraphTableViewCell.self)
        self.tableViewController.tableView.register(cellClass: ImageFootnoteViewCell.self)
        self.tableViewController.tableView.register(headerFooterViewClass: EmptyTableHeaderFooterView.self)
        self.rightActions = [self.shareButton]
        self.shareButton.addTarget(self, action: #selector(shareTap), for: .touchUpInside)

        self.tableViewController.onScroll = { [weak self] _ in
            guard let self = self else { return }
            if self.tableViewController.tableView.indexPathsForVisibleRows?.contains(IndexPath(row: 0, section: 0)) == false {
                self.navigationBarView.setTitleHidden(value: false, withAnimation: true)
                self.separatorView.alpha = 1
            } else {
                self.navigationBarView.setTitleHidden(value: true, withAnimation: false)
                self.separatorView.alpha = 0
            }
        }
        
        self.viewModelBuilder.onUrlSelected = { [weak self] url in
            let viewController = SFSafariViewController(url: url)
            self?.present(viewController, animated: true)
        }
        
        self.fetchData()
    }
    
    func update(status: CastingApplicationStatus) {
        self.casting?.applicationStatus = status
        guard let casting = self.casting else { return }
        self.render(casting: casting)
        self.showSuccessToast(L10n.castingApplicationSent)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.tableViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.separatorView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.height.equalTo(1)
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    private func render(castingShort: CastingShort) {
        let commonSection = self.makeCommonSection(id: castingShort.id,
                                                   title: castingShort.title,
                                                   cover: castingShort.thumbnail)
        let viewModels: [TableSectionViewModel] = [commonSection]
        self.tableViewController.update(viewModels: viewModels)
    }
    
    private func render(casting: Casting) {
        let commonSection = self.makeCommonSection(id: casting.id,
                                                   title: casting.title,
                                                   cover: casting.cover)
        let buttonSection = self.makeApplicationStatusSectionViewModel(id: casting.id,
                                                                       title: casting.title,
                                                                       status: casting.applicationStatus,
                                                                       available: casting.available)
        var infoSection = DefaultTableSectionViewModel(cellModels: [])
        infoSection.headerViewModel = EmptyTableHeaderFooterViewModel.make(height: 5, color: Color.grey4.value)
        if let partner = casting.partner?.name {
            let partnerViewModel = CastingInfoTableCellViewModel.init(title: L10n.castingAnnounced,
                                                               subtitle: partner)
            infoSection.cellModels.append(partnerViewModel)
        }
        
        var untilTitle = L10n.castingAcceptedUntil
        if Date() > casting.untilDate {
            untilTitle = L10n.castingStatusClosed
        }
        
        let untilViewModel = CastingInfoTableCellViewModel.init(title: untilTitle,
                                                                subtitle: casting.untilDateString)
        infoSection.cellModels.append(untilViewModel)
        
        switch casting.applicationStatus {
        case .accepted, .rejected, .inReview, .reviewed:
            let untilViewModel = CastingInfoTableCellViewModel.init(title: L10n.castingStatusTitle,
                                                                    subtitle: casting.applicationStatus.localizedString)
            infoSection.cellModels.append(untilViewModel)
        default:
            break
        }
        infoSection.footerViewModel = EmptyTableHeaderFooterViewModel.make(height: 20, color: Color.grey4.value)
        
        
        let blocksViewModels = self.viewModelBuilder.makeMaterialBlocks(blocks: casting.blocks)
        let blocksSection = DefaultTableSectionViewModel(cellModels: blocksViewModels)
        let sections: [TableSectionViewModel] = [commonSection, buttonSection, infoSection, blocksSection]
        self.tableViewController.update(viewModels: sections)
    }
    
    private func fetchData() {
        self.apiService.fetchCasting(id: self.castingShort.id) { [weak self] result in
            switch result {
            case .success(let casting):
                self?.casting = casting
                self?.render(casting: casting)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    @objc
    private func shareTap() {
        self.sharePresent()
    }
}

private extension CastingDetailViewController {
    
    
    func makeCommonSection(id: String,
                           title: String,
                           cover: Image) -> TableSectionViewModel {
        let titleViewModel = CastingHeaderTextTableCellViewModel(text: title)
        let headerImageViewModel = CastingHeaderImageTableCellViewModel(image: cover)
        let section = DefaultTableSectionViewModel(cellModels: [titleViewModel,
                                                                headerImageViewModel])
        return section
    }
    
    func makeApplicationStatusSectionViewModel(id: String,
                                               title: String,
                                               status: CastingApplicationStatus,
                                               available: CastingAvailability) -> TableSectionViewModel {
        var section = DefaultTableSectionViewModel(cellModels: [])
        
        guard case .notExist = status else { return section }
        
        switch available {
        case .yes:
            let buttonViewModel = CastingSendButtonTableCellViewModel(state: .normal) { [weak self] _ in
                self?.onCastingForm?(id, title)
            }
            section.cellModels.append(buttonViewModel)
        case .fillingRequired:
            let buttonViewModel = CastingSendButtonTableCellViewModel(state: .normal) { [weak self] _ in
                self?.onCastingInfo?(title)
            }
            section.cellModels.append(buttonViewModel)
        default:
            break
        }
        return section
    }
}
