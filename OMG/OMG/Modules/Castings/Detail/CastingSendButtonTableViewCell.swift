//
//  CastingSendButtonTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class CastingSendButtonTableViewCell: TableViewCell {
    
    private lazy var button = OMGButton.makeCastingSend()
    private var touchHandler: ((UITableViewCell) -> Void)?
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.backgroundColor = Color.grey4.value
        self.contentView.addSubview(self.button)
        self.button.onTouch = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.touchHandler?(strongSelf)
        }
        self.setNeedsUpdateConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CastingSendButtonTableCellViewModel
        self.touchHandler = viewModel.touchHandler
        self.button.update(state: viewModel.state)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.button.snp.makeConstraints {
            $0.top.equalToSuperview().offset(20)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalToSuperview()
        }
    }
}

struct CastingSendButtonTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return CastingSendButtonTableViewCell.self }
    var state: OMGButton.ViewState
    var touchHandler: ((UITableViewCell) -> Void)?
}
