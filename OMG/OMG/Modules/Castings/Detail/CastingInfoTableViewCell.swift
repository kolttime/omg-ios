//
//  CastingInfoTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class CastingInfoTableViewCell: TableViewCell {
    
    private lazy var titleLabel = UILabel()
    private lazy var subtitleLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.backgroundColor = Color.grey4.value
        self.contentView.addSubview(self.subtitleLabel)
        self.contentView.addSubview(self.titleLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(self.titleLabel.snp.bottom)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalToSuperview()
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CastingInfoTableCellViewModel
        self.titleLabel.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: viewModel.title)
        self.subtitleLabel.attributedText = CustomFont.bodyBold.attributesWithParagraph.make(string: viewModel.subtitle)
    }
}

struct CastingInfoTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return CastingInfoTableViewCell.self }
    let title: String
    let subtitle: String
}
