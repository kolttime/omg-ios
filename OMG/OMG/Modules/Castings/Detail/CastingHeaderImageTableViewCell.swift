//
//  CastingHeaderCastingHeaderImageTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class CastingHeaderImageTableViewCell: TableViewCell {
    
    private var imageHeight: CGFloat = Device.isIphone5 ? 200 : 225
    
    private lazy var originalImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.previewImageView)
        self.contentView.addSubview(self.originalImageView)
        self.clipsToBounds = true
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CastingHeaderImageTableCellViewModel
        let data = Data(base64Encoded: viewModel.image.base64Preview)
        if let data = data {
            self.previewImageView.image = UIImage(data: data)
        }
        self.originalImageView.kf.setImage(with: viewModel.image.original, options: [.transition(.fade(0.2))])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.originalImageView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(15)
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.height.equalTo(self.imageHeight)
        }
        self.previewImageView.snp.makeConstraints {
            $0.edges.equalTo(self.originalImageView)
        }
    }
}

struct CastingHeaderImageTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return CastingHeaderImageTableViewCell.self }
    let image: Image
}
