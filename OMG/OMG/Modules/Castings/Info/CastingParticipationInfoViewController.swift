//
//  CastingParticipationInfoViewController.swift
//  OMG
//
//  Created by Roman Makeev on 04/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Gallery

class CastingParticipationInfoViewController: LargeTitleScrollViewController, ToastAlertPresentable {
    
    var onContinue: Action?
    
    private let apiService: ApiService
    private let backTitle: String
    private lazy var scrollViewController = ScrollViewController()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = CustomFont.large.attributesWithParagraph.make(string: L10n.castingFormInfo)
        return label
    }()
    
    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    private lazy var currentProgressLabel = UILabel()
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var continueButton = OMGButton.makeContinueFillProfile()
    
    init(backTitle: String, apiService: ApiService) {
        self.backTitle = backTitle
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.backTitle)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.castingFormInfoParticipation)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.castingFormTitle)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.add(self.scrollViewController)
        
        self.scrollViewController.scrollView.addSubview(self.infoLabel)
        self.scrollViewController.scrollView.addSubview(self.currentProgressLabel)
        self.view.addSubview(self.continueButton)
        self.view.addSubview(self.loadingLabel)

        self.continueButton.onTouch = { [weak self] in
            self?.onContinue?()
        }
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupScroll(scrollInteraction: self.scrollViewController, refreshControl: nil)
        self.infoLabel.isHidden = true
        self.loadingLabel.blink()
        self.fetchData()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.continueButton.snp.top).offset(-8)
        }
        self.infoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.currentProgressLabel.snp.makeConstraints {
            $0.top.equalTo(self.infoLabel.snp.bottom).offset(25)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.continueButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    private func fetchData() {
        self.apiService.fetchProfile { [weak self] result in
            self?.loadingLabel.isHidden = true
            switch result {
            case .success(let model):
                if let progress = model.fillProgress {
                    self?.currentProgressLabel.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.profileFillProgress("\(progress)%"))
                } else {
                    self?.currentProgressLabel.attributedText = NSAttributedString()
                }
                self?.infoLabel.isHidden = false
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
}
