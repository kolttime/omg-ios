//
//  CastingsModuleBuilder.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

class CastingsModuleBuilder {
    
    private let apiService: ApiService
    
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    func makeCastingListModule() -> CastingsListViewController {
        return CastingsListViewController(apiService: self.apiService)
    }
    
    func makeCastingDetailModule(castingShort: CastingShort) -> CastingDetailViewController {
        return CastingDetailViewController(castingShort: castingShort, apiService: self.apiService)
    }
    
    func makeCastingFormModule(castingId: String, backTitle: String) -> CastingFormViewController {
        return CastingFormViewController(castingId: castingId,
                                         backTitle: backTitle,
                                         apiService: self.apiService)
    }
    
    func makeCastingInfoModule(backTitle: String) -> CastingParticipationInfoViewController {
        return CastingParticipationInfoViewController(backTitle: backTitle,
                                                      apiService: self.apiService)
    }
}
