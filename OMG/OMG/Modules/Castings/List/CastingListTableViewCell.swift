//
//  CastingListTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import SnapKit

class CastingTableViewCell: TableViewCell {
    
    private var onCellSelected: Action?
    private static let imageHeight: CGFloat = 200
    private static let sideOffset: CGFloat = 10
    private static let subtitleTopOffset: CGFloat = 5

    private static let bottomOffset: CGFloat = Device.isIphone5 ? 25 : 30
    
    private var subtitileTopConstraint: Constraint?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var subtitleTextView: UITextView = {
        let textView = UITextView.makeStaticTextView()
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 5, bottom: 2, right: 4)
        textView.backgroundColor = Color.grey44.value
        return textView
    }()
    
    private lazy var originalImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var separatorView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.previewImageView)
        self.contentView.addSubview(self.originalImageView)
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.subtitleTextView)
        self.contentView.addSubview(self.separatorView)
        self.clipsToBounds = true
        self.setNeedsUpdateConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.originalImageView.snp.makeConstraints {
            $0.left.equalToSuperview().offset(CastingTableViewCell.sideOffset)
            $0.right.equalToSuperview().offset(-CastingTableViewCell.sideOffset)
            $0.top.equalToSuperview()
            $0.height.equalTo(CastingTableViewCell.imageHeight)
        }
        self.previewImageView.snp.makeConstraints { $0.edges.equalTo(self.originalImageView) }
        
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalTo(self.originalImageView.snp.bottom).offset(CastingTableViewCell.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.subtitleTextView.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            self.subtitileTopConstraint = $0.top.equalTo(self.titleLabel.snp.bottom).offset(CastingTableViewCell.subtitleTopOffset).constraint
            $0.bottom.equalToSuperview().offset(-CastingTableViewCell.bottomOffset)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! CastingTableCellViewModel
        self.titleLabel.attributedText = viewModel.titleAttributed
        self.subtitleTextView.attributedText = viewModel.subtitleAttributed
        if viewModel.subtitleAttributed.string.isEmpty {
            self.subtitileTopConstraint?.update(offset: 0)
        } else {
            self.subtitileTopConstraint?.update(offset: CastingTableViewCell.subtitleTopOffset)
        }
        let data = Data(base64Encoded: viewModel.image.base64Preview)
        if let data = data {
            self.previewImageView.image = UIImage(data: data)
        }
        self.originalImageView.kf.setImage(with: viewModel.image.original, options: [.transition(.fade(0.2))])
        self.onCellSelected = viewModel.cellSelectionHandler
    }
    
    override func didSelect() {
        self.onCellSelected?()
    }
}

struct CastingTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return CastingTableViewCell.self }
    let title: String
    let subtitle: String
    let image: Image
    var cellSelectionHandler: Action?
    let titleAttributed: NSAttributedString
    let subtitleAttributed: NSAttributedString
    
    init(title: String,
         subtitle: String,
         image: Image,
         cellSelectionHandler: Action?) {
        self.title = title
        self.subtitle = subtitle
        self.image = image
        self.cellSelectionHandler = cellSelectionHandler
        self.titleAttributed = CustomFont.large.attributesWithParagraph.make(string: title)
        self.subtitleAttributed = CustomFont.bodyRegular.attributesWithParagraph
            .colored(color: Color.white.value)
            .make(string: subtitle)
    }
}
