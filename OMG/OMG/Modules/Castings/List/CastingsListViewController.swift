//
//  CastingsListViewController.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class CastingsListViewController: LargeTitleRootScrollViewController, ToastAlertPresentable {
    
    var onCastingDetail: ((CastingShort) -> Void)?
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return refreshControl
    }()
    
    private let apiService: ApiService
    var toastPresenter = ToastAlertPresenter()
    lazy var tableViewController = ALTableViewController()
    
    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundBarColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.largeTitle = CustomFont.headline.attributesWithParagraph.make(string: L10n.sharedCastings)
        
        self.tableViewController.tableView.register(cellClass: CastingTableViewCell.self)
        self.setupScroll(scrollInteraction: self.tableViewController, refreshControl: self.refreshControl)
        self.add(self.tableViewController)
        self.view.addSubview(self.loadingLabel)
        self.fetchData()
        self.loadingLabel.blink()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.tableViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-self.airdronTabbarController!.currentTabbarHeight)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    private func fetchData() {
        self.apiService.fetchCastings { [weak self] result in
            self?.loadingLabel.isHidden = true
            self?.refreshControl.endRefreshing()
            switch result {
            case.success(let castings):
                guard let self = self else { return }
                
                let viewModels: [TableCellViewModel] = castings.map { casting in
                    let selectionHandler: Action = { [weak self] in
                        self?.onCastingDetail?(casting)
                    }
                    let viewModel = CastingTableCellViewModel(title: casting.title,
                                                              subtitle: casting.listUntilDateString,
                                                              image: casting.thumbnail,
                                                              cellSelectionHandler: selectionHandler)
                    return viewModel
                }
                let section = DefaultTableSectionViewModel(cellModels: viewModels)
                self.tableViewController.update(viewModels: [section])
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    @objc
    func refresh() {
        self.fetchData()
    }
}
