//
//  CastingFormViewController.swift
//  OMG
//
//  Created by Roman Makeev on 25/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Gallery

class CastingFormViewController: LargeTitleScrollViewController, ToastAlertPresentable {
    
    var onParticipate: ((CastingApplicationStatus) -> Void)?
    
    private let apiService: ApiService
    private let backTitle: String
    private let castingId: String
    private lazy var scrollViewController = ScrollViewController()
    var onDismiss: Action?
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var attachInfoLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.castingFormAttachTitle)
        return label
    }()
    
    private lazy var resetButton: ImageButton = {
        return ImageButton.makeResetButton()
    }()
    
    private lazy var sendButton = OMGButton.makeSend()
    
    private lazy var fillLengthAttachView = AttachImageView()
    private lazy var faceAttachView = AttachImageView()
    private lazy var sideViewAttachView = AttachImageView()
    private var fillLengthImage: Image?
    private var faceAttachImage: Image?
    private var sideViewImage: Image?
    var onFillLenght: Action?
    var onFaceAttach: Action?
    var onSideView: Action?
    
    init(castingId: String,
         backTitle: String,
         apiService: ApiService) {
        self.castingId = castingId
        self.backTitle = backTitle
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFillLength(image: Gallery.Image) {
        self.fillLengthAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.fillLengthImage = uploadedImage
                self?.fillLengthAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.fillLengthAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateSendButton()
        }
    }
    
    func updateFace(image: Gallery.Image) {
        self.faceAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.faceAttachImage = uploadedImage
                self?.faceAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.faceAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateSendButton()
        }
    }
    
    func updateSideView(image: Gallery.Image) {
        self.sideViewAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.sideViewImage = uploadedImage
                self?.sideViewAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.sideViewAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateSendButton()
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.rightActions = [self.resetButton]
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.backTitle)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.castingFormTitle)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.castingFormTitle)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.add(self.scrollViewController)
        
        self.fillLengthAttachView.defaultText = L10n.castingFormFullLength
        self.faceAttachView.defaultText = L10n.castingFormFace
        self.sideViewAttachView.defaultText = L10n.castingFormSideView

        self.scrollViewController.scrollView.addSubview(self.attachInfoLabel)
        self.scrollViewController.scrollView.addSubview(self.fillLengthAttachView)
        self.scrollViewController.scrollView.addSubview(self.faceAttachView)
        self.scrollViewController.scrollView.addSubview(self.sideViewAttachView)
        self.view.addSubview(self.sendButton)
        
        self.fillLengthAttachView.onImageTap = { [weak self] in
            self?.onFillLenght?()
        }
        
        self.faceAttachView.onImageTap = { [weak self] in
            self?.onFaceAttach?()
        }
        
        self.sideViewAttachView.onImageTap = { [weak self] in
            self?.onSideView?()
        }
        
        self.resetButton.addTarget(self, action: #selector(self.resetTouch), for: .touchUpInside)
        
        self.sendButton.update(state: .disabled)
        self.sendButton.onTouch = { [weak self] in
            self?.applyCasting()
        }
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupScroll(scrollInteraction: self.scrollViewController, refreshControl: nil)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.sendButton.snp.top).offset(-8)
        }
        self.attachInfoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.fillLengthAttachView.snp.makeConstraints {
            $0.top.equalTo(self.attachInfoLabel.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.faceAttachView.snp.makeConstraints {
            $0.top.equalTo(self.fillLengthAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.sideViewAttachView.snp.makeConstraints {
            $0.top.equalTo(self.faceAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.bottom.equalToSuperview()
        }
        self.sendButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    @objc
    func resetTouch() {
        self.sideViewImage = nil
        self.faceAttachImage = nil
        self.fillLengthImage = nil
        self.sideViewAttachView.state = .empty
        self.faceAttachView.state = .empty
        self.fillLengthAttachView.state = .empty
        self.validateSendButton()
    }
    
    private func validateSendButton() {
        var value = self.fillLengthImage != nil
        value = value && self.faceAttachImage != nil
        value = value && self.sideViewImage != nil
        
        if !value {
            self.sendButton.update(state: .disabled)
        } else {
            self.sendButton.update(state: .normal)
        }
    }
    
    func applyCasting() {
        guard let full = self.fillLengthImage,
              let face = self.faceAttachImage,
            let side = self.sideViewImage else { return }
        self.sendButton.update(state: .uploading)
        self.apiService.confirmParticipation(id: self.castingId,
                                             fullId: full.id,
                                             faceId: face.id,
                                             sideId: side.id) { [weak self] result in
                                                self?.sendButton.update(state: .normal)
                                                switch result {
                                                case .success(let response):
                                                    self?.onParticipate?(response.applicationStatus)
                                                case .failure(let error):
                                                    self?.showToastErrorAlert(error)
                                                }
        }
    }
}
