//
//  AttachImageView.swift
//  OMG
//
//  Created by Roman Makeev on 25/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

enum AttachImageState {
    
    case empty
    case uploading
    case uploaded(url: URL)
}

class AttachImageView: AirdronView {
    
    var onImageTap: Action?
    
    private let emptyHeight: CGFloat = Device.isIphone5 ? 100 : 75
    private let nonEmptyHeight: CGFloat = 100

    private let imageSize = CGSize(width: 90, height: 90)
    
    var state: AttachImageState = .empty {
        didSet {
            self.updateState()
            self.invalidateIntrinsicContentSize()
        }
    }
    
    private let textAttributes = CustomFont.bodyRegular.attributesWithParagraph
    private let attachString = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).underlying(color: Color.grey44.value).make(string: L10n.sharedAttach)
    private let replaceString = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).underlying(color: Color.grey44.value).make(string: L10n.sharedReplace)
    
    private lazy var activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var replaceLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.replaceString
        return label
    }()
    
    private lazy var attachLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.attachString
        return label
    }()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var defaultText: String = String.empty {
        didSet {
            self.label.attributedText = self.textAttributes.make(string: self.defaultText)
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.label)
        self.addSubview(self.replaceLabel)
        self.addSubview(self.attachLabel)
        self.activityIndicator.color = Color.grey77.value
        self.replaceLabel.isUserInteractionEnabled = true
        self.attachLabel.isUserInteractionEnabled = true
        self.addSubview(self.imageView)
        self.addSubview(self.activityIndicator)
        self.updateState()
        let replaceTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTap))
        let attachTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTap))
        self.replaceLabel.addGestureRecognizer(replaceTap)
        self.attachLabel.addGestureRecognizer(attachTap)
        self.backgroundColor = Color.grey4.value
    }
    
    private func updateState() {
        switch self.state {
        case .empty:
            self.imageView.isHidden = true
            self.activityIndicator.isHidden = true
            self.replaceLabel.isHidden = true
            self.attachLabel.isHidden = false
            self.activityIndicator.stopAnimating()
        case .uploading:
            self.imageView.isHidden = true
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.replaceLabel.isHidden = false
            self.attachLabel.isHidden = true
        case .uploaded(let url):
            self.imageView.isHidden = false
            self.imageView.kf.setImage(with: url) { [weak self] _, _, _, _ in
                self?.activityIndicator.isHidden = true
                self?.activityIndicator.stopAnimating()
            }
            self.replaceLabel.isHidden = false
            self.attachLabel.isHidden = true
        }
        self.setupConstraints()
        self.invalidateIntrinsicContentSize()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIView.noIntrinsicMetric
        if case.empty = self.state {
            return CGSize(width: width, height: self.emptyHeight)
        } else {
            return CGSize(width: width, height: self.nonEmptyHeight)
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.imageView.snp.removeConstraints()
        self.label.snp.removeConstraints()
        self.activityIndicator.snp.removeConstraints()
        self.attachLabel.snp.removeConstraints()
        self.replaceLabel.snp.removeConstraints()
        
        if case .empty = self.state {
            self.label.snp.makeConstraints {
                $0.left.equalToSuperview().offset(15)
                $0.top.equalToSuperview().offset(12)
                $0.right.equalToSuperview().offset(-15)
            }
            self.attachLabel.snp.makeConstraints {
                $0.left.equalToSuperview().offset(15)
                $0.top.equalTo(self.label.snp.bottom)
                $0.right.equalToSuperview().offset(-15)
            }
        } else {
            self.imageView.snp.makeConstraints {
                $0.left.equalToSuperview().offset(5)
                $0.top.equalToSuperview().offset(5)
                $0.size.equalTo(self.imageSize)
            }
            self.label.snp.makeConstraints {
                $0.left.equalTo(self.imageView.snp.right).offset(8)
                $0.top.equalToSuperview().offset(12)
                $0.right.equalToSuperview().offset(-15)
            }
            self.replaceLabel.snp.makeConstraints {
                $0.left.equalTo(self.imageView.snp.right).offset(8)
                $0.top.equalTo(self.label.snp.bottom)
                $0.right.equalToSuperview().offset(-15)
            }
        }
        self.activityIndicator.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalToSuperview().offset(35)
        }
    }
    
    @objc
    func imageTap() {
        self.onImageTap?()
    }
}
