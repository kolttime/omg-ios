//
//  InstagramController.swift
//  OMG
//
//  Created by Roman Makeev on 13/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class InstagramBindController: OMGNavigationBarViewController, ToastAlertPresentable {
   
    var onSuccess: Action?
    
    var toastPresenter = ToastAlertPresenter()
    
    lazy var webView = WKWebView()
    
    private let clientId: String = InstagramConstants.clientId
    private let accessToken: String
    private let redurectUrl: String
    private let apiService: ApiService
    
    init(accessToken: String, apiService: ApiService) {
        self.accessToken = accessToken
        self.apiService = apiService
        self.redurectUrl = Endpoints.instagramRedirect.url
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.view.backgroundColor = Color.white.value
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code&state=\(self.accessToken)",
                             arguments: [Endpoints.instagramAuth.url,
                                         self.clientId,
                                         self.redurectUrl])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webView.load(urlRequest)
        webView.navigationDelegate = self
        self.view.addSubview(self.webView)
        self.webView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.bottom.equalTo(self.bottomLayoutEdge)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}

extension InstagramBindController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.request.url?.absoluteString.hasPrefix(Endpoints.instagramCode.url) == true {
            decisionHandler(.cancel)
            self.apiService.instagramCode(url: navigationAction.request.url!) { [weak self] result in
                switch result {
                case .success(let response):
                    self?.onSuccess?()
                case .failure(let error):
                    self?.showToastErrorAlert(error)
                }
            }
        } else {
            decisionHandler(.allow)
        }
    }
}
