//
//  ProfileBodyView.swift
//  OMG
//
//  Created by Roman Makeev on 24/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ProfileBodyView: AirdronView {
    
    private let numberTextAttributes = CustomFont.large.attributesWithParagraph
    private let unitTextAttributes = CustomFont.tech.attributesWithParagraph
    private let labelTextAttributes = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey44.value)
    
    func set(height: String, weight: String, foot: String, bust: String, waist: String, hip: String) {
        let heightString = NSMutableAttributedString()
        heightString.append(self.numberTextAttributes.make(string: height))
        heightString.append(self.unitTextAttributes.make(string: " "))
        heightString.append(self.unitTextAttributes.make(string: L10n.sharedCm.lowercased()))

        let weightString = NSMutableAttributedString()
        weightString.append(self.numberTextAttributes.make(string: weight))
        weightString.append(self.unitTextAttributes.make(string: " "))
        weightString.append(self.unitTextAttributes.make(string: L10n.sharedKg.lowercased()))
        
        self.heightNumberLabel.attributedText = heightString
        self.weightNumberLabel.attributedText = weightString
        self.footNumberLabel.attributedText = self.numberTextAttributes.make(string: foot)
        self.bustNumberLabel.attributedText = self.numberTextAttributes.make(string: bust)
        self.waistNumberLabel.attributedText = self.numberTextAttributes.make(string: waist)
        self.hipNumberLabel.attributedText = self.numberTextAttributes.make(string: hip)
    }
    
    private lazy var heightLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.labelTextAttributes.make(string: L10n.profileHeight)
        return label
    }()
    
    private lazy var weightLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.labelTextAttributes.make(string: L10n.profileWeight)
        return label
    }()
    
    private lazy var footLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.labelTextAttributes.make(string: L10n.profileShoe)
        return label
    }()
    
    private lazy var bustLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.labelTextAttributes.make(string: L10n.profileBust)
        return label
    }()
    
    private lazy var waistLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.labelTextAttributes.make(string: L10n.profileWaist)
        return label
    }()
    
    private lazy var hipLabel: UILabel = {
        let label = UILabel()
        label.attributedText = self.labelTextAttributes.make(string: L10n.profileHips)
        return label
    }()
    
    private lazy var heightNumberLabel = UILabel()
    
    private lazy var weightNumberLabel = UILabel()
    
    private lazy var footNumberLabel = UILabel()
    
    private lazy var bustNumberLabel = UILabel()
    
    private lazy var waistNumberLabel = UILabel()
    
    private lazy var hipNumberLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.hipLabel)
        self.addSubview(self.hipNumberLabel)
        self.addSubview(self.heightLabel)
        self.addSubview(self.heightNumberLabel)
        self.addSubview(self.weightLabel)
        self.addSubview(self.weightNumberLabel)
        self.addSubview(self.waistLabel)
        self.addSubview(self.waistNumberLabel)
        self.addSubview(self.footLabel)
        self.addSubview(self.footNumberLabel)
        self.addSubview(self.bustLabel)
        self.addSubview(self.bustNumberLabel)
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 110)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.heightNumberLabel.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.top.equalToSuperview()
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.weightNumberLabel.snp.makeConstraints {
            $0.left.equalTo(self.heightNumberLabel.snp.right)
            $0.top.equalToSuperview()
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.footNumberLabel.snp.makeConstraints {
            $0.left.equalTo(self.weightNumberLabel.snp.right)
            $0.top.equalToSuperview()
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.heightLabel.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.top.equalTo(self.heightNumberLabel.snp.bottom).offset(5)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.weightLabel.snp.makeConstraints {
            $0.left.equalTo(self.heightNumberLabel.snp.right)
            $0.top.equalTo(self.weightNumberLabel.snp.bottom).offset(5)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.footLabel.snp.makeConstraints {
            $0.left.equalTo(self.weightNumberLabel.snp.right)
            $0.top.equalTo(self.footNumberLabel.snp.bottom).offset(5)
            $0.width.equalToSuperview().dividedBy(3)
        }
        
        self.bustNumberLabel.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.top.equalTo(self.heightLabel.snp.bottom).offset(20)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.waistNumberLabel.snp.makeConstraints {
            $0.left.equalTo(self.bustNumberLabel.snp.right)
            $0.top.equalTo(self.weightLabel.snp.bottom).offset(20)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.hipNumberLabel.snp.makeConstraints {
            $0.left.equalTo(self.waistNumberLabel.snp.right)
            $0.top.equalTo(self.footLabel.snp.bottom).offset(20)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.bustLabel.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.top.equalTo(self.bustNumberLabel.snp.bottom).offset(5)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.waistLabel.snp.makeConstraints {
            $0.left.equalTo(self.bustLabel.snp.right)
            $0.top.equalTo(self.waistNumberLabel.snp.bottom).offset(5)
            $0.width.equalToSuperview().dividedBy(3)
        }
        self.hipLabel.snp.makeConstraints {
            $0.left.equalTo(self.waistLabel.snp.right)
            $0.top.equalTo(self.hipNumberLabel.snp.bottom).offset(5)
            $0.width.equalToSuperview().dividedBy(3)
        }
    }
}
