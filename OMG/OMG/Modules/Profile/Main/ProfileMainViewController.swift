//
//  ProfileMainViewController.swift
//  OMG
//
//  Created by Roman Makeev on 22/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Gallery
import SnapKit

// TODO: Need refactoring

extension BodyInfo {
    
    var isFilled: Bool {
        var value = self.bust != nil
        value = value && self.eyes != nil
        value = value && self.height != nil
        value = value && self.weight != nil
        value = value && self.waist != nil
        value = value && self.footwear != nil
        return value
    }
}

extension AdditionalInfo {
    
    var isFilled: Bool {
        var value = self.citizenship != nil
        value = value && self.internationalPassport != nil
        value = value && self.motherAgency != nil
        value = value && !self.languages.isEmpty
        return value
    }
}

class ProfileMainViewController: OMGNavigationBarViewController, ToastAlertPresentable {

    var onEdit: ((Model) -> Void)?
    var onImagePicker: Action?
    var onEditBody: ((Model) -> Void)?
    var onEditAdditionalInfo: ((Model) -> Void)?
    var onSocialNetworkInfo: ((Model) -> Void)?
    var onSupport: Action?

    private var model: Model?
    
    private let avatarSize: CGSize = {
        if Device.isIphone5 {
            return CGSize(width: 61, height: 61)
        }
        return CGSize(width: 80, height: 80)
    }()
    
    private let smallAvatarSize: CGSize = {
        return CGSize(width: 30, height: 30)
    }()
    
    private let imageLeftOffset: CGFloat = 15
    private var imageRightOffset: CGFloat {
        if Device.isIphone6p {
            return 25
        } else {
            return 15
        }
    }
    
    private var leftContentOffset: CGFloat {
        return self.imageLeftOffset + self.imageRightOffset + self.avatarSize.width
    }
    
    private let menuSpace: CGFloat = {
        if Device.isIphone5 {
            return 10
        }
        return 15
    }()
    
    private let fillBodyInformationHeight: CGFloat = {
        return Device.isIphone5 ? 50 : 100
    }()
    
    private let filledBodyInformationHeight: CGFloat = {
        return 150
    }()
    
    private let answerQuestionHeight: CGFloat = {
        return Device.isIphone5 ? 50 : 75
    }()
    
    private let connectionSocialNetworkHeight: CGFloat = {
        return Device.isIphone5 ? 75 : 50
    }()
    
    private var setAvatarHeight: CGFloat = 50
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return refreshControl
    }()
    
    var toastPresenter = ToastAlertPresenter()
    
    lazy var editButton = UIButton.makeLargeTitleButtonAction(title: L10n.profileEdit)

    private lazy var avatarImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Color.grey10.value
        view.layer.cornerRadius = self.avatarSize.width / 2
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var smallAvatarImageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = Color.grey10.value
        view.layer.cornerRadius = self.smallAvatarSize.width / 2
        view.clipsToBounds = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var ageLocationLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var setAvatarLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.attributedText = CustomFont.tech.attributesWithParagraph.make(string: L10n.profileSetAvatar2)
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        return label
    }()
    
    private lazy var fillBodyInformationButton: SectionButton = {
        let button = SectionButton(height: self.fillBodyInformationHeight)
        button.text = L10n.profileFillBodyInfo
        return button
    }()
    
    private lazy var answerQuestionButton: SectionButton = {
        let button = SectionButton(height: self.answerQuestionHeight)
        button.text = L10n.profileAnswerQuestions
        return button
    }()
    
    private lazy var socialNetworkButton: SectionButton = {
        let button = SectionButton(height: self.connectionSocialNetworkHeight)
        button.text = L10n.profileConnectSocialNetwork
        return button
    }()
    
    private lazy var setAvatarButton: SectionButton = {
        let button = SectionButton(height: self.setAvatarHeight)
        button.text = L10n.profileSetAvatar
        return button
    }()
    
    private lazy var supportButton: SectionImageButton = {
        let button = SectionImageButton.makeSupport()
        button.text = L10n.sharedSupport
        return button
    }()
    
    private lazy var instagramConnectionView = InstagramConnectionView()
    
    private lazy var profileProgressLabel = UILabel()

    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    func update(model: Model) {
        self.model = model
        self.render(model: model)
    }
    
    private lazy var bodyInfoView = ProfileBodyView()
    
    private let apiService: ApiService
    private lazy var scrollViewController = ScrollViewController()
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.rightActions = [self.editButton]
        self.leftAction = self.smallAvatarImageView
        self.smallAvatarImageView.snp.makeConstraints {
            $0.size.equalTo(self.smallAvatarSize)
        }
        self.toastPresenter.targetView = self.view
        self.add(self.scrollViewController)
        self.view.addSubview(self.loadingLabel)
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.scrollViewController.scrollView.refreshControl = self.refreshControl
        
        
        self.setupHandlers()
        self.fetchData()
        self.loadingLabel.blink()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-self.airdronTabbarController!.currentTabbarHeight)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    private func fetchData() {
        self.apiService.fetchProfile { [weak self] result in
            self?.loadingLabel.isHidden = true
            self?.refreshControl.endRefreshing()
            switch result {
            case .success(let model):
                self?.model = model
                self?.render(model: model)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    @objc
    func refresh() {
        self.fetchData()
    }
    
    private func render(model: Model) {
        self.avatarImageView.kf.setImage(with: model.avatar?.original)
        self.smallAvatarImageView.kf.setImage(with: model.avatar?.original)

        let fullName = CustomFont.headline.attributesWithParagraph.make(string: model.givenName + "\n" + model.familyName)
        let navigationTitleFullName = CustomFont.bodyBold.attributesWithParagraph.make(string: model.givenName + " " + model.familyName)
        
        self.nameLabel.attributedText = fullName
        self.navigationTitle = navigationTitleFullName
        
        let ageLocationString = L10n.pluralAge(model.birthday.age()) + "\n" + model.city + ", " + model.country.text
        self.ageLocationLabel.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: ageLocationString)
        
        if let progress = model.fillProgress {
            self.profileProgressLabel.attributedText = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey30.value).make(string: L10n.profileFillProgress("\(progress)%"))
        } else {
            self.profileProgressLabel.attributedText = NSAttributedString()
        }
        
        self.setAvatarLabel.isHidden = model.avatar != nil
        if model.bodyInfo.isFilled {
            self.bodyInfoView.set(height: "\(Int(model.bodyInfo.height!))",
                                  weight: "\(Int(model.bodyInfo.weight!))",
                                  foot: model.bodyInfo.footwear!.text,
                                  bust: "\(Int(model.bodyInfo.bust!))",
                                  waist: "\(Int(model.bodyInfo.waist!))",
                                  hip: "\(Int(model.bodyInfo.hip!))")
        }
        
        self.smallAvatarImageView.alpha = 0
        self.navigationBarView.setTitleHidden(value: true, withAnimation: false)
        
        self.makeAndShowView(model: model)
    }
    
    private func makeAndShowView(model: Model) {
        self.avatarImageView.removeFromSuperview()
        self.nameLabel.removeFromSuperview()
        self.ageLocationLabel.removeFromSuperview()
        self.fillBodyInformationButton.removeFromSuperview()
        self.answerQuestionButton.removeFromSuperview()
        self.socialNetworkButton.removeFromSuperview()
        self.setAvatarButton.removeFromSuperview()
        self.supportButton.removeFromSuperview()
        self.instagramConnectionView.removeFromSuperview()
        
        self.profileProgressLabel.removeFromSuperview()
        self.bodyInfoView.removeFromSuperview()
        
        self.scrollViewController.scrollView.addSubview(self.avatarImageView)
        self.scrollViewController.scrollView.addSubview(self.setAvatarLabel)
        self.scrollViewController.scrollView.addSubview(self.nameLabel)
        self.scrollViewController.scrollView.addSubview(self.ageLocationLabel)
        
        if !model.bodyInfo.isFilled {
            self.scrollViewController.scrollView.addSubview(self.fillBodyInformationButton)
        } else {
            self.scrollViewController.scrollView.addSubview(self.bodyInfoView)
        }
        
        if !model.additionalInfo.isFilled {
            self.scrollViewController.scrollView.addSubview(self.answerQuestionButton)
        }
        if model.socialNetworks.instagram == nil {
            self.scrollViewController.scrollView.addSubview(self.socialNetworkButton)
        } else {
            self.scrollViewController.scrollView.addSubview(self.instagramConnectionView)
        }
        
        if model.avatar == nil {
            self.scrollViewController.scrollView.addSubview(self.setAvatarButton)
        }
        self.scrollViewController.scrollView.addSubview(self.supportButton)
        self.scrollViewController.scrollView.addSubview(self.profileProgressLabel)
        self.makeConstraints(model: model)
    }
    
    private func makeConstraints(model: Model) {
        
        self.avatarImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(self.imageLeftOffset)
            $0.size.equalTo(self.avatarSize)
        }
        self.setAvatarLabel.snp.makeConstraints {
            $0.center.equalTo(self.avatarImageView)
        }
        self.nameLabel.snp.makeConstraints {
            $0.centerY.equalTo(self.avatarImageView)
            $0.left.equalToSuperview().offset(self.leftContentOffset)
            $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
        }
        
        var bottomConstraint: ConstraintItem!
        
        self.ageLocationLabel.snp.makeConstraints {
            $0.top.equalTo(self.nameLabel.snp.bottom).offset(5)
            $0.left.equalToSuperview().offset(self.leftContentOffset)
            $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
        }
         bottomConstraint = ageLocationLabel.snp.bottom
        
        if !model.bodyInfo.isFilled {
            self.fillBodyInformationButton.snp.makeConstraints {
                $0.top.equalTo(bottomConstraint).offset(20)
                $0.left.equalToSuperview().offset(self.leftContentOffset)
                $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            }
            
            bottomConstraint = fillBodyInformationButton.snp.bottom
        } else {
            self.bodyInfoView.snp.makeConstraints {
                $0.top.equalTo(bottomConstraint).offset(20)
                $0.left.equalToSuperview().offset(self.leftContentOffset)
                $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            }
            bottomConstraint = bodyInfoView.snp.bottom
        }
        
        if !model.additionalInfo.isFilled {
            self.answerQuestionButton.snp.makeConstraints {
                $0.top.equalTo(bottomConstraint).offset(self.menuSpace)
                $0.left.equalToSuperview().offset(self.leftContentOffset)
                $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            }
            
            bottomConstraint = answerQuestionButton.snp.bottom
        }
        
        if model.socialNetworks.instagram == nil {
            self.socialNetworkButton.snp.makeConstraints {
                $0.top.equalTo(bottomConstraint).offset(self.menuSpace)
                $0.left.equalToSuperview().offset(self.leftContentOffset)
                $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            }
            
            bottomConstraint = socialNetworkButton.snp.bottom
        } else {
            self.instagramConnectionView.snp.makeConstraints {
                $0.top.equalTo(bottomConstraint).offset(self.menuSpace)
                $0.left.equalToSuperview().offset(self.leftContentOffset)
                $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            }
            
            bottomConstraint = instagramConnectionView.snp.bottom
        }

        
        
        if model.avatar == nil {
            self.setAvatarButton.snp.makeConstraints {
                $0.top.equalTo(bottomConstraint).offset(self.menuSpace)
                $0.left.equalToSuperview().offset(self.leftContentOffset)
                $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            }
            bottomConstraint = setAvatarButton.snp.bottom
        }
        
        self.supportButton.snp.makeConstraints {
            $0.top.equalTo(bottomConstraint).offset(self.menuSpace)
            $0.left.equalToSuperview().offset(self.leftContentOffset)
            $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
        }
        bottomConstraint = supportButton.snp.bottom
        
        self.profileProgressLabel.snp.makeConstraints {
            $0.top.equalTo(bottomConstraint).offset(ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(self.leftContentOffset)
            $0.width.equalToSuperview().offset(-self.leftContentOffset - ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
    func setupHandlers() {
        self.editButton.addTarget(self, action: #selector(editTouch), for: .touchUpInside)
        self.setAvatarButton.addTarget(self, action: #selector(self.avatarTap), for: .touchUpInside)
        self.supportButton.addTarget(self, action: #selector(self.supportTap), for: .touchUpInside)

        let avatarTap = UITapGestureRecognizer(target: self, action: #selector(self.avatarTap))
        self.avatarImageView.addGestureRecognizer(avatarTap)
        self.fillBodyInformationButton.addTarget(self,
                                                 action: #selector(editBodyTouch),
                                                 for: .touchUpInside)
        self.answerQuestionButton.addTarget(self,
                                            action: #selector(additionalInfoTouch),
                                            for: .touchUpInside)
        
        self.socialNetworkButton.addTarget(self,
                                           action: #selector(socialNetworkTouch),
                                           for: .touchUpInside)
        
        self.scrollViewController.onScroll = { [weak self] scrollView in
            guard let self = self else { return }
            let value = scrollView.contentOffset.y < self.avatarImageView.frame.maxY
            UIView.animate(withDuration: 0.3, animations: { self.smallAvatarImageView.alpha = value ? 0 : 1 })
            self.navigationBarView.setTitleHidden(value: value, withAnimation: true)

        }
    }
    
    @objc
    func editTouch() {
        if let model = self.model {
            self.onEdit?(model)
        }
    }
    
    @objc
    func editBodyTouch() {
        if let model = self.model {
            self.onEditBody?(model)
        }
    }
    
    @objc
    func additionalInfoTouch() {
        if let model = self.model {
            self.onEditAdditionalInfo?(model)
        }
    }
    
    @objc
    func socialNetworkTouch() {
        if let model = self.model {
            self.onSocialNetworkInfo?(model)
        }
    }
    
    @objc
    func avatarTap() {
        self.onImagePicker?()
    }
    
    @objc
    func supportTap() {
        self.onSupport?()
    }
}

extension ProfileMainViewController: GalleryControllerDelegate {
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        if let image = images.first {
            self.upload(image: image)
        }
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) { }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) { }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func upload(image: Gallery.Image) {
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let avatar):
                self?.updateAvatar(image: avatar)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func updateAvatar(image: Image) {
        self.apiService.updateAvatar(id: image.id) { [weak self] result in
            switch result {
            case .success(let model):
                self?.model = model
                self?.render(model: model)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
}
