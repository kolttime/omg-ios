//
//  ProfileModuleBuilder.swift
//  OMG
//
//  Created by Roman Makeev on 22/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

class ProfileModuleBuilder {
    
    private let apiService: ApiService

    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    func makeProfileMain() -> ProfileMainViewController {
        return ProfileMainViewController(apiService: self.apiService)
    }
    
    func makeEditProfile(model: Model) -> ProfileEditViewController {
        return ProfileEditViewController(model: model, apiService: self.apiService)
    }
    
    func makeSocialNetworks(model: Model) -> SocialNetworksViewController {
        return SocialNetworksViewController(model: model, apiService: self.apiService)
    }
    
    func makeEditBody(model: Model) -> BodyEditViewController {
        return BodyEditViewController(sex: model.sex, bodyInfo: model.bodyInfo, apiService: self.apiService)
    }
    
    func makeEditMain(model: Model) -> MainEditViewController {
        return MainEditViewController(user: model.mainInfo, apiService: self.apiService)
    }
    
    func makeSearchCountryModule() -> SearchCountryViewController {
        return SearchCountryViewController(showCallingCode: false, apiService: self.apiService)
    }
    
    func makeAdditionalInformationEdit(model: Model) -> AdditionalInfoEditViewController {
        return AdditionalInfoEditViewController(additionalInfo: model.additionalInfo, apiService: self.apiService)
    }
    
    func makeSupportModule() -> SupportViewController {
        return SupportViewController(apiService: self.apiService)
    }
    
    func makeInstagramModule(accessToken: String) -> InstagramBindController {
        return InstagramBindController(accessToken: accessToken, apiService: self.apiService)
    }
}
