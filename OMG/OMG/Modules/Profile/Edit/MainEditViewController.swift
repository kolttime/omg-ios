//
//  MainEditViewController.swift
//  OMG
//
//  Created by Roman Makeev on 29/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class MainEditViewController: OMGNavigationBarViewController, PopupPresentable, ToastAlertPresentable {
    
    private let apiService: ApiService
    private lazy var scrollViewController = ScrollViewController()
    private lazy var givenNameTextField = OMGTextField.makeGivenName()
    private lazy var familyNameTextField = OMGTextField.makeFamilyName()
    private lazy var sexPickedButton = PickedButton.makeSex()
    private lazy var birthdayTextField = OMGTextField.makeBirthday()
    private lazy var countryButton = PickedButton.makeCountry()
    private lazy var cityTextField = OMGTextField.makeCity()
    private lazy var emailTextField = OMGTextField.makeEmail()
    private lazy var updateButton = OMGButton.makeUpdate()
    private var activeField: UIView?
    private let textfieldTopOffset: CGFloat = 20
    private let buttonTopOffset: CGFloat = 25
    private var selectedSex: SexType?
    private var birthdayMandatoryComplete: Bool = false
    private let user: UserCreation
    
    private var sexButtonState: PickedButtonType = .unpicked {
        didSet {
            self.sexPickedButton.pickedState = self.sexButtonState
        }
    }
    private weak var popupViewController: PopupViewController?
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var keyboardObserver = KeyboardObserver()
    
    var onCountySelect: Action?
    var onUpdate: ((Model) -> Void)?
    
    init(user: UserCreation,
         apiService: ApiService) {
        self.apiService = apiService
        self.user = user
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(country: Country) {
        self.user.country = country.name
        self.user.countryDisplayName = country.text
        self.countryButton.pickedState = .picked(string: country.text)
        self.checkValid()
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.profileMainInfo)
        self.view.backgroundColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.givenNameTextField)
        self.scrollViewController.scrollView.addSubview(self.familyNameTextField)
        self.scrollViewController.scrollView.addSubview(self.sexPickedButton)
        self.scrollViewController.scrollView.addSubview(self.birthdayTextField)
        self.scrollViewController.scrollView.addSubview(self.countryButton)
        self.scrollViewController.scrollView.addSubview(self.cityTextField)
        self.scrollViewController.scrollView.addSubview(self.emailTextField)
        self.scrollViewController.scrollView.addSubview(self.updateButton)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.updateButton.update(state: .disabled)
        self.setupHandlers()
        
        self.givenNameTextField.text = self.user.givenName
        self.familyNameTextField.text = self.user.familyName
        if let birthday = self.user.birthday {
            self.birthdayTextField.text = birthday
            self.birthdayMandatoryComplete = true
        }
        
        if let sex = SexType(rawValue: self.user.sex ?? String.empty) {
            self.selectedSex = sex
            self.sexButtonState = .picked(string: sex.localize)
        }
        
        self.countryButton.pickedState = .picked(string: self.user.countryDisplayName ?? String.empty)
        self.cityTextField.text = self.user.city
        self.emailTextField.text = self.user.email
        self.checkValid()
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.givenNameTextField.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.familyNameTextField.snp.makeConstraints {
            $0.top.equalTo(self.givenNameTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.sexPickedButton.snp.makeConstraints {
            $0.top.equalTo(self.familyNameTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.birthdayTextField.snp.makeConstraints {
            $0.top.equalTo(self.sexPickedButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.countryButton.snp.makeConstraints {
            $0.top.equalTo(self.birthdayTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.cityTextField.snp.makeConstraints {
            $0.top.equalTo(self.countryButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.emailTextField.snp.makeConstraints {
            $0.top.equalTo(self.cityTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.updateButton.snp.makeConstraints {
            $0.top.equalTo(self.emailTextField.snp.bottom).offset(2 * self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-self.buttonTopOffset - ViewSize.sideOffset)
        }
    }
    
    func setupHandlers() {
        self.updateButton.onTouch = { [weak self] in
            self?.updateTap()
        }
        self.countryButton.addTarget(self, action: #selector(countryTap), for: .touchUpInside)
        self.sexPickedButton.addTarget(self, action: #selector(sexTap), for: .touchUpInside)
        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.scrollViewController.scrollView.contentInset.bottom = frame.height
            guard let textField = strongSelf.activeField else {
                return
            }
            var viewRect = strongSelf.scrollViewController.scrollView.frame
            viewRect.origin.y -= frame.height - strongSelf.bottomLayoutEdgeInset
            var rect = textField.frame
            rect.origin.y += strongSelf.textfieldTopOffset
            guard !viewRect.contains(rect) else { return }
            let y = viewRect.maxY - textField.frame.maxY - strongSelf.bottomLayoutEdgeInset - strongSelf.textfieldTopOffset
            
            let point = CGPoint(x: 0, y: -y)
            self?.scrollViewController.scrollView.setContentOffset(point, animated: true)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
        }
        
        self.birthdayTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.givenNameTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.familyNameTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        
        self.givenNameTextField.onReturnHandler = { [weak self] in
            self?.familyNameTextField.becomeResponder()
        }
        
        self.familyNameTextField.onReturnHandler = { [weak self] in
            self?.familyNameTextField.resignResponder()
        }
        
        self.birthdayTextField.onReturnHandler = { [weak self] in
            self?.birthdayTextField.resignResponder()
        }
        
        self.familyNameTextField.onTextDidChanged = { [weak self] response in
            self?.user.familyName = response.text
            self?.checkValid()
        }
        
        self.givenNameTextField.onTextDidChanged = { [weak self] response in
            self?.user.givenName = response.text
            self?.checkValid()
        }
        
        self.cityTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.emailTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        
        self.cityTextField.onReturnHandler = { [weak self] in
            self?.emailTextField.becomeResponder()
        }
        
        self.emailTextField.onReturnHandler = { [weak self] in
            self?.emailTextField.resignResponder()
        }
        
        self.cityTextField.onTextDidChanged = { [weak self] response in
            self?.checkValid()
            self?.user.city = response.text
        }
        self.emailTextField.onTextDidChanged = { [weak self] response in
            self?.checkValid()
            self?.user.email = response.text
        }
        
        self.birthdayTextField.onTextDidChanged = { [weak self] response in
            self?.birthdayMandatoryComplete = response.mandatoryComplete
            if response.mandatoryComplete {
                self?.user.birthday = response.text
            } else {
                self?.user.birthday = nil
            }
            self?.checkValid()
        }
    }
    
    @objc
    private func sexTap() {
        if case .picking = self.sexButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
        self.birthdayTextField.resignResponder()
        self.familyNameTextField.resignResponder()
        self.givenNameTextField.resignResponder()
        self.sexButtonState = .picking
        let viewModels = SexType.allCases.map { type -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: type.localize)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            let type = SexType.allCases[index]
            self?.selectedSex = type
            self?.sexButtonState = .picked(string: type.localize)
            self?.user.sex = type.rawValue
            self?.checkValid()
        }
        
        let dismissHandler: Action = { [weak self] in
            if let sexType = self?.selectedSex {
                self?.sexButtonState = .picked(string: sexType.localize)
            } else {
                self?.sexButtonState = .unpicked
            }
            self?.checkValid()
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    @objc
    func countryTap() {
        self.onCountySelect?()
    }
    
    private func checkValid() {
        var isValid = self.givenNameTextField.text?.isEmpty == false
        isValid = isValid && self.familyNameTextField.text?.isEmpty == false
        isValid = isValid && self.birthdayMandatoryComplete && DateFormatter.dateDateFormatter().date(from: self.birthdayTextField.text) != nil
        isValid = isValid && self.selectedSex != nil
        isValid = isValid && self.cityTextField.text?.isEmpty == false
        isValid = isValid && self.emailTextField.text?.isEmail == true
        isValid = isValid && self.user.country != nil
        if isValid {
            self.updateButton.update(state: .normal)
        } else {
            self.updateButton.update(state: .disabled)
        }
    }
    
    @objc
    func updateTap() {
        self.updateMainInfo()
    }
    
    private func updateMainInfo() {
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.updateButton.update(state: .uploading)
        }
        self.apiService.updateMainInfo(user: self.user) { [weak self] result in
            switch result {
            case .success(let model):
                self?.onUpdate?(model)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
            self?.updateButton.update(state: .normal)
        }
    }
}
