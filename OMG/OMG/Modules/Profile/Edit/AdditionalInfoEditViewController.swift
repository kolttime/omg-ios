//
//  AdditionalInfoEditViewController.swift
//  OMG
//
//  Created by Roman Makeev on 31/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

enum AnswerType: Int, CaseIterable {
    
    case yes = 0
    case no = 1
    
    var localize: String {
        switch self {
        case .yes:
            return L10n.sharedYes
        default:
            return L10n.sharedNo
        }
    }
    
    var value: Bool {
        switch self {
        case .yes:
            return true
        default:
            return false
        }
    }
}

class AdditionalInfoEditViewController: OMGNavigationBarViewController,
                                        PopupPresentable,
                                        ToastAlertPresentable {
    
    private let apiService: ApiService
    private var additionalInfo: AdditionalInfo
    private lazy var scrollViewController = ScrollViewController()
    
    init(additionalInfo: AdditionalInfo, apiService: ApiService) {
        self.apiService = apiService
        self.additionalInfo = additionalInfo
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var toastPresenter = ToastAlertPresenter()
    private weak var popupViewController: PopupViewController?
    private lazy var keyboardObserver = KeyboardObserver()
    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    private var activeField: UIView?
    private lazy var updateButton = OMGButton.makeUpdate()
    private let textfieldTopOffset: CGFloat = 20
    private lazy var languageButtons = [PickedButton.makeLanguage()]
    private lazy var languageLevelButtons = [PickedButton.makeLanguageLevel()]
    
    private lazy var citizenshipButton = PickedButton.makeCitizenship()
    private lazy var validInternationalPassportButton = PickedButton.makeValidInternationalPassport()
    private lazy var motherAgencyButton = PickedButton.makeMotherAgency()
    
    private lazy var proficiencyLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.profileProficiencyInLanguages)
        return label
    }()
    
    lazy var languageButtonContainer = UIView()
    
    private lazy var addLanguageButton: ImageButton = {
        let button = ImageButton()
        let attributes = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value)
        var mutableString = NSMutableAttributedString()
        mutableString.append(attributes.make(string: "+"))
        mutableString.append(attributes.underlying(color: Color.grey44.value).make(string: L10n.profileAddLanguage))

        button.text = mutableString
        return button
    }()
    
    private lazy var removeLastLanguageButton: ImageButton = {
        let button = ImageButton()
        let attributes = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value)
        var mutableString = NSMutableAttributedString()
        mutableString.append(attributes.underlying(color: Color.grey44.value).make(string: L10n.profileRemoveLastLanguage))
        button.text = mutableString
        return button
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .equalSpacing
        stack.spacing = self.textfieldTopOffset
        return stack
    }()
    
    private var languages: [SelectableValue] = []
    private var selectedLanguages: [SelectableValue?] = [nil]
    private var languageButtonState: [PickedButtonType] = [.unpicked]
    private var languageLevels: [SelectableValue] = []
    private var selectedLanguageLevels: [SelectableValue?] = [nil]
    private var languageLevelButtonState: [PickedButtonType] = [.unpicked]
    
    private var selectedPassport: AnswerType?
    private var validInternationalPassportButtonState: PickedButtonType = .unpicked {
        didSet {
            self.validInternationalPassportButton.pickedState = self.validInternationalPassportButtonState
        }
    }
    
    private var selectedMotherAgency: AnswerType?
    private var motherAgencyButtonState: PickedButtonType = .unpicked {
        didSet {
            self.motherAgencyButton.pickedState = self.motherAgencyButtonState
        }
    }
    
    var onUpdate: ((Model) -> Void)?
    var onCountySelect: Action?
    
    func update(country: Country) {
        self.additionalInfo.citizenship = country
        self.citizenshipButton.pickedState = .picked(string: country.text)
        //self.checkValid()
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.profileAddInfo)
        self.view.backgroundColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.citizenshipButton)
        self.scrollViewController.scrollView.addSubview(self.validInternationalPassportButton)
        self.scrollViewController.scrollView.addSubview(self.motherAgencyButton)
        self.scrollViewController.scrollView.addSubview(self.proficiencyLabel)
        self.scrollViewController.scrollView.addSubview(self.stackView)
        self.stackView.addArrangedSubview(self.languageButtons.first!)
        self.stackView.addArrangedSubview(self.languageLevelButtons.first!)
        self.scrollViewController.scrollView.addSubview(self.languageButtonContainer)
        self.languageButtonContainer.addSubview(self.addLanguageButton)
        self.languageButtonContainer.addSubview(self.removeLastLanguageButton)
        self.scrollViewController.scrollView.addSubview(self.updateButton)

        self.view.addSubview(self.loadingLabel)
        
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupHandlers()
        self.loadingLabel.blink()
        self.scrollViewController.view.isHidden = true
        
        self.fetchData()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.citizenshipButton.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.validInternationalPassportButton.snp.makeConstraints {
            $0.top.equalTo(self.citizenshipButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.motherAgencyButton.snp.makeConstraints {
            $0.top.equalTo(self.validInternationalPassportButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.proficiencyLabel.snp.makeConstraints {
            $0.top.equalTo(self.motherAgencyButton.snp.bottom).offset(40)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.stackView.snp.makeConstraints {
            $0.top.equalTo(self.proficiencyLabel.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.bottom.equalTo(self.addLanguageButton.snp.top).offset(-self.textfieldTopOffset)
        }
        self.updateButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-25)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.languageButtonContainer.snp.makeConstraints {
            $0.bottom.equalTo(self.updateButton.snp.top).offset(-40)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.height.equalTo(25)
        }
        self.addLanguageButton.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
        }
        self.removeLastLanguageButton.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    func setupHandlers() {
        
        self.updateButton.onTouch = { [weak self] in
            self?.updateTap()
        }
        
        self.addLanguageButton.addTarget(self, action: #selector(self.addLanguageTouched), for: .touchUpInside)
        self.removeLastLanguageButton.addTarget(self, action: #selector(self.removeLanguageTouched), for: .touchUpInside)
        self.citizenshipButton.addTarget(self, action: #selector(countryTap), for: .touchUpInside)
        self.validInternationalPassportButton.addTarget(self, action: #selector(passportTap), for: .touchUpInside)
        self.motherAgencyButton.addTarget(self, action: #selector(motherAgencyTap), for: .touchUpInside)
        self.languageButtons.first?.addTarget(self, action: #selector(self.languageTap), for: .touchUpInside)
        self.languageLevelButtons.first?.addTarget(self, action: #selector(self.languageLevelTap), for: .touchUpInside)

        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.scrollViewController.scrollView.contentInset.bottom = frame.height
            guard let textField = strongSelf.activeField else {
                return
            }
            var viewRect = strongSelf.scrollViewController.scrollView.frame
            viewRect.origin.y -= frame.height - strongSelf.bottomLayoutEdgeInset
            var rect = textField.frame
            rect.origin.y += strongSelf.textfieldTopOffset
            guard !viewRect.contains(rect) else { return }
            let y = viewRect.maxY - textField.frame.maxY - strongSelf.bottomLayoutEdgeInset - strongSelf.textfieldTopOffset
            
            let point = CGPoint(x: 0, y: -y)
            self?.scrollViewController.scrollView.setContentOffset(point, animated: true)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}

// MARK: Interacting
extension AdditionalInfoEditViewController {
    
    @objc
    func addLanguageTouched() {
        self.addLanguageButtons()
    }
    
    private func addLanguageButtons() {
        let newLanguageButton = PickedButton.makeLanguage()
        let newLanguageLevelButton = PickedButton.makeLanguageLevel()
        
        self.languageButtons.append(newLanguageButton)
        self.languageLevelButtons.append(newLanguageLevelButton)
        self.stackView.addArrangedSubview(newLanguageButton)
        self.stackView.addArrangedSubview(newLanguageLevelButton)
        self.languageButtonState.append(.unpicked)
        self.languageLevelButtonState.append(.unpicked)
        self.selectedLanguages.append(nil)
        self.selectedLanguageLevels.append(nil)
        newLanguageButton.addTarget(self, action: #selector(self.languageTap), for: .touchUpInside)
        newLanguageLevelButton.addTarget(self, action: #selector(self.languageLevelTap), for: .touchUpInside)
        self.removeLastLanguageButton.isHidden = languageButtons.count < 2
    }
    
    @objc
    func removeLanguageTouched() {
        self.languageLevelButtons.last?.removeFromSuperview()
        self.languageButtons.last?.removeFromSuperview()
        self.languageButtons.removeLast()
        self.languageLevelButtons.removeLast()
        self.languageButtonState.removeLast()
        self.languageLevelButtonState.removeLast()
        self.selectedLanguages.removeLast()
        self.selectedLanguageLevels.removeLast()
        self.removeLastLanguageButton.isHidden = languageButtons.count < 2
    }
    
    @objc
    func updateTap() {
        self.additionalInfo.languages = zip(self.selectedLanguages, self.selectedLanguageLevels)
            .filter { language, level in language != nil && level != nil }
            .map { language, level in return UserLanguage.init(language: language!, level: level!) }
        
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.updateButton.update(state: .uploading)
        }
        self.apiService.updateAdditionalInfo(info: self.additionalInfo) { [weak self] result in
            switch result {
            case .success(let model):
                self?.onUpdate?(model)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
            self?.updateButton.update(state: .normal)
        }
    }
    
    @objc
    func countryTap() {
        self.onCountySelect?()
    }
    
    @objc
    func languageTap(sender: PickedButton) {
        guard let index = self.languageButtons.firstIndex(of: sender) else {
            return
        }
        self.selectLanguage(buttonIndex: index)
    }
    
    @objc
    func languageLevelTap(sender: PickedButton) {
        guard let index = self.languageLevelButtons.firstIndex(of: sender) else {
            return
        }
        self.selectLanguageLevel(buttonIndex: index)
    }
    
    @objc
    func passportTap() {
        self.dismissPopup()
        self.validInternationalPassportButtonState = .picking
        let viewModels = AnswerType.allCases.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.localize)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = AnswerType(rawValue: index)!
            self.selectedPassport = value
            self.additionalInfo.internationalPassport = value.value
            self.validInternationalPassportButtonState = .picked(string: value.localize)
        }
        
        let dismissHandler: Action = { [weak self] in
            if let value = self?.selectedPassport {
                self?.validInternationalPassportButtonState = .picked(string: value.localize)
            } else {
                self?.validInternationalPassportButtonState = .unpicked
            }
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    @objc
    func motherAgencyTap() {
        self.dismissPopup()
        self.motherAgencyButtonState = .picking
        let viewModels = AnswerType.allCases.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.localize)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = AnswerType(rawValue: index)!
            self.selectedMotherAgency = value
            self.additionalInfo.motherAgency = value.value
            self.motherAgencyButtonState = .picked(string: value.localize)
        }
        
        let dismissHandler: Action = { [weak self] in
            if let value = self?.selectedMotherAgency {
                self?.motherAgencyButtonState = .picked(string: value.localize)
            } else {
                self?.motherAgencyButtonState = .unpicked
            }
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    func selectLanguage(buttonIndex: Int) {
        self.dismissPopup()
        self.languageButtonState[buttonIndex] = .picking
        self.languageButtons[buttonIndex].pickedState = self.languageButtonState[buttonIndex]
        let viewModels = self.languages.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.text)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = self.languages[index]
            self.selectedLanguages[buttonIndex] = value
            self.languageButtonState[buttonIndex] = .picked(string: value.text)
            self.languageButtons[buttonIndex].pickedState = self.languageButtonState[buttonIndex] 
        }
        
        let dismissHandler: Action = { [weak self] in
            guard let self = self else { return }
            if let value = self.selectedLanguages[buttonIndex] {
                self.languageButtonState[buttonIndex] = .picked(string: value.text)
            } else {
                self.languageButtonState[buttonIndex] = .unpicked
            }
            self.languageButtons[buttonIndex].pickedState = self.languageButtonState[buttonIndex]
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    func selectLanguageLevel(buttonIndex: Int) {
        self.dismissPopup()
        self.languageLevelButtonState[buttonIndex] = .picking
        self.languageLevelButtons[buttonIndex].pickedState = self.languageLevelButtonState[buttonIndex]
        let viewModels = self.languageLevels.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.text)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = self.languageLevels[index]
            self.selectedLanguageLevels[buttonIndex] = value
            self.languageLevelButtonState[buttonIndex] = .picked(string: value.text)
            self.languageLevelButtons[buttonIndex].pickedState = self.languageLevelButtonState[buttonIndex]
        }
        
        let dismissHandler: Action = { [weak self] in
            guard let self = self else { return }
            if let value = self.selectedLanguageLevels[buttonIndex] {
                self.languageLevelButtonState[buttonIndex] = .picked(string: value.text)
            } else {
                self.languageLevelButtonState[buttonIndex] = .unpicked
            }
            self.languageLevelButtons[buttonIndex].pickedState = self.languageLevelButtonState[buttonIndex]
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    func fetchLanguages() {
        self.apiService.fetchLanguage { [weak self] result in
            switch result {
            case .success(let languages):
                self?.languages = languages
                self?.fetchLanguageLevels()
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func fetchLanguageLevels() {
        self.apiService.fetchLanguageLevel { [weak self] result in
            self?.loadingLabel.isHidden = true
            self?.scrollViewController.view.isHidden = false
            switch result {
            case .success(let languageLevels):
                self?.languageLevels = languageLevels
                self?.setInitialValues()
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func fetchData() {
        self.fetchLanguages()
    }
    
    private func dismissPopup() {
        
        if case .picking = self.validInternationalPassportButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
        if case .picking = self.motherAgencyButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
    }
    
    private func setInitialValues() {
        if let citizenShip = self.additionalInfo.citizenship {
            self.citizenshipButton.pickedState = .picked(string: citizenShip.text)
        }
        if let passport = self.additionalInfo.internationalPassport {
            if passport {
                self.selectedPassport = .yes
            } else {
                self.selectedPassport = .no
            }
            self.validInternationalPassportButtonState = .picked(string: self.selectedPassport!.localize)
        }
        if let agency = self.additionalInfo.motherAgency {
            if agency {
                self.selectedMotherAgency = .yes
            } else {
                self.selectedMotherAgency = .no
            }
            self.motherAgencyButtonState = .picked(string: self.selectedMotherAgency!.localize)
        }
        
        self.additionalInfo.languages.enumerated().forEach { offset, language in
            if offset > 0 {
                self.addLanguageButtons()
            }
            self.selectedLanguages[offset] = language.language
            self.selectedLanguageLevels[offset] = language.level
            self.languageButtonState[offset] = .picked(string: language.language.text)
            self.languageButtons[offset].pickedState = self.languageButtonState[offset]
            self.languageLevelButtonState[offset] = .picked(string: language.level.text)
            self.languageLevelButtons[offset].pickedState = self.languageLevelButtonState[offset]
        }
        self.removeLastLanguageButton.isHidden = languageButtons.count < 2
    }
}
