//
//  SocialNetworksViewController.swift
//  OMG
//
//  Created by Roman Makeev on 10/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SocialNetworksViewController: OMGNavigationBarViewController, ToastAlertPresentable {
    
    private let transition = OMGAlertTransitioningManager()
    
    var toastPresenter = ToastAlertPresenter()
    
    private let apiService: ApiService
    private var model: Model
    var onUpdate: ((Model) -> Void)?
    var onInstagram: ((String) -> Void)?
    
    init(model: Model, apiService: ApiService) {
        self.apiService = apiService
        self.model = model
        super.init()
    }
    
    func updateConnection() {
        self.apiService.fetchProfile { [weak self] result in
            switch result {
            case .success(let result):
                self?.update(model: result)
                self?.onUpdate?(result)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func update(model: Model) {
        self.model = model
        self.updateState()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var instagramButton: SectionButton = {
        let button = SectionButton(height: 50)
        button.text = L10n.profileInstagram
        button.addTarget(self, action: #selector(instagramTouch), for: .touchUpInside)
        return button
    }()
    
    private lazy var instagramDisconectButton: DetailButton = {
        let button = DetailButton(height: 50, detail: CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).underlying(color: Color.grey44.value).make(string: L10n.sharedDisconnect))
        button.text = L10n.profileInstagram
        button.addTarget(self, action: #selector(instagramDisconnectTouch), for: .touchUpInside)
        return button
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.profileSocialNetworksInfo)
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.instagramButton)
        self.view.addSubview(self.instagramDisconectButton)
        self.updateState()
    }
    
    @objc
    func logoutTouch() {
        self.apiService.logout()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.instagramButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight + 10)
        }
        self.instagramDisconectButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight + 10)
        }
    }
    
    private func updateState() {
        if self.model.socialNetworks.instagram == nil {
            self.instagramButton.isHidden = false
            self.instagramDisconectButton.isHidden = true
        } else {
            self.instagramButton.isHidden = true
            self.instagramDisconectButton.isHidden = false
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    @objc
    func instagramTouch() {
        self.fetchToken { token in
            self.onInstagram?(token)
        }
    }
    
    @objc
    func instagramDisconnectTouch() {
        let alert = OMGAlertViewController.makeInstagramDisconnecting(target: self, cancelSelector: #selector(self.cancelInstagram), detachSelector: #selector(self.detachInstagram))
        alert.transitioningDelegate = self.transition
        alert.modalPresentationStyle = .custom
        self.present(alert, animated: true, completion: nil)
    }
    
    private func fetchToken(completion: ((String) -> Void)?) {
        self.apiService.fetchInstagramToken { [weak self] result in
            switch result {
            case .success(let token):
                completion?(token.token)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    @objc
    func detachInstagram() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        self.model.socialNetworks.instagram = nil
        self.apiService.updateSocialNetworks(networks: self.model.socialNetworks) { [weak self] result in
            switch result {
            case .success(let model):
                self?.update(model: model)
                self?.onUpdate?(model)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    @objc
    func cancelInstagram() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
