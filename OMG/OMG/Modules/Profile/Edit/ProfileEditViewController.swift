//
//  ProfileEditViewController.swift
//  OMG
//
//  Created by Roman Makeev on 23/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ProfileEditViewController: OMGNavigationBarViewController {
    
    private let apiService: ApiService
    private var model: Model
    var onEditBody: ((Model) -> Void)?
    var onEditMain: ((Model) -> Void)?
    var onEditAdditional: ((Model) -> Void)?
    var onEditSocialNetworks: ((Model) -> Void)?

    init(model: Model, apiService: ApiService) {
        self.apiService = apiService
        self.model = model
        super.init()
    }
    
    func update(model: Model) {
        self.model = model
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var logoutButton: SectionButton = {
        let button = SectionButton(height: 50, detail: "↓")
        button.text = L10n.profileLogout
        button.addTarget(self, action: #selector(self.logoutTouch), for: .touchUpInside)
        return button
    }()
    
    private lazy var bodyInformationButton: SectionButton = {
        let button = SectionButton(height: 50)
        button.text = L10n.profileBodyInfo
        button.addTarget(self, action: #selector(editBodyTouch), for: .touchUpInside)
        return button
    }()
    
    private lazy var mainInformationButton: SectionButton = {
        let button = SectionButton(height: 50)
        button.text = L10n.profileMainInfo
        button.addTarget(self, action: #selector(editMainTouch), for: .touchUpInside)
        return button
    }()
    
    private lazy var additionalInformationButton: SectionButton = {
        let button = SectionButton(height: 50)
        button.text = L10n.profileAddInfo
        button.addTarget(self, action: #selector(editAdditionalTouch), for: .touchUpInside)
        return button
    }()
    
    private lazy var socialNetworkButton: SectionButton = {
        let button = SectionButton(height: 50)
        button.text = L10n.profileSocialNetworksInfo
        button.addTarget(self, action: #selector(editSocialNetworkTouch), for: .touchUpInside)
        return button
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.profileEdit)
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.logoutButton)
        self.view.addSubview(self.mainInformationButton)
        self.view.addSubview(self.bodyInformationButton)
        self.view.addSubview(self.additionalInformationButton)
        self.view.addSubview(self.socialNetworkButton)

    }
    
    @objc
    func logoutTouch() {
        self.apiService.logout()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.mainInformationButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight + 10)
        }
        self.bodyInformationButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.mainInformationButton.snp.bottom).offset(15)
        }
        self.additionalInformationButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.bodyInformationButton.snp.bottom).offset(15)
        }
        self.socialNetworkButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.additionalInformationButton.snp.bottom).offset(15)
        }
        self.logoutButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    @objc
    func editBodyTouch() {
        self.onEditBody?(self.model)
    }
    
    @objc
    func editMainTouch() {
        self.onEditMain?(self.model)
    }
    
    @objc
    func editAdditionalTouch() {
        self.onEditAdditional?(self.model)
    }
    
    @objc
    func editSocialNetworkTouch() {
        self.onEditSocialNetworks?(self.model)
    }
}
