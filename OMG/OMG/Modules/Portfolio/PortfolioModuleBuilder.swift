//
//  PortfolioModuleBuilder.swift
//  OMG
//
//  Created by Roman Makeev on 04/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

class PortfolioModuleBuilder {
    
    private let apiService: ApiService
    
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    func makePortfolioAlbumList() -> PortfolioAlbumListViewController {
        return PortfolioAlbumListViewController(apiService: self.apiService)
    }
    
    func makeAlbumViewController(albumShort: AlbumShort) -> AlbumViewController {
        return AlbumViewController(albumShort: albumShort,
                                   backTitle: L10n.sharedPortfolio,
                                   apiService: self.apiService)
    }
    
    func maleFullPhotoView(image: Image) -> FullPhotoViewController {
        return FullPhotoViewController(image: image)
    }
    
    func makePayment(albumShort: AlbumShort,
                     albumImages: [AlbumImage]) -> PaymentViewController {
        return PaymentViewController(albumShort: albumShort, albumImages: albumImages)
    }
    
    func makePolaroidInfo(albumShort: AlbumShort,
                          albumImages: [AlbumImage]) -> AlbumPolaroidInfoViewController {
        return AlbumPolaroidInfoViewController(albumShort: albumShort,
                                               backTitle: albumShort.title,
                                               images: albumImages)
    }
    
    func makeAlbumPolaroid(albumShort: AlbumShort) -> AlbumPolaroidViewController {
        return AlbumPolaroidViewController(albumShort: albumShort,
                                           backTitle: L10n.sharedPortfolio,
                                           apiService: self.apiService)
    }
    
    func makeUploadPhotoNew(albumShort: AlbumShort) -> AlbumUploadViewController {
        let interactor = AddPhotoInteractor(apiService: self.apiService)
        let vc = AlbumUploadViewController(albumShort: albumShort)
        vc.interactor = interactor
        interactor.output = vc
        return vc
    }
}
