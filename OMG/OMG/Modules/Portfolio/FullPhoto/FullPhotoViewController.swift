//
//  FullPhotoViewController.swift
//  OMG
//
//  Created by Roman Makeev on 14/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class FullPhotoViewController: OMGNavigationBarViewController, ToastAlertPresentable {
    
    var toastPresenter = ToastAlertPresenter()
    private lazy var scrollView = UIScrollView()
    private lazy var imageView = UIImageView()

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.isHidden = true
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    private let image: Image
    
    init(image: Image) {
        self.image = image
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph
            .colored(color: Color.grey44.value)
            .make(string: L10n.sharedBack)
        self.backgroundBarColor = UIColor.clear
        self.toastPresenter.targetView = self.view
        self.scrollView.addSubview(self.imageView)
        self.view.addSubview(self.scrollView)
        self.view.addSubview(self.activityIndicator)
        self.scrollView.delegate = self
        self.imageView.kf.setImage(with: self.image.original, options: [.transition(.fade(0.2))])
        self.view.backgroundColor = UIColor.black
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        self.imageView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageView.sizeToFit()
        self.scrollView.frame = self.view.bounds
        self.scrollView.contentSize = self.view.bounds.size
        self.adjustZoomScale()
        self.adjustCentered(self.scrollView)
    }
    
    func adjustZoomScale() {
        let imageViewSize = self.imageView.bounds.size
        let scrollViewSize = self.scrollView.bounds.size
        let widthScale = scrollViewSize.width / imageViewSize.width
        let heightScale = scrollViewSize.height / imageViewSize.height
        let minimumZoomScale = min(widthScale, heightScale)
        guard minimumZoomScale.isNormal else { return }
        self.scrollView.minimumZoomScale = minimumZoomScale
        self.scrollView.maximumZoomScale = minimumZoomScale * 10
        self.scrollView.setZoomScale(minimumZoomScale, animated: false)
    }
    
    func adjustCentered(_ scrollView: UIScrollView) {
        let imageViewSize = self.imageView.frame.size
        let scrollViewSize = scrollView.frame.size
        
        let verticalPadding = max((scrollViewSize.height - imageViewSize.height) / 2, 0)
        let horizontalPadding = max((scrollViewSize.width - imageViewSize.width) / 2, 0)
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}

extension FullPhotoViewController: UIScrollViewDelegate {
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        self.adjustCentered(scrollView)
    }
}
