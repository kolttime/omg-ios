//
//  AlbumPolaroidInfoViewController.swift
//  OMG
//
//  Created by Roman Makeev on 30/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class AlbumPolaroidInfoViewController: LargeTitleScrollViewController {
    
    private let backTitle: String
    private let images: [AlbumImage]
    private let albumShort: AlbumShort
    
    var onPayment: ((AlbumShort, [AlbumImage]) -> Void)?
    
    init(albumShort: AlbumShort,
         backTitle: String,
         images: [AlbumImage]) {
        self.albumShort = albumShort
        self.backTitle = backTitle
        self.images = images
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var checkoutButton = OMGButton.makeCheckout()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.portfolioPolaroidAttachInfo)
        return label
    }()
    
    private lazy var costLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        if let price = self.albumShort.price?.removeZerosFromEnd() {
            label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.portfolioPolaroidCostInfo(String(price) + " " + self.albumShort.currency.representableString.lowercased()))
        }
        
        return label
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.backTitle)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: "Title")
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: "Title")
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.infoLabel)
        self.view.addSubview(self.costLabel)
        self.view.addSubview(self.checkoutButton)
        
        self.checkoutButton.addTarget(self,
                                      action: #selector(checkoutTouch),
                                      for: .touchUpInside)
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.infoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(minNavigationAndLargeTitleHeight)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.costLabel.snp.makeConstraints {
            $0.top.equalTo(self.infoLabel.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.checkoutButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
    }
    
    @objc
    func checkoutTouch() {
        self.onPayment?(self.albumShort, self.images)
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}
