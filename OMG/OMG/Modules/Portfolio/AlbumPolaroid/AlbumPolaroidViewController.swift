//
//  AlbumPolaroidViewController.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Gallery

class AlbumPolaroidViewController: LargeTitleScrollViewController, ToastAlertPresentable {

    private let apiService: ApiService
    private let backTitle: String
    private let albumShort: AlbumShort
    private var album: Album?
    
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var attachInfoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.portfolioPolaroidAttachInfo)
        return label
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    private lazy var proceedButton = OMGButton.makeProceed()

    private lazy var scrollViewController = ScrollViewController()
    
    private lazy var frontFullLengthAttachView = AttachImageView()
    private lazy var behindFullHeightAttachView = AttachImageView()
    private lazy var leftSideFullLengthAttachView = AttachImageView()
    private lazy var rightSideFullLengthAttachView = AttachImageView()
    private lazy var facePortraitAttachView = AttachImageView()
    private lazy var faceProfileLeftAttachView = AttachImageView()
    private lazy var faceProfileRightAttachView = AttachImageView()

    private var frontFullLengthImage: Image?
    private var behindFullHeightImage: Image?
    private var leftSideFullLengthImage: Image?
    private var rightSideFullLengthImage: Image?
    private var facePortraitImage: Image?
    private var faceProfileLeftImage: Image?
    private var faceProfileRightImage: Image?

    var onFrontFullLength: Action?
    var onBehindFullHeight: Action?
    var onLeftSideFullLength: Action?
    var onRightSideFullLength: Action?
    var onFacePortrait: Action?
    var onFaceProfileLeft: Action?
    var onFaceProfileRight: Action?
    var onInfo: ((AlbumShort, [AlbumImage]) -> Void)?

    init(albumShort: AlbumShort,
         backTitle: String,
         apiService: ApiService) {
        self.albumShort = albumShort
        self.backTitle = backTitle
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateFrontFullLength(image: Gallery.Image) {
        self.frontFullLengthAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.frontFullLengthImage = uploadedImage
                self?.frontFullLengthAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.frontFullLengthAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    func updateBehindFullHeight(image: Gallery.Image) {
        self.behindFullHeightAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.behindFullHeightImage = uploadedImage
                self?.behindFullHeightAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.behindFullHeightAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    func updateLeftSideFullLength(image: Gallery.Image) {
        self.leftSideFullLengthAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.leftSideFullLengthImage = uploadedImage
                self?.leftSideFullLengthAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.leftSideFullLengthAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    func updateRightSideFullLength(image: Gallery.Image) {
        self.rightSideFullLengthAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.rightSideFullLengthImage = uploadedImage
                self?.rightSideFullLengthAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.rightSideFullLengthAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    func updateFacePortrait(image: Gallery.Image) {
        self.facePortraitAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.facePortraitImage = uploadedImage
                self?.facePortraitAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.facePortraitAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    func updateFaceProfileLeft(image: Gallery.Image) {
        self.faceProfileLeftAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.faceProfileLeftImage = uploadedImage
                self?.faceProfileLeftAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.faceProfileLeftAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    func updateFaceProfileRightImage(image: Gallery.Image) {
        self.faceProfileRightAttachView.state = .uploading
        self.apiService.uploadImage(byAsset: image.asset) { [weak self] result in
            switch result {
            case .success(let uploadedImage):
                self?.faceProfileRightImage = uploadedImage
                self?.faceProfileRightAttachView.state = .uploaded(url: uploadedImage.original)
            case .failure(let error):
                self?.faceProfileLeftAttachView.state = .empty
                self?.showToastErrorAlert(error)
            }
            self?.validateProceedButton()
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.backTitle)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.portfolioUploadNew)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.portfolioUploadNew)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.add(self.scrollViewController)
        
        self.scrollViewController.scrollView.addSubview(self.attachInfoLabel)
        self.scrollViewController.scrollView.addSubview(self.frontFullLengthAttachView)
        self.scrollViewController.scrollView.addSubview(self.behindFullHeightAttachView)
        self.scrollViewController.scrollView.addSubview(self.leftSideFullLengthAttachView)
        self.scrollViewController.scrollView.addSubview(self.rightSideFullLengthAttachView)
        self.scrollViewController.scrollView.addSubview(self.facePortraitAttachView)
        self.scrollViewController.scrollView.addSubview(self.faceProfileLeftAttachView)
        self.scrollViewController.scrollView.addSubview(self.faceProfileRightAttachView)
        self.scrollViewController.scrollView.addSubview(self.proceedButton)

        self.view.addSubview(self.loadingLabel)
        
        self.frontFullLengthAttachView.defaultText = L10n.portfolioPolaroidFrontFillLenght
        self.behindFullHeightAttachView.defaultText = L10n.portfolioPolaroidBehindFullHeight
        self.leftSideFullLengthAttachView.defaultText = L10n.portfolioPolaroidLeftSideFullLenght
        self.rightSideFullLengthAttachView.defaultText = L10n.portfolioPolaroidRightSideFullLenght
        self.facePortraitAttachView.defaultText = L10n.portfolioPolaroidFacePortrait
        self.faceProfileLeftAttachView.defaultText = L10n.portfolioPolaroidFaceProfileLeft
        self.faceProfileRightAttachView.defaultText = L10n.portfolioPolaroidFaceProfileRight
        
        self.frontFullLengthAttachView.onImageTap = { [weak self] in
            self?.onFrontFullLength?()
        }
        
        self.behindFullHeightAttachView.onImageTap = { [weak self] in
            self?.onBehindFullHeight?()
        }
        
        self.leftSideFullLengthAttachView.onImageTap = { [weak self] in
            self?.onLeftSideFullLength?()
        }
        
        self.rightSideFullLengthAttachView.onImageTap = { [weak self] in
            self?.onRightSideFullLength?()
        }
        
        self.facePortraitAttachView.onImageTap = { [weak self] in
            self?.onFacePortrait?()
        }
        
        self.faceProfileLeftAttachView.onImageTap = { [weak self] in
            self?.onFaceProfileLeft?()
        }
        
        self.faceProfileRightAttachView.onImageTap = { [weak self] in
            self?.onFaceProfileRight?()
        }
        
        self.proceedButton.update(state: .disabled)
        
        self.proceedButton.onTouch = { [weak self] in
            guard let self = self else { return }
            let images = self.prepareImages()
            self.onInfo?(self.albumShort, images)
        }
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupScroll(scrollInteraction: self.scrollViewController, refreshControl: nil)
        
        self.scrollViewController.scrollView.contentInset.bottom = 50
        
        self.scrollViewController.view.isHidden = true
        
        self.loadingLabel.blink()
        self.fetchData()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.attachInfoLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.frontFullLengthAttachView.snp.makeConstraints {
            $0.top.equalTo(self.attachInfoLabel.snp.bottom).offset(25)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.behindFullHeightAttachView.snp.makeConstraints {
            $0.top.equalTo(self.frontFullLengthAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.leftSideFullLengthAttachView.snp.makeConstraints {
            $0.top.equalTo(self.behindFullHeightAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.rightSideFullLengthAttachView.snp.makeConstraints {
            $0.top.equalTo(self.leftSideFullLengthAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.facePortraitAttachView.snp.makeConstraints {
            $0.top.equalTo(self.rightSideFullLengthAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.faceProfileLeftAttachView.snp.makeConstraints {
            $0.top.equalTo(self.facePortraitAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.faceProfileRightAttachView.snp.makeConstraints {
            $0.top.equalTo(self.faceProfileLeftAttachView.snp.bottom).offset(15)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.proceedButton.snp.makeConstraints {
            $0.top.equalTo(self.faceProfileRightAttachView.snp.bottom).offset(50)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
            $0.bottom.equalToSuperview()
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    func validateProceedButton() {
        var value = self.frontFullLengthImage != nil
        value = value && self.behindFullHeightImage != nil
        value = value && self.leftSideFullLengthImage != nil
        value = value && self.rightSideFullLengthImage != nil
        value = value && self.facePortraitImage != nil
        value = value && self.faceProfileLeftImage != nil
        value = value && self.faceProfileRightImage != nil

        if !value {
            self.proceedButton.update(state: .disabled)
        } else {
            self.proceedButton.update(state: .normal)
        }
    }
    
    private func fetchData() {
        self.apiService.fetchAlbum(id: self.albumShort.id) { [weak self] result in
            self?.loadingLabel.isHidden = true
            self?.scrollViewController.view.isHidden = false
            guard let self = self else { return }
            switch result {
            case.success(let album):
                self.album = album
                album.images.forEach { albumImage in
                    if let type = albumImage.type {
                        switch type {
                        case .facePortrait:
                            self.facePortraitImage = albumImage.image
                            self.facePortraitAttachView.state = .uploaded(url: albumImage.image.original)
                        case .behindFullHeight:
                            self.behindFullHeightImage = albumImage.image
                            self.behindFullHeightAttachView.state = .uploaded(url: albumImage.image.original)
                        case .faceProfileLeft:
                            self.faceProfileLeftImage = albumImage.image
                            self.faceProfileLeftAttachView.state = .uploaded(url: albumImage.image.original)
                        case .faceProfileRight:
                            self.faceProfileRightImage = albumImage.image
                            self.faceProfileRightAttachView.state = .uploaded(url: albumImage.image.original)
                        case .frontFullHeight:
                            self.frontFullLengthImage = albumImage.image
                            self.frontFullLengthAttachView.state = .uploaded(url: albumImage.image.original)
                        case .leftSideFullHeight:
                            self.leftSideFullLengthImage = albumImage.image
                            self.leftSideFullLengthAttachView.state = .uploaded(url: albumImage.image.original)
                        case .rightSideFullHeight:
                            self.rightSideFullLengthImage = albumImage.image
                            self.rightSideFullLengthAttachView.state = .uploaded(url: albumImage.image.original)
                        }
                    }
                    self.validateProceedButton()
                }
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
    }
    
    private func prepareImages() -> [AlbumImage] {
        var images = [AlbumImage]()
        if let frontFullLengthImage = self.frontFullLengthImage {
            images.append(AlbumImage(image: frontFullLengthImage, type: .frontFullHeight))
        }
        if let behindFullHeightImage = self.behindFullHeightImage {
            images.append(AlbumImage(image: behindFullHeightImage, type: .behindFullHeight))
        }
        if let leftSideFullLengthImage = self.leftSideFullLengthImage {
            images.append(AlbumImage(image: leftSideFullLengthImage, type: .leftSideFullHeight))
        }
        if let rightSideFullLengthImage = self.rightSideFullLengthImage {
            images.append(AlbumImage(image: rightSideFullLengthImage, type: .rightSideFullHeight))
        }
        if let facePortraitImage = self.facePortraitImage {
            images.append(AlbumImage(image: facePortraitImage, type: .facePortrait))
        }
        if let faceProfileLeftImage = self.faceProfileLeftImage {
            images.append(AlbumImage(image: faceProfileLeftImage, type: .faceProfileLeft))
        }
        if let faceProfileRightImage = self.faceProfileRightImage {
            images.append(AlbumImage(image: faceProfileRightImage, type: .faceProfileRight))
        }
        return images
    }
}
