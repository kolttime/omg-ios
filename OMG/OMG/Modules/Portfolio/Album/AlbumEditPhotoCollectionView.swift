//
//  AlbumEditEditPhotoCollectionView.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

private class CircleHoleView: AirdronView {
    
    public var strokeColor: UIColor = UIColor.white
    public var strokeWidth: CGFloat = 2
    
    public override func initialSetup() {
        super.initialSetup()
        self.contentMode = .redraw
        self.backgroundColor = UIColor.clear
    }
    
    public override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(ovalIn: rect)
        self.strokeColor.setFill()
        path.fill()
        
        let frame = rect.insetBy(dx: strokeWidth, dy: strokeWidth)
        let path1 = UIBezierPath(ovalIn: frame)
        path1.fill(with: .clear, alpha: 1)
    }
}


private class RectView: AirdronView {
    
    public var strokeColor: UIColor = Color.grey4.value
    public var strokeWidth: CGFloat = 5
    
    public override func initialSetup() {
        super.initialSetup()
        self.contentMode = .redraw
        self.backgroundColor = UIColor.clear
    }
    
    public override func draw(_ rect: CGRect) {
        let path = UIBezierPath.init(rect: rect)
        self.strokeColor.setFill()
        path.fill()
        
        let frame = rect.insetBy(dx: strokeWidth, dy: strokeWidth)
        let path1 = UIBezierPath.init(rect: frame)
        path1.fill(with: .clear, alpha: 1)
    }
}

class EditPhotoCollectionViewCell: CollectionViewCell {
    
    var cellSelectionHandler: Action?
    
    private let circleSize = CGSize(width: 18, height: 18)
    
    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "picture-overlay"))
        return view
    }()
    
    private lazy var originalImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var unselectedCircleView = CircleHoleView()

    private lazy var selectedCircleView = CircleView()

    private lazy var marginView = RectView()
    
    private weak var observer: Observable<Bool>?
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.previewImageView)
        self.contentView.addSubview(self.originalImageView)
        self.contentView.addSubview(self.overlayView)
        self.contentView.addSubview(self.unselectedCircleView)
        self.contentView.addSubview(self.selectedCircleView)
        self.contentView.addSubview(self.marginView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.overlayView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(60)
        }
        self.previewImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.originalImageView.snp.makeConstraints { $0.edges.equalTo(self.previewImageView) }
        self.marginView.snp.makeConstraints { $0.edges.equalTo(self.previewImageView) }

        self.selectedCircleView.snp.makeConstraints {
            $0.size.equalTo(self.circleSize)
            $0.left.equalToSuperview().offset(10)
            $0.top.equalToSuperview().offset(10)
        }
        
        self.unselectedCircleView.snp.makeConstraints {
            $0.edges.equalTo(self.selectedCircleView)
        }
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! EditPhotoCollectionCellViewModel
        let data = Data(base64Encoded: viewModel.image.base64Preview)
        if let data = data {
            self.previewImageView.image = UIImage(data: data)
        }
        self.originalImageView.kf.setImage(with: viewModel.image.original, options: [.transition(.fade(0.2))])
        self.cellSelectionHandler = viewModel.cellSelectionHandler
        self.selectedCircleView.isHidden = !viewModel.selectedObserver.value
        self.unselectedCircleView.isHidden = viewModel.selectedObserver.value
        self.marginView.isHidden = !viewModel.selectedObserver.value
        self.observer = viewModel.selectedObserver
        viewModel.selectedObserver.add(observer: self) { [weak self] _, newValue in
            self?.selectedCircleView.isHidden = !newValue
            self?.unselectedCircleView.isHidden = newValue
            self?.marginView.isHidden = !newValue
        }
    }
    
    override func didSelect() {
        self.cellSelectionHandler?()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.observer?.remove(observer: self)
    }
    
    deinit {
        self.observer?.remove(observer: self)
    }
}

extension EditPhotoCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return width
    }
}

struct EditPhotoCollectionCellViewModel: CollectionCellViewModel {
    
    var cellType: CollectionViewCell.Type { return EditPhotoCollectionViewCell.self }
    let image: Image
    let selectedObserver: Observable<Bool>
    let cellSelectionHandler: Action?
}
