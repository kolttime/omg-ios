//
//  AlbumPhotoCollectionView.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PhotoCollectionViewCell: CollectionViewCell {
    
    var cellSelectionHandler: Action?
    var cellLongSelectionHandler: Action?

    private lazy var originalImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.previewImageView)
        self.contentView.addSubview(self.originalImageView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectionHandler))
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(self.longTapSelectionHandler))
        self.contentView.addGestureRecognizer(tap)
        self.contentView.addGestureRecognizer(longTap)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.previewImageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.originalImageView.snp.makeConstraints { $0.edges.equalTo(self.previewImageView) }
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! PhotoCollectionCellViewModel
        let data = Data(base64Encoded: viewModel.image.base64Preview)
        if let data = data {
            self.previewImageView.image = UIImage(data: data)
        }
        self.originalImageView.kf.setImage(with: viewModel.image.original, options: [.transition(.fade(0.2))])
        self.cellSelectionHandler = viewModel.cellSelectionHandler
        self.cellLongSelectionHandler = viewModel.cellLongSelectionHandler

    }
    
    @objc
    func selectionHandler() {
        self.cellSelectionHandler?()
    }
    
    @objc
    func longTapSelectionHandler() {
        self.cellLongSelectionHandler?()
    }
}

extension PhotoCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return width
    }
}

struct PhotoCollectionCellViewModel: CollectionCellViewModel {
    
    var cellType: CollectionViewCell.Type { return PhotoCollectionViewCell.self }
    let image: Image
    let cellSelectionHandler: Action?
    let cellLongSelectionHandler: Action?
}
