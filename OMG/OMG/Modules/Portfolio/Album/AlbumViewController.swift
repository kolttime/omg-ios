//
//  AlbumViewController.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class AlbumViewController: LargeTitleScrollViewController, ToastAlertPresentable {

    var onFullPhoto: ((Image) -> Void)?
    var onUploadNew: ((AlbumShort) -> Void)?
    var onUpdateCount: ((String, Int) -> Void)?
    
    private let backTitle: String
    private let apiService: ApiService
    private var albumShort: AlbumShort
    private var album: Album?
    
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumColumnSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        return layout
    }()
    
    private var removedImages: [String] = [] {
        didSet {
            self.makeRemoveButton(removedImages: self.removedImages)
        }
    }
    
    private var selectedObservers: [String: Observable<Bool>] = [:]
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    private lazy var paidTextView: UITextView = {
        let textView = UITextView.makeStaticTextView()
        textView.textContainerInset = UIEdgeInsets(top: 6, left: 7, bottom: 4, right: 7)
        textView.clipsToBounds = true
        textView.backgroundColor = Color.grey4.value
        textView.isSelectable = false
        textView.attributedText = CustomFont.tech.attributesWithParagraph.make(string: L10n.portfolioPaid.uppercased())
        return textView
    }()
    
    init(albumShort: AlbumShort,
         backTitle: String,
         apiService: ApiService) {
        self.albumShort = albumShort
        self.backTitle = backTitle
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var editButton: ImageButton = {
        return ImageButton.makeEditButton()
    }()
    
    private lazy var cancelButton: ImageButton = {
        return ImageButton.makeCancelButton()
    }()
    
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var uploadNewButton = OMGButton.makeUploadNew()
    private var removeButton: OMGButton?
    
    override func setupConstraints() {
        super.setupConstraints()
        self.collectionViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.uploadNewButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        // TODO: КОСТЫЛЬ
        self.paidTextView.snp.makeConstraints {
            let largeTitleWidth = self.largeTitle?.width(height: CustomFont.headline.lineHeight) ?? 0
            $0.left.equalToSuperview().offset(largeTitleWidth + 5)
            $0.centerY.equalToSuperview()
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.paidTextView.isHidden = !self.albumShort.paid
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.backTitle)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: self.albumShort.title)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: self.albumShort.title)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.add(self.collectionViewController)
        self.view.addSubview(self.uploadNewButton)
        self.view.addSubview(self.loadingLabel)
        // TODO: КОСТЫЛЬ
        self.largeTitleView.titleLabel.addSubview(self.paidTextView)
        self.collectionViewController.collectionView.register(cellClass: PhotoCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: EditPhotoCollectionViewCell.self)
        self.collectionView.alwaysBounceVertical = true
        self.setupScroll(scrollInteraction: self.collectionViewController,
                         refreshControl: nil)
        self.collectionView.contentInset.bottom = self.bottomLayoutEdgeInset + 100
        self.setupHandlers()
        self.fetchData()
        self.loadingLabel.blink()
    }
    
    private func setupHandlers() {
        self.editButton.addTarget(self,
                                  action: #selector(editTouch),
                                  for: .touchUpInside)
        self.cancelButton.addTarget(self,
                                    action: #selector(cancelTouch),
                                    for: .touchUpInside)
        self.uploadNewButton.addTarget(self, action: #selector(self.uploadNewTouch), for: .touchUpInside)
        
    }
    
    private func fetchData() {
        self.apiService.fetchAlbum(id: self.albumShort.id) { [weak self] result in
            self?.loadingLabel.isHidden = true
            guard let self = self else { return }
            switch result {
            case.success(let album):
                self.album = album
                self.selectedObservers.removeAll()
                album.images.forEach {
                    self.selectedObservers[$0.image.id] = Observable<Bool>(value: false)
                }
                self.makeAndShowPhotos()
                self.rightActions = [self.editButton]
            case .failure(let error):
                self.showToastErrorAlert(error)
            }
        }
    }
    
    private func deleteData() {
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.removeButton?.update(state: .uploading)
        }
        self.apiService.deletePhotosFromAlbum(albumId: self.albumShort.id,
                                              photoIds: self.removedImages) { [weak self] result in
            switch result {
            case .success(let album):
                self?.album = album
                self?.albumShort = album.toShort()
                self?.removedImages = []
                self?.selectedObservers.removeAll()
                self?.onUpdateCount?(album.id, album.count)
                album.images.forEach {
                    self?.selectedObservers[$0.image.id] = Observable<Bool>(value: false)
                }
                self?.cancelTouch()
            case .failure(let error):
                self?.removeButton?.update(state: .normal)
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
        }
    }
    
    private func makeRemoveButton(removedImages: [String]) {
        self.removeButton?.removeFromSuperview()
        guard removedImages.count > 0 else { return }
        let button = OMGButton.makeRemove(count: removedImages.count)
        self.view.addSubview(button)
        button.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
        self.removeButton = button
        button.addTarget(self, action: #selector(self.deleteTouch), for: .touchUpInside)
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    @objc
    func deleteTouch() {
        self.deleteData()
    }
    
    @objc
    func editTouch() {
        self.uploadNewButton.isHidden = true
        self.makeAndShowEditingPhotos()
        self.rightActions = [self.cancelButton]
    }
    
    @objc
    func cancelTouch() {
        self.uploadNewButton.isHidden = false
        self.makeAndShowPhotos()
        self.rightActions = [self.editButton]
        self.removedImages = []
        self.selectedObservers.forEach { _, observer in observer.value = false }
    }
    
    @objc
    func uploadNewTouch() {
        self.onUploadNew?(self.albumShort)
    }
    
    private func makeAndShowPhotos() {
        let viewModels = self.album?.images.map { image -> CollectionCellViewModel in
            let selectionHandler: Action = { [weak self] in
                self?.onFullPhoto?(image.image)
            }
            
            let longSelectionHandler: Action = { [weak self] in
                guard let self = self else { return }
                guard let observer = self.selectedObservers[image.image.id] else { return }
                if !observer.value {
                    self.editTouch()
                    observer.value = true
                    self.removedImages.append(image.image.id)
                }
            }
            let viewModel = PhotoCollectionCellViewModel(image: image.image,
                                                         cellSelectionHandler: selectionHandler,
                                                         cellLongSelectionHandler: longSelectionHandler)
            return viewModel
        }
        let section = DefaultCollectionSectionViewModel(cellModels: viewModels ?? [])
        self.collectionViewController.update(viewModels: [section])
    }
    
    private func makeAndShowEditingPhotos() {
        let viewModels = self.album?.images.compactMap { image -> CollectionCellViewModel? in
            guard let observer = self.selectedObservers[image.image.id] else { return nil }
            let viewModel = EditPhotoCollectionCellViewModel(image: image.image,
                                                             selectedObserver: observer)
            { [weak observer, weak self] in
                guard let observer = observer else { return }
                observer.value = !observer.value
                if observer.value {
                    self?.removedImages.append(image.image.id)
                } else {
                    self?.removedImages.removeAll(where: { $0 == image.image.id })
                }
            }
            return viewModel
        }
        let section = DefaultCollectionSectionViewModel(cellModels: viewModels ?? [])
        self.collectionViewController.update(viewModels: [section])
    }
    
    func add(images: [AlbumImage]) {
        self.album?.images.append(contentsOf: images)
        images.forEach {
            self.selectedObservers[$0.image.id] = Observable<Bool>(value: false)
        }
        self.makeAndShowPhotos()
        self.showSuccessToast(L10n.pluralImageUploaded(images.count))
    }
}
