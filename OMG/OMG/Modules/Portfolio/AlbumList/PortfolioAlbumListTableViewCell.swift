//
//  PortfolioAlbumListTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 08/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PortfolioAlbumListTableViewCell: TableViewCell {
    
    private var viewModel: PortfolioAlbumListTableCellViewModel?
    
    private lazy var titleLabel = UILabel()
    private lazy var subtitleLabel = UILabel()
    private lazy var containerView = UIView()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.containerView)
        self.containerView.backgroundColor = Color.grey4.value
        self.containerView.addSubview(self.subtitleLabel)
        self.containerView.addSubview(self.titleLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.containerView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(10)
            $0.right.equalToSuperview().offset(-10)
            $0.bottom.equalToSuperview()
        }
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
        }
        self.subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalToSuperview().offset(-15)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! PortfolioAlbumListTableCellViewModel
        self.viewModel = viewModel
        self.titleLabel.attributedText = CustomFont.bodyBold.attributesWithParagraph.make(string: viewModel.title)
        self.subtitleLabel.attributedText = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey44.value).make(string: viewModel.subtitle)
    }
    
    override class func height(for viewModel: ViewModelType,
                tableView: UITableView) -> CGFloat {
        return 80
    }
    
    override func didSelect() {
        self.viewModel?.selectionHandler?()
    }
}

struct PortfolioAlbumListTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return PortfolioAlbumListTableViewCell.self }
    let title: String
    let subtitle: String
    let selectionHandler: Action?
}
