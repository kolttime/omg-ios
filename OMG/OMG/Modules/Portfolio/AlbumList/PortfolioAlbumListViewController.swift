//
//  PortfolioAlbumListViewController.swift
//  OMG
//
//  Created by Roman Makeev on 04/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PortfolioAlbumListViewController: LargeTitleRootScrollViewController, ToastAlertPresentable {
    
    var onAlbum: ((AlbumShort) -> Void)?
    private var albums: [AlbumShort] = []
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return refreshControl
    }()
    
    private let apiService: ApiService
    var toastPresenter = ToastAlertPresenter()
    lazy var tableViewController = TableViewController()
    
    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundBarColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.largeTitle = CustomFont.headline.attributesWithParagraph.make(string: L10n.sharedPortfolio)
        self.tableViewController.tableView.register(cellClass: PortfolioAlbumListTableViewCell.self)
        self.tableViewController.tableView.contentInset.bottom = ViewSize.sideOffset
        self.setupScroll(scrollInteraction: self.tableViewController, refreshControl: self.refreshControl)
        self.add(self.tableViewController)
        self.view.addSubview(self.loadingLabel)
        self.fetchData()
        self.loadingLabel.blink()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.tableViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-self.airdronTabbarController!.currentTabbarHeight)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    private func fetchData() {
        self.apiService.fetchPortfolio { [weak self] result in
            self?.loadingLabel.isHidden = true
            self?.refreshControl.endRefreshing()
            switch result {
            case .success(let albums):
                self?.albums = albums
                self?.render(albums: albums)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    private func render(albums: [AlbumShort]) {
        let viewModels = albums.map { album -> TableCellViewModel in
            let viewModel = PortfolioAlbumListTableCellViewModel(title: album.title,
                                                                 subtitle: L10n.pluralImage(album.count))
            { [weak self] in
                self?.onAlbum?(album)
            }
            return viewModel
        }
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }
    
    func update(count: Int, atAlbumId id: String) {
        guard let index = self.albums.firstIndex(where: { $0.id == id }) else { return }
        self.albums[index].count = count
        self.render(albums: self.albums)
    }
    
    func add(count: Int, atAlbumId id: String) {
        guard let index = self.albums.firstIndex(where: { $0.id == id }) else { return }
        self.albums[index].count += count
        self.render(albums: self.albums)
    }
    
    @objc
    func refresh() {
        self.fetchData()
    }
}
