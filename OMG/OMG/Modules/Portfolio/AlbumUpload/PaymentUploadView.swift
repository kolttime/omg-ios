//
//  PaymentUploadView.swift
//  OMG
//
//  Created by Roman Makeev on 27/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PaymentUploadView: AirdronView {
    
    static let contentHeight: CGFloat = 145
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.grey10.value
        return view
    }()
    
    private lazy var countLabel = UILabel()
    private lazy var totalLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.separatorView)
        self.addSubview(self.countLabel)
        self.addSubview(self.totalLabel)
    }

    func configure(count: NSAttributedString, total: NSAttributedString) {
        self.countLabel.attributedText = count
        self.totalLabel.attributedText = total
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.separatorView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.countLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(15)
        }
        self.totalLabel.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(15)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: PaymentUploadView.contentHeight)
    }
}
