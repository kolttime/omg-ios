//
//  AddPhotoInteractor.swift
//  OMG
//
//  Created by Roman Makeev on 16/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Gallery
import Photos

enum UploadType {
    
    case initial
    case beforeExportImage
    case afterExportImage
    case error
    case complete
}

typealias UploadedImage = (asset: PHAsset,
                           observer: Observable<UIImage?>,
                           uploadObserver: Observable<UploadType>)

protocol AddPhotoInteractorInput: class {
    
    func makeViewModels(images: [Gallery.Image]) -> [CollectionCellViewModel]
    func resetUploading()
    func uploadImages(albumId: String, isPaid: Bool)
}

protocol AddPhotoInteractorOutput: class {
    
    func didClear(asset: PHAsset, cell: UICollectionViewCell)
    func didSelect(image: UIImage)
    func didUpdateUpload(count: Int)
    func didReceive(error: AirdronError)
    func didUploadUnpaidImages(response: UploadImagesResponse, images: [AlbumImage])
    func didUploadPaidImages(images: [AlbumImage])
    func didExportPhotos()
}

// TODO: Make custom operation with cancelling request
class AddPhotoInteractor: AddPhotoInteractorInput {
    
    weak var output: AddPhotoInteractorOutput?
    
    private let apiService: ApiService
    private let threadSafeError = ThreadSafeAirdronError()
    
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    private var tempUrlDict = ThreadSafeDict<PHAsset, URL>()
    private var uploadedImages = [Image]() {
        didSet {
            self.output?.didUpdateUpload(count: self.uploadedImages.count)
        }
    }
    private var uploadedImagesDict = [PHAsset: String]()
    private var exportRequestsId = ThreadSafeDict<PHAsset, PHContentEditingInputRequestID>()
    private var uploadedRequestsDict = [PHAsset: AirdronDataRequestWrapper]()

    private let operationQueue = OperationQueue()
    private weak var completeExportOperation: Operation?
    
    func makeViewModels(images: [Gallery.Image]) -> [CollectionCellViewModel] {
        var preparedImages: [UploadedImage] = []
        self.completeExportOperation?.cancel()
        
       let viewModels = images.map { imageFile -> CollectionCellViewModel in
            let observer = Observable<UIImage?>(value: nil)
            let uploadObserver = Observable<UploadType>(value: .initial)
            
            let uploadedImage = UploadedImage(asset: imageFile.asset,
                                              observer: observer,
                                              uploadObserver: uploadObserver)
            preparedImages.append(uploadedImage)
            let exportOperation = AirdronAsyncOperation()
        
            uploadObserver.value = .initial
            exportOperation.block = { [weak self,
                weak exportOperation,
                weak observer,
                weak uploadObserver] in
                guard let self = self else {
                    exportOperation?.cancel()
                    return
                }
                
                DispatchQueue.main.async {
                    uploadObserver?.value = .beforeExportImage
                }

                let id = imageFile.asset.resolveEditingImageAndCopy { input, error, tempUrl, fullImage in
                    if let _ = error {
                        uploadObserver?.value = .error
                        exportOperation?.complete()
                        return
                    }
                    
                    guard let temp = tempUrl else {
                        uploadObserver?.value = .error
                        exportOperation?.complete()
                        return
                    }
                    
                    uploadObserver?.value = .afterExportImage
                    self.tempUrlDict[imageFile.asset] = temp
                    observer?.value = fullImage
                    self.exportRequestsId[imageFile.asset] = nil
                    exportOperation?.complete()
                }
                self.exportRequestsId[imageFile.asset] = id
            }
            
            let uploadOperation = AirdronAsyncOperation()
            
            uploadOperation.addDependency(exportOperation)
            
            uploadOperation.block = { [weak self, weak uploadOperation, weak uploadObserver] in
                guard let self = self, let tempUrl = self.tempUrlDict[imageFile.asset] else {
                    uploadOperation?.complete()
                    return
                }
                let requestCompletionHandler: ((Result<AirdronDataRequestWrapper>) -> Void) = { result in
                    switch result {
                    case .success(let wrapper):
                        self.uploadedRequestsDict[imageFile.asset] = wrapper
                    case .failure:
                        print("encoding error")
                    }
                }
                
                self.apiService.uploadImage(byFileUrl: tempUrl,
                                            requestCompletionHandler: requestCompletionHandler,
                                            progressHandler: nil) { result in
                    self.tempUrlDict[imageFile.asset] = nil
                    switch result {
                    case .success(let image):
                        uploadObserver?.value = .complete
                        self.uploadedImages.append(image)
                        self.uploadedImagesDict[imageFile.asset] = image.id
                    case .failure:
                        uploadObserver?.value = .error
                    }
                    uploadOperation?.complete()
                }
            }
            
            self.operationQueue.addOperation(exportOperation)
            self.operationQueue.addOperation(uploadOperation)
            
            let clearHandler: ((PHAsset, UICollectionViewCell) -> Void) = { [weak self, weak uploadOperation, weak exportOperation] asset, cell in
                self?.tempUrlDict[asset] = nil
                if let requestId = self?.exportRequestsId[asset] {
                    asset.cancelContentEditingInputRequest(requestId)
                }
                self?.exportRequestsId[asset] = nil
                if let id = self?.uploadedImagesDict[imageFile.asset] {
                    self?.uploadedImages.removeAll(where: { $0.id == id })
                    self?.uploadedImagesDict[imageFile.asset] = nil
                }
                self?.uploadedRequestsDict[asset]?.cancel()
                self?.uploadedRequestsDict[asset] = nil
                exportOperation?.complete()
                uploadOperation?.complete()

                self?.output?.didClear(asset: asset, cell: cell)
            }
            let selectionHandler: ((UIImage?) -> Void) = { [weak self] image in
                guard let image = image else { return }
                self?.output?.didSelect(image: image)
            }
            let viewModel =  AddPhotoCollectionCellViewModel(image: uploadedImage,
                                                             cellSelectionHandler: selectionHandler,
                                                             clearHandler: clearHandler)
            return viewModel
        }
        
        self.completeExportOperation = self.operationQueue.waitCurrentUploadsOperation { [weak self] in
            self?.output?.didExportPhotos()
        }
        
        return viewModels
    }
    
    func uploadImages(albumId: String, isPaid: Bool) {
        let images = self.uploadedImages.map { AlbumImage.init(image: $0, type: nil) }
        if let error = self.threadSafeError.getError() {
            self.output?.didReceive(error: error)
        } else if isPaid {
            self.output?.didUploadPaidImages(images: images)
        } else {
            self.uploadUnpaidImagesInAlbum(albumId: albumId, images: images)
        }
    }
    
    private func uploadUnpaidImagesInAlbum(albumId: String, images: [AlbumImage]) {
        self.apiService.uploadImagesInAlbum(albumId: albumId,
                                            albumImages: images,
                                            payment: nil) { [weak self] result in
                                                switch result {
                                                case .success(let response):
                                                    self?.output?.didUploadUnpaidImages(response: response,
                                                                                       images: images)
                                                case .failure(let error):
                                                    self?.output?.didReceive(error: error)
                                                }
        }
    }
    
    func resetUploading() {
        self.operationQueue.cancelAllOperations()
        self.uploadedRequestsDict.forEach { _, request in
            request.cancel()
        }
        self.uploadedRequestsDict.removeAll()
        self.exportRequestsId.forEach { asset, value in
            asset.cancelContentEditingInputRequest(value)
        }
        self.uploadedImagesDict.removeAll()
        self.uploadedImages.removeAll()
        self.exportRequestsId.reset()
        self.tempUrlDict.reset()
    }
}
