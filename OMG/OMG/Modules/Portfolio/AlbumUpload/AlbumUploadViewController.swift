//
//  AlbumUploadViewController.swift
//  OMG
//
//  Created by Roman Makeev on 13/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Gallery
import Photos

// TODO: Need to refactor
class AlbumUploadViewController: LargeTitleScrollViewController, ToastAlertPresentable {
    
    private let albumShort: AlbumShort
    var interactor: AddPhotoInteractorInput!
    
    private var isExporting = false
    
    var onFullPhoto: ((UIImage) -> Void)?
    var onAddImages: Action?
    var onUploadedImages: ((UploadImagesResponse, [AlbumImage]) -> Void)?
    var onPayment: ((AlbumShort, [AlbumImage]) -> Void)?

    
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumColumnSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        return layout
    }()
    
    private lazy var paymentUploadView = PaymentUploadView()
    
    init(albumShort: AlbumShort) {
        self.albumShort = albumShort
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var resetButton: ImageButton = {
        return ImageButton.makeResetButton()
    }()
    
    private var uploadButton: OMGButton?
    
    override func setupConstraints() {
        super.setupConstraints()
        self.collectionViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.paymentUploadView.snp.top)
        }
        
        self.paymentUploadView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.albumShort.title.formatedForBackTitle)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.portfolioUploadNew)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.portfolioUploadNew)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.add(self.collectionViewController)
        self.view.addSubview(self.paymentUploadView)
        
        self.paymentUploadView.isHidden = true
        self.collectionViewController.collectionView.register(cellClass: AddActionCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: AddPhotoCollectionViewCell.self)

        self.collectionView.alwaysBounceVertical = true
        self.setupScroll(scrollInteraction: self.collectionViewController,
                         refreshControl: nil)
        self.collectionView.contentInset.bottom = PaymentUploadView.contentHeight
        self.setupHandlers()
        self.rightActions = [self.resetButton]
        self.initialViewModels()
    }
    
    private func setupHandlers() {
        self.resetButton.addTarget(self,
                                  action: #selector(resetTouch),
                                  for: .touchUpInside)
        
    }
    
    private func makeUploadButton(imagesCount: Int, isExporting: Bool) {
        self.uploadButton?.removeFromSuperview()
        guard imagesCount > 0 else { return }
        let button = OMGButton.makeUpload(count: imagesCount)
        self.view.addSubview(button)
        button.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
        self.uploadButton = button
        if isExporting {
            self.uploadButton?.update(state: .disabled)
        }
        button.onTouch = { [weak self] in
            self?.uploadTouch()
        }
    }
    
    private func makeCheckoutButton(isExporting: Bool) {
        self.uploadButton?.removeFromSuperview()
        let button = OMGButton.makeCheckout()
        self.view.addSubview(button)
        button.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-25)
        }
        self.uploadButton = button
        if isExporting {
            self.uploadButton?.update(state: .disabled)
        }
        button.onTouch = { [weak self] in
            self?.paymentTouch()
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    @objc
    private func resetTouch() {
        self.initialViewModels()
        self.interactor.resetUploading()
    }
    
    @objc
    private func uploadTouch() {
        self.uploadButton?.update(state: .uploading)
        self.interactor.uploadImages(albumId: self.albumShort.id, isPaid: self.albumShort.paid)
    }
    
    @objc
    private func paymentTouch() {
        self.interactor.uploadImages(albumId: self.albumShort.id, isPaid: self.albumShort.paid)
    }
    
    func add(images: [Gallery.Image]) {
        self.isExporting = true
        self.uploadButton?.update(state: .disabled)
        let viewModels = self.interactor.makeViewModels(images: images)
        self.collectionViewController.collectionView.performBatchUpdates({
            viewModels.forEach { viewModel in
                self.collectionViewController.insert(viewModel,
                                                     for: self.collectionViewController.collectionView.numberOfItems(inSection: 0) - 1,
                                                     in: 0)
            }
        },
        completion: nil)
    }
    
    private func initialViewModels() {
        let addViewModel = AddActionCollectionCellViewModel { [weak self] in
            self?.onAddImages?()
        }
        let section = DefaultCollectionSectionViewModel(cellModels: [addViewModel])
        self.collectionViewController.update(viewModels: [section])
    }
}

extension AlbumUploadViewController: AddPhotoInteractorOutput {
    
    func didReceive(error: AirdronError) {
        self.uploadButton?.update(state: .normal)
        self.showToastErrorAlert(error)
    }
    
    func didClear(asset: PHAsset, cell: UICollectionViewCell) {
        guard let index = self.collectionView.indexPath(for: cell) else { return }
        self.collectionViewController.delete(index.item, in: index.section)
    }
    
    func didSelect(image: UIImage) {
        self.onFullPhoto?(image)
    }
    
    func didUpdateUpload(count: Int) {
        if !self.albumShort.paid {
            self.makeUploadButton(imagesCount: count, isExporting: self.isExporting)
        } else {
            self.makeCheckoutButton(isExporting: self.isExporting)
            if let price = self.albumShort.price, count > 0 {
                let priceString = self.makePriceString(price: price,
                                                       currency: self.albumShort.currency,
                                                       count: count)
                let totalString = self.makeTotalString(price: price,
                                                       currency: self.albumShort.currency,
                                                       count: count)
                self.paymentUploadView.isHidden = false
                self.paymentUploadView.configure(count: priceString, total: totalString)

            } else {
                self.paymentUploadView.isHidden = true

            }
        }
    }
    
    func didUploadUnpaidImages(response: UploadImagesResponse, images: [AlbumImage]) {
        self.onUploadedImages?(response, images)
    }
    
    func didUploadPaidImages(images: [AlbumImage]) {
        self.onPayment?(self.albumShort, images)
    }
    
    func didExportPhotos() {
        self.isExporting = false
        self.uploadButton?.update(state: .normal)
    }
}

private extension AlbumUploadViewController {
    
    private func makePriceString(price: Double, currency: Currency, count: Int) -> NSAttributedString {
        let price = L10n.portfolioPerImages(currency.representableString + "\(price.removeZerosFromEnd())")
        let xString = " × "
        let countString = String(count)
        
        let priceAttributedString = NSMutableAttributedString()
        priceAttributedString.append(CustomFont.bodyRegular.attributesWithParagraph.make(string: price))
        priceAttributedString.append(CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: xString))
        priceAttributedString.append(CustomFont.bodyRegular.attributesWithParagraph.make(string: countString))
        return priceAttributedString
    }
    
    private func makeTotalString(price: Double, currency: Currency, count: Int) -> NSAttributedString {
        let total = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.portfolioTotalPrice + " ")
        let sum = currency.representableString + "\((price * Double(count)).removeZerosFromEnd())"

        
        let totalAttributedString = NSMutableAttributedString()
        totalAttributedString.append(total)
        totalAttributedString.append(CustomFont.bodyRegular.attributesWithParagraph.make(string: sum))
        return totalAttributedString
    }
}
