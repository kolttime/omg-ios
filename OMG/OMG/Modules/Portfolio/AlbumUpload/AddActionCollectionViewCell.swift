//
//  AddActionCollectionViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class AddActionCollectionViewCell: CollectionViewCell {
    
    var cellSelectionHandler: Action?
    var cellLongSelectionHandler: Action?
    
    private lazy var plusImageView: UIImageView = {
        let imageView = UIImageView.init(image: #imageLiteral(resourceName: "plus"))
        imageView.contentMode = .center
        imageView.backgroundColor = Color.grey4.value
        return imageView
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.plusImageView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.plusImageView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(15)
        }
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! AddActionCollectionCellViewModel
        self.cellSelectionHandler = viewModel.cellSelectionHandler
    }
    
    override func didSelect() {
        self.cellSelectionHandler?()
    }
}

extension AddActionCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return width
    }
}

struct AddActionCollectionCellViewModel: CollectionCellViewModel {
    
    var cellType: CollectionViewCell.Type { return AddActionCollectionViewCell.self }
    let cellSelectionHandler: Action?
}
