//
//  PaymentViewController.swift
//  OMG
//
//  Created by Roman Makeev on 29/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import Appsee

class PaymentViewController: LargeTitleScrollViewController, ToastAlertPresentable {

    var toastPresenter = ToastAlertPresenter()
    
    private let albumShort: AlbumShort
    private let albumImages: [AlbumImage]
    
    init(albumShort: AlbumShort,
         albumImages: [AlbumImage]) {
        self.albumShort = albumShort
        self.albumImages = albumImages
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var cardholderTextField = OMGTextField.makeСardHolder()
    private lazy var validThruTextField = OMGTextField.makeValidThru()
    private lazy var codeTextField = OMGTextField.makeCardCode()
    private lazy var cardNumberTextField = OMGTextField.makeСardNumber()
    private lazy var payCreditCardButton = OMGButton.makePayCreditCard()
    
    private var cardNumberComplete: Bool = false {
        didSet {
            self.validate()
        }
    }
    private var validThruComplete: Bool = false {
        didSet {
            self.validate()
        }
    }
    private var codeComplete: Bool = false {
        didSet {
            self.validate()
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        Appsee.markView(asSensitive: self.view)
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.portfolioUploadNew)
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.portfolioPayment)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.portfolioPayment)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.cardNumberTextField)
        self.view.addSubview(self.validThruTextField)
        self.view.addSubview(self.codeTextField)
        self.view.addSubview(self.cardholderTextField)
        self.view.addSubview(self.payCreditCardButton)

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.endEditingHandler))
        tap.cancelsTouchesInView = false
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.endEditingHandler))
        swipe.cancelsTouchesInView = false
        swipe.direction = [.down, .up]
        self.view.addGestureRecognizer(swipe)
        self.view.addGestureRecognizer(tap)
        
        self.payCreditCardButton.update(state: .disabled)
        self.setupHandlers()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.cardNumberTextField.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(self.minNavigationAndLargeTitleHeight + 10)
        }
        
        self.validThruTextField.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalTo(self.view.snp.centerX).offset(-10)
            $0.top.equalTo(self.cardNumberTextField.snp.bottom).offset(20)
        }
        
        self.codeTextField.snp.makeConstraints {
            $0.left.equalTo(self.view.snp.centerX).offset(10)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.cardNumberTextField.snp.bottom).offset(20)
        }
        
        self.cardholderTextField.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.codeTextField.snp.bottom).offset(20)
        }
        
        self.payCreditCardButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-ViewSize.sideOffset)
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
    
    private func setupHandlers() {
        self.cardholderTextField.onTextDidChanged = { response in
            self.validate()
        }
        
        self.codeTextField.onTextDidChanged = { response in
            self.codeComplete = response.mandatoryComplete
        }
        
        self.cardNumberTextField.onTextDidChanged = { response in
            self.cardNumberComplete = response.mandatoryComplete
        }
        
        self.validThruTextField.onTextDidChanged = { response in
            self.validThruComplete = response.mandatoryComplete
        }
    }
    
    private func validate() {
        var validate = self.cardholderTextField.text?.isEmpty == false
        validate = validate && self.cardNumberComplete
        validate = validate && self.validThruComplete
        validate = validate && self.codeComplete
        
        if validate {
            self.payCreditCardButton.update(state: .normal)
        } else {
            self.payCreditCardButton.update(state: .disabled)
        }
    }
    
    @objc
    func endEditingHandler() {
        self.view.endEditing(true)
    }
    
    deinit {
        Appsee.unmarkView(asSensitive: self.view)
    }
}
