//
//  SupportViewController.swift
//  OMG
//
//  Created by Roman Makeev on 13/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class SupportViewController: OMGNavigationBarViewController, ToastAlertPresentable {
    
    private let transition = OMGAlertTransitioningManager()
    
    var toastPresenter = ToastAlertPresenter()
    private lazy var keyboardObserver = KeyboardObserver()
    private var bottomConstraint: Constraint?
    private let apiService: ApiService
    private var chatId: String?
    private let emptyStateView = SupportEmptyStateView()
    private var messages: [Message] = []
    private var isPossiblyLoading: Bool = true
    private var isLoading: Bool = false

    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    
    private var collectionView: UICollectionView { return self.collectionViewController.collectionView! }
    
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.minimumInteritemSpacing = 20
        layout.columnCount = 1
        return layout
    }()
    
    private lazy var supportImageButton: RightImageButton = {
        let imageButton = RightImageButton.makeSupportButton()
        return imageButton
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    private lazy var messageInputView: MessageInputView = {
        let view = MessageInputView()
        view.placeholder = L10n.sharedMessage
        view.onTouched = { [weak self, weak view] in
            guard let chatId = self?.chatId else { return }
            self?.sendMessage(chatId: chatId, message: view?.text ?? String.empty)
            view?.text = String.empty
            view?.resign()
        }
        return view
    }()
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.collectionView.register(cellClass: MessageIncomingCollectionViewCell.self)
        self.collectionView.register(cellClass: MessageOutgoingCollectionViewCell.self)
        self.collectionView.contentInset.bottom = 20
        self.collectionView.alwaysBounceVertical = true
        self.emptyStateView.isHidden = true
        self.toastPresenter.targetView = self.view
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.sharedSupport)
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedProfile)
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.loadingLabel)
        self.view.addSubview(self.emptyStateView)
        self.add(self.collectionViewController)
        self.view.addSubview(self.messageInputView)
        self.setupHandlers()
        self.loadingLabel.blink()
        self.fetchChat()
    }
    
    private func setupHandlers() {
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.bottomConstraint?.update(offset: 0)
            UIView.animate(withDuration: duration) {
                self?.view.layoutIfNeeded()
            }
        }
        
        self.keyboardObserver.onKeyboardWillShow = { [weak self] keyboardSize, duration in
            var offset: CGFloat = 0
            if #available(iOS 11.0, *) {
                offset = self?.view.safeAreaInsets.bottom ?? 0.0
            }
            self?.bottomConstraint?.update(offset: -keyboardSize.size.height + offset)
            UIView.animate(withDuration: duration, animations: {
                self?.view.layoutIfNeeded()
            }) { _ in
                guard let self = self else { return }
                if self.collectionView.contentOffset.y >= self.collectionView.contentSize.height - 2 * self.collectionView.bounds.height {
                    self.scrollToEnd()
                }
            }
        }
        
        self.collectionViewController.onScroll = { [weak self] scrollView in
            if let chatId = self?.chatId, scrollView.contentOffset.y < 100 {
                self?.fetchNextMessages(chatId: chatId) { count in
                    self?.collectionView.scrollToItem(at: IndexPath.init(item: count, section: 0), at: .top, animated: false)
                }
            }
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.messageInputView.snp.remakeConstraints {
            self.bottomConstraint = $0.bottom.equalTo(self.bottomLayoutEdge).constraint
            $0.left.equalTo(self.leftLayoutEdge)
            $0.right.equalTo(self.rightLayoutEdge)
        }
        self.collectionViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.messageInputView.snp.top)
        }
        self.emptyStateView.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.centerY.equalToSuperview()
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    private func fetchChat() {
        self.apiService.fetchChat { [weak self] result in
            switch result {
            case .success(let chat):
               self?.chatId = chat.id
               self?.fetchMessages(chatId: chat.id)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    private func fetchMessages(chatId: String) {
        self.apiService.fetchMessages(chatId: chatId,
                                      lastMessageId: nil) { [weak self] result in
                                        guard let self = self else { return }
                                        switch result {
                                        case .success(let messages):
                                            if !messages.isEmpty {
                                                self.rightActions = [self.supportImageButton]
                                            }
                                            self.emptyStateView.isHidden = !messages.isEmpty
                                            self.messages = messages.reversed()
                                            self.fetchNextMessages(chatId: chatId) { _ in
                                                self.loadingLabel.isHidden = true
                                                self.scrollToEnd()
                                            }
                                        case .failure(let error):
                                            self.loadingLabel.isHidden = true
                                            self.showToastErrorAlert(error)
                                        }
        }
    }
    
    private func fetchNextMessages(chatId: String, completion: ((Int) -> Void)?) {
        guard !self.isLoading, self.isPossiblyLoading else { return }
        self.isLoading = true
        self.apiService.fetchMessages(chatId: chatId,
                                      lastMessageId: self.messages.first?.id) { [weak self] result in
                                        self?.isLoading = false
                                        guard let self = self else { return }
                                        switch result {
                                        case .success(let messages):
                                            if messages.isEmpty {
                                                self.isPossiblyLoading = false
                                            }
                                            self.messages = messages.reversed() + self.messages
                                            self.render(messages: self.messages)
                                            completion?(messages.count)
                                        case .failure(let error):
                                            self.loadingLabel.isHidden = true
                                            self.showToastErrorAlert(error)
                                        }
        }
    }
    
    private func sendMessage(chatId: String, message: String) {
        self.apiService.sendMessage(chatId: chatId, message: message) { [weak self] result in
                                        guard let self = self else { return }
                                        switch result {
                                        case .success(let message):
                                            self.emptyStateView.isHidden = true
                                            self.renderNew(message: message)
                                            self.scrollToEnd()
                                        case .failure(let error):
                                            self.showToastErrorAlert(error)
                                        }
        }
    }
    
    private func render(messages: [Message]) {
        let viewModels = self.makeViewModels(messages: messages)
        let section = DefaultCollectionSectionViewModel(cellModels: viewModels)
        self.collectionViewController.update(viewModels: [section])
    }
    
    private func renderNew(message: Message) {
        guard self.collectionView.numberOfSections > 0 else { return }
        let viewModel = self.makeViewModel(message: message)
        self.collectionViewController.insert(viewModel, for: self.collectionView.numberOfItems(inSection: 0), in: 0)
    }
    
    
    override func needShowTabbar() -> Bool {
        return false
    }
}

// Helper
private extension SupportViewController {
    
    func scrollToEnd() {
        guard self.collectionView.numberOfSections > 0 else {
            return
        }
        guard self.collectionView.numberOfItems(inSection: 0) > 0 else {
            return
        }
        self.collectionView.scrollToItem(at: IndexPath.init(row: self.collectionView.numberOfItems(inSection: 0) - 1,
                                                            section: 0),
                                         at: .bottom,
                                         animated: false)
    }
    
    func scrollToTop() {
        guard self.collectionView.numberOfSections > 0 else {
            return
        }
        guard self.collectionView.numberOfItems(inSection: 0) > 0 else {
            return
        }
        self.collectionView.scrollToItem(at: IndexPath.init(row: 0, section: 0),
                                         at: .top,
                                         animated: false)
    }
    
    
    func makeViewModels(messages: [Message]) -> [CollectionCellViewModel] {
        return messages.map { message -> CollectionCellViewModel in
            var messageText = message.message.replacingOccurrences(of: " ", with: "")
            messageText = message.message.replacingOccurrences(of: "\n", with: " ")
            if message.my {
                let text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.white.value).make(string: messageText)
                return MessageOutgoingCollectionCellViewModel.init(text: text)
            } else {
                let text = CustomFont.bodyRegular.attributesWithParagraph.make(string: messageText)
                return MessageIncomingCollectionCellViewModel.init(text: text)
            }
        }
    }
    
    func makeViewModel(message: Message) -> CollectionCellViewModel {
        var messageText = message.message.replacingOccurrences(of: " ", with: "")
        messageText = message.message.replacingOccurrences(of: "\n", with: " ")
        if message.my {
            let text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.white.value).make(string: messageText)
            return MessageOutgoingCollectionCellViewModel.init(text: text)
        } else {
            let text = CustomFont.bodyRegular.attributesWithParagraph.make(string: messageText)
            return MessageIncomingCollectionCellViewModel.init(text: text)
        }
    }
    
}
