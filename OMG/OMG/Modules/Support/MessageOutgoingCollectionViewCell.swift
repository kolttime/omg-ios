//
//  MessageOutgoingCollectionViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 19/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class MessageOutgoingCollectionViewCell: CollectionViewCell {
    
    private lazy var containerView: UIView = UIView()
    static var maxWidth: CGFloat {
        if Device.isIphoneXSmax || Device.isIphone6p {
            return 320
        } else if Device.isIphone5 {
            return 250
        }
        return 300
    }
    
    private static let textInset = UIEdgeInsets(top: 10, left: 15, bottom: 15, right: 15)
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    public override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.containerView.backgroundColor = UIColor(hashRgbaValue: 0xA1A6B0FF)
        self.contentView.addSubview(self.containerView)
        self.contentView.addSubview(self.textLabel)
    }
    
    override public func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! MessageOutgoingCollectionCellViewModel
        self.textLabel.attributedText = viewModel.text
        self.setNeedsLayout()        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let rightOffset: CGFloat = 20 + MessageOutgoingCollectionViewCell.textInset.right
        
        let beginTextHeight = self.textLabel.attributedText?.height(width: CGFloat.greatestFiniteMagnitude) ?? 0

        let textWidth = min(MessageOutgoingCollectionViewCell.maxWidth - rightOffset,
                            self.textLabel.attributedText?.width(height: beginTextHeight) ?? 0)
        let textHeight = self.textLabel.attributedText?.height(width: textWidth) ?? 0
        self.textLabel.frame.size = CGSize(width: textWidth, height: textHeight)
        self.textLabel.frame.origin = CGPoint(x: self.contentView.bounds.width - textWidth - rightOffset, y: MessageOutgoingCollectionViewCell.textInset.top)
        
        var containerInset = MessageOutgoingCollectionViewCell.textInset
        containerInset.bottom = -containerInset.bottom
        containerInset.top = -containerInset.top
        containerInset.left = -containerInset.left
        containerInset.right = -containerInset.right
        
        self.containerView.frame = self.textLabel.frame.inset(by: containerInset)
    }
}

extension MessageOutgoingCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel,
                       width: CGFloat) -> CGFloat {
        let viewModel = model as! MessageOutgoingCollectionCellViewModel

        let rightOffset: CGFloat = 20 + MessageOutgoingCollectionViewCell.textInset.right
        let beginTextHeight = viewModel.text.height(width: CGFloat.greatestFiniteMagnitude)
        let textWidth = min(MessageOutgoingCollectionViewCell.maxWidth - rightOffset,
                            viewModel.text.width(height: beginTextHeight))
        var textHeight = viewModel.text.height(width: textWidth)
        textHeight += MessageOutgoingCollectionViewCell.textInset.top + MessageOutgoingCollectionViewCell.textInset.bottom
        return textHeight
    }
}

public struct MessageOutgoingCollectionCellViewModel: CollectionCellViewModel {
    
    public var cellType: CollectionViewCell.Type { return MessageOutgoingCollectionViewCell.self }
    public let text: NSAttributedString
}
