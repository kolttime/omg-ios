//
//  SupportEmptyStateView.swift
//  OMG
//
//  Created by Roman Makeev on 18/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SupportEmptyStateView: AirdronView {
    
    private static let image = #imageLiteral(resourceName: "SupportEmptyState")
    private static let space: CGFloat = 40
    private static let maxTextHeight: CGFloat = 50

    private lazy var imageView = UIImageView(image: SupportEmptyStateView.image)
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey77.value).make(string: L10n.chatEmpty)
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.imageView)
        self.addSubview(self.titleLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.imageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.centerX.equalToSuperview()
            $0.size.equalTo(SupportEmptyStateView.image.size)
        }
        self.titleLabel.snp.makeConstraints {
            $0.top.equalTo(self.imageView.snp.bottom).offset(SupportEmptyStateView.space)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        var height = SupportEmptyStateView.image.size.height
        height += SupportEmptyStateView.space
        height += SupportEmptyStateView.maxTextHeight
        return CGSize(width: UIView.noIntrinsicMetric, height: height)
    }
}
