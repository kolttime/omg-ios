//
//  SupportCollectionViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 18/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class MessageIncomingCollectionViewCell: CollectionViewCell {
    
    private lazy var containerView: UIView = UIView()
    static var maxWidth: CGFloat {
        if Device.isIphoneXSmax || Device.isIphone6p {
            return 305
        } else if Device.isIphone5 {
            return 235
        }
        return 285
    }
    
    private static let avatarImage = #imageLiteral(resourceName: "veronika")
    private static let textInset = UIEdgeInsets(top: 10, left: 15, bottom: 15, right: 15)

    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var avatarImageView = UIImageView(image: MessageIncomingCollectionViewCell.avatarImage)
    
    public override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.containerView.backgroundColor = Color.grey4.value
        self.contentView.addSubview(self.containerView)
        self.contentView.addSubview(self.textLabel)
        self.contentView.addSubview(self.avatarImageView)
    }
    
    override public func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! MessageIncomingCollectionCellViewModel
        self.textLabel.attributedText = viewModel.text
        self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let imageLeft: CGFloat = 15
        let textLeft = imageLeft + MessageIncomingCollectionViewCell.avatarImage.size.width + MessageIncomingCollectionViewCell.textInset.left + 5
        self.avatarImageView.frame.size = MessageIncomingCollectionViewCell.avatarImage.size
        self.avatarImageView.frame.origin = CGPoint(x: imageLeft, y: 0)
        
        let textWidth = min(MessageIncomingCollectionViewCell.maxWidth - textLeft,
                            self.textLabel.attributedText?.width(height: CGFloat.greatestFiniteMagnitude) ?? 0)
        let textHeight = self.textLabel.attributedText?.height(width: textWidth) ?? 0
        self.textLabel.frame.size = CGSize(width: textWidth, height: textHeight)
        self.textLabel.frame.origin = CGPoint(x: textLeft, y: MessageIncomingCollectionViewCell.textInset.top)
        
        var containerInset = MessageIncomingCollectionViewCell.textInset
        containerInset.bottom = -containerInset.bottom
        containerInset.top = -containerInset.top
        containerInset.left = -containerInset.left
        containerInset.right = -containerInset.right

        self.containerView.frame = self.textLabel.frame.inset(by: containerInset)
    }
}

extension MessageIncomingCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel,
                       width: CGFloat) -> CGFloat {
        let viewModel = model as! MessageIncomingCollectionCellViewModel
        let imageLeft: CGFloat = 15
        let textLeft = imageLeft + MessageIncomingCollectionViewCell.avatarImage.size.width + MessageIncomingCollectionViewCell.textInset.left + 5
        let textWidth = min(MessageIncomingCollectionViewCell.maxWidth - textLeft,
                            viewModel.text.width(height: CGFloat.greatestFiniteMagnitude))
        var textHeight = viewModel.text.height(width: textWidth)
        textHeight += MessageIncomingCollectionViewCell.textInset.top + MessageIncomingCollectionViewCell.textInset.bottom
        return max(textHeight, MessageIncomingCollectionViewCell.avatarImage.size.height)
    }
}

public struct MessageIncomingCollectionCellViewModel: CollectionCellViewModel {
    
    public var cellType: CollectionViewCell.Type { return MessageIncomingCollectionViewCell.self }
    public let text: NSAttributedString
}
