//
//  PopupViewController.swift
//  MDW
//
//  Created by Roman Makeev on 11/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

// TODO: Need to refactor
class PopupViewController: AirdronViewController {
    
    static let defaultColor: UIColor = Color.white.value
    
    private let selectedItem: Int?
    private var viewModels: [PopupTableCellViewModel]
    var onSelectedItem: ((Int) -> Void)?
    var onDismiss: Action?
    var onDismissAfterPan: Action?

    private var tableContentHeight: CGFloat
    private let tableEdges = UIEdgeInsets(top: 7, left: 0, bottom: 19, right: 0)
    private lazy var separatorView = UIView()
    private let currentBackgroundColor: UIColor
    private var currentAnimator: UIViewPropertyAnimator?
    private let tableContentTreshold: CGFloat = 400
    private lazy var tableUnderlayView = UIView()
    private let animateDuration: TimeInterval = 0.3
    init(viewModels: [PopupTableCellViewModel],
         selectedItem: Int?,
         backgroundColor: UIColor = PopupViewController.defaultColor) {
        self.viewModels = viewModels
        self.selectedItem = selectedItem
        let tableContentHeight = CGFloat(viewModels.count) * PopupTableViewCell.contentHeight + tableEdges.top + tableEdges.bottom
        self.tableContentHeight = min(tableContentHeight, tableContentTreshold)
        self.currentBackgroundColor = backgroundColor
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var tableViewContoller: ALTableViewController = {
        let tvc = ALTableViewController()
        return tvc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.tableViewContoller.tableView.backgroundColor = Color.white.value
        self.view.addSubview(self.tableUnderlayView)
        self.add(self.tableViewContoller)
        self.view.addSubview(self.separatorView)
        self.separatorView.backgroundColor = Color.grey10.value
        self.setupTable()
        self.setupConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissTap))
        self.view.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan))
        pan.cancelsTouchesInView = false
        pan.delegate = self
        self.tableViewContoller.tableView.addGestureRecognizer(pan)
        self.view.setNeedsUpdateConstraints()
        self.view.clipsToBounds = true
        self.updateTable()
    }
    
    
    
    func setupConstraints() {
        self.tableViewContoller.tableView.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.tableContentHeight)
        }
        self.tableUnderlayView.snp.makeConstraints { $0.edges.equalTo(self.tableViewContoller.tableView) }
        self.separatorView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.tableViewContoller.tableView.snp.top)
            $0.height.equalTo(1)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableContentHeight = min(self.tableContentHeight, self.view.bounds.height)
    }
    
    func setupTable() {
        self.tableViewContoller.tableView.contentInset.top = self.tableEdges.top
        self.tableViewContoller.tableView.bounces = false
        self.tableViewContoller.tableView.register(cellClass: PopupTableViewCell.self)
    }
    
    func updateTable() {
        self.viewModels.enumerated().forEach { offset, _ in
            self.viewModels[offset].cellSelectionHandler = { [weak self] cell in
                guard let index = self?.tableViewContoller.tableView.indexPath(for: cell)?.row else { return }
                self?.onSelectedItem?(index)
            }
        }
        let section = DefaultTableSectionViewModel(cellModels: self.viewModels)
        self.tableViewContoller.update(viewModels: [section])
    }
    
    @objc
    func dismissTap() {
        self.onDismiss?()
    }
    
    func startShowingAnimation() {
        self.tableViewContoller.tableView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.tableContentHeight)
        self.separatorView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.tableContentHeight)
        UIView.animate(withDuration: self.animateDuration, delay: 0, options: [.curveEaseOut], animations: {
            self.tableViewContoller.tableView.transform = CGAffineTransform.identity
            self.separatorView.transform = CGAffineTransform.identity
        })
    }
    
    func startHiddenAnimation(completion: Action?) {
        self.tableViewContoller.tableView.transform = CGAffineTransform.identity
        self.separatorView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: self.animateDuration, delay: 0, options: [.curveEaseOut], animations: {
            self.tableViewContoller.tableView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.tableContentHeight)
            self.separatorView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.tableContentHeight)
            self.view.backgroundColor = UIColor.clear
        }) { _ in
            completion?()
        }
    }
    
    @discardableResult
    private func makeAnimation(showing: Bool = true, completion: Action? = nil) -> UIViewPropertyAnimator {
        let translate = CGAffineTransform.identity.translatedBy(x: 0, y: self.tableViewContoller.view.bounds.height)
        let identity = CGAffineTransform.identity
        let begin = showing ? translate : identity
        let end = !showing ? translate : identity
        
        self.tableViewContoller.view.transform = begin
        self.separatorView.transform = begin

        let parameters = UICubicTimingParameters(animationCurve: .easeOut)
        let animator = UIViewPropertyAnimator(duration: self.animateDuration, timingParameters: parameters)
        animator.addAnimations {
            self.tableViewContoller.view.transform = end
            self.separatorView.transform = end
        }
        animator.startAnimation()
        animator.addCompletion { _ in
            completion?()
        }
        return animator
    }
    
    @objc
    func handlePan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            guard recognizer.location(in: self.tableUnderlayView).y < 50 else {
                self.currentAnimator = nil
                return
            }
            self.currentAnimator?.stopAnimation(true)
            self.tableViewContoller.tableView.isScrollEnabled = false
            self.currentAnimator = self.makeAnimation()
            self.currentAnimator?.pauseAnimation()
            self.currentAnimator?.fractionComplete = 1
        case .changed:
            self.currentAnimator?.fractionComplete = 1 - (recognizer.translation(in: self.tableViewContoller.view).y / self.tableViewContoller.view.bounds.height)
        case .ended:
            self.tableViewContoller.tableView.isScrollEnabled = true
            guard let fraction = self.currentAnimator?.fractionComplete else { return }
            if fraction > CGFloat(0.75) {
                self.currentAnimator?.continueAnimation(withTimingParameters: nil, durationFactor: 0)
            } else {
                self.currentAnimator?.isReversed = true
                self.currentAnimator?.continueAnimation(withTimingParameters: nil, durationFactor: 0)
                self.currentAnimator?.addCompletion { [weak self] _ in
                    self?.onDismissAfterPan?()
                }
            }
        default:
            break
        }
    }
}

extension PopupViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
