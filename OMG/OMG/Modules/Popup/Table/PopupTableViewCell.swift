//
//  PopupTableViewCell.swift
//  MDW
//
//  Created by Roman Makeev on 11/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PopupTableViewCell: TableViewCell {
    
    static let contentHeight: CGFloat = 40
    private lazy var titleLabel = UILabel()
    private lazy var containerView = UIView()

    private var cellSelectionHandler: ((UITableViewCell) -> Void)?

    override func initialSetup() {
        super.initialSetup()
        self.containerView.backgroundColor = Color.white.value
        self.contentView.addSubview(self.containerView)
        self.containerView.addSubview(self.titleLabel)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectTap))
        self.contentView.addGestureRecognizer(tap)
        self.setNeedsUpdateConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(PopupTableViewCell.contentHeight)
        }
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.centerY.equalToSuperview()
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! PopupTableCellViewModel
        self.titleLabel.attributedText = viewModel.title
        self.cellSelectionHandler = viewModel.cellSelectionHandler
    }
    
    @objc
    func selectTap() {
        self.containerView.backgroundColor = Color.grey10.value
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.cellSelectionHandler?(self)

        }
    }
}

struct PopupTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return PopupTableViewCell.self }
    var title: NSAttributedString
    var detail: NSAttributedString?
    var cellSelectionHandler: ((UITableViewCell) -> Void)?
    
    init(title: NSAttributedString, detail: NSAttributedString? = nil) {
        self.title = title
        self.detail = detail
    }
}
