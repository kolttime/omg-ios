//
//  PopupCollectionPresentable.swift
//  OMG
//
//  Created by Roman Makeev on 05/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol PopupCollectionPresentable: class {
    
    func showPopup(targetViewController: AirdronViewController,
                   viewModels: [CollectionCellViewModel],
                   selectedItem: Int?,
                   topConstraint: ConstraintRelatableTarget?,
                   topOffset: CGFloat,
                   backgroundColor: UIColor,
                   dismissItemHandler: Action?) -> PopupCollectionViewController
}

extension PopupCollectionPresentable where Self: AirdronViewController {
    
    func showPopup(targetViewController: AirdronViewController,
                   viewModels: [CollectionCellViewModel],
                   selectedItem: Int? = nil,
                   topConstraint: ConstraintRelatableTarget? = nil,
                   topOffset: CGFloat = 0,
                   backgroundColor: UIColor = PopupViewController.defaultColor,
                   dismissItemHandler: Action?) -> PopupCollectionViewController {
        let filterViewController = PopupCollectionViewController(viewModels: viewModels,
                                                                 selectedItem: selectedItem,
                                                                 backgroundColor: backgroundColor)
        // TODO: Add to tabbar root view, its correct ?
        targetViewController.addChild(filterViewController)
        targetViewController.view.addSubview(filterViewController.view)
        filterViewController.view.snp.makeConstraints {
            if let topConstraint = topConstraint {
                $0.top.equalTo(topConstraint).offset(topOffset)
            } else {
                $0.top.equalToSuperview().offset(topOffset)
            }
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        filterViewController.didMove(toParent: self)
        
        filterViewController.onDismiss = { [weak filterViewController] in
            filterViewController?.view.isUserInteractionEnabled = false
            dismissItemHandler?()
            filterViewController?.startHiddenAnimation {
                filterViewController?.view.snp.removeConstraints()
                filterViewController?.removeFromParentWithView()
            }
        }
        filterViewController.onDismissAfterPan = { [weak filterViewController] in
            dismissItemHandler?()
            filterViewController?.view.isUserInteractionEnabled = false
            filterViewController?.view.snp.removeConstraints()
            filterViewController?.removeFromParentWithView()
        }
        filterViewController.startShowingAnimation()
        return filterViewController
    }
}
