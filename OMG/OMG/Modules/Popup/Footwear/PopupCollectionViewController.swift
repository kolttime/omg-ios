//
//  PopupCollectionViewController.swift
//  OMG
//
//  Created by Roman Makeev on 04/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

// TODO: Need to refactor
class PopupCollectionViewController: AirdronViewController {
    
    static let defaultColor: UIColor = Color.white.value
    
    private let selectedItem: Int?
    private var viewModels: [CollectionCellViewModel]
    var onDismiss: Action?
    var onDismissAfterPan: Action?
    
    private let collectionEdges = UIEdgeInsets(top: 7, left: 0, bottom: 19, right: 0)
    private lazy var separatorView = UIView()
    private let currentBackgroundColor: UIColor
    private var currentAnimator: UIViewPropertyAnimator?
    private let collectionContentTreshold: CGFloat = 400
    private lazy var collectionUnderlayView = UIView()
    private let animateDuration: TimeInterval = 0.3
    
    init(viewModels: [CollectionCellViewModel],
         selectedItem: Int?,
         backgroundColor: UIColor = PopupViewController.defaultColor) {
        self.viewModels = viewModels
        self.selectedItem = selectedItem
        self.currentBackgroundColor = backgroundColor
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var collectionContentHeight: CGFloat = {
        let height = CGFloat(viewModels.count) * PopupTableViewCell.contentHeight + collectionEdges.top + collectionEdges.bottom + CGFloat(viewModels.count) * self.collectionViewLayout.minimumInteritemSpacing + self.collectionViewLayout.sectionInset.top + self.collectionViewLayout.sectionInset.bottom
        return min(height, self.collectionContentTreshold)
    }()
    
    private lazy var collectionViewLayout: AirdronCollectionViewWaterfallLayout = {
        let layout = AirdronCollectionViewWaterfallLayout()
        layout.columnCount = 3
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        layout.minimumInteritemSpacing = 15
        return layout
    }()
    
    private lazy var collectionViewController = CollectionWaterfallViewController(layout: self.collectionViewLayout)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.collectionViewController.collectionView.backgroundColor = Color.white.value
        self.view.addSubview(self.collectionUnderlayView)
        self.add(self.collectionViewController)
        self.view.addSubview(self.separatorView)
        self.separatorView.backgroundColor = Color.grey10.value
        self.setupCollection()
        self.setupConstraints()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissTap))
        self.view.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan))
        pan.cancelsTouchesInView = false
        pan.delegate = self
        self.collectionViewController.collectionView.addGestureRecognizer(pan)
        self.view.setNeedsUpdateConstraints()
        self.view.clipsToBounds = true
        self.updateTable()
    }
    
    func setupConstraints() {
        self.collectionViewController.collectionView.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(self.collectionContentHeight)
        }
        self.collectionUnderlayView.snp.makeConstraints { $0.edges.equalTo(self.collectionViewController.collectionView) }
        self.separatorView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.collectionViewController.collectionView.snp.top)
            $0.height.equalTo(1)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.collectionContentHeight = min(self.collectionContentHeight, self.view.bounds.height)
    }
    
    func setupCollection() {
        self.collectionViewController.collectionView.contentInset.top = self.collectionEdges.top
        self.collectionViewController.collectionView.bounces = false
        self.collectionViewController.collectionView.register(cellClass: PopupCollectionViewCell.self)
        self.collectionViewController.collectionView.register(cellClass: FootwearCollectionViewCell.self)

    }
    
    func updateTable() {
        let section = DefaultCollectionSectionViewModel(cellModels: self.viewModels)
        self.collectionViewController.update(viewModels: [section])
    }
    
    @objc
    func dismissTap() {
        self.onDismiss?()
    }
    
    func startShowingAnimation() {
        self.collectionViewController.collectionView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.collectionContentHeight)
        self.separatorView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.collectionContentHeight)
        UIView.animate(withDuration: self.animateDuration, delay: 0, options: [.curveEaseOut], animations: {
            self.collectionViewController.collectionView.transform = CGAffineTransform.identity
            self.separatorView.transform = CGAffineTransform.identity
        })
    }
    
    func startHiddenAnimation(completion: Action?) {
        self.collectionViewController.collectionView.transform = CGAffineTransform.identity
        self.separatorView.transform = CGAffineTransform.identity
        UIView.animate(withDuration: self.animateDuration, delay: 0, options: [.curveEaseOut], animations: {
            self.collectionViewController.collectionView.transform = CGAffineTransform.identity.translatedBy(x: 0,
                                                                                                             y: self.collectionContentHeight)
            self.separatorView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.collectionContentHeight)
            self.view.backgroundColor = UIColor.clear
        }) { _ in
            completion?()
        }
    }
    
    @discardableResult
    private func makeAnimation(showing: Bool = true, completion: Action? = nil) -> UIViewPropertyAnimator {
        let translate = CGAffineTransform.identity.translatedBy(x: 0,
                                                                y: self.collectionViewController.view.bounds.height)
        let identity = CGAffineTransform.identity
        let begin = showing ? translate : identity
        let end = !showing ? translate : identity
        
        self.collectionViewController.view.transform = begin
        self.separatorView.transform = begin
        
        let parameters = UICubicTimingParameters(animationCurve: .easeOut)
        let animator = UIViewPropertyAnimator(duration: self.animateDuration, timingParameters: parameters)
        animator.addAnimations {
            self.collectionViewController.view.transform = end
            self.separatorView.transform = end
        }
        animator.startAnimation()
        animator.addCompletion { _ in
            completion?()
        }
        return animator
    }
    
    @objc
    func handlePan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            guard recognizer.location(in: self.collectionUnderlayView).y < 50 else {
                self.currentAnimator = nil
                return
            }
            self.currentAnimator?.stopAnimation(true)
            self.collectionViewController.collectionView.isScrollEnabled = false
            self.currentAnimator = self.makeAnimation()
            self.currentAnimator?.pauseAnimation()
            self.currentAnimator?.fractionComplete = 1
        case .changed:
            self.currentAnimator?.fractionComplete = 1 - (recognizer.translation(in: self.collectionViewController.view).y / self.collectionViewController.view.bounds.height)
        case .ended:
            self.collectionViewController.collectionView.isScrollEnabled = true
            guard let fraction = self.currentAnimator?.fractionComplete else { return }
            if fraction > CGFloat(0.75) {
                self.currentAnimator?.continueAnimation(withTimingParameters: nil, durationFactor: 0)
            } else {
                self.currentAnimator?.isReversed = true
                self.currentAnimator?.continueAnimation(withTimingParameters: nil, durationFactor: 0)
                self.currentAnimator?.addCompletion { [weak self] _ in
                    self?.onDismissAfterPan?()
                }
            }
        default:
            break
        }
    }
    
    func select() {
        self.view.isUserInteractionEnabled = false
        self.startHiddenAnimation {
            self.view.snp.removeConstraints()
            self.removeFromParentWithView()
        }
    }
}

extension PopupCollectionViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
