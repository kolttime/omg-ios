//
//  PopupCollectionViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 04/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PopupCollectionViewCell: CollectionViewCell {
    
    static let contentHeight: CGFloat = 25
    private lazy var titleLabel = UILabel()
    private lazy var containerView = UIView()
    
    private var cellSelectionHandler: ((UICollectionViewCell) -> Void)?
    
    override func initialSetup() {
        super.initialSetup()
        self.containerView.backgroundColor = Color.white.value
        self.contentView.addSubview(self.containerView)
        self.containerView.addSubview(self.titleLabel)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectTap))
        self.contentView.addGestureRecognizer(tap)
        self.setNeedsUpdateConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
            $0.height.equalTo(PopupCollectionViewCell.contentHeight)
        }
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
    }
    
    override func configure(viewModel: CollectionViewCell.ViewModelType) {
        let viewModel = viewModel as! PopupCollectionCellViewModel
        self.titleLabel.attributedText = viewModel.title
        self.cellSelectionHandler = viewModel.cellSelectionHandler
    }
    
    @objc
    func selectTap() {
        self.containerView.backgroundColor = Color.grey10.value
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.cellSelectionHandler?(self)
            
        }
    }
}

extension PopupCollectionViewCell: WaterfallCollectionViewCell {
    
    static func height(forViewModel model: CollectionCellViewModel, width: CGFloat) -> CGFloat {
        return PopupCollectionViewCell.contentHeight
    }
}

struct PopupCollectionCellViewModel: CollectionCellViewModel {
    
    var cellType: CollectionViewCell.Type { return PopupCollectionViewCell.self }
    var title: NSAttributedString
    var cellSelectionHandler: ((UICollectionViewCell) -> Void)?
    
    init(title: NSAttributedString) {
        self.title = title
    }
}
