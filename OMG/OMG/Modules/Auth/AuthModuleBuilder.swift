//
//  AuthModuleBuilder.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class AuthModuleBuilder {
    
    private let apiService: ApiService
    private let sessionManager: OMGUserSessionManager

    init(apiService: ApiService,
         sessionManager: OMGUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
    }
    
    func makeLaunchModule() -> LaunchViewController {
        return LaunchViewController()
    }
    
    func makePhoneNumberModule() -> PhoneNumberViewController {
        return PhoneNumberViewController(apiService: self.apiService)
    }
    
    func makeVerificationModule(phone: String, authResponse: AuthResponse) -> VerificationViewController {
        return VerificationViewController(phone: phone,
                                          authResponse: authResponse,
                                          apiService: apiService,
                                          sessionManager: self.sessionManager)
    }
    
    func makePrivacyModule(privacy: Privacy) -> PrivacyViewController {
        return PrivacyViewController(privacy: privacy,
                                     backTitle: L10n.sharedWelcome,
                                     apiService: self.apiService)
    }
    
    func makeInfoModule() -> InfoViewController {
        return InfoViewController()
    }
    
    func makeSearchCountryModule(showCallingCode: Bool) -> SearchCountryViewController {
        return SearchCountryViewController(showCallingCode: showCallingCode, apiService: self.apiService)
    }
    
    func makeFillInfoModule(controllers: [PageViewController]) -> FillInfoContainerViewController {
        return FillInfoContainerViewController(controllers: controllers)
    }
    
    func makeIntroduceModule() -> IntroduceProfileViewController {
        return IntroduceProfileViewController(apiService: self.apiService)
    }
    
    func makeContactInfoModule() -> ContactProfileViewController {
        return ContactProfileViewController(apiService: self.apiService, sessionManager: self.sessionManager)
    }
    
    func makeBodyModule() -> BodyProfileViewController {
        return BodyProfileViewController(apiService: self.apiService)
    }
    
    func makeDoneModule() -> DoneProfileViewController {
        return DoneProfileViewController(apiService: self.apiService)
    }
    
    func makeAdditionalInformationEdit(model: Model) -> AdditionalInfoEditViewController {
        return AdditionalInfoEditViewController(additionalInfo: model.additionalInfo, apiService: self.apiService)
    }
    
    func makeInstagramModule() -> InstagramLoginController {
        return InstagramLoginController(apiService: self.apiService, sessionManager: self.sessionManager)
    }
    
    func makeSocialNetworks(model: Model) -> SocialNetworksViewController {
        return SocialNetworksViewController(model: model, apiService: self.apiService)
    }
    
    func makeInstagramBindModule(accessToken: String) -> InstagramBindController {
        return InstagramBindController(accessToken: accessToken, apiService: self.apiService)
    }
}
