//
//  SearchCountryViewController.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SearchCountryViewController: SearchableViewController, ToastAlertPresentable {
    
    private let apiService: ApiService
    var toastPresenter = ToastAlertPresenter()
    private let limiter = DebouncedLimiter()
    private let showCallingCode: Bool
    var onSelectCounty: ((Country) -> Void)?
    
    private var originViewModels: [SearchableCountryTableCellViewModel] = []
    private var currentViewModels: [SearchableCountryTableCellViewModel] = []

    init(showCallingCode: Bool = true, apiService: ApiService) {
        self.showCallingCode = showCallingCode
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.tableViewController.tableView.keyboardDismissMode = .onDrag
        self.toastPresenter.targetView = self.view
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedBack)
        self.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: L10n.loginSearchCountry)
        self.tableViewController.tableView.register(cellClass: SearchableCountryTableViewCell.self)
        self.fetchData()
        self.searchTextField.onTextDidChanged = { [weak self] response in
            self?.searchItems(by: response.text)
        }
        self.loadingLabel.blink()
    }
    
    private func fetchData() {
        self.apiService.fetchCountry { [weak self] result in
            self?.loadingLabel.isHidden = true
            switch result {
            case.success(let countries):
                guard let self = self else { return }
                let selectionHandler: ((Country) -> Void) = { country in
                    self.onSelectCounty?(country)
                }
                self.originViewModels = countries.map { return SearchableCountryTableCellViewModel(country: $0,
                                                                                                   showCallingCode: self.showCallingCode,
                                                                                                   selectionHandler: selectionHandler) }
                self.currentViewModels = self.originViewModels
                self.render(viewModels: self.currentViewModels)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    private func render(viewModels: [SearchableCountryTableCellViewModel]) {
        let section = DefaultTableSectionViewModel(cellModels: viewModels)
        self.tableViewController.update(viewModels: [section])
    }
    
    private func searchItems(by text: String) {
        if text.isEmpty {
            self.currentViewModels = self.originViewModels
            self.render(viewModels: self.currentViewModels)

        }
        self.limiter.execute { [weak self] in
            guard let self = self else { return }
            DispatchQueue.global(qos: .userInitiated).async {
                let foundedModels = self.originViewModels.filter {
                    var result = $0.country.text.lowercased().hasMatches(forSearchText: text.lowercased())
                    result = result || $0.country.callingCode.lowercased().hasMatches(forSearchText: text.lowercased())
                    return result
                }
                DispatchQueue.main.async {
                    self.currentViewModels = foundedModels
                    self.render(viewModels: foundedModels)
                }
            }
        }
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}
