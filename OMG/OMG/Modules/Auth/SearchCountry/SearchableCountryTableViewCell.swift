//
//  SearchableCountryTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SearchableCountryTableViewCell: TableViewCell {
    
    var viewModel: SearchableCountryTableCellViewModel?
    
    private lazy var codeLabel: UILabel = {
        let label = UILabel()
        label.setHugging(priority: .required)
        label.setResistance(priority: .required)
        return label
    }()
    
    private lazy var countryLabel = UILabel()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.codeLabel)
        self.contentView.addSubview(self.countryLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.codeLabel.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-5)
        }
        
        self.countryLabel.snp.makeConstraints {
            $0.right.equalTo(self.codeLabel.snp.left).offset(-ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.bottom.equalToSuperview().offset(-5)
        }
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! SearchableCountryTableCellViewModel
        self.viewModel = viewModel
        self.countryLabel.attributedText = CustomFont.large.attributesWithParagraph.make(string: viewModel.country.text)
        self.codeLabel.attributedText = CustomFont.large.attributesWithParagraph.make(string: "+" + viewModel.country.callingCode)
        self.codeLabel.textAlignment = .right
        self.codeLabel.isHidden = !viewModel.showCallingCode
    }
    
    override class func height(for viewModel: ViewModelType,
                           tableView: UITableView) -> CGFloat {
        return 45
    }
    
    override func didSelect() {
        guard let country = self.viewModel?.country else { return }
        self.viewModel?.selectionHandler?(country)
    }
}

struct SearchableCountryTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return SearchableCountryTableViewCell.self }
    let country: Country
    let showCallingCode: Bool
    let selectionHandler: ((Country) -> Void)?
}
