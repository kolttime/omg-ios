//
//  PhoneNumberViewController.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import CoreTelephony
import libPhoneNumber_iOS

class PhoneNumberViewController: LargeTitleRootViewController, ToastAlertPresentable {
    
    var onCancel: Action?
    
    var onVerification: ((String, AuthResponse) -> Void)?
    var onCallingCode: Action?
    
    private var bottomConstraint: Constraint?
    private var bottomOffset: CGFloat = 20

    private var textFieldTopOffset: CGFloat {
        if Device.isIphone5 {
            return 125
        } else {
            return 145
        }
    }
    
    private var textFieldLeftConstraint: Constraint?
    private var textFieldWidthConstraint: Constraint?
    private let textFieldOffsetLeft: CGFloat = 16
    
    private lazy var cancelButton = UIButton.makeLargeTitleButtonAction(title: L10n.sharedCancel)
    
    private lazy var getCodeButton = OMGButton.makeGetCode()
    
    private lazy var phoneNumberTextField = OMGTextField.makeInputNumberTextField()
    
    private lazy var callingCodeButton: ImageButton = {
        let button = ImageButton()
        button.space = 0
        button.image = #imageLiteral(resourceName: "DropDown.pdf")
        button.imageTintColor = Color.grey30.value
        button.setHugging(priority: .required)
        button.setResistance(priority: .required)
        return button
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.loginInfoLabel)
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.grey10.value
        return view
    }()
    
    var toastPresenter = ToastAlertPresenter()
    
    private let keyboardObserver = KeyboardObserver()
    private let phoneUtil = NBPhoneNumberUtil()

    private let apiService: ApiService
    private var selectingCallingCode: String?
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.backgroundTitleColor = Color.white.value
        self.view.backgroundColor = UIColor.white
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.sharedLogin)
        self.largeTitleActionView = self.cancelButton
        self.getCodeButton.update(state: .disabled)
        self.view.addSubview(self.phoneNumberTextField)
        self.view.addSubview(self.getCodeButton)
        self.view.addSubview(self.infoLabel)
        self.view.addSubview(self.callingCodeButton)
        self.view.addSubview(self.separatorView)
        
        self.phoneNumberTextField.modifyTextFieldDelegate = self
        let callingCode = phoneUtil.getCountryCode(forRegion: self.detectCountryCode())?.stringValue
                          ?? LanguageManager.defaultLanguage().defaultCallingCode
        self.update(callingCode: callingCode)
        self.setupHandlers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateTextFieldConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.callingCodeButton.snp.makeConstraints {
            $0.bottom.equalTo(self.phoneNumberTextField).offset(-1)
            self.textFieldLeftConstraint = $0.left.equalToSuperview().constraint
        }
        
        self.phoneNumberTextField.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.titleAreaHeight + self.textFieldTopOffset)
            $0.left.equalTo(self.callingCodeButton.snp.right).offset(self.textFieldOffsetLeft)
            self.textFieldWidthConstraint = $0.width.equalTo(self.placeholderTextFieldWidth()).constraint
        }
        
        self.getCodeButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            self.bottomConstraint = $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.bottomOffset).constraint
        }
        
        self.infoLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.top.equalToSuperview().offset(self.titleAreaHeight + 10)
        }
        
        self.separatorView.snp.makeConstraints {
            $0.bottom.equalTo(self.phoneNumberTextField)
            $0.width.equalTo(1)
            $0.height.equalTo(40)
            $0.left.equalTo(self.callingCodeButton.snp.right).offset(5)
        }
    }
    
    func updateTextFieldConstraints() {
        let textFieldHeight = self.phoneNumberTextField.textField.intrinsicContentSize.height
        let placeholderWidth = self.phoneNumberTextField.textField.attributedPlaceholder?.width(height: textFieldHeight) ?? 0
        let textWidth = self.phoneNumberTextField.textField.attributedText?.width(height: textFieldHeight) ?? 0
        let maxTextWidth = max(placeholderWidth, textWidth)
        let callingCodeButtonWidth = self.callingCodeButton.intrinsicContentSize.width
        let space = self.textFieldOffsetLeft
        
        let tresholdTextWidth = (UIScreen.main.bounds.width - callingCodeButtonWidth - space - 2 * ViewSize.sideOffset)
        let actualTextWidth = min(maxTextWidth, tresholdTextWidth)

        let areaWidth = actualTextWidth + callingCodeButtonWidth + space
        let suggestLeftOffset: CGFloat = (UIScreen.main.bounds.width - areaWidth) / 2
        let actualLeftOffset = max(ViewSize.sideOffset, suggestLeftOffset)
        
        self.textFieldLeftConstraint?.update(offset: actualLeftOffset)
        self.textFieldWidthConstraint?.update(offset: actualTextWidth)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
    }
    
    func update(callingCode: String) {
        self.phoneNumberTextField.text = nil
        self.selectingCallingCode = callingCode
        self.callingCodeButton.text = CustomFont.large.attributesWithParagraph.make(string: "+" + callingCode)
        self.updateTextFieldConstraints()
    }
}

extension PhoneNumberViewController: ModifyTextDelegate {
    
    func textField(_ textField: OMGTextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) {
        if let selectCallingCode = self.selectingCallingCode,
           let codeNumber = Int(selectCallingCode) {
            let countryCode = phoneUtil.getRegionCode(forCountryCode: NSNumber.init(value: codeNumber))
            let formatter = NBAsYouTypeFormatter.init(regionCode: countryCode)
            // TODO: Хуйня ?
            if isDeletion(inRange: range, string: string) {
                let updatedText: String = self.replaceCharacters(inText: textField.text ?? String.empty,
                                                                 range: range,
                                                                 withCharacters: String.empty)
                textField.text = updatedText
                textField.textField.caretPosition = range.location
            } else {
                
                let updatedString = self.replaceCharacters(inText: textField.text ?? String.empty,
                                                           range: range,
                                                           withCharacters: string)
                if let newString = formatter?.inputString(self.deleteNonDigitSymbols(string: updatedString)) {
                    textField.text = newString
                    let newPosition = range.location + string.count + newString.count - updatedString.count
                    textField.textField.caretPosition = newPosition
                }
            }
        }
    }
    
    private func replaceCharacters(inText text: String, range: NSRange, withCharacters newText: String) -> String {
        if 0 < range.length {
            let result = NSMutableString(string: text)
            result.replaceCharacters(in: range, with: newText)
            return result as String
        } else {
            let result = NSMutableString(string: text)
            result.insert(newText, at: range.location)
            return result as String
        }
    }
    
    private func isDeletion(inRange range: NSRange, string: String) -> Bool {
        return 0 < range.length && 0 == string.count
    }
    
    private func deleteNonDigitSymbols(string: String) -> String {
        return string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
}

// MARK: Handlers
extension PhoneNumberViewController {
    
    func setupHandlers() {
        self.cancelButton.addTarget(self, action: #selector(self.cancelTouch), for: .touchUpInside)
        self.getCodeButton.onTouch = { [weak self] in
            self?.getCodeTouch()
        }
        
        self.phoneNumberTextField.onTextDidChanged = { [weak self] response in
            guard let self = self else { return }
            if self.isValidFormattedNumber(string: response.text) {
                self.getCodeButton.update(state: .normal)
            } else {
                self.getCodeButton.update(state: .disabled)
            }
            self.updateTextFieldConstraints()
        }

        self.keyboardObserver.onKeyboardWillShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.bottomConstraint?.update(offset: -frame.height
                - strongSelf.bottomOffset
                + strongSelf.bottomLayoutEdgeInset)
            strongSelf.view.animateLayout(duration: duration)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.bottomConstraint?.update(offset: -strongSelf.bottomOffset)
            strongSelf.view.animateLayout(duration: duration)
        }
        
        self.callingCodeButton.addTarget(self, action: #selector(self.callingCodeTouch), for: .touchUpInside)
    }
    
    @objc
    func getCodeTouch() {
        guard let phone = self.phoneNumberTextField.text,
              let callingCode = self.selectingCallingCode else { return }
        let validPhone = callingCode + self.deleteNonDigitSymbols(string: phone)
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.getCodeButton.update(state: .uploading)
        }
        self.apiService.auth(byPhone: validPhone) { [weak self] result in
            switch result {
            case .success(let response):
                self?.onVerification?(validPhone, response)
            case .failure(let error):
                self?.phoneNumberTextField.resignResponder()
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
            self?.getCodeButton.update(state: .normal)
        }
    }
    
    @objc
    func cancelTouch() {
        self.onCancel?()
    }
    
    @objc
    func callingCodeTouch() {
        self.onCallingCode?()
    }
    
}

// MARK: Help
extension PhoneNumberViewController {
    
    func placeholderTextFieldWidth() -> CGFloat {
        let textFieldHeight = self.phoneNumberTextField.textField.intrinsicContentSize.height
        let placeholderWidth = self.phoneNumberTextField.textField.attributedPlaceholder?.width(height: textFieldHeight) ?? 0
        return placeholderWidth
    }
    
    func detectCountryCode() -> String {
        let carrier = CTTelephonyNetworkInfo().subscriberCellularProvider
        let code: String? = carrier?.isoCountryCode ?? Locale.current.regionCode
        return code?.lowercased() ?? LanguageManager.defaultLanguage().rawValue.lowercased()
    }
    
    func currentCallingIntegerCode() -> Int? {
        guard let selectCallingCode = self.selectingCallingCode,
              let codeNumber = Int(selectCallingCode) else { return nil }
        return codeNumber
    }
    
    func isValidFormattedNumber(string: String) -> Bool {
        if let codeNumber = self.currentCallingIntegerCode() {
            let countryCode = self.phoneUtil.getRegionCode(forCountryCode: NSNumber.init(value: codeNumber))
            let number = try? self.phoneUtil.parse(string, defaultRegion: countryCode)
            if let number = number {
                if phoneUtil.isValidNumber(forRegion: number, regionCode: countryCode) {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
