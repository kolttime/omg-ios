//
//  VerificationViewController.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import CountdownLabel

class VerificationViewController: LargeTitleRootViewController, ToastAlertPresentable {
    
    var onCancel: Action?
    var onCompletion: Action?
    var onActivation: Action?
    
    private let resendCodeButtonBottomOffset: CGFloat = 20
    private let timeCount: TimeInterval = 60
    private lazy var cancelButton = UIButton.makeLargeTitleButtonAction(title: L10n.sharedCancel)
    
    private lazy var enterCodeFromSmsLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.verificationCodeFromSms)
        return label
    }()
    
    private var codeFieldTopOffset: CGFloat {
        if Device.isIphone5 {
            return 125
        } else {
            return 145
        }
    }
    
    private var codeFieldSideOffset: CGFloat {
        if Device.isIphone5 {
            return 20
        } else {
            return 40
        }
    }
    
    private lazy var resendCodeButton = OMGButton.makeResend()
    
    private lazy var codeField: OMGCodeTextField = {
        let codeField = OMGCodeTextField()
        codeField.delegate = self
        return codeField
    }()
    
    private let apiService: ApiService
    private let keyboardObserver = KeyboardObserver()
    private var authResponse: AuthResponse
    private let sessionManager: OMGUserSessionManager
    private let phone: String
    private var resendCodeButtonBottomConstraint: Constraint?
    
    lazy var countDownLabel: CountdownLabel = {
        let countdownLabel = CountdownLabel()
        countdownLabel.setCountDownTime(minutes: self.timeCount)
        countdownLabel.timeFormat = "m:ss"
        countdownLabel.countdownAttributedText = CountdownAttributedText(text: "HERE",
                                                                         replacement: "HERE",
                                                                         attributes: CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey30.value))
        countdownLabel.start()
        countdownLabel.countdownDelegate = self
        return countdownLabel
    }()
    
    var toastPresenter = ToastAlertPresenter()
    
    init(phone: String,
         authResponse: AuthResponse,
         apiService: ApiService,
         sessionManager: OMGUserSessionManager) {
        self.apiService = apiService
        self.authResponse = authResponse
        self.phone = phone
        self.sessionManager = sessionManager
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.backgroundTitleColor = Color.white.value
        self.view.backgroundColor = UIColor.white
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.sharedVerifying)
        self.largeTitleActionView = self.cancelButton
        self.view.addSubview(self.enterCodeFromSmsLabel)
        self.view.addSubview(self.resendCodeButton)
        self.view.addSubview(self.codeField)
        self.resendCodeButton.update(state: .disabled)
        self.resendCodeButton.addSubview(self.countDownLabel)
        self.setupHandlers()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.resendCodeButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            self.resendCodeButtonBottomConstraint = $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.resendCodeButtonBottomOffset).constraint
        }
        self.countDownLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        self.codeField.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.titleAreaHeight + self.codeFieldTopOffset)
            $0.left.equalToSuperview().offset(self.codeFieldSideOffset)
            $0.right.equalToSuperview().offset(-self.codeFieldSideOffset)
        }
        self.enterCodeFromSmsLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(self.codeField.snp.bottom).offset(10)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardObserver.isSuspended = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardObserver.isSuspended = false
        self.codeField.firstResponder()
    }
}

extension VerificationViewController: CountdownLabelDelegate {
    
    func countdownFinished() {
        self.countDownLabel.alpha = 0
        self.resendCodeButton.update(state: .normal)
    }
}

extension VerificationViewController: CodeFieldViewDelegate {
    
    func codeDidChage(_ code: String) { }
    
    func didEnterCode(_ code: String) {
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.resendCodeButton.update(state: .uploading)
            self.countDownLabel.isHidden = true
        }
        self.apiService.verification(byKey: self.authResponse.key, code: code) { [weak self] result in
            switch result {
            case .success(let response):
                if let token = response.token {
                    self?.sessionManager.save(token: token)
                    if let user = response.user {
                        self?.sessionManager.save(user: user)
                        self?.sessionManager.setFirstLaunch()
                        self?.onCompletion?()
                    } else {
                        self?.onActivation?()
                    }
                } else {
                    self?.codeField.showError()
                }
                
            case .failure(let error):
                self?.codeField.resignResponder()
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
            if self?.countDownLabel.isCounting == true {
                self?.countDownLabel.isHidden = false
                self?.resendCodeButton.update(state: .disabled)
            } else {
                self?.resendCodeButton.update(state: .normal)
            }
        }
    }
}
// MARK: Handlers
extension VerificationViewController {
    
    func setupHandlers() {
        self.resendCodeButton.onTouch = { [weak self] in
            self?.resendTap()
        }
        
        self.cancelButton.addTarget(self, action: #selector(self.cancelTouch), for: .touchUpInside)
        self.keyboardObserver.onKeyboardWillShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.resendCodeButtonBottomConstraint?.update(offset: -frame.height
                - strongSelf.resendCodeButtonBottomOffset
                + strongSelf.bottomLayoutEdgeInset)
            strongSelf.view.animateLayout(duration: duration)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.resendCodeButtonBottomConstraint?.update(offset: -strongSelf.resendCodeButtonBottomOffset)
            strongSelf.view.animateLayout(duration: duration)
        }
    }
    
    @objc
    func cancelTouch() {
        self.onCancel?()
    }
    
    @objc
    func resendTap() {
        self.countDownLabel.alpha = 1
        self.countDownLabel.setCountDownTime(minutes: self.timeCount)
        self.countDownLabel.start()
        self.resendCodeButton.update(state: .disabled)
        self.apiService.auth(byPhone: self.phone) { [weak self] result in
            switch result {
            case .success(let response):
                self?.authResponse = response
            case .failure(let error):
                self?.codeField.resignResponder()
                self?.showToastErrorAlert(error)
            }
        }
    }
}
