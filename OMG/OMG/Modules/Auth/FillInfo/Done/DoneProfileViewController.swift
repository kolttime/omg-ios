//
//  DoneProfileViewController.swift
//  OMG
//
//  Created by Roman Makeev on 18/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class DoneProfileViewController: PageViewController {
    
    private let apiService: ApiService
    private var model: Model?
    
    var onCastings: Action?
    var onEditAdditionalInfo: ((Model) -> Void)?
    var onSocialNetwork: ((Model) -> Void)?
    
    private lazy var congratulationsLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.large.attributesWithParagraph.colored(color: Color.green.value).make(string: L10n.profileCongratulations)
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.profileDoneInfo)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var footnoteLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.profileDoneFootnote)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var discoversButton = OMGButton.makeDiscoverCastings()

    private lazy var answerAdditionalQuestionButton = SectionButton.makeAnswerAdditionalQuestions()
    private lazy var connectSocialNetworkButton = SectionButton.makeConnectSocialNetwork()

    private var buttonBottomOffset: CGFloat {
        if Device.isIphone5 {
            return 20
        }
        return 25
    }
    
    private var footnoteBottomOffset: CGFloat {
        return ViewSize.sideOffset
    }
    
    private var answersTopOffset: CGFloat {
        return ViewSize.sideOffset
    }
    
    private var connectSocialNetworkTopOffset: CGFloat {
        if Device.isIphone5 {
            return 10
        }
        return 15
    }
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init(pageIndex: 3, pageTitle: L10n.loginProfileDone)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.congratulationsLabel)
        self.view.addSubview(self.subtitleLabel)
        self.view.addSubview(self.footnoteLabel)
        self.view.addSubview(self.answerAdditionalQuestionButton)
        self.view.addSubview(self.connectSocialNetworkButton)

        self.view.addSubview(self.discoversButton)
        self.discoversButton.onTouch = { [weak self] in
            self?.dicoverTap()
        }
        self.answerAdditionalQuestionButton.addTarget(self, action: #selector(self.additionalInfoTouch), for: .touchUpInside)
        self.connectSocialNetworkButton.addTarget(self, action: #selector(self.socialNetworkTouch), for: .touchUpInside)

        self.setupConstraints()
    }
    
    private func setupConstraints() {
        self.congratulationsLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(self.congratulationsLabel.snp.bottom).offset(10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.answerAdditionalQuestionButton.snp.makeConstraints {
            $0.top.equalTo(self.subtitleLabel.snp.bottom).offset(self.answersTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }

        self.connectSocialNetworkButton.snp.makeConstraints {
            $0.top.equalTo(self.answerAdditionalQuestionButton.snp.bottom).offset(self.connectSocialNetworkTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.discoversButton.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-buttonBottomOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.footnoteLabel.snp.makeConstraints {
            $0.bottom.equalTo(self.discoversButton.snp.top).offset(-self.footnoteBottomOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
    }
    
    @objc
    func dicoverTap() {
        self.onCastings?()
    }
    
    @objc
    func additionalInfoTouch() {
        if let model = self.model {
            self.onEditAdditionalInfo?(model)
        }
    }
    
    @objc
    func socialNetworkTouch() {
        if let model = self.model {
            self.onSocialNetwork?(model)
        }
    }
    
    func set(model: Model) {
        self.model = model
    }
}
