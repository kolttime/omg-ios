//
//  BodyProfileViewController.swift
//  OMG
//
//  Created by Roman Makeev on 18/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

// TODO: Need to refactor
class BodyProfileViewController: PageViewController, PopupPresentable, PopupCollectionPresentable, ToastAlertPresentable {
    
    private let apiService: ApiService
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init(pageIndex: 2, pageTitle: L10n.loginProfileBody)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var scrollViewController = ScrollViewController()
    private lazy var heightTextField = OMGTextField.makeHeight()
    private lazy var weightTextField = OMGTextField.makeWeight()
    private lazy var bustTextField = OMGTextField.makeBust()
    private lazy var waistTextField = OMGTextField.makeWaist()
    private lazy var hipTextField = OMGTextField.makeHip()
    private lazy var footwearButton = PickedButton.makeFootwear()
    private lazy var tattoosButton = PickedButton.makeTattoos()
    private lazy var hairButton = PickedButton.makeHair()
    private lazy var eyesButton = PickedButton.makeEyes()
    private lazy var nextButton = OMGButton.makeNext()
    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    var toastPresenter = ToastAlertPresenter()

    private lazy var skipForNowLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph
                                                     .colored(color: Color.grey44.value)
                                                     .underlying(color: Color.grey44.value)
                                                     .make(string: L10n.profileSkipForNow)
        label.textAlignment = .center
        return label
    }()
    
    private var activeField: UIView?
    private let textfieldTopOffset: CGFloat = 20
    private let buttonTopOffset: CGFloat = 25
    private weak var popupViewController: PopupViewController?
    private weak var popupCollectionViewController: PopupCollectionViewController?

    private lazy var keyboardObserver = KeyboardObserver()
    private var user: User?

    private var hairs: [SelectableValue] = []
    private var selectedHair: SelectableValue?
    private var hairButtonState: PickedButtonType = .unpicked {
        didSet {
            self.hairButton.pickedState = self.hairButtonState
        }
    }
    private var eyes: [SelectableValue] = []
    private var selectedEyes: SelectableValue?
    private var eyesButtonState: PickedButtonType = .unpicked {
        didSet {
            self.eyesButton.pickedState = self.eyesButtonState
        }
    }
    private var tattoos: [SelectableValue] = []
    private var selectedTattoo: SelectableValue?
    private var tattoosButtonState: PickedButtonType = .unpicked {
        didSet {
            self.tattoosButton.pickedState = self.tattoosButtonState
        }
    }
    
    private var footwears: [Footwear] = []
    private var selectedFootwear: Footwear?
    private var footwearButtonState: PickedButtonType = .unpicked {
        didSet {
            self.footwearButton.pickedState = self.footwearButtonState
        }
    }
    
    private var bodyInfo = BodyInfo()
    private var model: Model?
    
    var onNext: ((Model) -> Void)?

    func set(model: Model) {
        self.model = model
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.heightTextField)
        self.scrollViewController.scrollView.addSubview(self.weightTextField)
        self.scrollViewController.scrollView.addSubview(self.bustTextField)
        self.scrollViewController.scrollView.addSubview(self.waistTextField)
        self.scrollViewController.scrollView.addSubview(self.hipTextField)
        self.scrollViewController.scrollView.addSubview(self.footwearButton)
        self.scrollViewController.scrollView.addSubview(self.tattoosButton)
        self.scrollViewController.scrollView.addSubview(self.hairButton)
        self.scrollViewController.scrollView.addSubview(self.eyesButton)

        self.scrollViewController.scrollView.addSubview(self.nextButton)
        self.scrollViewController.scrollView.addSubview(self.skipForNowLabel)
        self.view.addSubview(self.loadingLabel)

        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupConstraints()
        self.setupHandlers()
        self.loadingLabel.blink()
        self.scrollViewController.view.isHidden = true
        self.nextButton.update(state: .disabled)
        self.fetchData()
    }
    
    func setupConstraints() {
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.heightTextField.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().dividedBy(2).offset(-1.5 * ViewSize.sideOffset)
        }
        self.weightTextField.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalTo(self.heightTextField.snp.right).offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().dividedBy(2).offset(-1.5 * ViewSize.sideOffset)
        }
        self.bustTextField.snp.makeConstraints {
            $0.top.equalTo(self.heightTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().dividedBy(2).offset(-1.5 * ViewSize.sideOffset)
        }
        self.waistTextField.snp.makeConstraints {
            $0.top.equalTo(self.heightTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalTo(self.bustTextField.snp.right).offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().dividedBy(2).offset(-1.5 * ViewSize.sideOffset)
        }
        self.hipTextField.snp.makeConstraints {
            $0.top.equalTo(self.waistTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().dividedBy(2).offset(-1.5 * ViewSize.sideOffset)
        }
        self.footwearButton.snp.makeConstraints {
            $0.top.equalTo(self.waistTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalTo(self.hipTextField.snp.right).offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().dividedBy(2).offset(-1.5 * ViewSize.sideOffset)
        }
        self.tattoosButton.snp.makeConstraints {
            $0.top.equalTo(self.footwearButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.hairButton.snp.makeConstraints {
            $0.top.equalTo(self.tattoosButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.eyesButton.snp.makeConstraints {
            $0.top.equalTo(self.hairButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.nextButton.snp.makeConstraints {
            $0.top.equalTo(self.eyesButton.snp.bottom).offset(2 * self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.skipForNowLabel.snp.makeConstraints {
            $0.top.equalTo(self.nextButton.snp.bottom).offset(self.buttonTopOffset)
            $0.bottom.equalToSuperview().offset(-self.buttonTopOffset - ViewSize.sideOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    func setupHandlers() {
        
        self.nextButton.onTouch = { [weak self] in
            self?.nextTap()
        }
        
        self.tattoosButton.addTarget(self, action: #selector(tattoosTap), for: .touchUpInside)
        self.hairButton.addTarget(self, action: #selector(hairTap), for: .touchUpInside)
        self.eyesButton.addTarget(self, action: #selector(eyesTap), for: .touchUpInside)
        self.footwearButton.addTarget(self, action: #selector(footwearTap), for: .touchUpInside)

        let tap = UITapGestureRecognizer(target: self, action: #selector(skipTap))
        self.skipForNowLabel.addGestureRecognizer(tap)
        
        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.scrollViewController.scrollView.contentInset.bottom = frame.height
            guard let textField = strongSelf.activeField else {
                return
            }
            var viewRect = strongSelf.view.frame
            viewRect.origin.y -= frame.height - strongSelf.bottomLayoutEdgeInset
            var rect = textField.frame
            rect.origin.y += strongSelf.textfieldTopOffset
            guard !viewRect.contains(rect) else { return }
            let y = viewRect.maxY - textField.frame.maxY - strongSelf.bottomLayoutEdgeInset - strongSelf.textfieldTopOffset
            
            let point = CGPoint(x: 0, y: -y)
            self?.scrollViewController.scrollView.setContentOffset(point, animated: true)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
        }
        
        self.heightTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.weightTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.bustTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.waistTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.hipTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.heightTextField.onReturnHandler = { [weak self] in
            self?.weightTextField.becomeResponder()
        }
        self.weightTextField.onReturnHandler = { [weak self] in
            self?.bustTextField.becomeResponder()
        }
        self.bustTextField.onReturnHandler = { [weak self] in
            self?.waistTextField.becomeResponder()
        }
        self.waistTextField.onReturnHandler = { [weak self] in
            self?.hipTextField.becomeResponder()
        }
        self.hipTextField.onReturnHandler = { [weak self] in
            self?.hipTextField.resignResponder()
        }
        self.heightTextField.onTextDidChanged = { [weak self] response in
            self?.bodyInfo.height = Double(response.text)
            self?.checkValid()
        }
        self.weightTextField.onTextDidChanged = { [weak self] response in
            self?.bodyInfo.weight = Double(response.text)
            self?.checkValid()
        }
        self.bustTextField.onTextDidChanged = { [weak self] response in
            self?.bodyInfo.bust = Double(response.text)
            self?.checkValid()
        }
        self.waistTextField.onTextDidChanged = { [weak self] response in
            self?.bodyInfo.waist = Double(response.text)
            self?.checkValid()
        }
        self.hipTextField.onTextDidChanged = { [weak self] response in
            self?.bodyInfo.hip = Double(response.text)
            self?.checkValid()
        }
    }
    
    @objc
    func tattoosTap() {
        self.dismissPopup()
        self.tattoosButtonState = .picking
        let viewModels = self.tattoos.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.text)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = self.tattoos[index]
            self.selectedTattoo = value
            self.bodyInfo.tattoos = value
            self.tattoosButtonState = .picked(string: value.text)
            self.checkValid()
        }
        
        let dismissHandler: Action = { [weak self] in
            if let value = self?.selectedTattoo {
                self?.tattoosButtonState = .picked(string: value.text)
            } else {
                self?.tattoosButtonState = .unpicked
            }
            self?.checkValid()
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    @objc
    func hairTap() {
        self.dismissPopup()
        self.hairButtonState = .picking
        let viewModels = self.hairs.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.text)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = self.hairs[index]
            self.selectedHair = value
            self.hairButtonState = .picked(string: value.text)
            self.bodyInfo.hair = value
            self.checkValid()
        }
        
        let dismissHandler: Action = { [weak self] in
            if let value = self?.selectedHair {
                self?.hairButtonState = .picked(string: value.text)
            } else {
                self?.hairButtonState = .unpicked
            }
            self?.checkValid()
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    @objc
    func eyesTap() {
        self.dismissPopup()
        self.eyesButtonState = .picking
        let viewModels = self.eyes.map { values -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: values.text)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            guard let self = self else { return }
            let value = self.eyes[index]
            self.selectedEyes = value
            self.eyesButtonState = .picked(string: value.text)
            self.bodyInfo.eyes = value
            self.checkValid()
        }
        
        let dismissHandler: Action = { [weak self] in
            if let value = self?.selectedEyes {
                self?.eyesButtonState = .picked(string: value.text)
            } else {
                self?.eyesButtonState = .unpicked
            }
            self?.checkValid()
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    @objc
    func nextTap() {
        self.updateBodyInfo()
    }
    
    @objc
    func skipTap() {
        guard let model = self.model else { return }
        self.onNext?(model)
    }
        
    @objc
    func footwearTap() {
        self.dismissPopup()
        self.footwearButtonState = .picking
        var viewModels = [CollectionCellViewModel]()
        
        let euViewModel = PopupCollectionCellViewModel(title: CustomFont.bodyBold.attributesWithParagraph.colored(color: Color.grey30.value).make(string: FootwearUnit.eu.rawValue))
        let ruViewModel = PopupCollectionCellViewModel(title: CustomFont.bodyBold.attributesWithParagraph.colored(color: Color.grey30.value).make(string: FootwearUnit.ru.rawValue))
        let usViewModel = PopupCollectionCellViewModel(title: CustomFont.bodyBold.attributesWithParagraph.colored(color: Color.grey30.value).make(string: FootwearUnit.us.rawValue))
        
        viewModels.append(euViewModel)
        viewModels.append(ruViewModel)
        viewModels.append(usViewModel)
        
        let euFootwear = self.footwears.filter { $0.unit == .eu }
        let ruFootwear = self.footwears.filter { $0.unit == .ru }
        let usFootwear = self.footwears.filter { $0.unit == .us }
        
        func selectFoot(_ footwear: Footwear) {
            self.selectedFootwear = footwear
            self.footwearButtonState = .picked(string: footwear.text)
            self.bodyInfo.footwear = footwear
            self.popupCollectionViewController?.select()
        }
        
        for i in 0..<min(min(euFootwear.count, ruFootwear.count), usFootwear.count) {
            let euViewModel = FootwearCollectionCellViewModel(footwear: euFootwear[i]) { _ in
                selectFoot(euFootwear[i])
            }
            let ruViewModel = FootwearCollectionCellViewModel(footwear: ruFootwear[i]) { _ in
                selectFoot(ruFootwear[i])
            }
            let usViewModel = FootwearCollectionCellViewModel(footwear: usFootwear[i]) { _ in
                selectFoot(usFootwear[i])
            }
            viewModels.append(euViewModel)
            viewModels.append(ruViewModel)
            viewModels.append(usViewModel)
        }
        
        
        let dismissHandler: Action = { [weak self] in
            if let value = self?.selectedFootwear {
                self?.footwearButtonState = .picked(string: value.text)
            } else {
                self?.footwearButtonState = .unpicked
            }
        }
        
        self.popupCollectionViewController = self.showPopup(targetViewController: self,
                                                            viewModels: viewModels,
                                                            dismissItemHandler: dismissHandler)
    }
    
    private func dismissPopup() {
        self.heightTextField.resignResponder()
        self.weightTextField.resignResponder()
        self.bustTextField.resignResponder()
        self.waistTextField.resignResponder()
        self.hipTextField.resignResponder()

        if case .picking = self.eyesButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
        if case .picking = self.hairButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
        
        if case .picking = self.tattoosButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
        
        if case .picking = self.footwearButtonState {
            self.popupViewController?.onDismiss?()
            return
        }
    }
    
    private func checkValid() {
        var isValid = self.heightTextField.text?.isEmpty == false
        isValid = isValid && self.weightTextField.text?.isEmpty == false
        isValid = isValid && self.bustTextField.text?.isEmpty == false
        isValid = isValid && self.waistTextField.text?.isEmpty == false
        isValid = isValid && self.hipTextField.text?.isEmpty == false
        isValid = isValid && self.selectedFootwear != nil
        isValid = isValid && self.selectedEyes != nil
        isValid = isValid && self.selectedHair != nil
        isValid = isValid && self.selectedTattoo != nil

        if isValid {
            self.nextButton.update(state: .normal)
        } else {
            self.nextButton.update(state: .disabled)
        }
    }
}

// MARK: Interacting
extension BodyProfileViewController {
 
    func fetchData() {
        self.fetchTattoos()
    }
    
    func fetchTattoos() {
        self.apiService.fetchTattoos { [weak self] result in
            switch result {
            case .success(let tattoos):
                self?.tattoos = tattoos
                self?.fetchEyes()
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func fetchEyes() {
        self.apiService.fetchEyes { [weak self] result in
            switch result {
            case .success(let eyes):
                self?.eyes = eyes
                self?.fetchFootwear()
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    
    func fetchFootwear() {
        self.apiService.fetchFootwears { [weak self] result in
            switch result {
            case .success(let footwears):
                self?.footwears = footwears
                self?.fetchHairs()
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    func fetchHairs() {
        self.apiService.fetchHair { [weak self] result in
            self?.loadingLabel.isHidden = true
            self?.scrollViewController.view.isHidden = false
            switch result {
            case .success(let hairs):
                self?.hairs = hairs
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    
    private func updateBodyInfo() {
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.nextButton.update(state: .uploading)
        }
        self.apiService.updateBodyInfo(info: self.bodyInfo) { [weak self] result in
            switch result {
            case .success(let model):
                self?.model = model
                self?.onNext?(model)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
            self?.nextButton.update(state: .normal)
        }
    }
}
