//
//  FillInfoContainerViewController.swift
//  OMG
//
//  Created by Roman Makeev on 17/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class FillInfoContainerViewController: LargeTitleRootViewController {
    
    var onCancel: Action?
    var onSkip: Action?

    private let normalAttributes = CustomFont.large.attributesWithParagraph.colored(color: Color.grey30.value)
    private let selectedAttributes = CustomFont.large.attributesWithParagraph

    private let pageControllerTopOffset: CGFloat = Device.isIphone5 ? 60 : 75
    private lazy var pagesController: UIPageViewController = {
        let controller = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: [:])
        
        return controller
    }()
    
    private lazy var scrollView = UIScrollView()
    private var labels: [UILabel] = []
    
    private lazy var cancelButton: ImageButton = {
        return ImageButton.makeCancelButton()
    }()
    
    private lazy var skipButton: ImageButton = {
        return ImageButton.makeSkipButton()
    }()
    
    private let controllers: [PageViewController]
    private var opennedControllersSet = Set<Int>()
    
    init(controllers: [PageViewController]) {
        self.controllers = controllers
        super.init()
        self.controllers.enumerated().forEach { offset, controller in
            let label = UILabel()
            label.attributedText = self.normalAttributes.make(string: "\(offset + 1). " + controller.pageTitle)
            self.labels.append(label)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setController(at index: Int) {
        self.pagesController.setViewControllers([self.controllers[index]],
                                                direction: .forward,
                                                animated: true,
                                                completion: nil)
        self.opennedControllersSet.insert(index)
        self.configureActionButton(index: index)
        self.setNeedLabel(index: index)
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundTitleColor = Color.white.value
        self.view.backgroundColor = UIColor.white
        self.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.sharedFillProfile)
        self.largeTitleActionView = self.cancelButton
        self.add(self.pagesController)
        self.view.addSubview(self.scrollView)
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.isUserInteractionEnabled = false
        self.pagesController.dataSource = self
        self.pagesController.delegate = self
        self.cancelButton.addTarget(self, action: #selector(self.cancelTouch), for: .touchUpInside)
        self.skipButton.addTarget(self, action: #selector(self.skipTouch), for: .touchUpInside)

        self.labels.forEach { self.scrollView.addSubview($0) }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.pagesController.view.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.titleAreaHeight + self.pageControllerTopOffset)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.scrollView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.bottom.equalTo(self.pagesController.view.snp.top).offset(-15)
            $0.height.equalTo(25)
            $0.right.equalToSuperview()
        }
        var left = self.scrollView.snp.left
        self.labels.forEach { label in
            label.snp.makeConstraints {
                $0.left.equalTo(left).offset(ViewSize.sideOffset)
                $0.bottom.equalToSuperview()
                $0.top.equalToSuperview()
            }
            left = label.snp.right
        }
        self.labels.last?.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
    }
    
    @objc
    func cancelTouch() {
        self.onCancel?()
    }
    
    @objc
    func skipTouch() {
        self.onSkip?()
    }
    
    private func setNeedLabel(index: Int) {
        var point = CGPoint.zero
        point.x = self.labels[index].frame.origin.x - ViewSize.sideOffset
        self.labels.forEach {
            $0.attributedText = self.normalAttributes.make(string: $0.text ?? String.empty)
        }
        self.labels[index].attributedText = self.selectedAttributes.make(string: self.labels[index].text ?? String.empty)
        self.scrollView.setContentOffset(point, animated: true)
    }
    
    private func configureActionButton(index: Int) {
        switch index {
        case 0:
            self.largeTitleActionView = self.cancelButton
        case 1:
            self.largeTitleActionView = self.cancelButton
        case 2:
            self.largeTitleActionView = self.skipButton
        case 3:
            self.largeTitleActionView = nil
        default:
            return
        }
    }
}

extension FillInfoContainerViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        guard let vc = pageViewController.viewControllers?.first as? PageViewController,
              let index = controllers.index(of: vc)
            else { return }
        self.configureActionButton(index: index)
        self.setNeedLabel(index: index)
    }
}

extension FillInfoContainerViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? PageViewController else { return nil }
        guard let current = controllers.index(of: viewController) else { return nil }
        guard let before = controllers.index(current, offsetBy: -1, limitedBy: 0) else { return nil }
        
        return controllers[before]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? PageViewController else { return nil }
        guard let current = self.controllers.index(of: viewController) else { return nil }
        guard let after = self.controllers.index(current, offsetBy: 1, limitedBy: self.opennedControllersSet.count - 1) else { return nil }
        return self.controllers[after]
    }
}
