//
//  IntroduceProfileViewController.swift
//  OMG
//
//  Created by Roman Makeev on 18/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class IntroduceProfileViewController: PageViewController, PopupPresentable {
    
    private let apiService: ApiService
    private lazy var scrollViewController = ScrollViewController()
    private lazy var givenNameTextField = OMGTextField.makeGivenName()
    private lazy var familyNameTextField = OMGTextField.makeFamilyName()
    private lazy var sexPickedButton = PickedButton.makeSex()
    private lazy var birthdayTextField = OMGTextField.makeBirthday()
    private lazy var nextButton = OMGButton.makeNext()
    private var activeField: UIView?
    private let textfieldTopOffset: CGFloat = 20
    private let buttonTopOffset: CGFloat = 25
    private var selectedSex: SexType?
    private var birthdayMandatoryComplete: Bool = false
    private let userActivation = UserCreation()
    private var buttonState: PickedButtonType = .unpicked {
        didSet {
            self.sexPickedButton.pickedState = self.buttonState
        }
    }
    private weak var popupViewController: PopupViewController?

    private lazy var keyboardObserver = KeyboardObserver()
    private var user: User?
    
    var onNext: ((UserCreation) -> Void)?
    
    init(apiService: ApiService) {
        self.apiService = apiService
        super.init(pageIndex: 0, pageTitle: L10n.loginProfileIntroduce)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.givenNameTextField)
        self.scrollViewController.scrollView.addSubview(self.familyNameTextField)
        self.scrollViewController.scrollView.addSubview(self.sexPickedButton)
        self.scrollViewController.scrollView.addSubview(self.birthdayTextField)
        self.scrollViewController.scrollView.addSubview(self.nextButton)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.nextButton.update(state: .disabled)
        self.setupConstraints()
        self.setupHandlers()
    }
    
    func setupConstraints() {
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.givenNameTextField.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.familyNameTextField.snp.makeConstraints {
            $0.top.equalTo(self.givenNameTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.sexPickedButton.snp.makeConstraints {
            $0.top.equalTo(self.familyNameTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.birthdayTextField.snp.makeConstraints {
            $0.top.equalTo(self.sexPickedButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.nextButton.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.buttonTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
    }
    
    func setupHandlers() {
        
        self.nextButton.onTouch = { [weak self] in
            self?.nextTap()
        }
        self.sexPickedButton.addTarget(self, action: #selector(sexTap), for: .touchUpInside)
        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.scrollViewController.scrollView.contentInset.bottom = frame.height
            guard let textField = strongSelf.activeField else {
                return
            }
            var viewRect = strongSelf.view.frame
            viewRect.origin.y -= frame.height - strongSelf.bottomLayoutEdgeInset
            var rect = textField.frame
            rect.origin.y += strongSelf.textfieldTopOffset
            guard !viewRect.contains(rect) else { return }
            let y = viewRect.maxY - textField.frame.maxY - strongSelf.bottomLayoutEdgeInset - strongSelf.textfieldTopOffset
            
            let point = CGPoint(x: 0, y: -y)
            self?.scrollViewController.scrollView.setContentOffset(point, animated: true)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
        }
        
        self.birthdayTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.givenNameTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.familyNameTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        
        self.givenNameTextField.onReturnHandler = { [weak self] in
            self?.familyNameTextField.becomeResponder()
        }
        
        self.familyNameTextField.onReturnHandler = { [weak self] in
            self?.familyNameTextField.resignResponder()
        }
        
        self.birthdayTextField.onReturnHandler = { [weak self] in
            self?.birthdayTextField.resignResponder()
        }
        
        self.familyNameTextField.onTextDidChanged = { [weak self] response in
            self?.userActivation.familyName = response.text
            self?.checkValid()
        }
        
        self.givenNameTextField.onTextDidChanged = { [weak self] response in
            self?.userActivation.givenName = response.text
            self?.checkValid()
        }
        
        self.birthdayTextField.onTextDidChanged = { [weak self] response in
            self?.birthdayMandatoryComplete = response.mandatoryComplete
            if response.mandatoryComplete {
                self?.userActivation.birthday = response.text
            } else {
                self?.userActivation.birthday = nil
            }
            self?.checkValid()
        }
    }
    
    @objc
    func nextTap() {
        self.onNext?(self.userActivation)
    }
    
    @objc
    private func sexTap() {
        if case .picking = self.buttonState {
            self.popupViewController?.onDismiss?()
            return
        }
        self.birthdayTextField.resignResponder()
        self.familyNameTextField.resignResponder()
        self.givenNameTextField.resignResponder()
        self.buttonState = .picking
        let viewModels = SexType.allCases.map { type -> PopupTableCellViewModel in
            let string = CustomFont.large.attributesWithParagraph.make(string: type.localize)
            return PopupTableCellViewModel(title: string)
        }
        
        let selectItemHandler: ((Int) -> Void) = { [weak self] index in
            let type = SexType.allCases[index]
            self?.selectedSex = type
            self?.buttonState = .picked(string: type.localize)
            self?.userActivation.sex = type.rawValue
            self?.checkValid()
        }
        
        let dismissHandler: Action = { [weak self] in
            if let sexType = self?.selectedSex {
                self?.buttonState = .picked(string: sexType.localize)
            } else {
                self?.buttonState = .unpicked
            }
            self?.checkValid()
        }
        
        self.popupViewController = self.showPopup(targetViewController: self,
                                                  viewModels: viewModels,
                                                  selectItemHandler: selectItemHandler,
                                                  dismissItemHandler: dismissHandler)
    }
    
    private func checkValid() {
        var isValid = self.givenNameTextField.text?.isEmpty == false
        isValid = isValid && self.familyNameTextField.text?.isEmpty == false
        isValid = isValid && self.birthdayMandatoryComplete && DateFormatter.dateDateFormatter().date(from: self.birthdayTextField.text) != nil
        isValid = isValid && self.selectedSex != nil
        
        if isValid {
            self.nextButton.update(state: .normal)
        } else {
            self.nextButton.update(state: .disabled)
        }
    }
}
