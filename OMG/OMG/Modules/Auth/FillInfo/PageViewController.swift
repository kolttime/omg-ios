//
//  PageViewController.swift
//  OMG
//
//  Created by Roman Makeev on 17/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class PageViewController: AirdronViewController {
    
    let pageIndex: Int
    let pageTitle: String
    
    init(pageIndex: Int, pageTitle: String) {
        self.pageIndex = pageIndex
        self.pageTitle = pageTitle
        super.init()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
