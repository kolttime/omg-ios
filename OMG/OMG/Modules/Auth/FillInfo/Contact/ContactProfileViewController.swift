//
//  ContactProfileViewController.swift
//  OMG
//
//  Created by Roman Makeev on 18/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ContactProfileViewController: PageViewController, ToastAlertPresentable {
    
    private let apiService: ApiService
    private let sessionManager: OMGUserSessionManager
    
    init(apiService: ApiService, sessionManager: OMGUserSessionManager) {
        self.apiService = apiService
        self.sessionManager = sessionManager
        super.init(pageIndex: 1, pageTitle: L10n.loginProfileContact)
    }
    
    private lazy var scrollViewController = ScrollViewController()
    private lazy var countryButton = PickedButton.makeCountry()
    private lazy var cityTextField = OMGTextField.makeCity()
    private lazy var emailTextField = OMGTextField.makeEmail()
    private lazy var nextButton = OMGButton.makeNext()
    private var activeField: UIView?
    private let textfieldTopOffset: CGFloat = 20
    private let buttonTopOffset: CGFloat = 25
    private var selectedCountry: Country?
    private weak var popupViewController: PopupViewController?
    private var userActivation = UserCreation()
    var toastPresenter = ToastAlertPresenter()

    private lazy var keyboardObserver = KeyboardObserver()
    private var user: User?
    
    var onCountySelect: Action?
    var onNext: ((Model) -> Void)?
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(country: Country) {
        self.selectedCountry = country
        self.userActivation.country = country.name
        self.countryButton.pickedState = .picked(string: country.text)
        self.checkValid()
    }
    
    func set(user: UserCreation) {
        self.userActivation = user
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.toastPresenter.targetView = self.view
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.countryButton)
        self.scrollViewController.scrollView.addSubview(self.cityTextField)
        self.scrollViewController.scrollView.addSubview(self.emailTextField)
        self.scrollViewController.scrollView.addSubview(self.nextButton)
        self.scrollViewController.scrollView.keyboardDismissMode = .onDrag
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupConstraints()
        self.setupHandlers()
        self.nextButton.update(state: .disabled)
    }
    
    func setupConstraints() {
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.countryButton.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.cityTextField.snp.makeConstraints {
            $0.top.equalTo(self.countryButton.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.emailTextField.snp.makeConstraints {
            $0.top.equalTo(self.cityTextField.snp.bottom).offset(self.textfieldTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
        self.nextButton.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.buttonTopOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.width.equalToSuperview().offset(-2 * ViewSize.sideOffset)
        }
    }
    
    func setupHandlers() {
        
        self.nextButton.onTouch = { [weak self] in
            self?.nextTap()
        }
        self.countryButton.addTarget(self, action: #selector(countryTap), for: .touchUpInside)
        self.keyboardObserver.onKeyboardDidShow = { [weak self] frame, duration in
            guard let strongSelf = self else { return }
            strongSelf.scrollViewController.scrollView.contentInset.bottom = frame.height
            guard let textField = strongSelf.activeField else {
                return
            }
            var viewRect = strongSelf.view.frame
            viewRect.origin.y -= frame.height - strongSelf.bottomLayoutEdgeInset
            var rect = textField.frame
            rect.origin.y += strongSelf.textfieldTopOffset
            guard !viewRect.contains(rect) else { return }
            let y = viewRect.maxY - textField.frame.maxY - strongSelf.bottomLayoutEdgeInset - strongSelf.textfieldTopOffset
            
            let point = CGPoint(x: 0, y: -y)
            self?.scrollViewController.scrollView.setContentOffset(point, animated: true)
        }
        
        self.keyboardObserver.onKeyboardWillHide = { [weak self] frame, duration in
            self?.scrollViewController.scrollView.contentInset.bottom = 0
        }
        
        self.cityTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        self.emailTextField.onBeginEditing = { [weak self] textField in
            self?.activeField = textField
        }
        
        self.cityTextField.onReturnHandler = { [weak self] in
            self?.emailTextField.becomeResponder()
        }
        
        self.emailTextField.onReturnHandler = { [weak self] in
            self?.emailTextField.resignResponder()
        }
        
        self.cityTextField.onTextDidChanged = { [weak self] response in
            self?.checkValid()
            self?.userActivation.city = response.text
        }
        self.emailTextField.onTextDidChanged = { [weak self] response in
            self?.checkValid()
            self?.userActivation.email = response.text
        }
    }
    
    @objc
    func nextTap() {
        self.activation()
    }
    
    @objc
    func countryTap() {
        self.onCountySelect?()
    }
    
    private func checkValid() {
        var isValid = self.cityTextField.text?.isEmpty == false
        isValid = isValid && self.emailTextField.text?.isEmail == true
        isValid = isValid && self.selectedCountry != nil
        
        if isValid {
            self.nextButton.update(state: .normal)
        } else {
            self.nextButton.update(state: .disabled)
        }
    }
    
    private func activation() {
        var requestComplete: Bool = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard !requestComplete else { return }
            self.nextButton.update(state: .uploading)
        }
        self.apiService.activation(userActivation: self.userActivation) { [weak self] result in
            switch result {
            case .success(let user):
                self?.sessionManager.save(user: user)
                self?.onNext?(user.toModel())
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
            requestComplete = true
            self?.nextButton.update(state: .normal)
        }
    }
}
