//
//  InfoViewController.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class InfoViewController: AirdronViewController {

    var onStart: Action?
    
    private lazy var imageView = UIImageView(image:#imageLiteral(resourceName: "infoCover.png"))
    
    private var imageHeight: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 475
        } else if Device.isIphone5 {
            return 335
        } else {
            return 425
        }
    }
    
    private var buttonBottomOffset: CGFloat {
        if Device.isIphone5 {
            return 20
        }
        return 25
    }

    private var helloBottomOffset: CGFloat {
        if Device.isIphone5 {
            return 15
        }
        return 25
    }
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.loginStartInfo)
        return label
    }()
    
    private lazy var button = OMGButton.makeStart()
    
    private lazy var helloLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.headline.attributesWithParagraph.colored(color: Color.white.value).make(string: L10n.loginStartHello)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.clipsToBounds = true
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.infoLabel)
        self.view.addSubview(self.button)
        self.view.addSubview(self.helloLabel)
        
        self.button.onTouch = { [weak self] in
            self?.onStart?()
        }
        
        self.setupConstraints()
    }
    
    private func setupConstraints() {
        self.imageView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.top.equalToSuperview()
            $0.height.equalTo(self.imageHeight)
        }
        self.infoLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            $0.top.equalTo(self.imageView.snp.bottom).offset(15)

        }
        self.button.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-self.buttonBottomOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.helloLabel.snp.makeConstraints {
            $0.bottom.equalTo(self.imageView.snp.bottom).offset(-self.helloBottomOffset)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
        }
    }
}
