//
//  LaunchViewController.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class LaunchViewController: AirdronViewController, ToastAlertPresentable {
    
    var onTermsOfUse: Action?
    var onPrivacyPolicy: Action?
    var onPhoneNumber: Action?
    var onInstagram: Action?
    
    private let termsOfUseUrl = URL(string: "www.termsofuse.com")
    private let policyPrivacyUrl = URL(string: "www.policyprivacy.com")
    var toastPresenter = ToastAlertPresenter()

    private lazy var imageView = UIImageView(image: #imageLiteral(resourceName: "cover.jpg"))
    private lazy var button = OMGButton.makeLaunchPhoneNumberButton()
    private lazy var instagramButton = OMGButton.makeInstagramButton()

    private lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.black.value.withAlphaComponent(0.5)
        return view
    }()
    
    private var topOffset: CGFloat {
        if Device.isIphoneX || Device.isIphoneXSmax {
            return 136
        } else {
            return 120
        }
    }
    
    private lazy var termsOfUseLabel: UITextView = {
        let textView = UITextView.makeStaticTextView()
        let font = Device.isIphone5 ? CustomFont.tech : CustomFont.termsOfUse
        let string = font.attributesWithParagraph.colored(color: Color.white.value).makeMutable(string: L10n.launchAgreement)
        string.append(font.attributesWithParagraph.colored(color: Color.black.value)
            .linking(string: self.termsOfUseUrl!.absoluteString)
            .underlying(color: Color.white.value)
            .make(string: L10n.sharedTermsOfUse))
        string.append(font.attributesWithParagraph.colored(color: Color.white.value).make(string: " " + L10n.sharedAnd))
        string.append(font.attributesWithParagraph.colored(color: Color.white.value)
            .linking(string: self.policyPrivacyUrl!.absoluteString)
            .underlying(color: Color.white.value)
            .make(string: " " + L10n.sharedPrivacyPolicy))
        textView.attributedText = string
        textView.linkTextAttributes = font.attributesWithParagraph.underlying(color: Color.white.value).colored(color: Color.white.value)
        textView.isUserInteractionEnabled = true
        textView.textAlignment = .center
        textView.delegate = self
        textView.backgroundColor = UIColor.clear
        return textView
    }()
    
    private lazy var welcomeLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.headline.attributesWithParagraph.colored(color: Color.white.value).make(string: L10n.launchWelcome)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.appName.attributesWithParagraph.colored(color: Color.white.value).make(string: L10n.launchApp)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.toastPresenter.targetView = self.view
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.imageView.contentMode = .scaleAspectFill
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.overlayView)
        self.view.addSubview(self.termsOfUseLabel)
        self.view.addSubview(self.button)
        self.view.addSubview(self.instagramButton)
        self.view.addSubview(self.welcomeLabel)
        self.view.addSubview(self.titleLabel)
        
        self.button.onTouch = { [weak self] in
            self?.onPhoneNumber?()
        }
        
        self.instagramButton.onTouch = { [weak self] in
            self?.onInstagram?()
        }
        
        self.setupConstraints()
    }
    
    private func setupConstraints() {
        self.imageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        self.overlayView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        self.termsOfUseLabel.snp.makeConstraints {
            $0.bottom.equalTo(self.bottomLayoutEdge).offset(-30)
            $0.centerX.equalToSuperview()
        }
        
        self.button.snp.makeConstraints {
            $0.bottom.equalTo(self.instagramButton.snp.top).offset(-10)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        
        self.instagramButton.snp.makeConstraints {
            $0.bottom.equalTo(self.termsOfUseLabel.snp.top).offset(-70)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.welcomeLabel.snp.makeConstraints {
            $0.bottom.equalTo(self.button.snp.top).offset(-35)
            $0.centerX.equalToSuperview()
        }
        self.titleLabel.snp.makeConstraints {
            $0.top.equalTo(self.topLayoutEdge).offset(self.topOffset)
            $0.centerX.equalToSuperview()
        }
    }
}

extension LaunchViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        self.handleURL(URL)
        return false
    }
    
    private func handleURL(_ url: URL) {
        if url == self.termsOfUseUrl {
            self.onTermsOfUse?()
        } else if url == self.policyPrivacyUrl {
            self.onPrivacyPolicy?()
        }
    }
}
