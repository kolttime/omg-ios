//
//  PrivacyViewController.swift
//  OMG
//
//  Created by Roman Makeev on 12/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Foundation
import UIKit
class PrivacyViewController: LargeTitleScrollViewController, ToastAlertPresentable {
    
    private let apiService: ApiService
    private let backTitle: String
    private let privacy: Privacy
    private lazy var scrollViewController = ScrollViewController()
    private lazy var textView = UITextView.makeStaticTextView()
    var onDismiss: Action?
    var toastPresenter = ToastAlertPresenter()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return refreshControl
    }()
    
    private lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    init(privacy: Privacy,
         backTitle: String,
         apiService: ApiService) {
        self.backTitle = backTitle
        self.apiService = apiService
        self.privacy = privacy
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.toastPresenter.targetView = self.view
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backButtonTitle = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: self.backTitle)
        self.backgroundBarColor = Color.white.value
        self.view.backgroundColor = Color.white.value
        self.add(self.scrollViewController)
        self.scrollViewController.scrollView.addSubview(self.textView)
        self.view.addSubview(self.loadingLabel)
        self.loadingLabel.blink()
        self.scrollViewController.scrollView.alwaysBounceVertical = true
        self.setupScroll(scrollInteraction: self.scrollViewController, refreshControl: self.refreshControl)
        self.fetchPrivacy()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.isMovingFromParent {
            self.onDismiss?()
        }
    }
    
    func fetchPrivacy() {
        self.apiService.fetchPrivacy(privacy: self.privacy) { [weak self] result in
            self?.refreshControl.endRefreshing()
            self?.loadingLabel.isHidden = true
            switch result {
            case .success(let privacyResponse):
                self?.largeTitle = CustomFont.headline.attributesWithParagraph.colored(color: Color.black.value).make(string: privacyResponse.title)
                self?.navigationTitle = CustomFont.bodyBold.attributesWithParagraph.make(string: privacyResponse.title)
                self?.textView.attributedText = CustomFont.paragraph.attributesWithParagraph.colored(color: Color.black.value).make(string: privacyResponse.text)
            case .failure(let error):
                self?.showToastErrorAlert(error)
            }
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.scrollViewController.view.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.textView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(5)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.height.equalTo(self.scrollViewController.view.snp.height)
            $0.width.equalTo(self.scrollViewController.view.snp.width).offset(-2 * ViewSize.sideOffset)
            $0.bottom.equalToSuperview()
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    @objc
    func refresh() {
        self.fetchPrivacy()
    }
    
    override func needShowTabbar() -> Bool {
        return false
    }
}
