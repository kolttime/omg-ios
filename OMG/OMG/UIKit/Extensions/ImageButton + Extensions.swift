//
//  ImageButton + Extensions.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

extension ImageButton {
    
    static func makeShareButton() -> ImageButton {
        let button = ImageButton()
        button.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedShare)
        return button
    }
    
    static func makeCancelButton() -> ImageButton {
        let button = ImageButton()
        button.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedCancel)
        return button
    }
    
    static func makeResetButton() -> ImageButton {
        let button = ImageButton()
        button.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedReset)
        return button
    }
    
    static func makeEditButton() -> ImageButton {
        let button = ImageButton()
        button.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.profileEdit)
        return button
    }
    
    static func makeSkipButton() -> ImageButton {
        let button = ImageButton()
        button.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.profileNext)
        return button
    }
    
}
