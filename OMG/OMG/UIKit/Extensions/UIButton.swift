//
//  UIButton.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    static func makeLargeTitleButtonAction(title: String) -> UIButton {
        let button = UIButton(type: .system)
        button.setAttributedTitle(CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: title),
                                  for: .normal)
        button.contentEdgeInsets = UIEdgeInsets.zero
        return button
    }
    
    static func makeShareButton() -> UIButton {
        return self.makeLargeTitleButtonAction(title: L10n.sharedShare)
    }
}
