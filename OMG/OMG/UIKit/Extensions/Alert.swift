//
//  Alert.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

extension AirdronError {
    
    var mdwErrorReason: String {
        if self.code == -1009 {
            return L10n.sharedNoInternet
        }
        if self.code == 504 || self.code == 522  {
            return L10n.sharedTimeoutError
        } else {
            return L10n.sharedApplicationError
        }
    }
}
