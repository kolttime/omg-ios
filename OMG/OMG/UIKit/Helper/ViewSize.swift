//
//  ViewSize.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class ViewSize {
    
    static var sideOffset: CGFloat {
        if Device.isIphone5 {
            return 15
        } else {
            return 20
        }
    }
}
