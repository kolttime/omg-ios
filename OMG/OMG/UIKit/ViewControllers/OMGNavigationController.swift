//
//  OMGNavigationController.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

class OMGNavigationBarViewController: NavigationBarViewController {
    
    override func initialSetup() {
        super.initialSetup()
        self.backButtonImage = #imageLiteral(resourceName: "Back Arrow.pdf")
        self.backButtonTintColor = Color.grey44.value
        self.backgroundBarColor = Color.white.value
    }
}
