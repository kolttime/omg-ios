//
//  InstagramConnectionLabel.swift
//  OMG
//
//  Created by Roman Makeev on 14/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class InstagramConnectionView: AirdronView {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey30.value).make(string: L10n.profileInstagram)
        return label
    }()
    
    private lazy var detailImageView: UIImageView = {
        let label = UIImageView(image: #imageLiteral(resourceName: "Check"))
        label.setHugging(priority: .required)
        label.setResistance(priority: .required)
        return label
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.white.value
        self.addSubview(self.titleLabel)
        self.addSubview(self.detailImageView)
        self.invalidateIntrinsicContentSize()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.detailImageView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        self.titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(self.detailImageView.snp.right).offset(6)
            $0.right.equalToSuperview().offset(-10)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 25)
    }
}
