//
//  OMGTextField.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import InputMask

typealias TextFieldResponse = (text: String, mandatoryComplete: Bool)

protocol ModifyTextDelegate: class {
    
    func textField(_ textField: OMGTextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String)
}


class OMGTextField: AirdronView {
    
    private let contentHeight: CGFloat = 50
    var needShowTopLabel = true {
        didSet {
            self.titleLabel.alpha = self.needShowTopLabel ? 1 : 0
        }
    }
    
    var onTextDidChanged: ((TextFieldResponse) -> Void)?
    var onReturnHandler: Action?
    var onBeginEditing: ((OMGTextField) -> Void)?
    
    private lazy var titleLabel = UILabel()
    lazy var textField = UITextField()
    lazy var separatorView = UIView()
    
    lazy var detailLabel = UILabel()
    
    // MARK: Can not use with modifyTextFieldDelegate simultaneously
    var maskedTextViewHandler: MaskedTextFieldDelegate? {
        didSet {
            if let handler = self.maskedTextViewHandler {
                self.textField.removeTarget(self, action: #selector(textDidChange), for: .editingChanged)
                handler.listener = self
                self.textField.delegate = handler
            } else {
                if !self.textField.canPerformAction(#selector(textDidChange), withSender: self) {
                    self.textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
                }
                self.textField.delegate = self
            }
        }
    }
    
    // MARK: Can not use with maskedTextViewHandler simultaneously
    weak var modifyTextFieldDelegate: ModifyTextDelegate?
    
    private let placeholderAttributes = CustomFont.large.attributes.colored(color: Color.grey30.value)
    private let inputTextAttributes = CustomFont.large.attributes.colored(color: Color.black.value)
    private let titleAttributes = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey44.value)
    private let detailAttributes = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey30.value)

    var textAlignment: NSTextAlignment = .left {
        didSet {
            self.textField.textAlignment = self.textAlignment
        }
    }
    var placeholder: String = String.empty {
        didSet {
            self.textField.attributedPlaceholder = self.placeholderAttributes.make(string: self.placeholder)
            self.titleLabel.attributedText = self.titleAttributes.make(string: self.placeholder)
        }
    }
    var keyboardType: UIKeyboardType = .default {
        didSet {
            self.textField.keyboardType = self.keyboardType
        }
    }
    
    var returnKeyType: UIReturnKeyType = .default {
        didSet {
            self.textField.returnKeyType = self.returnKeyType
        }
    }
    
    var autocorrectionType: UITextAutocorrectionType = .default {
        didSet {
            self.textField.autocorrectionType = self.autocorrectionType
        }
    }
    
    var autocapitalizationType: UITextAutocapitalizationType = .sentences {
        didSet {
            self.textField.autocapitalizationType = self.autocapitalizationType
        }
    }
    
    var isSecureTextEntry: Bool {
        set {
            self.textField.isSecureTextEntry = newValue
        }
        get {
            return self.textField.isSecureTextEntry
        }
    }
    
    var text: String? {
        set {
            self.textField.text = newValue
            guard self.needShowTopLabel else { return }
            if newValue?.isEmpty == false {
                self.titleLabel.alpha = 1
            } else {
                self.titleLabel.alpha = 0
            }
        }
        get {
            return self.textField.text
        }
    }
    
    var detailText: String? {
        didSet {
            if let text = self.detailText {
                self.detailLabel.attributedText = self.detailAttributes.make(string: text)
            } else {
                self.detailLabel.attributedText = nil
            }
        }
    }
    
    func resignResponder() {
        self.textField.resignFirstResponder()
    }
    
    func becomeResponder() {
        self.textField.becomeFirstResponder()
    }
    
    private func handle(text: String, mandatoryComplete: Bool) {
        self.onTextDidChanged?((text: self.textField.text ?? String.empty, mandatoryComplete: mandatoryComplete))
        guard self.needShowTopLabel else { return }
        if self.textField.text?.isEmpty == false {
            UIView.animate(withDuration: 0.2) {
                self.titleLabel.alpha = 1
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.titleLabel.alpha = 0
            }
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.addSubview(self.textField)
        self.addSubview(self.separatorView)
        self.addSubview(self.detailLabel)

        self.separatorView.backgroundColor = Color.grey10.value
        self.textField.defaultTextAttributes = self.inputTextAttributes
        self.titleLabel.alpha = 0
        self.textField.delegate = self
        self.textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        self.setNeedsUpdateConstraints()
        self.invalidateIntrinsicContentSize()
    }
    
    @objc
    func textDidChange() {
        self.handle(text: self.textField.text ?? String.empty, mandatoryComplete: true)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(1).priority(ConstraintPriority.high)
            $0.right.equalToSuperview()
        }
        self.textField.snp.makeConstraints {
            $0.left.equalToSuperview().priority(ConstraintPriority.high)
            $0.right.equalToSuperview()
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(5)
        }
        self.separatorView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.detailLabel.snp.makeConstraints {
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-8)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentHeight)
    }
    
    func showError() {
        self.shake()
        self.textField.becomeFirstResponder()
        UIView.animate(withDuration: UIView.shakeDuration, animations: { self.separatorView.backgroundColor = Color.black.value })
    }
}

extension OMGTextField: MaskedTextFieldDelegateListener {
    
    func textField(_ textField: UITextField,
                   didFillMandatoryCharacters complete: Bool,
                   didExtractValue value: String) {
        self.handle(text: value, mandatoryComplete: complete)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onReturnHandler?()
        return true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let modifyDelegate = self.modifyTextFieldDelegate {
            modifyDelegate.textField(self,
                                     shouldChangeCharactersIn: range,
                                     replacementString: string)
            self.handle(text: self.textField.text ?? String.empty, mandatoryComplete: true)
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.onBeginEditing?(self)
        self.separatorView.backgroundColor = Color.black.value
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.separatorView.backgroundColor = Color.grey10.value
    }
    
    // Need because it is implemented in radmadrobots MaskedTextFieldDelegateListener
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.separatorView.backgroundColor = Color.grey10.value
    }
}

extension OMGTextField {
    
    static func makeInputNumberTextField() -> OMGTextField {
        let field = OMGTextField()
        field.needShowTopLabel = false
        field.placeholder = L10n.loginPhoneNumber
        field.autocorrectionType = .no
        field.separatorView.isHidden = true
        field.keyboardType = .phonePad
        return field
    }
    
    static func makeSearch() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.sharedSearch
        field.autocorrectionType = .no
        return field
    }
    
    static func makeGivenName() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileGivenName
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func makeFamilyName() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileFamilyName
        field.autocorrectionType = .no
        return field
    }
    
    static func makeBirthday() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileBirthday
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[00]{.}[00]{.}[0000]")
        field.autocorrectionType = .no
        field.keyboardType = .numberPad
        return field
    }
    
    static func makeCity() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileCity
        field.autocorrectionType = .no
        field.returnKeyType = .next
        return field
    }
    
    static func makeEmail() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileEmail
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.keyboardType = .emailAddress
        return field
    }
    
    static func makeHeight() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileHeight
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.detailText = L10n.sharedCm
        field.autocorrectionType = .no
        field.keyboardType = .numbersAndPunctuation
        field.returnKeyType = .next
        return field
    }
    
    static func makeWeight() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileWeight
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.detailText = L10n.sharedKg
        field.autocorrectionType = .no
        field.keyboardType = .numbersAndPunctuation
        field.returnKeyType = .next
        return field
    }
    
    static func makeBust() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileBust
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.detailText = L10n.sharedCm
        field.autocorrectionType = .no
        field.keyboardType = .numbersAndPunctuation
        field.returnKeyType = .next
        return field
    }
    
    static func makeWaist() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileWaist
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.detailText = L10n.sharedCm
        field.autocorrectionType = .no
        field.keyboardType = .numbersAndPunctuation
        field.returnKeyType = .next
        return field
    }
    
    static func makeHip() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileHips
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.detailText = L10n.sharedCm
        field.autocorrectionType = .no
        field.keyboardType = .numbersAndPunctuation
        field.returnKeyType = .next
        return field
    }
    
    static func makeFoorwear() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.profileShoe
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.detailText = L10n.sharedEu
        field.autocorrectionType = .no
        field.keyboardType = .numbersAndPunctuation
        return field
    }
    
    static func makeСardNumber() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.portfolioPaymentCardNumber
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[0000]{ }[0000]{ }[0000]{ }[0000]")
        field.autocorrectionType = .no
        field.keyboardType = .numberPad
        return field
    }
    
    static func makeValidThru() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.portfolioPaymentValidThru
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[00]{/}[00]")
        field.autocorrectionType = .no
        field.keyboardType = .numberPad
        return field
    }
    
    static func makeCardCode() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.portfolioPaymentCode
        field.maskedTextViewHandler = MaskedTextFieldDelegate(primaryFormat: "[000]")
        field.keyboardType = .numberPad
        field.isSecureTextEntry = true
        return field
    }
    
    static func makeСardHolder() -> OMGTextField {
        let field = OMGTextField()
        field.placeholder = L10n.portfolioPaymentCardholder
        field.autocorrectionType = .no
        return field
    }
}
