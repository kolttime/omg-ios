//
//  CodeFieldView.swift
//  MDW
//
//  Created by Roman Makeev on 06/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol CodeFieldViewDelegate: class {
    
    func codeDidChage(_ code: String)
    func didEnterCode(_ code: String)
}

class CodeFieldView: AirdronView, UIKeyInput {
    
    weak var delegate: CodeFieldViewDelegate?
    
    private var code: [String] = [] {
        didSet {
            self.delegate?.codeDidChage(self.code.joined())
        }
    }
    
    private var characters: [CodeCharacterView] = []
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .equalSpacing
        return stackView
    }()
    
    private let length: Int = 5
    private var index = 0

    override func initialSetup() {
        super.initialSetup()
        self.generateViews()
        self.characters.forEach { self.stackView.addArrangedSubview($0) }
        self.addSubview(self.stackView)
        self.setNeedsUpdateConstraints()
    }
    
    private func generateViews() {
        for _ in 0..<self.length {
            let view = CodeCharacterView()
            self.addSubview(view)
            self.characters.append(view)
        }
    }
    
    func reset() {
        self.index = 0
        self.code = []
        for characterView in self.characters {
            characterView.clear()
        }
    }
    
    func setCode(_ code: String) {
        self.reset()
        guard code.count <= self.length  else {
            return
        }
        for char in code {
            self.insertText(String(char))
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.stackView.snp.makeConstraints {
            $0.left.equalToSuperview().priority(ConstraintPriority.high)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.right.equalToSuperview()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 120, height: 25)
    }
    
    // MARK: UIKeyInput
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    var hasText: Bool {
        return !self.code.isEmpty
    }
    
    func insertText(_ text: String) {
        guard !text.isEmpty, self.code.count < self.length else {
            return
        }
        self.characters[self.index].setChar(text)
        self.code.append(text)
        if index + 1 == self.length {
            self.delegate?.didEnterCode(self.code.joined())
        }
        self.index += 1
    }
    
    func deleteBackward() {
        guard !self.code.isEmpty else {
            return
        }
        self.index -= 1
        self.characters[self.index].clear()
        _ = self.code.popLast()
    }
    
    var keyboardType: UIKeyboardType {
        get {
            return .numberPad
        }
        set {
            
        }
    }
}
