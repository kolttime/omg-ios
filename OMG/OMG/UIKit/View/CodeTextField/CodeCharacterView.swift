//
//  CodeCharacterView.swift
//  MDW
//
//  Created by Roman Makeev on 06/08/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

import Foundation
import UIKit

public class CodeCharacterView: AirdronView {
    
    private lazy var codeLabel: UILabel = UILabel()
    
    override public func initialSetup() {
        super.initialSetup()
        self.addSubview(self.codeLabel)
        self.codeLabel.textAlignment = .center
        self.clipsToBounds = true
        self.backgroundColor = UIColor.white
        self.clear()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.codeLabel.frame = self.bounds
    }
    
    public override var intrinsicContentSize: CGSize {
        return CGSize(width: 20, height: 25)
    }
    
    func setChar(_ char: String) {
        guard !char.isEmpty else {
            self.clear()
            return
        }
        self.codeLabel.attributedText = CustomFont.large.attributes.colored(color: Color.black.value).make(string: char)
        self.codeLabel.textAlignment = .center
    }
    
    func clear() {
        self.codeLabel.attributedText = CustomFont.large.attributes.colored(color: Color.grey30.value).make(string: "—")
        self.codeLabel.textAlignment = .center
    }
}
