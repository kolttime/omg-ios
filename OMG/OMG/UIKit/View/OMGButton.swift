//
//  OMGButton.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class OMGButton: AirdronControl {
    
    var onTouch: Action?
    var onDisableTouch: Action?
    
    private let textStyle: CustomFont = .bodyRegular
    private let borderColor: UIColor
    private let borderWidth: CGFloat = 1
    private var progress: Double = 0 {
        didSet {
            if self.progress >= 0 && self.progress <= 1 {
                UIView.animate(withDuration: 4) {
                    self.progressView.frame.size.width = self.bounds.width * CGFloat(self.progress)
                }
            }
        }
    }
    private var showProgress: Bool = false {
        didSet {
            self.progressView.isHidden = !self.showProgress
        }
    }
    
    private let normalColor: UIColor
    private let disabledColor: UIColor
    private let progressColor: UIColor
    private let doneColor: UIColor
    private let uploadingColor: UIColor
    private let highlightColor: UIColor
    
    private let normalTextColor: UIColor
    private let disabledTextColor: UIColor
    private let progressTextColor: UIColor
    private let doneTextColor: UIColor
    private let uploadingTextColor: UIColor
    private let highlightTextColor: UIColor
    
    private let normalText: String
    private let disabledText: String
    private let progressText: String
    private let doneText: String
    private let uploadingText: String
    private let highlightText: String
    
    private lazy var titleLabel = UILabel()
    private lazy var progressView = UIView()
    
    init(normalColor: UIColor = Color.black.value,
         disabledColor: UIColor = Color.white.value,
         highlightColor: UIColor = Color.white.value,
         uploadingColor: UIColor = Color.white.value,
         progressColor: UIColor = Color.green.value,
         doneColor: UIColor = Color.red.value,
         borderColor: UIColor = Color.grey4.value,
         normalTextColor: UIColor = Color.white.value,
         disabledTextColor: UIColor = Color.grey77.value,
         progressTextColor: UIColor = Color.white.value,
         doneTextColor: UIColor = Color.white.value,
         uploadingTextColor: UIColor = Color.white.value,
         highlightTextColor: UIColor = Color.grey77.value,
         normalText: String = String.empty,
         disabledText: String = String.empty,
         progressText: String = String.empty,
         doneText: String = String.empty,
         uploadingText: String = String.empty,
         highlightText: String = String.empty) {
        self.normalColor = normalColor
        self.disabledColor = disabledColor
        self.progressColor = progressColor
        self.doneColor = doneColor
        self.uploadingColor = uploadingColor
        self.borderColor = borderColor
        self.highlightColor = highlightColor
        self.normalTextColor = normalTextColor
        self.disabledTextColor = disabledTextColor
        self.progressTextColor = progressTextColor
        self.doneTextColor = doneTextColor
        self.uploadingTextColor = uploadingTextColor
        self.highlightTextColor = highlightTextColor
        self.normalText = normalText
        self.disabledText = disabledText
        self.progressText = progressText
        self.doneText = doneText
        self.uploadingText = uploadingText
        self.highlightText = highlightText
        
        super.init(frame: CGRect.zero)
        self.addTarget(self, action: #selector(self.touchDown), for: .touchDown)
        self.addTarget(self, action: #selector(self.touchUpInside), for: .touchUpInside)
        self.addTarget(self, action: #selector(self.touchUpOutside), for: .touchUpOutside)
    }
    
    @objc
    func touchDown() {
        if case .normal = self.buttonState {
            self.backgroundColor = self.highlightColor
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.highlightTextColor)
                .make(string: self.highlightText)
        }
    }
    
    @objc
    func touchUpInside() {
        if case .normal = self.buttonState {
            self.backgroundColor = self.normalColor
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.normalTextColor)
                .make(string: self.normalText)
            self.onTouch?()
        } else if case .disabled = self.buttonState {
            self.onDisableTouch?()
        }
    }
    
    @objc
    func touchUpOutside() {
        if case .normal = self.buttonState {
            self.backgroundColor = self.normalColor
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.normalTextColor)
                .make(string: self.normalText)
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    enum ViewState {
        case normal
        case disabled
        case uploadingProgress(progress: Double)
        case uploading
        case done
    }
    
    var buttonState: ViewState = .normal
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.progressView)
        self.addSubview(self.titleLabel)
        self.backgroundColor = self.normalColor
        self.layer.borderColor = self.borderColor.cgColor
        self.progressView.isHidden = true
        self.update(state: self.buttonState)
        self.setNeedsUpdateConstraints()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.progressView.snp.makeConstraints {
            $0.left.equalToSuperview()
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        self.titleLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview().offset(-2.5)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 60)
    }
    
    func update(state: ViewState) {
        self.buttonState = state
        switch state {
        case .normal:
            self.showProgress = false
            self.backgroundColor = self.normalColor
            self.layer.borderWidth = 0
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.normalTextColor)
                .make(string: self.normalText)
        case .disabled:
            self.showProgress = false
            self.backgroundColor = self.disabledColor
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.disabledTextColor)
                .make(string: self.disabledText)
        case .done:
            self.showProgress = false
            self.backgroundColor = self.doneColor
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.doneTextColor)
                .make(string: self.doneText)
        case .uploading:
            self.showProgress = false
            self.backgroundColor = self.uploadingColor
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.uploadingTextColor)
                .make(string: self.uploadingText)
            
        case .uploadingProgress(let progress):
            self.showProgress = true
            self.progress = progress
            self.progressView.backgroundColor = self.progressColor
            self.backgroundColor = self.uploadingColor
            self.layer.borderWidth = 0
            self.titleLabel.attributedText = self.textStyle.attributesWithParagraph.colored(color: self.progressTextColor)
                .make(string: self.progressText)
        }
    }
}

extension OMGButton {
    
    static func makePrimaryButton(normalText: String = String.empty,
                                  disabledText: String = String.empty,
                                  progressText: String = String.empty,
                                  doneText: String = String.empty,
                                  uploadingText: String = String.empty,
                                  highlightText: String = String.empty,
                                  normalColor: UIColor = Color.black.value) -> OMGButton {
        return OMGButton(normalColor: normalColor,
                         disabledColor: Color.grey4.value,
                         highlightColor: Color.black33.value,
                         uploadingColor: Color.grey4.value,
                         progressColor: Color.grey4.value,
                         doneColor: Color.grey4.value,
                         normalTextColor: Color.white.value,
                         disabledTextColor: Color.grey30.value,
                         progressTextColor: Color.grey44.value,
                         doneTextColor: Color.grey30.value,
                         uploadingTextColor: Color.grey44.value,
                         highlightTextColor: Color.grey30.value,
                         normalText: normalText,
                         disabledText: disabledText,
                         progressText: progressText,
                         doneText: doneText,
                         uploadingText: uploadingText,
                         highlightText: highlightText)
    }
    
    static func makeLaunchPhoneNumberButton() -> OMGButton {
        return self.makePrimaryButton(normalText: L10n.loginPhoneNumber,
                                      disabledText: L10n.loginPhoneNumber,
                                      progressText: L10n.loginPhoneNumber,
                                      doneText: L10n.loginPhoneNumber,
                                      uploadingText: L10n.loginPhoneNumber,
                                      highlightText: L10n.loginPhoneNumber)
    }
    
    static func makeInstagramButton() -> OMGButton {
        return self.makePrimaryButton(normalText: L10n.profileInstagram,
                                      disabledText: L10n.profileInstagram,
                                      progressText: L10n.profileInstagram,
                                      doneText: L10n.profileInstagram,
                                      uploadingText: L10n.profileInstagram,
                                      highlightText: L10n.profileInstagram)
    }
    
    static func makeGetCode() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.loginGetCode,
                                           disabledText: L10n.loginGetCode,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.loginGetCode)
    }
    
    static func makeResend() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.sharedResendCode,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.sharedResendCode)
    }
    
    static func makeStart() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.sharedStart,
                                           uploadingText: L10n.sharedStart,
                                           highlightText: L10n.sharedStart)
    }
    
    static func makeCastingSend() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.castingButtonSend,
                                           disabledText: L10n.castingButtonPending,
                                           doneText: L10n.castingButtonClosed,
                                           highlightText: L10n.castingButtonSend)
    }
    
    static func makeNext() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.profileNext,
                                           disabledText: L10n.profileNext,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.profileNext)
    }
    
    static func makeDiscoverCastings() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.profileDoneDiscovers,
                                           disabledText: L10n.profileDoneDiscovers,
                                           highlightText: L10n.profileDoneDiscovers)
    }
    
    static func makeUpdate() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.sharedUpdate,
                                           disabledText: L10n.sharedUpdate,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.sharedUpdate)
    }
    
    static func makeSend() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.sharedSend,
                                           disabledText: L10n.sharedSend,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.sharedSend)
    }
    
    static func makeContinueFillProfile() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.castingFormInfoContinue,
                                           disabledText: L10n.castingFormInfoContinue,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.castingFormInfoContinue)
    }
    
    static func makeUploadNew() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.portfolioUploadNew,
                                           disabledText: L10n.portfolioUploadNew,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.portfolioUploadNew)
    }
    
    static func makeRemove(count: Int) -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.portfolioRemove(count),
                                           disabledText: L10n.portfolioRemove(count),
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.portfolioRemove(count),
                                           normalColor: Color.red.value)
    }
    
    static func makeUpload(count: Int) -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.portfolioUpload(count),
                                           disabledText: L10n.portfolioUpload(count),
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.portfolioUpload(count))
    }
    
    static func makeCheckout() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.portfolioCheckout,
                                           disabledText: L10n.portfolioCheckout,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.portfolioCheckout)
    }
    
    static func makePayCreditCard() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.portfolioPayCard,
                                           disabledText: L10n.portfolioPayCard,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.portfolioPayCard)
    }
    
    static func makeProceed() -> OMGButton {
        return OMGButton.makePrimaryButton(normalText: L10n.portfolioPolaroidProceed,
                                           disabledText: L10n.portfolioPolaroidProceed,
                                           uploadingText: L10n.sharedJustMoment,
                                           highlightText: L10n.portfolioPolaroidProceed)
    }
}
