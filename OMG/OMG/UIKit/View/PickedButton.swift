//
//  PickedButton.swift
//  OMG
//
//  Created by Roman Makeev on 19/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

enum PickedButtonType {
    
    case unselected
    case picking
    case unpicked
    case picked(string: String)
}

class PickedButton: AirdronControl {
    
    private let contentHeight: CGFloat = 50
    
    private lazy var textLabel = UILabel()
    private lazy var pickedLabel = UILabel()
    private lazy var titleLabel = UILabel()

    private lazy var separatorView = UIView()
    private let titleAttributes = CustomFont.tech.attributesWithParagraph.colored(color: Color.grey44.value)
    private let textAttributes = CustomFont.large.attributes
    private let pickedText = "↓"
    private let pickedColor = Color.black.value
    private let unpickedColor = Color.grey30.value
    
    var defaultText: String = String.empty {
        didSet {
            self.textLabel.attributedText = self.textAttributes.colored(color: self.unpickedColor).make(string: self.defaultText)
            self.titleLabel.attributedText = self.titleAttributes.make(string: self.defaultText)
        }
    }
    
    var pickedState: PickedButtonType = .unselected {
        didSet {
            switch self.pickedState {
            case .unselected:
                self.textLabel.attributedText = self.textAttributes.colored(color: self.unpickedColor).make(string: self.defaultText)
                self.pickedLabel.attributedText = self.textAttributes.colored(color: self.unpickedColor).make(string: self.pickedText)
            case .picking:
                self.textLabel.attributedText = self.textAttributes.colored(color: self.pickedColor).make(string: self.textLabel.text!)
                self.pickedLabel.attributedText = self.textAttributes.colored(color: self.pickedColor).make(string: self.pickedText)
            case .unpicked:
                self.textLabel.attributedText = self.textAttributes.colored(color: self.unpickedColor).make(string: self.defaultText)
               self.pickedLabel.attributedText = self.textAttributes.colored(color: self.unpickedColor).make(string: self.pickedText)
            case .picked(let string):
                self.textLabel.attributedText = self.textAttributes.colored(color: self.pickedColor).make(string: string)
                self.titleLabel.attributedText = self.titleAttributes.make(string: self.defaultText)
                self.titleLabel.isHidden = false
            }
        }
    }
    
    var selectedText: String = String.empty
    
    override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.titleLabel)
        self.addSubview(self.textLabel)
        self.addSubview(self.separatorView)
        self.addSubview(self.pickedLabel)
        self.defaultText = String.empty
        self.separatorView.backgroundColor = Color.grey10.value
        self.titleLabel.isHidden = true
        self.pickedLabel.setResistance(priority: .required)
        self.pickedLabel.setHugging(priority: .required)
        self.pickedLabel.attributedText = self.textAttributes.colored(color: self.unpickedColor).make(string: self.pickedText)
        self.invalidateIntrinsicContentSize()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.pickedLabel.snp.makeConstraints {
            $0.right.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-4)
        }
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview().offset(1).priority(ConstraintPriority.high)
            $0.right.equalToSuperview()
        }
        self.textLabel.snp.makeConstraints {
            $0.left.equalToSuperview().priority(ConstraintPriority.high)
            $0.right.equalTo(self.pickedLabel.snp.left).offset(16)
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(5)
        }
        self.separatorView.snp.makeConstraints {
            $0.bottom.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                self.alpha = 0.5
            } else {
                self.alpha = 1.0
            }
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.contentHeight)
    }
}

extension PickedButton {
    
    static func makeSex() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileSex
        return pickedButton
    }
    
    static func makeCountry() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileCountry
        return pickedButton
    }
    
    static func makeClothingSize() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileClothingSize
        return pickedButton
    }
    
    static func makeTattoos() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileTattoos
        return pickedButton
    }
    
    static func makeHair() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileHair
        return pickedButton
    }
    
    static func makeEyes() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileEyes
        return pickedButton
    }
    
    static func makeCitizenship() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileCitizenship
        return pickedButton
    }
    
    static func makeValidInternationalPassport() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileValidInternationalPassport
        return pickedButton
    }
    
    static func makeMotherAgency() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileMotherAgency
        return pickedButton
    }
    
    static func makeLanguage() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileLanguage
        return pickedButton
    }
    
    static func makeLanguageLevel() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileLanguageLevel
        return pickedButton
    }
    
    static func makeFootwear() -> PickedButton {
        let pickedButton = PickedButton()
        pickedButton.defaultText = L10n.profileShoe
        return pickedButton
    }
}
