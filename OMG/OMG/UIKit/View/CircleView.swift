//
//  CircleView.swift
//  OMG
//
//  Created by Roman Makeev on 17/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class CircleView: AirdronView {
    
    public var mainColor: UIColor = Color.grey4.value {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    public override func initialSetup() {
        super.initialSetup()
        self.contentMode = .redraw
        self.backgroundColor = UIColor.clear
    }
    
    public override func draw(_ rect: CGRect) {
        let path = UIBezierPath(ovalIn: rect)
        self.mainColor.setFill()
        path.fill()
    }
}
