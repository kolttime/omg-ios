//
//  SectionImageButton.swift
//  OMG
//
//  Created by Roman Makeev on 13/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SectionImageButton: AirdronControl {
    
    private let titleAttributes = CustomFont.bodyRegular.attributesWithParagraph
    private let height: CGFloat
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var detailImageView: UIImageView = {
        let label = UIImageView()
        label.setHugging(priority: .required)
        label.setResistance(priority: .required)
        return label
    }()
    
    
    var text: String = String.empty {
        didSet {
            self.titleLabel.attributedText = self.titleAttributes.make(string: self.text)
        }
    }
    
    override func initialSetup() {
        super.initialSetup()
        self.backgroundColor = Color.grey4.value
        self.addSubview(self.titleLabel)
        self.addSubview(self.detailImageView)
        self.invalidateIntrinsicContentSize()
    }
    
    init(height: CGFloat, image: UIImage) {
        self.height = height
        super.init(frame: CGRect.zero)
        self.detailImageView.image = image
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.detailImageView.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-15)
            $0.top.equalToSuperview().offset(10)
        }
        self.titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(10)
            $0.left.equalToSuperview().offset(15)
            $0.right.equalTo(self.detailImageView.snp.left).offset(-10)
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.height)
    }
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                self.alpha = 0.5
            } else {
                self.alpha = 1.0
            }
        }
    }
}

extension SectionImageButton {
    
    static func makeSupport() -> SectionImageButton {
        let button = SectionImageButton(height: 50, image: #imageLiteral(resourceName: "Support"))
        button.text = L10n.profileDoneConnectSocial
        return button
    }
}
