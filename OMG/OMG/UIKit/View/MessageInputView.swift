//
//  MessageInputView.swift
//  OMG
//
//  Created by Roman Makeev on 14/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

import Foundation
import UIKit
import NextGrowingTextView

public class MessageInputView: AirdronView {
    
    public var onTouched: Action?
    private lazy var inputTextView = NextGrowingTextView()
    private let image: UIImage = #imageLiteral(resourceName: "Paperplane")
    private lazy var imageButton = ImageButton()
    
    public var text: String! {
        get {
            return self.inputTextView.textView.text
        }
        
        set {
            self.inputTextView.textView.text = newValue
        }
    }
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.grey10.value
        return view
    }()
    
    public var placeholder: String = "" {
        didSet {
            self.inputTextView.placeholderAttributedText = CustomFont.bodyRegular.attributes.colored(color: Color.grey30.value).make(string: self.placeholder)
        }
    }
    
    public override func initialSetup() {
        super.initialSetup()
        self.imageButton.image = self.image
        self.imageButton.tintColor = Color.black.value
        self.addSubview(self.inputTextView)
        self.addSubview(self.imageButton)
        self.addSubview(self.separatorView)
        self.backgroundColor = UIColor.white
        self.inputTextView.backgroundColor = Color.white.value
        self.inputTextView.textView.delegate = self
        self.imageButton.addTarget(self, action: #selector(self.tap), for: .touchUpInside)
//        self.inputTextView.textView.typingAttributes = CustomFont.bodyRegular.attributesWithParagraph
        self.setNeedsUpdateConstraints()
    }
    
    public override func setupConstraints() {
        super.setupConstraints()
        self.separatorView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        self.imageButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-8)
            $0.size.equalTo(self.image.size)
            $0.right.equalToSuperview().offset(-10)
        }
        self.inputTextView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(8)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalTo(self.imageButton.snp.left).offset(-12)
            $0.bottom.equalToSuperview().offset(-8)
        }
    }
    
    @objc
    func tap() {
        self.onTouched?()
    }
    
    func resign() {
        self.inputTextView.resignFirstResponder()
    }
    func firstResponder() {
        self.inputTextView.becomeFirstResponder()
    }
}

extension MessageInputView: UITextViewDelegate {
    
    public func textViewDidChange(_ textView: UITextView) {
//        textView.typingAttributes = CustomFont.bodyRegular.attributesWithParagraph
    }
}
