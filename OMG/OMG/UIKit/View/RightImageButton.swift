//
//  RightImageButton.swift
//  OMG
//
//  Created by Roman Makeev on 19/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

open class RightImageButton: AirdronControl {
    
    private lazy var titleLabel = UILabel()
    private lazy var imageView = UIImageView()
    
    public var text: NSAttributedString? {
        didSet {
            self.titleLabel.attributedText = self.text
            self.removeConstraints()
            self.addConstraints()
            self.invalidateIntrinsicContentSize()
        }
    }
    
    public var image: UIImage? {
        didSet {
            self.imageView.image = self.image
            self.removeConstraints()
            self.addConstraints()
            self.invalidateIntrinsicContentSize()
        }
    }
    
    public var imageTintColor: UIColor? {
        didSet {
            self.imageView.tintColor = self.imageTintColor ?? UIColor.clear
        }
    }
    
    public var space: CGFloat = 6 {
        didSet {
            self.removeConstraints()
            self.addConstraints()
            self.invalidateIntrinsicContentSize()
        }
    }
    
    open override func initialSetup() {
        super.initialSetup()
        self.addSubview(self.imageView)
        self.addSubview(self.titleLabel)
        self.setNeedsUpdateConstraints()
        self.invalidateIntrinsicContentSize()
    }
    
    override open func setupConstraints() {
        super.setupConstraints()
        self.removeConstraints()
        self.addConstraints()
    }
    
    func removeConstraints() {
        self.imageView.snp.removeConstraints()
        self.titleLabel.snp.removeConstraints()
    }
    
    func addConstraints() {
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().priority(ConstraintPriority.high)
            $0.centerY.equalToSuperview()
        }
        self.imageView.snp.makeConstraints {
            let space = self.titleLabel.text == nil ? 0 : self.space
            $0.left.equalTo(self.titleLabel.snp.right).offset(space)
            $0.centerY.equalToSuperview()
        }
    }
    
    open override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                self.imageView.alpha = 0.5
                self.titleLabel.alpha = 0.5
            } else {
                self.imageView.alpha = 1.0
                self.titleLabel.alpha = 1.0
            }
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        let space = (self.image != nil && self.text != nil) ? self.space : 0
        let titleHeight: CGFloat = self.text?.height(width: CGFloat.greatestFiniteMagnitude) ?? 0
        let titleWidth: CGFloat = self.text?.width(height: titleHeight) ?? 0
        let height = max(titleHeight, self.image?.size.height ?? 0)
        let width = titleWidth + space + (self.image?.size.width ?? 0)
        return CGSize(width: width, height: height)
    }
}

extension RightImageButton {
    
    static func makeSupportButton() -> RightImageButton {
        let button = RightImageButton()
        button.text = CustomFont.bodyRegular.attributesWithParagraph.make(string: "Veronika")
        button.image = #imageLiteral(resourceName: "veronika")
        return button
    }
}
