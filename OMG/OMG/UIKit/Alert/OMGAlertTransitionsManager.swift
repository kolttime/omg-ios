//
//  OMGAlertTransitionsManager.swift
//  OMG
//
//  Created by Roman Makeev on 10/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

final class OMGAlertTransitioningManager: NSObject, UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        return OMGAlertPresentationController(presentedViewController: presented,
                                              presenting: presenting)
    }
}
