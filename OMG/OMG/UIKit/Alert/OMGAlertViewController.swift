//
//  OMGAlertViewController.swift
//  OMG
//
//  Created by Roman Makeev on 10/12/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class OMGAlertViewController: UIViewController {
        
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = Color.white.value
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fill
        stackView.spacing = 30
        return stackView
    }()
    
    var actions: [ImageButton] = [] {
        didSet {
            self.stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
            self.actions.forEach { self.stackView.addArrangedSubview($0) }
        }
    }
    
    var titleError: NSAttributedString? = nil {
        didSet {
            self.titleLabel.attributedText = titleError
        }
    }
    
    var message: NSAttributedString? = nil {
        didSet {
            self.messageLabel.attributedText = self.message
        }
    }
    
    var heightConstraint: Constraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.messageLabel)
        self.view.addSubview(self.stackView)
        
        self.titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalToSuperview().offset(20)
        }
        
        self.messageLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.titleLabel.snp.bottom).offset(10)
        }
        
        self.stackView.snp.makeConstraints {
            $0.height.equalTo(25)
            $0.right.equalToSuperview().offset(-30)
            $0.bottom.equalToSuperview().offset(-30)
        }
    }
}

extension OMGAlertViewController {
    
    static func makeInstagramDisconnecting(target: Any?, cancelSelector: Selector, detachSelector: Selector) -> OMGAlertViewController {
        let controller = OMGAlertViewController()
        controller.titleError = CustomFont.large.attributesWithParagraph.make(string: L10n.profileInstagramAlertTitle)
        controller.message = CustomFont.bodyRegular.attributesWithParagraph.make(string: L10n.profileInstagramAlertMessage)
        
        let cancel = ImageButton()
        cancel.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.black.value).make(string: L10n.sharedCancel)
        cancel.addTarget(target, action: cancelSelector, for: .touchUpInside)
        
        let detach = ImageButton()
        detach.text = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.red.value).make(string: L10n.sharedDetach)
        detach.addTarget(target, action: detachSelector, for: .touchUpInside)

        controller.actions = [cancel, detach]
        return controller
    }
}
