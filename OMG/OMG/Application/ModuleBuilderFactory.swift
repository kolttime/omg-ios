//
//  ModuleBuilderFactory.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol ModuleBuilderFactory {
    
    func makeMenuModuleBuilder() -> MenuModuleBuilder
    
    func makeAuthModuleBuilder() -> AuthModuleBuilder
    
    func makeCastingsModuleBuilder() -> CastingsModuleBuilder
    
    func makeProfileModuleBuilder() -> ProfileModuleBuilder
    
    func makePortfolioModuleBuilder() -> PortfolioModuleBuilder
}

final class ModuleBuilderFactoryImp: ModuleBuilderFactory {
    
    private let serviceContainer: ServiceContainer
    
    init(serviceContainer: ServiceContainer) {
        self.serviceContainer = serviceContainer
    }
    
    func makeMenuModuleBuilder() -> MenuModuleBuilder {
        return MenuModuleBuilder()
    }
    
    func makeAuthModuleBuilder() -> AuthModuleBuilder {
        return AuthModuleBuilder(apiService: self.serviceContainer.apiService,
                                 sessionManager: self.serviceContainer.sessionManager)
    }
    
    func makeCastingsModuleBuilder() -> CastingsModuleBuilder {
        return CastingsModuleBuilder(apiService: self.serviceContainer.apiService)
    }
    
    func makeProfileModuleBuilder() -> ProfileModuleBuilder {
        return ProfileModuleBuilder(apiService: self.serviceContainer.apiService)
    }
    
    func makePortfolioModuleBuilder() -> PortfolioModuleBuilder {
        return PortfolioModuleBuilder(apiService: self.serviceContainer.apiService)
    }
}
