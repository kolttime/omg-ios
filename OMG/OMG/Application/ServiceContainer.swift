//
//  ServiceContainer.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol ServiceContainer {
    
    var apiService: ApiService { get }
    var sessionManager: OMGUserSessionManager { get }
}

final class ServiceContainerImp: ServiceContainer {
    
    private let networkManager: NetworkManager
    private let uploadManager: UploadManager
    private let backgroundNetworkManager: NetworkManager
    private let backgroundUploadManager: UploadManager
    private let credential: CredentialStorage
    
    init(networkManager: NetworkManager,
         uploadManager: UploadManager,
         backgroundNetworkManager: NetworkManager,
         backgroundUploadManager: UploadManager,
         credential: CredentialStorage) {
        self.networkManager = networkManager
        self.uploadManager = uploadManager
        self.backgroundNetworkManager = backgroundNetworkManager
        self.backgroundUploadManager = backgroundUploadManager
        self.credential = credential
    }
    
    lazy var apiService: ApiService = ApiServiceImp(networkManager: self.networkManager,
                                                    uploadManager: self.uploadManager,
                                                    backgroundUploadManager: self.backgroundUploadManager,
                                                    sessionManager: self.sessionManager,
                                                    credential: self.credential)
    
    lazy var sessionManager = OMGUserSessionManager(credential: self.credential)
}
