//
//  CoordinatorFactory.swift
//  OMG
//
//  Created by Roman Makeev on 06/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol CoordinatorPresentable {
    
    var coordinatorFactory: CoordinatorFactory { get set }
}

protocol CoordinatorFactory {
    
    func makeMenuCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> MenuCoordinator
    
    func makeAuthCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> AuthCoordinator
    
    func makeCastingsCoordinator(presenter: AirdronNavigationController,
                                 coordinatorFactory: CoordinatorFactory) -> CastingsCoordinator
    
    func makeProfileCoordinator(presenter: AirdronNavigationController,
                                coordinatorFactory: CoordinatorFactory) -> ProfileCoordinator
    
    func makePortfolioCoordinator(presenter: AirdronNavigationController,
                                  coordinatorFactory: CoordinatorFactory) -> PortfolioCoordinator
}

final class CoordinatorFactoryImp: CoordinatorFactory {
    
    private let moduleBuilderFactory: ModuleBuilderFactory
    
    init(moduleBuilderFactory: ModuleBuilderFactory) {
        self.moduleBuilderFactory = moduleBuilderFactory
    }
    
    func makeMenuCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> MenuCoordinator {
        return MenuCoordinator(presenter: presenter,
                               moduleBuilder: self.moduleBuilderFactory.makeMenuModuleBuilder(),
                               coordinatorFactory: self)
    }
    
    func makeAuthCoordinator(presenter: AirdronNavigationController,
                             coordinatorFactory: CoordinatorFactory) -> AuthCoordinator {
        return AuthCoordinator(presenter: presenter,
                               moduleBuilder: self.moduleBuilderFactory.makeAuthModuleBuilder(),
                               coordinatorFactory: self)
    }
    
    func makeCastingsCoordinator(presenter: AirdronNavigationController,
                                 coordinatorFactory: CoordinatorFactory) -> CastingsCoordinator {
        return CastingsCoordinator(presenter: presenter,
                                   moduleBuilder: self.moduleBuilderFactory.makeCastingsModuleBuilder(),
                                   coordinatorFactory: coordinatorFactory)
    }
    
    func makeProfileCoordinator(presenter: AirdronNavigationController,
                                coordinatorFactory: CoordinatorFactory) -> ProfileCoordinator {
        return ProfileCoordinator(presenter: presenter,
                                  moduleBuilder: self.moduleBuilderFactory.makeProfileModuleBuilder(),
                                  coordinatorFactory: coordinatorFactory)
    }
    
    func makePortfolioCoordinator(presenter: AirdronNavigationController,
                                  coordinatorFactory: CoordinatorFactory) -> PortfolioCoordinator {
        return PortfolioCoordinator(presenter: presenter,
                                    moduleBuilder: self.moduleBuilderFactory.makePortfolioModuleBuilder(),
                                    coordinatorFactory: coordinatorFactory)
    }
}
