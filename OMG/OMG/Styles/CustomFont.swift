//
//  CustomFont.swift
//  OMG
//
//  Created by Roman Makeev on 04/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

enum CustomFont {
    
    case appName
    case headline
    case large
    case bodyBold
    case bodyRegular
    case paragraphHeading
    case paragraph
    case tech
    case termsOfUse
    case footnote
    
    static let spectralSemibold = "Spectral-SemiBold"
    static let spectralRegular = "Spectral-Regular"
    static let suisseRegular = "SuisseIntl-Regular"
    static let suisseBold = "SuisseIntl-Bold"
    static let suisseSemibold = "SuisseIntl-SemiBold"

    var font: UIFont {
        switch self {
        case .appName:
            return UIFont(name: CustomFont.suisseBold, size: 60)!
        case .headline:
            if Device.isIphone5 {
                return UIFont(name: CustomFont.suisseBold, size: 24)!
            } else {
                return UIFont(name: CustomFont.suisseBold, size: 29)!
            }
        case .large:
            return UIFont(name: CustomFont.suisseSemibold, size: 22)!
        case .bodyBold:
            return UIFont(name: CustomFont.suisseSemibold, size: 17)!
        case .bodyRegular:
            return UIFont(name: CustomFont.suisseRegular, size: 17)!
        case .paragraphHeading:
            return UIFont(name: CustomFont.spectralSemibold, size: 17)!
        case .paragraph:
            return UIFont(name: CustomFont.spectralRegular, size: 17)!
        case .tech:
            return UIFont(name: CustomFont.suisseRegular, size: 13)!
        case .termsOfUse:
            return UIFont(name: CustomFont.suisseRegular, size: 15)!
        case .footnote:
            return UIFont(name: CustomFont.spectralRegular, size: 13)!
        }
    }
    
    var color: UIColor {
        switch self {
        default:
            return Color.black.value
        }
    }
    
    var letterSpacing: NSNumber {
        switch self {
        case .headline:
            return NSNumber(value: -0.5)
        case .large:
            return NSNumber(value: -0.1)
        case .paragraphHeading:
            return NSNumber(value: -0.5)
        case .paragraph:
            return NSNumber(value: -0.5)
        default:
            return NSNumber(value: 0.0)
        }
    }
    
    var lineHeight: CGFloat {
        switch self {
        case .appName:
            return 70
        case .headline:
            if Device.isIphone5 {
                return 30
            } else {
                return 35
            }
        case .large:
            return 25
        case .bodyBold:
            return 25
        case .bodyRegular:
            return 25
        case .paragraphHeading:
            return 25
        case .paragraph:
            return 25
        case .tech:
            return 15
        case .termsOfUse:
            return 20
        case .footnote:
            return 15
        }
    }
    
    var lineHeightMultiple: CGFloat {
        return self.lineHeight / self.font.lineHeight
    }
    
    var paragraphStyle: NSMutableParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = self.lineHeightMultiple
        return paragraphStyle
    }
    
    var attributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor : self.color,
                NSAttributedString.Key.font : self.font,
                NSAttributedString.Key.kern: self.letterSpacing]
    }
    
    var attributesWithParagraph: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor : self.color,
                NSAttributedString.Key.font : self.font,
                NSAttributedString.Key.kern: self.letterSpacing,
                NSAttributedString.Key.paragraphStyle: self.paragraphStyle]
    }
}
