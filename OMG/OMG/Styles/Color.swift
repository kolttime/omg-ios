//
//  Color.swift
//  OMG
//
//  Created by Roman Makeev on 04/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

enum Color {
    
    case white
    case black
    case black33
    case grey77
    case grey44
    case grey30
    case grey10
    case grey4
    case red
    case green
    
    var value: UIColor {
        switch self {
        case .white:
            return UIColor.white
        case .black:
            return UIColor(hashRgbaValue: 0x000000FF)
        case .black33:
            return UIColor(hashRgbaValue: 0x333333FF)
        case .grey77:
            return UIColor(hashRgbaValue: 0x555555FF)
        case .grey44:
            return UIColor(hashRgbaValue: 0x909090FF)
        case .grey30:
            return UIColor(hashRgbaValue: 0xB2B2B2FF)
        case .grey10:
            return UIColor(hashRgbaValue: 0xE5E5E5FF)
        case .grey4:
            return UIColor(hashRgbaValue: 0xF5F5F5FF)
        case .red:
            return UIColor(hashRgbaValue: 0xE52E2EFF)
        case .green:
            return UIColor(hashRgbaValue: 0x23B26AFF)
        
        }
    }
}
