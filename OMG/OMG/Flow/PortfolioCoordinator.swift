//
//  PortfolioCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 04/11/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Gallery
import Appsee

protocol PortfolioCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startPortfolioFlow(presenter: AirdronNavigationController)
}

extension PortfolioCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startPortfolioFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makePortfolioCoordinator(presenter: presenter,
                                                                          coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class PortfolioCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: PortfolioModuleBuilder
    private let galleryImageSelector = GalleryMultiselectImageSelector()
    private let gallerySingleImageSelector = GalleryImageSelector()

    init(presenter: AirdronNavigationController,
         moduleBuilder: PortfolioModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showAlbumListModule()
    }
    
    func showAlbumListModule() {
        let viewController = self.moduleBuilder.makePortfolioAlbumList()
        viewController.onAlbum = { [weak self, weak viewController] album in
            guard let albumListViewController = viewController else { return }
            if album.id == "polaroid" {
                self?.showPolaroidModule(albumShort: album)
            } else {
                self?.showAlbumModule(albumShort: album,
                                      albumListViewController: albumListViewController)
            }
        }
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
    func showAlbumModule(albumShort: AlbumShort, albumListViewController: PortfolioAlbumListViewController) {
        let viewController = self.moduleBuilder.makeAlbumViewController(albumShort: albumShort)
        viewController.onFullPhoto = { [weak self] image in
            self?.showFullPhotoModule(image: image)
        }
        viewController.onUploadNew = { [weak self, weak viewController, weak albumListViewController] albumShort in
            guard let albumViewController = viewController else { return }
            guard let albumListViewController = albumListViewController else { return }

            self?.showAlbumUploadModule(albumShort: albumShort,
                                        albumViewController: albumViewController,
                                        albumListViewController: albumListViewController)
        }
        viewController.onUpdateCount = { [weak albumListViewController] albumId, count in
            albumListViewController?.update(count: count, atAlbumId: albumId)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showFullPhotoModule(image: Image) {
        let viewController = self.moduleBuilder.maleFullPhotoView(image: image)
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showAlbumUploadModule(albumShort: AlbumShort,
                               albumViewController: AlbumViewController,
                               albumListViewController: PortfolioAlbumListViewController) {
        
        let viewController = self.moduleBuilder.makeUploadPhotoNew(albumShort: albumShort)
        viewController.onAddImages = { [weak self, weak viewController] in
            self?.showImagePicker { images in
                viewController?.add(images: images)
            }
        }
        viewController.onUploadedImages = { [weak self, weak albumViewController, weak albumListViewController] _, images in
            self?.presenter.popViewController(animated: true)
            albumViewController?.add(images: images)
            albumListViewController?.add(count: images.count, atAlbumId: albumShort.id)
        }
        viewController.onPayment = { [weak self] albumShort, images in
            self?.showPaymentModule(albumShort: albumShort, images: images)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showImagePicker(selectedImagesHandler: @escaping (([Gallery.Image]) -> Void)) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab]
        Config.Camera.imageLimit = 10
        galleryController.delegate = self.galleryImageSelector
        self.galleryImageSelector.onSelectImages = { images in
            selectedImagesHandler(images)
        }
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            Appsee.markView(asSensitive: galleryController.view)
        }
    }
    
    func showPaymentModule(albumShort: AlbumShort, images: [AlbumImage]) {
        let viewController = self.moduleBuilder.makePayment(albumShort: albumShort, albumImages: images)
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showPolaroidModule(albumShort: AlbumShort) {
        let viewController = self.moduleBuilder.makeAlbumPolaroid(albumShort: albumShort)
        viewController.onFaceProfileRight = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateFaceProfileRightImage(image: image)
            }
        }
        viewController.onFaceProfileLeft = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateFaceProfileLeft(image: image)
            }
        }
        viewController.onFacePortrait = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateFacePortrait(image: image)
            }
        }
        viewController.onRightSideFullLength = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateRightSideFullLength(image: image)
            }
        }
        viewController.onLeftSideFullLength = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateLeftSideFullLength(image: image)
            }
        }
        viewController.onBehindFullHeight = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateBehindFullHeight(image: image)
            }
        }
        viewController.onFrontFullLength = { [weak self, weak viewController] in
            self?.showSingleImagePicker { image in
                viewController?.updateFrontFullLength(image: image)
            }
        }
        viewController.onInfo = { [weak self] album, images in
            self?.showInfoModule(albumShort: album, images: images)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showInfoModule(albumShort: AlbumShort, images: [AlbumImage]) {
        let viewController = self.moduleBuilder.makePolaroidInfo(albumShort: albumShort,
                                                                 albumImages: images)
        viewController.onPayment = { [weak self] album, images in
            self?.showPaymentModule(albumShort: album, images: images)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showSingleImagePicker(selectedImageHandler: @escaping ((Gallery.Image) -> Void)) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        galleryController.delegate = self.gallerySingleImageSelector
        self.gallerySingleImageSelector.onSelectImage = { image in
            selectedImageHandler(image)
        }
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            Appsee.markView(asSensitive: galleryController.view)
        }
    }
}

