//
//  ProfileCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 22/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Gallery
import Appsee

protocol ProfileCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startProfileFlow(presenter: AirdronNavigationController)
}

extension ProfileCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startProfileFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makeProfileCoordinator(presenter: presenter,
                                                                         coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class ProfileCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: ProfileModuleBuilder
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: ProfileModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showProfileModule()
    }
    
    func showProfileModule() {
        let viewController = self.moduleBuilder.makeProfileMain()
        viewController.onEdit = { [weak self, weak viewController] model in
            guard let viewController = viewController else { return }
            self?.showEditProfile(model: model, profileViewController: viewController)
        }
        viewController.onImagePicker = { [weak self, weak viewController] in
            guard let self = self, let viewController = viewController else { return }
            self.showImagePicker(viewController: viewController)
        }
        viewController.onEditBody = { [weak self, weak viewController] model in
            self?.showEditBodyModule(model: model) { updatedModel in
                viewController?.update(model: updatedModel)
            }
        }
        viewController.onEditAdditionalInfo = { [weak self, weak viewController] model in
            self?.showEditAdditionalInformationModule(model: model) { updatedModel in
                viewController?.update(model: updatedModel)
            }
        }
        viewController.onSocialNetworkInfo = { [weak self, weak viewController] model in
            self?.showEditSocialNetworkModule(model: model) { updatedModel in
                viewController?.update(model: updatedModel)
            }
        }
        viewController.onSupport = { [weak self] in
           self?.startSupportModule()
        }
        
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
    func showEditProfile(model: Model, profileViewController: ProfileMainViewController) {
        let viewController = self.moduleBuilder.makeEditProfile(model: model)
        viewController.onEditBody = { [weak self, weak viewController, weak profileViewController] model in
            self?.showEditBodyModule(model: model) { updatedModel in
                profileViewController?.update(model: updatedModel)
                viewController?.update(model: updatedModel)
            }
        }
        viewController.onEditMain = { [weak self, weak viewController, weak profileViewController] model in
            self?.showEditMainModule(model: model) { updatedModel in
                profileViewController?.update(model: updatedModel)
                viewController?.update(model: updatedModel)
            }
        }
        viewController.onEditAdditional = { [weak self, weak viewController, weak profileViewController] model in
            self?.showEditAdditionalInformationModule(model: model) { updatedModel in
                profileViewController?.update(model: updatedModel)
                viewController?.update(model: updatedModel)
            }
        }
        viewController.onEditSocialNetworks = { [weak self, weak viewController, weak profileViewController] model in
            self?.showEditSocialNetworkModule(model: model) { updatedModel in
                profileViewController?.update(model: updatedModel)
                viewController?.update(model: updatedModel)
            }
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showEditBodyModule(model: Model, completion: ((Model) -> Void)?) {
        let viewController = self.moduleBuilder.makeEditBody(model: model)
        viewController.onUpdate = { [weak self] model in
            completion?(model)
            let _ = self?.presenter.popViewController(animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
   
    func showEditMainModule(model: Model, completion: ((Model) -> Void)?) {
        let viewController = self.moduleBuilder.makeEditMain(model: model)
        viewController.onUpdate = { [weak self] model in
            completion?(model)
            let _ = self?.presenter.popViewController(animated: true)
        }
        viewController.onCountySelect = { [weak self, weak viewController] in
            self?.startSearchCountryModule { country in
                viewController?.update(country: country)
            }
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showEditAdditionalInformationModule(model: Model, completion: ((Model) -> Void)?) {
        let viewController = self.moduleBuilder.makeAdditionalInformationEdit(model: model)
        viewController.onUpdate = { [weak self] model in
            completion?(model)
            let _ = self?.presenter.popViewController(animated: true)
        }
        viewController.onCountySelect = { [weak self, weak viewController] in
            self?.startSearchCountryModule { country in
                viewController?.update(country: country)
            }
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showEditSocialNetworkModule(model: Model, completion: ((Model) -> Void)?) {
        let viewController = self.moduleBuilder.makeSocialNetworks(model: model)
        viewController.onUpdate = { [weak self] model in
            completion?(model)
        }
        viewController.onInstagram = { [weak self, weak viewController] token in
            guard let viewController = viewController else { return }
            self?.startInstagramModule(token: token,
                                       parentViewController: viewController)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showImagePicker(viewController: ProfileMainViewController) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        galleryController.delegate = viewController
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            Appsee.markView(asSensitive: galleryController.view)
        }
    }
    
    func startSearchCountryModule(completion: @escaping ((Country) -> Void)) {
        let module = self.moduleBuilder.makeSearchCountryModule()
        module.onSelectCounty = { [weak self] country in
            _ = self?.presenter.popViewController(animated: true)
            completion(country)
        }
        self.presenter.pushViewController(module, animated: true)
    }
    
    func startSupportModule() {
        let module = self.moduleBuilder.makeSupportModule()
        self.presenter.pushViewController(module, animated: true)
    }
    
    func startInstagramModule(token: String, parentViewController: SocialNetworksViewController) {
        let module = self.moduleBuilder.makeInstagramModule(accessToken: token)
        module.onSuccess = { [weak self, weak parentViewController] in
            parentViewController?.updateConnection()
            self?.presenter.popViewController(animated: true)
        }
        self.presenter.pushViewController(module, animated: true)
    }
}
