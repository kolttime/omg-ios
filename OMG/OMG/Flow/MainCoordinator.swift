//
//  MainCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 07/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

final class MainCoordinator: BaseCoordinator,
MenuCoordinatorFlowPresentable,
AuthCoordinatorFlowPresentable {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: UIWindow
    private let sessionManager: OMGUserSessionManager
    private let navigationController = AirdronNavigationController()
    
    init(presenter: UIWindow,
         sessionManager: OMGUserSessionManager,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.coordinatorFactory = coordinatorFactory
        self.sessionManager = sessionManager
        super.init()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.logout),
                                               name: Notification.Name.OMGNotificationLogout,
                                               object: nil)
    }
    
    override  func start() {
        self.presenter.rootViewController = self.navigationController
        if !self.sessionManager.isRegistred {
            self.startAuthFlow(presenter: self.navigationController) { [weak self] in
                guard let self = self else { return }
                self.startMenuFlow(presenter: self.navigationController)
            }
        } else {
            self.startMenuFlow(presenter: self.navigationController)
        }
    }
    
    @objc
    func logout() {
        self.childCoordinators.removeAll()
        self.start()
    }
}
