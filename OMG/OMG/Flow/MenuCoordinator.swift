//
//  MenuCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

protocol MenuCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startMenuFlow(presenter: AirdronNavigationController)
}

extension MenuCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startMenuFlow(presenter: AirdronNavigationController) {
        let coordinator = self.coordinatorFactory.makeMenuCoordinator(presenter: presenter, coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.start()
    }
}

class MenuCoordinator: BaseCoordinator, CastingsCoordinatorFlowPresentable, ProfileCoordinatorFlowPresentable, PortfolioCoordinatorFlowPresentable {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: MenuModuleBuilder
    private let tabbarViewController = AirdronTabbarController()
    
    enum TabIndex: Int {
        case castings
        case portfolio
        case profile
        
        var image: UIImage {
            switch self {
            case .castings:
                return #imageLiteral(resourceName: "Castings.pdf")
            case .portfolio:
                return #imageLiteral(resourceName: "Portfolio")
            case .profile:
                return #imageLiteral(resourceName: "Profile.pdf")
            }
        }
        
        var text: String {
            switch self {
            case .castings:
                return L10n.sharedCastings
            case .portfolio:
                return L10n.sharedPortfolio
            case .profile:
                return L10n.sharedProfile
            }
        }
        
        static let allValues: [TabIndex] = [.castings, .portfolio, .profile]
    }
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: MenuModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        let tabbarViewController = self.makeTabbarController()
        self.presenter.setViewControllers([tabbarViewController], animated: false)
        tabbarViewController.selectTab(atIndex: TabIndex.castings.rawValue)
    }
    
    private func makeTabbarController() -> AirdronTabbarController {
        let tabbarViewController = AirdronTabbarController()
        tabbarViewController.viewControllers = self.makeTabControllers()
        tabbarViewController.separatorColor = Color.grey4.value
        tabbarViewController.onSelectedTab = { [weak self, weak tabbarViewController] index in
            guard let navigationController = tabbarViewController?.viewControllers[index] as? AirdronNavigationController else { return }
            switch TabIndex(rawValue: index) {
            case .castings?:
                if navigationController.topViewController == nil {
                    self?.startCastingsFlow(presenter: navigationController) {
                        tabbarViewController?.selectTab(atIndex: TabIndex.profile.rawValue)
                    }
                }
            case .portfolio?:
                if navigationController.topViewController == nil {
                    self?.startPortfolioFlow(presenter: navigationController)
                }
            case .profile?:
                if navigationController.topViewController == nil {
                    self?.startProfileFlow(presenter: navigationController)
                }
            default:
                break
            }
        }
        return tabbarViewController
    }
    
    private func makeTabControllers() -> [AirdronTabResponder] {
        return TabIndex.allValues.map { return self.makeTabWith(image: $0.image,
                                                                text: $0.text) }
        
    }
    
    private func makeTabWith(image: UIImage, text: String) -> AirdronTabResponder {
        let vc = AirdronNavigationController()
        let normalColor = Color.grey30.value
        let selectedColor = Color.black.value
        vc.airdronTabItem.normalImage = image
        vc.airdronTabItem.selectedImage = image
        vc.airdronTabItem.text = text
        vc.airdronTabItem.normalTextColor = normalColor
        vc.airdronTabItem.selectedTextColor = selectedColor
        vc.airdronTabItem.normalImageColor = normalColor
        vc.airdronTabItem.selectedImageColor = selectedColor
        vc.view.backgroundColor = Color.white.value
        return vc
    }
}
