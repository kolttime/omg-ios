//
//  AuthCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 11/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

protocol AuthCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startAuthFlow(presenter: AirdronNavigationController, completion: Action?)
}

extension AuthCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startAuthFlow(presenter: AirdronNavigationController, completion: Action?) {
        let coordinator = self.coordinatorFactory.makeAuthCoordinator(presenter: presenter, coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.onCompletion = {
            completion?()
        }
        coordinator.start()
    }
}

class AuthCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: AuthModuleBuilder
    private let navigationPresenter = AirdronNavigationController()
    var onCompletion: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: AuthModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.presenter.present(self.navigationPresenter, animated: false)
        self.startLaunchModule()
    }
    
    private func startLaunchModule() {
        let viewController = self.moduleBuilder.makeLaunchModule()
        viewController.onPhoneNumber = { [weak self] in
            self?.startPhoneNumberModule()
        }
        viewController.onTermsOfUse = { [weak self] in
            self?.startPrivacyModule(privacy: .termsOfUse)
        }
        viewController.onPrivacyPolicy = { [weak self] in
            self?.startPrivacyModule(privacy: .privacyPolicy)
        }
        viewController.onInstagram = { [weak self] in
            self?.startInstagramModule()
        }
        
        self.navigationPresenter.setViewControllers([viewController], animated: false)
    }
    
    private func startPhoneNumberModule() {
        let viewController = self.moduleBuilder.makePhoneNumberModule()
        viewController.onCancel = { [weak self] in
            _ = self?.navigationPresenter.popViewController(animated: true)
        }
        viewController.onVerification = { [weak self] phone, response in
            self?.startVerificationModule(phone: phone, authResponse: response)
        }
        viewController.onCallingCode = { [weak self, weak viewController] in
            self?.startSearchCountryModule { country in
                viewController?.update(callingCode: country.callingCode)
            }
        }
        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    
    func startVerificationModule(phone: String, authResponse: AuthResponse) {
        let module = self.moduleBuilder.makeVerificationModule(phone: phone, authResponse: authResponse)
        module.onCancel = { [weak self] in
            _ = self?.navigationPresenter.popViewController(animated: true)
        }
        module.onCompletion = { [weak self] in
            self?.navigationPresenter.dismiss(animated: true, completion: nil)
            self?.onCompletion?()
            self?.onFlowFinished?()
        }
        module.onActivation = { [weak self] in
            self?.startInfoModule()
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }
    
    func startPrivacyModule(privacy: Privacy) {
        let module = self.moduleBuilder.makePrivacyModule(privacy: privacy)
        self.navigationPresenter.pushViewController(module, animated: true)
    }
    
    func startInfoModule() {
        let module = self.moduleBuilder.makeInfoModule()
        module.onStart = { [weak self] in
            self?.startFillInfoModule()
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }
    
    func startSearchCountryModule(showCallingCode: Bool = true, completion: @escaping ((Country) -> Void)) {
        let module = self.moduleBuilder.makeSearchCountryModule(showCallingCode: showCallingCode)
        module.onSelectCounty = { [weak self] country in
            _ = self?.navigationPresenter.popViewController(animated: true)
            completion(country)
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }
    
    func startFillInfoModule() {
        
        let introduceModule = self.moduleBuilder.makeIntroduceModule()
        let contactInfoModule = self.moduleBuilder.makeContactInfoModule()
        let bodyModule = self.moduleBuilder.makeBodyModule()
        let doneModule = self.moduleBuilder.makeDoneModule()
        
        let module = self.moduleBuilder.makeFillInfoModule(controllers: [introduceModule,
                                                                         contactInfoModule,
                                                                         bodyModule,
                                                                         doneModule])
        contactInfoModule.onCountySelect = { [weak self, weak contactInfoModule] in
            self?.startSearchCountryModule(showCallingCode: false) { country in
                contactInfoModule?.update(country: country)
            }
        }
        
        module.onCancel = { [weak self] in
            _ = self?.navigationPresenter.popViewController(animated: true)
        }
        
        module.setController(at: introduceModule.pageIndex)
        
        introduceModule.onNext = { [weak module, unowned contactInfoModule] userActivation in
            contactInfoModule.set(user: userActivation)
            module?.setController(at: contactInfoModule.pageIndex)
        }
        
        contactInfoModule.onNext = { [weak self, weak module, unowned bodyModule] model in
            self?.onCompletion?()
            module?.onCancel = {
                self?.navigationPresenter.dismiss(animated: true)
            }
            module?.onSkip = {
                bodyModule.skipTap()
            }
            module?.setController(at: bodyModule.pageIndex)
            bodyModule.set(model: model)
        }
        
        bodyModule.onNext = { [weak module, unowned doneModule] model in
            module?.setController(at: doneModule.pageIndex)
            doneModule.set(model: model)
        }
        
        doneModule.onCastings = { [weak self] in
            self?.navigationPresenter.dismiss(animated: true)
        }
        
        doneModule.onEditAdditionalInfo = { [weak self, weak doneModule] model in
            self?.showEditAdditionalInformationModule(model: model) { updatedModel in
                doneModule?.set(model: updatedModel)
            }
        }
        doneModule.onSocialNetwork = { [weak self, weak doneModule] model in
            self?.showEditSocialNetworkModule(model: model) { updatedModel in
                doneModule?.set(model: updatedModel)
            }
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }
    
    func showEditAdditionalInformationModule(model: Model, completion: ((Model) -> Void)?) {
        let viewController = self.moduleBuilder.makeAdditionalInformationEdit(model: model)
        viewController.onUpdate = { [weak self] model in
            completion?(model)
            let _ = self?.navigationPresenter.popViewController(animated: true)
        }
        viewController.onCountySelect = { [weak self, weak viewController] in
            self?.startSearchCountryModule(showCallingCode: false) { country in
                viewController?.update(country: country)
            }
        }
        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    
    func startInstagramModule() {
        let module = self.moduleBuilder.makeInstagramModule()
        module.onCompletion = { [weak self] in
            self?.navigationPresenter.dismiss(animated: true, completion: nil)
            self?.onCompletion?()
            self?.onFlowFinished?()
        }
        module.onActivation = { [weak self] in
            let _ = self?.navigationPresenter.popViewController(animated: true)
            self?.startInfoModule()
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }
    
    func showEditSocialNetworkModule(model: Model, completion: ((Model) -> Void)?) {
        let viewController = self.moduleBuilder.makeSocialNetworks(model: model)
        viewController.onUpdate = { [weak self] model in
            completion?(model)
            let _ = self?.navigationPresenter.popViewController(animated: true)
        }
        viewController.onInstagram = { [weak self, weak viewController] token in
            guard let viewController = viewController else { return }
            self?.startInstagramBindModule(token: token,
                                           parentViewController: viewController)
        }
        self.navigationPresenter.pushViewController(viewController, animated: true)
    }
    
    func startInstagramBindModule(token: String, parentViewController: SocialNetworksViewController) {
        let module = self.moduleBuilder.makeInstagramBindModule(accessToken: token)
        module.onSuccess = { [weak self, weak parentViewController] in
            parentViewController?.updateConnection()
            let _ = self?.navigationPresenter.popViewController(animated: true)
        }
        self.navigationPresenter.pushViewController(module, animated: true)
    }
}
