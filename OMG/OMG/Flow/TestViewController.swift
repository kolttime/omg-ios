//
//  TestViewController.swift
//  MDW
//
//  Created by Roman Makeev on 22/07/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import InputMask

class TestTypographyViewController: LargeTitleRootViewController {
    
    private lazy var testButton: UIButton = {
        let button = UIButton(type: .system)
        button.setAttributedTitle(CustomFont.bodyRegular.attributesWithParagraph.make(string: "Menu"),
                                  for: UIControl.State())
        return button
    }()
    
    private lazy var headlineLabel = UILabel()
    private lazy var largeLabel = UILabel()
    private lazy var bodyBoldLabel = UILabel()
    private lazy var bodyRegularLabel = UILabel()
    private lazy var bodyRegularLinkLabel = UILabel()
    private lazy var paragraphLabel = UILabel()
    private lazy var techLabel = UILabel()

    override func initialSetup() {
        super.initialSetup()
        self.largeTitle = CustomFont.headline.attributesWithParagraph.make(string: "Typography")
        self.largeTitleActionView = self.testButton
        self.backgroundTitleColor = Color.white.value
        self.view.backgroundColor = UIColor.white
        self.headlineLabel.numberOfLines = 0
        self.largeLabel.numberOfLines = 0
        self.bodyBoldLabel.numberOfLines = 0
        self.bodyRegularLabel.numberOfLines = 0
        self.bodyRegularLinkLabel.numberOfLines = 0
        self.paragraphLabel.numberOfLines = 0
        self.techLabel.numberOfLines = 0

        self.view.addSubview(self.headlineLabel)
        self.view.addSubview(self.largeLabel)
        self.view.addSubview(self.bodyBoldLabel)
        self.view.addSubview(self.bodyRegularLabel)
        self.view.addSubview(self.bodyRegularLinkLabel)
        self.view.addSubview(self.paragraphLabel)
        self.view.addSubview(self.techLabel)

        self.headlineLabel.attributedText = CustomFont.headline.attributesWithParagraph.make(string: "Headline with two lines watercoolish text here")
        self.largeLabel.attributedText = CustomFont.large.attributesWithParagraph.make(string: "Full and unconditional acceptance of this offer, unconditional acceptance of")
        self.bodyBoldLabel.attributedText = CustomFont.bodyBold.attributesWithParagraph.make(string: "The agreement may be changed by Crost LLC at any time without")
        self.bodyRegularLabel.attributedText = CustomFont.bodyRegular.attributesWithParagraph.make(string: "The agreement may be changed by Crost LLC at any time without")
        self.bodyRegularLinkLabel.attributedText = CustomFont.bodyRegular.attributesWithParagraph.underlying(color: Color.black.value).make(string: "The agreement may be changed by Crost LLC at any time without")
        self.paragraphLabel.attributedText = CustomFont.paragraph.attributesWithParagraph.make(string: "Mobile application, filling out the requested form with his personal data , registration")
        self.techLabel.attributedText = CustomFont.tech.attributesWithParagraph.make(string: "For participation in events and events, including for the purpose of further")
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.headlineLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalToSuperview().offset(self.titleAreaHeight + 20)
        }
        self.largeLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.headlineLabel.snp.bottom).offset(20)
        }
        self.bodyBoldLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.largeLabel.snp.bottom).offset(20)
        }
        self.bodyRegularLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.bodyBoldLabel.snp.bottom).offset(20)
        }
        self.bodyRegularLinkLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.bodyRegularLabel.snp.bottom).offset(20)
        }
        self.paragraphLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.bodyRegularLinkLabel.snp.bottom).offset(20)
        }
        self.techLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalTo(self.paragraphLabel.snp.bottom).offset(20)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(self.bottomLayoutEdgeInset)
        print(self.topLayoutEdgeInset)

    }
}
