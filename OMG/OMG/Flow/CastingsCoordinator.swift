//
//  CastingsCoordinator.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import Gallery
import Appsee

protocol CastingsCoordinatorFlowPresentable: CoordinatorPresentable {
    
    func startCastingsFlow(presenter: AirdronNavigationController, profileHandler: Action?)
}

extension CastingsCoordinatorFlowPresentable where Self: BaseCoordinator {
    
    func startCastingsFlow(presenter: AirdronNavigationController, profileHandler: Action?) {
        let coordinator = self.coordinatorFactory.makeCastingsCoordinator(presenter: presenter,
                                                                          coordinatorFactory: self.coordinatorFactory)
        self.addDependency(coordinator)
        coordinator.onFlowFinished = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        coordinator.profileHandler = {
            profileHandler?()
        }
        coordinator.start()
    }
}

class CastingsCoordinator: BaseCoordinator {
    
    var coordinatorFactory: CoordinatorFactory
    private let presenter: AirdronNavigationController
    private let moduleBuilder: CastingsModuleBuilder
    private let galleryImageSelector = GalleryImageSelector()
    var profileHandler: Action?
    
    init(presenter: AirdronNavigationController,
         moduleBuilder: CastingsModuleBuilder,
         coordinatorFactory: CoordinatorFactory) {
        self.presenter = presenter
        self.moduleBuilder = moduleBuilder
        self.coordinatorFactory = coordinatorFactory
    }
    
    override func start() {
        self.showCastingListModule()
    }
    
    func showCastingListModule() {
        let viewController = self.moduleBuilder.makeCastingListModule()
        viewController.onCastingDetail = { [weak self] casting in
            self?.showCastingDetailModule(castingShort: casting)
        }
        self.presenter.setViewControllers([viewController], animated: false)
    }
    
    func showCastingDetailModule(castingShort: CastingShort) {
        let viewController = self.moduleBuilder.makeCastingDetailModule(castingShort: castingShort)
        viewController.onCastingForm = { [weak self, weak viewController] id, backTitle in
            guard let viewController = viewController else { return }
            self?.showCastingFormModule(castingId: id,
                                       backTitle: backTitle.formatedForBackTitle,
                                       castingDetailViewController: viewController)
        }
        
        viewController.onCastingInfo = { [weak self] backTitle in
            self?.showCastingInfoModule(backTitle: backTitle)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showCastingFormModule(castingId: String,
                               backTitle: String,
                               castingDetailViewController: CastingDetailViewController) {
        let viewController = self.moduleBuilder.makeCastingFormModule(castingId: castingId, backTitle: backTitle)
        viewController.onFillLenght = { [weak self, weak viewController] in
            self?.showImagePicker { image in
                viewController?.updateFillLength(image: image)
            }
        }
        viewController.onFaceAttach = { [weak self, weak viewController] in
            self?.showImagePicker { image in
                viewController?.updateFace(image: image)
            }
        }
        viewController.onSideView = { [weak self, weak viewController] in
            self?.showImagePicker { image in
                viewController?.updateSideView(image: image)
            }
        }
        viewController.onParticipate = { [weak self, weak castingDetailViewController] status in
            castingDetailViewController?.update(status: status)
            let _ = self?.presenter.popViewController(animated: true)
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showCastingInfoModule(backTitle: String) {
        let viewController = self.moduleBuilder.makeCastingInfoModule(backTitle: backTitle)
        viewController.onContinue = { [weak self] in
            guard let self = self else { return }
            let controller = self.presenter.viewControllers.first!
            self.presenter.setViewControllers([controller], animated: false)
            self.profileHandler?()
            
        }
        self.presenter.pushViewController(viewController, animated: true)
    }
    
    func showImagePicker(selectedImageHandler: @escaping ((Gallery.Image) -> Void)) {
        let galleryController = GalleryController()
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        galleryController.delegate = self.galleryImageSelector
        self.galleryImageSelector.onSelectImage = { image in
            selectedImageHandler(image)
        }
        self.presenter.present(galleryController, animated: true) { [unowned galleryController] in
            Appsee.markView(asSensitive: galleryController.view)
        }
    }
}
