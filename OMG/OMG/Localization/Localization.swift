// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable identifier_name line_length type_body_length
enum L10n {
  /// %d
  static func pluralAge(_ p1: Int) -> String {
    return L10n.tr("Localizable", "pluralAge", p1)
  }
  /// %d
  static func pluralImage(_ p1: Int) -> String {
    return L10n.tr("Localizable", "pluralImage", p1)
  }
  /// %d
  static func pluralImageUploaded(_ p1: Int) -> String {
    return L10n.tr("Localizable", "pluralImageUploaded", p1)
  }
  /// Applications are accepted until
  static let castingAcceptedUntil = L10n.tr("Localizable", "casting.acceptedUntil")
  /// Announced by
  static let castingAnnounced = L10n.tr("Localizable", "casting.announced")
  /// Application sent
  static let castingApplicationSent = L10n.tr("Localizable", "casting.application.sent")
  /// Casting is closed
  static let castingButtonClosed = L10n.tr("Localizable", "casting.button.closed")
  /// Your application is pending
  static let castingButtonPending = L10n.tr("Localizable", "casting.button.pending")
  /// Send an application
  static let castingButtonSend = L10n.tr("Localizable", "casting.button.send")
  /// Attach snap photos:
  static let castingFormAttachTitle = L10n.tr("Localizable", "casting.form.attachTitle")
  /// Face photo without makeup
  static let castingFormFace = L10n.tr("Localizable", "casting.form.face")
  /// Full-length photo in a black swimsuit
  static let castingFormFullLength = L10n.tr("Localizable", "casting.form.fullLength")
  /// Before send an application please fill your profile completely and answer a couple questions.
  static let castingFormInfo = L10n.tr("Localizable", "casting.form.info")
  /// Side-view face photo without makeup
  static let castingFormSideView = L10n.tr("Localizable", "casting.form.sideView")
  /// Application Form
  static let castingFormTitle = L10n.tr("Localizable", "casting.form.title")
  /// Continue fill profile
  static let castingFormInfoContinue = L10n.tr("Localizable", "casting.form.info.continue")
  /// Casting participation
  static let castingFormInfoParticipation = L10n.tr("Localizable", "casting.form.info.participation")
  /// Accepted
  static let castingStatusAccepted = L10n.tr("Localizable", "casting.status.accepted")
  /// Acceptance of applications over
  static let castingStatusClosed = L10n.tr("Localizable", "casting.status.closed")
  /// In review
  static let castingStatusInReview = L10n.tr("Localizable", "casting.status.inReview")
  /// Rejected
  static let castingStatusRejected = L10n.tr("Localizable", "casting.status.rejected")
  /// Reviewed
  static let castingStatusReviewed = L10n.tr("Localizable", "casting.status.reviewed")
  /// Your application status
  static let castingStatusTitle = L10n.tr("Localizable", "casting.status.title")
  /// Casting results
  static let castingStatusResultTitle = L10n.tr("Localizable", "casting.status.result.title")
  /// If you have any issue or question please write to us.
  static let chatEmpty = L10n.tr("Localizable", "chat.empty")
  /// By loggin in you are agreeing to the\n
  static let launchAgreement = L10n.tr("Localizable", "launch.agreement")
  /// OMG
  static let launchApp = L10n.tr("Localizable", "launch.app")
  /// Welcome!\nPlease Login
  static let launchWelcome = L10n.tr("Localizable", "launch.welcome")
  /// Get code
  static let loginGetCode = L10n.tr("Localizable", "login.getCode")
  /// We will send code by sms\nfor verify you and recognize
  static let loginInfoLabel = L10n.tr("Localizable", "login.infoLabel")
  /// Phone number
  static let loginPhoneNumber = L10n.tr("Localizable", "login.phoneNumber")
  /// Select country code
  static let loginSearchCountry = L10n.tr("Localizable", "login.searchCountry")
  /// Body parameters
  static let loginProfileBody = L10n.tr("Localizable", "login.profile.body")
  /// Contact info
  static let loginProfileContact = L10n.tr("Localizable", "login.profile.contact")
  /// Done
  static let loginProfileDone = L10n.tr("Localizable", "login.profile.done")
  /// Introduce yourself
  static let loginProfileIntroduce = L10n.tr("Localizable", "login.profile.introduce")
  /// Hello, again!
  static let loginStartHello = L10n.tr("Localizable", "login.start.hello")
  /// We want to get to know you little closer. Please leave us some information by filling your profile.
  static let loginStartInfo = L10n.tr("Localizable", "login.start.info")
  /// Checkout
  static let portfolioCheckout = L10n.tr("Localizable", "portfolio.checkout")
  /// Paid
  static let portfolioPaid = L10n.tr("Localizable", "portfolio.paid")
  /// Pay by Credit Card
  static let portfolioPayCard = L10n.tr("Localizable", "portfolio.payCard")
  /// Payment
  static let portfolioPayment = L10n.tr("Localizable", "portfolio.payment")
  /// %@ per images
  static func portfolioPerImages(_ p1: String) -> String {
    return L10n.tr("Localizable", "portfolio.perImages", p1)
  }
  /// Remove (%d)
  static func portfolioRemove(_ p1: Int) -> String {
    return L10n.tr("Localizable", "portfolio.remove", p1)
  }
  /// Total:
  static let portfolioTotalPrice = L10n.tr("Localizable", "portfolio.totalPrice")
  /// Upload (%d)
  static func portfolioUpload(_ p1: Int) -> String {
    return L10n.tr("Localizable", "portfolio.upload", p1)
  }
  /// Upload new
  static let portfolioUploadNew = L10n.tr("Localizable", "portfolio.uploadNew")
  /// Cardholder
  static let portfolioPaymentCardholder = L10n.tr("Localizable", "portfolio.payment.cardholder")
  /// Card number
  static let portfolioPaymentCardNumber = L10n.tr("Localizable", "portfolio.payment.cardNumber")
  /// Code
  static let portfolioPaymentCode = L10n.tr("Localizable", "portfolio.payment.code")
  /// Pay by Credit Card
  static let portfolioPaymentPayByCreditCard = L10n.tr("Localizable", "portfolio.payment.payByCreditCard")
  /// Valid thru
  static let portfolioPaymentValidThru = L10n.tr("Localizable", "portfolio.payment.validThru")
  /// All photos must be taken in good light. You should be in a black bathing suit, in heels, without makeup and with collected hair.
  static let portfolioPolaroidAttachInfo = L10n.tr("Localizable", "portfolio.polaroid.attachInfo")
  /// Behind, full height
  static let portfolioPolaroidBehindFullHeight = L10n.tr("Localizable", "portfolio.polaroid.behindFullHeight")
  /// Upload one stack of photos to polaroid album costs %@
  static func portfolioPolaroidCostInfo(_ p1: String) -> String {
    return L10n.tr("Localizable", "portfolio.polaroid.costInfo", p1)
  }
  /// Face portrait
  static let portfolioPolaroidFacePortrait = L10n.tr("Localizable", "portfolio.polaroid.facePortrait")
  /// Face profile left
  static let portfolioPolaroidFaceProfileLeft = L10n.tr("Localizable", "portfolio.polaroid.faceProfileLeft")
  /// Face profile right
  static let portfolioPolaroidFaceProfileRight = L10n.tr("Localizable", "portfolio.polaroid.faceProfileRight")
  /// Front, full length
  static let portfolioPolaroidFrontFillLenght = L10n.tr("Localizable", "portfolio.polaroid.frontFillLenght")
  /// Left side, full length
  static let portfolioPolaroidLeftSideFullLenght = L10n.tr("Localizable", "portfolio.polaroid.leftSideFullLenght")
  /// Proceed
  static let portfolioPolaroidProceed = L10n.tr("Localizable", "portfolio.polaroid.proceed")
  /// Right side, full length
  static let portfolioPolaroidRightSideFullLenght = L10n.tr("Localizable", "portfolio.polaroid.rightSideFullLenght")
  /// Additional information
  static let profileAddInfo = L10n.tr("Localizable", "profile.addInfo")
  /// Add language
  static let profileAddLanguage = L10n.tr("Localizable", "profile.addLanguage")
  /// Answer questions
  static let profileAnswerQuestions = L10n.tr("Localizable", "profile.answerQuestions")
  /// Birthday
  static let profileBirthday = L10n.tr("Localizable", "profile.birthday")
  /// Body parameters
  static let profileBodyInfo = L10n.tr("Localizable", "profile.bodyInfo")
  /// Bust
  static let profileBust = L10n.tr("Localizable", "profile.bust")
  /// Citizenship
  static let profileCitizenship = L10n.tr("Localizable", "profile.citizenship")
  /// City
  static let profileCity = L10n.tr("Localizable", "profile.city")
  /// Clothing size
  static let profileClothingSize = L10n.tr("Localizable", "profile.clothingSize")
  /// Congratulations!
  static let profileCongratulations = L10n.tr("Localizable", "profile.congratulations")
  /// Connect social network
  static let profileConnectSocialNetwork = L10n.tr("Localizable", "profile.connectSocialNetwork")
  /// Contact info
  static let profileContactInfo = L10n.tr("Localizable", "profile.contactInfo")
  /// Country
  static let profileCountry = L10n.tr("Localizable", "profile.country")
  /// Done
  static let profileDone = L10n.tr("Localizable", "profile.done")
  /// Edit
  static let profileEdit = L10n.tr("Localizable", "profile.edit")
  /// Email
  static let profileEmail = L10n.tr("Localizable", "profile.email")
  /// Eyes
  static let profileEyes = L10n.tr("Localizable", "profile.eyes")
  /// Family name
  static let profileFamilyName = L10n.tr("Localizable", "profile.familyName")
  /// Fill body information
  static let profileFillBodyInfo = L10n.tr("Localizable", "profile.fillBodyInfo")
  /// Fill progress %@
  static func profileFillProgress(_ p1: String) -> String {
    return L10n.tr("Localizable", "profile.fillProgress", p1)
  }
  /// Given name
  static let profileGivenName = L10n.tr("Localizable", "profile.givenName")
  /// Hair
  static let profileHair = L10n.tr("Localizable", "profile.hair")
  /// Height
  static let profileHeight = L10n.tr("Localizable", "profile.height")
  /// Hips
  static let profileHips = L10n.tr("Localizable", "profile.hips")
  /// Instagram
  static let profileInstagram = L10n.tr("Localizable", "profile.instagram")
  /// Introduce yourself
  static let profileIntroduceYourself = L10n.tr("Localizable", "profile.introduceYourself")
  /// Language
  static let profileLanguage = L10n.tr("Localizable", "profile.language")
  /// Profiency level
  static let profileLanguageLevel = L10n.tr("Localizable", "profile.languageLevel")
  /// Logout
  static let profileLogout = L10n.tr("Localizable", "profile.logout")
  /// Main information
  static let profileMainInfo = L10n.tr("Localizable", "profile.mainInfo")
  /// Mother Agency
  static let profileMotherAgency = L10n.tr("Localizable", "profile.motherAgency")
  /// Next
  static let profileNext = L10n.tr("Localizable", "profile.next")
  /// Proficiency in languages:
  static let profileProficiencyInLanguages = L10n.tr("Localizable", "profile.proficiencyInLanguages")
  /// Remove last
  static let profileRemoveLastLanguage = L10n.tr("Localizable", "profile.removeLastLanguage")
  /// Set avatar
  static let profileSetAvatar = L10n.tr("Localizable", "profile.setAvatar")
  /// Set\navatar
  static let profileSetAvatar2 = L10n.tr("Localizable", "profile.setAvatar2")
  /// Sex
  static let profileSex = L10n.tr("Localizable", "profile.sex")
  /// Shoe
  static let profileShoe = L10n.tr("Localizable", "profile.shoe")
  /// Skip for now
  static let profileSkipForNow = L10n.tr("Localizable", "profile.skipForNow")
  /// Social networks
  static let profileSocialNetworksInfo = L10n.tr("Localizable", "profile.socialNetworksInfo")
  /// Tattoos
  static let profileTattoos = L10n.tr("Localizable", "profile.tattoos")
  /// Valid international passport
  static let profileValidInternationalPassport = L10n.tr("Localizable", "profile.validInternationalPassport")
  /// Waist
  static let profileWaist = L10n.tr("Localizable", "profile.waist")
  /// Weight
  static let profileWeight = L10n.tr("Localizable", "profile.weight")
  /// Answer additional questions
  static let profileDoneAnswerQuestions = L10n.tr("Localizable", "profile.done.answerQuestions")
  /// Connect social network
  static let profileDoneConnectSocial = L10n.tr("Localizable", "profile.done.connectSocial")
  /// Discover Castings
  static let profileDoneDiscovers = L10n.tr("Localizable", "profile.done.discovers")
  /// Or start to use platform immediately
  static let profileDoneFootnote = L10n.tr("Localizable", "profile.done.footnote")
  /// Now you completely in our digital family. Thank you for fill profile, but you can also:
  static let profileDoneInfo = L10n.tr("Localizable", "profile.done.info")
  /// You can reconnect it later
  static let profileInstagramAlertMessage = L10n.tr("Localizable", "profile.instagram.alert.message")
  /// Are you sure want to disconnect Instagram?
  static let profileInstagramAlertTitle = L10n.tr("Localizable", "profile.instagram.alert.title")
  /// Female
  static let profileSexFemale = L10n.tr("Localizable", "profile.sex.female")
  /// Male
  static let profileSexMale = L10n.tr("Localizable", "profile.sex.male")
  /// Non-Binary
  static let profileSexNonBinary = L10n.tr("Localizable", "profile.sex.nonBinary")
  /// and
  static let sharedAnd = L10n.tr("Localizable", "shared.and")
  /// Application error.\nSorry, we'll fix it soon.
  static let sharedApplicationError = L10n.tr("Localizable", "shared.applicationError")
  /// Attach
  static let sharedAttach = L10n.tr("Localizable", "shared.attach")
  /// Back
  static let sharedBack = L10n.tr("Localizable", "shared.back")
  /// Cancel
  static let sharedCancel = L10n.tr("Localizable", "shared.cancel")
  /// Castings
  static let sharedCastings = L10n.tr("Localizable", "shared.castings")
  /// CM
  static let sharedCm = L10n.tr("Localizable", "shared.cm")
  /// Detach
  static let sharedDetach = L10n.tr("Localizable", "shared.detach")
  /// Disconnect
  static let sharedDisconnect = L10n.tr("Localizable", "shared.disconnect")
  /// EU
  static let sharedEu = L10n.tr("Localizable", "shared.eu")
  /// Fill profile
  static let sharedFillProfile = L10n.tr("Localizable", "shared.fillProfile")
  /// Just a moment
  static let sharedJustMoment = L10n.tr("Localizable", "shared.justMoment")
  /// KG
  static let sharedKg = L10n.tr("Localizable", "shared.kg")
  /// Loading
  static let sharedLoading = L10n.tr("Localizable", "shared.loading")
  /// Login
  static let sharedLogin = L10n.tr("Localizable", "shared.login")
  /// Message
  static let sharedMessage = L10n.tr("Localizable", "shared.message")
  /// No
  static let sharedNo = L10n.tr("Localizable", "shared.no")
  /// No internet.\nCheck the connection.
  static let sharedNoInternet = L10n.tr("Localizable", "shared.noInternet")
  /// Portfolio
  static let sharedPortfolio = L10n.tr("Localizable", "shared.portfolio")
  /// Privacy Policy
  static let sharedPrivacyPolicy = L10n.tr("Localizable", "shared.privacyPolicy")
  /// Profile
  static let sharedProfile = L10n.tr("Localizable", "shared.profile")
  /// Replace
  static let sharedReplace = L10n.tr("Localizable", "shared.replace")
  /// Get a new code
  static let sharedResendCode = L10n.tr("Localizable", "shared.resendCode")
  /// Reset
  static let sharedReset = L10n.tr("Localizable", "shared.reset")
  /// Search
  static let sharedSearch = L10n.tr("Localizable", "shared.search")
  /// Send
  static let sharedSend = L10n.tr("Localizable", "shared.send")
  /// Share
  static let sharedShare = L10n.tr("Localizable", "shared.share")
  /// Skip
  static let sharedSkip = L10n.tr("Localizable", "shared.skip")
  /// Start
  static let sharedStart = L10n.tr("Localizable", "shared.start")
  /// Support
  static let sharedSupport = L10n.tr("Localizable", "shared.support")
  /// Terms of Use
  static let sharedTermsOfUse = L10n.tr("Localizable", "shared.termsOfUse")
  /// Slow connection,\nplease try refresh.
  static let sharedTimeoutError = L10n.tr("Localizable", "shared.timeoutError")
  /// Until
  static let sharedUntil = L10n.tr("Localizable", "shared.until")
  /// Update
  static let sharedUpdate = L10n.tr("Localizable", "shared.update")
  /// Verifying
  static let sharedVerifying = L10n.tr("Localizable", "shared.verifying")
  /// Welcome
  static let sharedWelcome = L10n.tr("Localizable", "shared.welcome")
  /// Yes
  static let sharedYes = L10n.tr("Localizable", "shared.yes")
  /// Code from SMS
  static let verificationCodeFromSms = L10n.tr("Localizable", "verification.codeFromSms")
  /// Get a new code
  static let verificationGetANewCode = L10n.tr("Localizable", "verification.getANewCode")
}
// swiftlint:enable identifier_name line_length type_body_length

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
