//
//  MaterialBlockViewModelBuilder.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class MaterialBlockViewModelBuilder {
    
    var onUrlSelected: ((URL) -> Void)?
    
    func makeParagraphBlock(title: String? = nil,
                            paragraph: String? = nil,
                            edges: UIEdgeInsets) -> TableCellViewModel {
        return ParagraphTableCellViewModel(title: title, paragraph: paragraph, edges: edges) { [weak self] url in
            self?.onUrlSelected?(url)
        }
    }
    
    func makeImageBlock(image: Image, caption: String?, edges: UIEdgeInsets) -> TableCellViewModel {
        return ImageFootnoteCellViewModel(image: image, caption: caption, edges: edges)
    }
    
    func makeMaterialBlocks(blocks: [MaterialBlock]) -> [TableCellViewModel] {
        let edges = self.makeEdges(blocks: blocks)
        let viewModels: [TableCellViewModel] = blocks.enumerated().compactMap { offset, block in
            switch block.type {
            case .image where block.image != nil:
                return self.makeImageBlock(image: block.image!, caption: block.caption, edges: edges[offset])
            case .text:
                return self.makeParagraphBlock(title: block.head,
                                               paragraph: block.text,
                                               edges: edges[offset])
            default:
                return nil
            }
        }
        return viewModels
    }
    
    func makeMaterialBlocksSection(blocks: [MaterialBlock]) -> TableSectionViewModel {
        return DefaultTableSectionViewModel(cellModels: self.makeMaterialBlocks(blocks: blocks))
    }
    
    func makeEdges(blocks: [MaterialBlock]) -> [UIEdgeInsets] {
        
        let minusTopValue: CGFloat = Device.isIphone5 ? 5 : 0
        
        var edges: [UIEdgeInsets] = blocks.enumerated().compactMap { offset, block in
            if offset == 0 {
                return UIEdgeInsets(top: 20 - minusTopValue, left: 0, bottom: 0, right: 0)
            } else {
                let previousIndex = offset - 1
                let previousBlock = blocks[previousIndex]
                let previousType = blocks[previousIndex].type
                switch block.type {
                case .image where block.image != nil:
                    if previousType == .image {
                        return UIEdgeInsets(top: 10 - minusTopValue, left: 0, bottom: 0, right: 0)
                    } else {
                        return UIEdgeInsets(top: 30  - minusTopValue, left: 0, bottom: 0, right: 0)
                    }
                case .text:
                    if previousType == .image {
                        return UIEdgeInsets(top: 20 - minusTopValue, left: 0, bottom: 0, right: 0)
                    } else if previousType == .text {
                        if previousBlock.text == nil && block.head != nil {
                            return UIEdgeInsets(top: 10 - minusTopValue, left: 0, bottom: 0, right: 0)
                        }
                        if block.head == nil && previousBlock.text != nil {
                            return UIEdgeInsets(top: 25 - minusTopValue, left: 0, bottom: 0, right: 0)
                        }
                        if block.head == nil && previousBlock.text == nil {
                            return UIEdgeInsets(top: 10 - minusTopValue, left: 0, bottom: 0, right: 0)
                        }
                        if block.head != nil && previousBlock.text != nil {
                            return UIEdgeInsets(top: 30 - minusTopValue, left: 0, bottom: 0, right: 0)
                        }
                    }
                    return UIEdgeInsets(top: 10 - minusTopValue, left: 0, bottom: 0, right: 0)
                default:
                    return UIEdgeInsets(top: 10 - minusTopValue, left: 0, bottom: 0, right: 0)
                }
            }
        }
        if !edges.isEmpty {
            edges[edges.count - 1].bottom = 64
        }
        return edges
    }
}
