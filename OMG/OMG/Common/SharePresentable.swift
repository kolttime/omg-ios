//
//  SharePresentable.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

protocol SharePresentable: class {
    
    var shareButton: ImageButton { get set }
    var shareContent: MaterialSharable { get }
    func sharePresent()
}

extension SharePresentable where Self: UIViewController {
    
    func sharePresent() {
        DispatchQueue.main.async {
            let activityVC = UIActivityViewController(activityItems: [self.shareContent.title, Endpoints.sharingAddress + self.shareContent.additionalPath + self.shareContent.permalink], applicationActivities: nil)
            activityVC.excludedActivityTypes = [.assignToContact,
                                                .addToReadingList,
                                                .openInIBooks]
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}
