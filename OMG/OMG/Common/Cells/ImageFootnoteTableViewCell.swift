//
//  ImageFootnoteTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ImageFootnoteViewCell: TableViewCell {
    
    private var edges: UIEdgeInsets = UIEdgeInsets.zero
    private var topConstaint: Constraint?
    private var footnoteTopConstaint: Constraint?

    private var bottomConstaint: Constraint?
    private lazy var footnoteLabel = UILabel()
    
    private lazy var originalImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.previewImageView)
        self.contentView.addSubview(self.originalImageView)
        self.contentView.addSubview(self.footnoteLabel)
        self.clipsToBounds = true
        self.setNeedsUpdateConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ImageFootnoteCellViewModel
        let data = Data(base64Encoded: viewModel.image.base64Preview)
        if let data = data {
            self.previewImageView.image = UIImage(data: data)
        }
        self.originalImageView.kf.setImage(with: viewModel.image.original, options: [.transition(.fade(0.2))])
        self.topConstaint?.update(offset: viewModel.edges.top)
        self.bottomConstaint?.update(offset: -viewModel.edges.bottom)
        if let caption = viewModel.caption, !caption.isEmpty {
            self.footnoteTopConstaint?.update(offset: 10)
            self.footnoteLabel.attributedText = CustomFont.footnote.attributesWithParagraph.colored(color: Color.grey44.value).make(string: caption)
        } else {
            self.footnoteTopConstaint?.update(offset: 0)
            self.footnoteLabel.attributedText = nil
        }
        self.edges = viewModel.edges
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.originalImageView.snp.makeConstraints {
            $0.left.equalToSuperview().offset(10)
            $0.right.equalToSuperview().offset(-10)
            self.topConstaint = $0.top.equalToSuperview().offset(self.edges.top).priority(.high).constraint
            $0.height.equalTo(200)
        }
        self.previewImageView.snp.makeConstraints {
            $0.edges.equalTo(self.originalImageView)
        }
        self.footnoteLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            self.footnoteTopConstaint = $0.top.equalTo(self.originalImageView.snp.bottom).offset(10).constraint
            self.bottomConstaint = $0.bottom.equalToSuperview().offset(-self.edges.bottom).constraint
        }
    }
}

struct ImageFootnoteCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return ImageFootnoteViewCell.self }
    let image: Image
    let caption: String?
    let edges: UIEdgeInsets
}
