//
//  ParagraphTableViewCell.swift
//  OMG
//
//  Created by Roman Makeev on 16/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ParagraphTableViewCell: TableViewCell {
    
    private lazy var titleLabel = UILabel()
    private var viewModel: ParagraphTableCellViewModel?
    
    private lazy var paragraphLabel: UITextView = {
        let textView = UITextView.makeStaticTextView()
        textView.dataDetectorTypes = [.link]
        textView.delegate = self
        return textView
    }()
    
    private var edges = UIEdgeInsets.zero
    private var topConstraint: Constraint?
    private var bottomConstraint: Constraint?
    private var midConstraint: Constraint?
    private var title: NSAttributedString?
    private var paragraph: NSAttributedString?
    private var cellSelectionHandler: Action?
    
    override func initialSetup() {
        super.initialSetup()
        self.contentView.addSubview(self.titleLabel)
        self.titleLabel.numberOfLines = 0
        self.contentView.addSubview(self.paragraphLabel)
        self.setNeedsUpdateConstraints()
    }
    
    override func configure(viewModel: TableViewCell.ViewModelType) {
        let viewModel = viewModel as! ParagraphTableCellViewModel
        self.viewModel = viewModel
        self.edges = viewModel.edges
        self.title = viewModel.title
        self.paragraph = viewModel.paragraph
        self.topConstraint?.update(offset: viewModel.edges.top)
        self.bottomConstraint?.update(offset: -viewModel.edges.bottom)
        if viewModel.title == nil || viewModel.paragraph == nil {
            self.midConstraint?.update(offset: 0)
        } else {
            self.midConstraint?.update(offset: 10)
        }
        self.titleLabel.attributedText = viewModel.title ?? NSAttributedString()
        self.paragraphLabel.attributedText = self.makeLinked(attributedText: viewModel.paragraph ?? NSAttributedString())
        self.cellSelectionHandler = viewModel.cellSelectionHandler
    }
    
    private func makeLinked(attributedText: NSAttributedString) -> NSAttributedString {
        let pattern = "\\[([^\\[\\]]+)\\]\\(([^\\(\\)]+)\\)"
        do {
            let regex = try NSRegularExpression(pattern: pattern)
            let result = regex.matches(in: attributedText.string, range: NSMakeRange(0, attributedText.string.utf16.count))
            let resultString: NSMutableAttributedString = NSMutableAttributedString()
            var lastLocation: Int = 0
            
            for match in result {
                let fullMatchRange = match.range(at: 0)
                let nameRange = match.range(at: 1)
                let linkRange = match.range(at: 2)
                let name = attributedText.attributedSubstring(from: nameRange)
                let link = attributedText.attributedSubstring(from: linkRange).string
                
                let newRang = NSMakeRange(lastLocation, fullMatchRange.location - lastLocation)
                resultString.append(attributedText.attributedSubstring(from: newRang))
                
                lastLocation = fullMatchRange.location + fullMatchRange.length
                
                let nameString: NSMutableAttributedString = NSMutableAttributedString(attributedString: name)
                let localNameRange = NSMakeRange(0, nameString.length)
                nameString.addAttributes([NSAttributedString.Key.link: link as Any], range: localNameRange)
//                nameString.addAttributes([NSAttributedString.Key.foregroundColor: Color.red.value as Any], range: localNameRange)
//                nameString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue as Any], range: localNameRange)
//                nameString.addAttributes([NSAttributedString.Key.underlineColor: Color.red.value as Any], range: localNameRange)
                resultString.append(nameString)
            }
            let newRang = NSMakeRange(lastLocation, attributedText.length - lastLocation)
            resultString.append(attributedText.attributedSubstring(from: newRang))
            return resultString
        } catch {
            return attributedText
        }
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.titleLabel.snp.makeConstraints {
            self.topConstraint = $0.top.equalToSuperview().offset(self.edges.top).constraint
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.paragraphLabel.snp.makeConstraints {
            self.midConstraint = $0.top.equalTo(self.titleLabel.snp.bottom).offset(10).constraint
            if self.title == nil || self.paragraph == nil {
                self.midConstraint?.update(offset: 0)
            }
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
            self.bottomConstraint = $0.bottom.equalToSuperview().offset(-self.edges.bottom).constraint
        }
    }
    
    override func didSelect() {
        self.cellSelectionHandler?()
    }
}

extension ParagraphTableViewCell: UITextViewDelegate {
    
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        self.handleURL(URL)
        return false
    }
    
    private func handleURL(_ url: URL) {
        self.viewModel?.onUrlSelected?(url)
    }
}

struct ParagraphTableCellViewModel: TableCellViewModel {
    
    var cellType: TableViewCell.Type { return ParagraphTableViewCell.self }
    var title: NSAttributedString?
    var paragraph: NSAttributedString?
    var edges: UIEdgeInsets
    var cellSelectionHandler: Action?
    var onUrlSelected: ((URL) -> Void)?
    
    init(title: NSAttributedString? = nil,
         paragraph: NSAttributedString? = nil,
         edges: UIEdgeInsets,
         onUrlSelected: ((URL) -> Void)? = nil) {
        self.title = title
        self.paragraph = paragraph
        self.edges = edges
        self.cellSelectionHandler = nil
        self.onUrlSelected = onUrlSelected
    }
    
    init(title: String? = nil,
         paragraph: String? = nil,
         edges: UIEdgeInsets,
         onUrlSelected: ((URL) -> Void)? = nil) {
        if let title = title {
            self.title = CustomFont.paragraphHeading.attributesWithParagraph
                .colored(color: Color.black.value)
                .make(string: title)
        }
        if let paragraph = paragraph {
            self.paragraph = CustomFont.paragraph.attributesWithParagraph
                .colored(color: Color.black.value)
                .make(string: paragraph)
        }
        self.edges = edges
        self.onUrlSelected = onUrlSelected
        self.cellSelectionHandler = nil
    }
}
