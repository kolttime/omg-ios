//
//  ToastAlertPresenter.swift
//  MDW
//
//  Created by Roman Makeev on 16/09/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

protocol ToastAlertPresentable: class {
    var toastPresenter: ToastAlertPresenter { get }
}

class ToastAlertPresenter {
    
    private lazy var toast = ToastAlertView()
    
    private var dismissWorkItem: DispatchWorkItem?
    
    weak var targetView: UIView?
    var bottomOffset: CGFloat = 0
    
    func presentToast(text: String, backgroundColor: UIColor) {
        guard let targetWidth = self.targetView?.bounds.width else { return }
        self.toast.alpha = 0.0
        self.dismiss()
        self.toast.configure(text: text, parentWidth: targetWidth, backgroundColor: backgroundColor)
        self.targetView?.addSubview(self.toast)
        self.toast.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-self.bottomOffset)
        }
        self.scheduleDismiss(after: 2)
        self.toast.animateShowing()
    }

    
    private func dismissToast() {
        self.toast.animateHidden(duration: 0.2) {
            self.toast.removeFromSuperview()
        }
    }
    
    func dismiss() {
        self.cancelDismiss()
        self.toast.removeFromSuperview()
    }
}

private extension ToastAlertPresenter {
    
    func scheduleDismiss(after: TimeInterval) {
        self.dismissWorkItem = DispatchWorkItem { [weak self] in
            self?.dismissToast()
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + after, execute: self.dismissWorkItem!)
    }
    
    func cancelDismiss() {
        self.dismissWorkItem?.cancel()
        self.dismissWorkItem = nil
    }
}

extension ToastAlertPresentable where Self: AirdronViewController {
    
    func showToastErrorAlert(_ error: AirdronError) {
        guard error.code != -999 else { return }
        if let tabbarController = self.airdronTabbarController, self.needShowTabbar() {
            self.toastPresenter.bottomOffset = tabbarController.currentTabbarHeight + 50
        } else {
            self.toastPresenter.bottomOffset = 50
        }
        self.toastPresenter.presentToast(text: error.mdwErrorReason, backgroundColor: Color.red.value)
    }
    
    func showSuccessToast(_ text: String) {
        if let tabbarController = self.airdronTabbarController, self.needShowTabbar() {
            self.toastPresenter.bottomOffset = tabbarController.currentTabbarHeight + 50
        } else {
            self.toastPresenter.bottomOffset = 50
        }
        self.toastPresenter.presentToast(text: text, backgroundColor: Color.green.value)
    }
}
