//
//  NotificationCenter + Extensions.swift
//  OMG
//
//  Created by Roman Makeev on 23/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public extension Notification.Name {
    
    public static let OMGNotificationLogout = Notification.Name("OMGNotificationLogoutIdentifier")
}
