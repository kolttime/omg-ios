//
//  DateFormatter.swift
//  OMG
//
//  Created by Roman Makeev on 15/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation

public extension DateFormatter {
    
    static func dateTimeDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy_HH:mm"
        return dateFormatter
    }
    
    static func dateDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter
    }
    
    static func timeDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }
    
    static func showDateDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM"
        return dateFormatter
    }
    
    func stringWithoutDots(from: Date) -> String {
        var string = self.string(from: from)
        string = String(string.filter { !".".contains($0) })
        return string
    }
}

extension Date {
    
    func age() -> Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year ?? 0
    }
}
