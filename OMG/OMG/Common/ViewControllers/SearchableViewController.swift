//
//  SearchableViewController.swift
//  OMG
//
//  Created by Roman Makeev on 13/10/2018.
//  Copyright © 2018 Roman Makeev. All rights reserved.
//

import Foundation
import UIKit

class SearchableViewController: OMGNavigationBarViewController {
    
    lazy var tableViewController = TableViewController()
    lazy var searchTextField = OMGTextField.makeSearch()
    
    lazy var loadingLabel: UILabel = {
        let label = UILabel()
        label.attributedText = CustomFont.bodyRegular.attributesWithParagraph.colored(color: Color.grey44.value).make(string: L10n.sharedLoading)
        return label
    }()
    
    override func initialSetup() {
        super.initialSetup()
        self.view.backgroundColor = Color.white.value
        self.view.addSubview(self.searchTextField)
        self.view.addSubview(self.tableViewController.view)
        self.view.addSubview(self.loadingLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        self.searchTextField.snp.makeConstraints {
            $0.top.equalToSuperview().offset(self.navigationBarAreaHeight + 5)
            $0.left.equalToSuperview().offset(ViewSize.sideOffset)
            $0.right.equalToSuperview().offset(-ViewSize.sideOffset)
        }
        self.tableViewController.view.snp.makeConstraints {
            $0.top.equalTo(self.searchTextField.snp.bottom)
            $0.left.equalToSuperview()
            $0.right.equalToSuperview()
            $0.bottom.equalTo(self.bottomLayoutEdge)
        }
        self.loadingLabel.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}
